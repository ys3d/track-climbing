import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/util/style_mapper.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';

class Ascend with ChangeNotifier {
  int routeID;
  int _gradeID;
  int styleID;
  bool toprope;
  int rating;
  String _name;
  DateTime dateTime;
  String location;
  bool outdoor;
  bool marked;
  String comment;
  ClimbingType type;
  int speedTime;
  int speedType;
  double height;
  int tries;

  Ascend(
    this._gradeID,
    this.styleID,
    this.toprope,
    this.rating,
    this._name,
    this.dateTime,
    this.location,
    this.outdoor,
    this.marked, {
    this.comment = "",
    this.routeID = -1,
    this.type = ClimbingType.SPORT_CLIMBING,
    this.speedTime = 0,
    this.speedType = 0,
    this.height = 0,
    this.tries = 2,
  });

  static Ascend getDefault({
    @required DateTime startDate,
    @required String locationName,
    @required bool outdoor,
    @required double standardHeight,
  }) {
    return Ascend(
      GradeManager.STANDARD_ROUTE_GRADE,
      StyleMapper().getDefaultClimbingStyle(),
      false,
      3,
      "",
      startDate,
      locationName,
      outdoor,
      false,
      height: standardHeight,
      tries: 2,
    );
  }

  Ascend.fromMap(Map<String, dynamic> data) {
    this.routeID = data[DBController.ascendId] ?? -1;
    this._gradeID = data[DBController.ascendGradeId];
    this.styleID = data[DBController.ascendStyleId];
    this.toprope = data[DBController.routeTopRope] == 1;
    this.rating = data[DBController.ascendRating];
    this._name = data[DBController.ascendName] ?? "";
    this.dateTime =
        data[DBController.sessionTimeStart] == null ? null : DateTime.parse(data[DBController.sessionTimeStart]);
    this.location = data[DBController.sessionLocation];
    this.outdoor = data[DBController.sessionOutdoor] == 1;
    this.marked = data[DBController.ascendMarked] == 1;
    this.comment = data[DBController.ascendComment] ?? "";
    this.type = EnumToString.fromString(ClimbingType.values, data[DBController.ascendType]);
    this.speedTime = data[DBController.speedTime] ?? 0;
    this.speedType = data[DBController.speedType] ?? 0;
    this.height = data[DBController.ascendHeight] ?? 0;
    this.tries = data[DBController.ascendTries];
  }

  Ascend copy() {
    return Ascend(
      _gradeID,
      styleID,
      toprope,
      rating,
      _name,
      dateTime,
      location,
      outdoor,
      marked,
      routeID: routeID,
      comment: comment,
      type: type,
      speedTime: speedTime,
      speedType: speedType,
      height: height,
      tries: tries,
    );
  }

  Map<String, dynamic> toMap(int sessionId) {
    return {
      DBController.sessionId: sessionId,
      DBController.ascendGradeId: this._gradeID,
      DBController.ascendStyleId: this.styleID,
      DBController.routeTopRope: this.toprope ? 1 : 0,
      DBController.ascendName: this._name,
      DBController.ascendRating: this.rating,
      DBController.ascendMarked: this.marked ? 1 : 0,
      DBController.ascendComment: this.comment,
      DBController.ascendType: this.type.name,
      DBController.speedTime: this.speedTime,
      DBController.speedType: this.speedType,
      DBController.ascendHeight: this.height,
      DBController.ascendTries: this.tries,
    };
  }

  Map<String, dynamic> toJson(int sessionId, int routeId) {
    return {
      DBController.ascendId: routeId,
      DBController.sessionId: sessionId,
      DBController.ascendGradeId: this._gradeID,
      DBController.ascendStyleId: this.styleID,
      DBController.routeTopRope: this.toprope ? 1 : 0,
      DBController.ascendName: this._name,
      DBController.ascendRating: this.rating,
      DBController.ascendMarked: this.marked ? 1 : 0,
      DBController.ascendComment: this.comment,
      DBController.ascendType: this.type.name,
      DBController.speedTime: this.speedTime,
      DBController.speedType: this.speedType,
      DBController.ascendHeight: this.height,
      DBController.ascendTries: this.tries,
    };
  }

  bool isBoulder() {
    return this.type == ClimbingType.BOULDER;
  }

  static int getMinTriesValue(int styleId) {
    return {
      1: 1, // onsight
      2: 1, // flash
      3: 2, // red
      4: 1, // af
      5: 1, // hang
      6: 0, // 3/4
      7: 0, // 1/2
      8: 0, // 1/4
      100: 1, // boulder flash
      101: 2, //top
      102: 1, // project
    }[styleId];
  }

  String get name => _name;

  set name(String value) {
    _name = value;
    notifyListeners();
  }

  int get gradeID => _gradeID;

  set gradeID(int value) {
    _gradeID = value;
    notifyListeners();
  }
}
