import 'dart:io';

import 'package:climbing_track/backend/Location.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:climbing_track/backend/util/json_creator.dart';
import 'package:climbing_track/ui/dialog/confirm_dialog.dart';
import 'package:climbing_track/util/notifications_handler.dart';
import 'package:climbing_track/view_model/best_performance_view_model.dart';
import 'package:climbing_track/view_model/google_drive_backup_handler.dart';
import 'package:climbing_track/view_model/google_drive_backup_result.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Data {
  Data._privateConstructor();

  factory Data() {
    return _instance;
  }

  static final Data _instance = Data._privateConstructor();

  final DBController dbController = DBController();

  int sessionCount = 0;
  Map<String, List<String>> _locations = {INDOOR_KEY: [], OUTDOOR_KEY: []};

  Session _currentSession;
  int _currentSessionId;
  Ascend currentAscend;

  static const int MAX_SESSION_AMOUNT = 16;
  static const String SESSION_ACTIVE_STATE = "active";
  static const String SESSION_CLOSED_STATE = "end";
  static const String INDOOR_KEY = "indoor";
  static const String OUTDOOR_KEY = "outdoor";
  static const String LAST_DRIVE_BACKUP_KEY = "last_google_drive_backup_key";

  Future<bool> loadData() async {
    bool result = await loadSessions();
    await loadLocations();
    return result;
  }

  void reset() {
    this.sessionCount = 0;
    _locations = {INDOOR_KEY: [], OUTDOOR_KEY: []};
    this._currentSession = null;
    SessionState().setState(SessionStateEnum.none);
  }

  void createSession() {
    this._currentSession = Session.getDefault();
  }

  Future<bool> startSession(BuildContext context) async {
    if (!this._currentSession.start()) {
      return false;
    }

    BestPerformanceViewModel.informed = false;

    // standard route height
    List<dynamic> result = await _saveStandardHeight();
    // first value: true, if height changes and there are ascends to change
    // second value: old ascend height
    if (result[0]) {
      bool res = await ConfirmDialog().showHeightUpdateDialog(context);
      if (res) {
        dbController.updateRoutesHeight(
          result[1].toDouble(),
          _currentSession.standardHeight,
          _currentSession.locationName,
        );
      }
    }

    dbController.insert(DBController.sessionTable, _currentSession.toMap(SESSION_ACTIVE_STATE)).then((value) {
      this._currentSession.sessionId = value;
      this._currentSessionId = value;
    });

    _createCoordinates();

    this.currentAscend = Ascend.getDefault(
      startDate: _currentSession.startDate,
      locationName: _currentSession.locationName,
      outdoor: _currentSession.outdoor,
      standardHeight: _currentSession.standardHeight,
    );

    return true;
  }

  void endSession(DateTime dateTime, {@required String failNotificationTitle, @required String failNotificationBody}) {
    this._currentSession.stop(dateTime);
    this.sessionCount += 1;

    String key = this._currentSession.outdoor ? OUTDOOR_KEY : INDOOR_KEY;
    if (!_locations[key].contains(_currentSession.locationName)) {
      _locations[key].add(_currentSession.locationName);
    }

    dbController.updateSessionTable({
      DBController.sessionTimeEnd: _currentSession.endDate.toIso8601String(),
      DBController.sessionStatus: SESSION_CLOSED_STATE
    }, this._currentSessionId).then((value) {
      _createAutomaticBackup();
      _createGoogleDriveBackup(
        failNotificationTitle: failNotificationTitle,
        failNotificationBody: failNotificationBody,
      );
    });
  }

  /// does not update Locations
  void addSession(Session session) {
    dbController.insert(DBController.sessionTable, session.toMap(SESSION_CLOSED_STATE));
    this.sessionCount += 1;
  }

  void deleteCurrentSession() {
    dbController.delete(DBController.ascendsTable, _currentSessionId);
    dbController.delete(DBController.sessionTable, _currentSessionId);
    clearSession();
  }

  void clearSession() {
    this._currentSession = null;
  }

  List<String> getIndoorLocations() {
    return this._locations[INDOOR_KEY];
  }

  List<String> getOutdoorLocations() {
    return this._locations[OUTDOOR_KEY];
  }

  List<String> getLocations() {
    return this.getIndoorLocations() + this.getOutdoorLocations();
  }

  Session getCurrentSession() {
    return _currentSession;
  }

  void addCurrentRoute() {
    Ascend copy = this.currentAscend.copy();
    dbController
        .insert(DBController.ascendsTable, copy.toMap(_currentSessionId))
        .then((value) => {copy.routeID = value});

    _currentSession.addRoute();
    this.currentAscend.name = "";
    this.currentAscend.comment = "";
  }

  void insertRouteToCurrentSession(int index, Map route) {
    Map<String, dynamic> insertRoute = new Map<String, dynamic>.from(route);
    insertRoute[DBController.sessionId] = _currentSessionId;
    dbController
        .insert(DBController.ascendsTable, insertRoute)
        .then((value) => insertRoute[DBController.ascendId] = value);
    _currentSession.addRoute();
  }

  bool removeRouteFromCurrentSession(Map route) {
    if (route[DBController.ascendId] == -1) {
      return false;
    }
    dbController.deleteAscend(route[DBController.ascendId]);
    this._currentSession.routeCount--;
    return true;
  }

  /// does not load locations
  Future<bool> loadSessions() async {
    this.sessionCount = 0;
    this._currentSession = null;
    List<Map<String, dynamic>> result = await dbController.queryAllRows(DBController.sessionTable);
    this.sessionCount = result.length;
    for (Map element in result) {
      if (element[DBController.sessionStatus] != SESSION_CLOSED_STATE) {
        int routeCount = await getRouteCount(element[DBController.sessionId]);
        List<Map<String, dynamic>> result =
            await dbController.getStandardHeightFromLocation(element[DBController.sessionLocation]);
        double standardHeight = result.isEmpty ? 0 : result.first[DBController.standardHeight];

        _currentSession = Session.fromMap(element, routeCount: routeCount, standardHeight: standardHeight);

        _currentSessionId = element[DBController.sessionId];
        this.currentAscend = Ascend.getDefault(
          startDate: _currentSession.startDate,
          locationName: _currentSession.locationName,
          outdoor: _currentSession.outdoor,
          standardHeight: _currentSession.standardHeight,
        );
      }
    }

    return true;
  }

  Future<void> loadLocations() async {
    Map<String, bool> indoor = Map();
    Map<String, bool> outdoor = Map();
    var result = await DBController().queryAllRows(DBController.sessionTable);
    result.forEach((session) {
      session[DBController.sessionOutdoor] == 1
          ? outdoor[session[DBController.sessionLocation]] = true
          : indoor[session[DBController.sessionLocation]] = true;
    });
    this._locations[INDOOR_KEY] = indoor.keys.toList();
    this._locations[OUTDOOR_KEY] = outdoor.keys.toList();
  }

  Future<int> getRouteCount(int sessionID) async {
    List<Map<String, dynamic>> result = await dbController.queryAscendsBySessionId(sessionID);
    return result.length;
  }

  /// Returns list of all previous ascents of this route or boulder
  Future<List<Ascend>> findExistingAscents(Ascend ascend) async {
    if (ascend.name == "") return [];

    var result = await DBController().rawQuery(
      "SELECT * FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
      "WHERE LOWER(${DBController.ascendName}) == (?) "
      "AND ${DBController.ascendType} == (?) "
      "AND LOWER(${DBController.sessionLocation}) == (?) "
      "AND ${DBController.ascendGradeId} == (?) "
      "AND ${DBController.ascendId} != (?) "
      "ORDER BY ${DBController.sessionTimeStart} DESC",
      [
        ascend.name.toLowerCase(),
        ascend.type.name,
        ascend.location.toLowerCase(),
        ascend.gradeID,
        ascend.routeID,
      ],
    );

    if (result == null || result.isEmpty || result.first == null) return [];

    return List.generate(result.length, (index) => Ascend.fromMap(result[index]));
  }

  void _createCoordinates() async {
    if (Location().saveLocation) {
      List<Map<String, dynamic>> result = await dbController.queryAllRows(DBController.locationsTable);
      if (result.any((element) => element[DBController.sessionLocation] == this._currentSession.locationName)) {
        return;
      } else {
        try {
          if (await Location().hasPermission()) {
            Position position = await Geolocator.getCurrentPosition();
            await dbController.insert(DBController.locationsTable, {
              DBController.sessionLocation: this._currentSession.locationName,
              DBController.latitude: position.latitude,
              DBController.longitude: position.longitude
            });
          }
        } on Exception catch (_) {}
      }
    }
  }

  Future<List<dynamic>> _saveStandardHeight() async {
    if (_currentSession.standardHeight > 0) {
      List<Map<String, dynamic>> result = await dbController.getStandardHeightFromLocation(
        _currentSession.locationName,
      );

      if (result.isEmpty) {
        await dbController.insert(DBController.standardHeightTable, {
          DBController.sessionLocation: _currentSession.locationName,
          DBController.standardHeight: _currentSession.standardHeight,
        });

        List<Map<String, dynamic>> locations = await dbController.queryRows(
          DBController.sessionTable,
          where: "${DBController.sessionLocation} == (?)",
          whereArgs: [_currentSession.locationName],
        );

        return [locations.isNotEmpty, 0.0];
      } else {
        double height = result.first[DBController.standardHeight];
        if (height != _currentSession.standardHeight) {
          await dbController.setStandardHeight(
            {DBController.standardHeight: _currentSession.standardHeight},
            result.first[DBController.standardHeightId],
          );
          return [true, height];
        }
      }
    }

    return [false];
  }

  Future<bool> _createAutomaticBackup() async {
    if (!await Permission.storage.isGranted) return false;

    if (!DataStore().settingsDataStore.automaticBackups) return false;

    String dir;
    if (Platform.isAndroid) {
      List<Directory> path = await getExternalStorageDirectories();
      dir = path[0].path;
    } else if (Platform.isIOS) {
      Directory documentsDirectory = await getApplicationDocumentsDirectory();
      dir = join(documentsDirectory.path, "Climbing_Tracker");
    } else {
      return false;
    }

    await JsonCreator().saveBackupFile(dir, DataStore().settingsDataStore.overwriteAutomaticBackups);

    return true;
  }

  /// If google drive backups are activated
  /// Backup of all data is created and uploaded to google drive
  /// current time will be written to shared preferences in $LAST_DRIVE_BACKUP_KEY
  void _createGoogleDriveBackup({
    @required String failNotificationTitle,
    @required String failNotificationBody,
  }) async {
    // if deactivated quit
    if (!DataStore().settingsDataStore.googleDriveActivated) return;

    // create backup and upload to google drive
    final String content = await JsonCreator().createJsonString();
    GoogleDriveBackupResult googleDriveBackupResult = await GoogleDriveBackupHandler().uploadFileToGoogleDrive(
      content,
      signeIn: false,
    );
    print("Created Google Drive Backup: ${googleDriveBackupResult.result}");
    if (googleDriveBackupResult.result) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString(LAST_DRIVE_BACKUP_KEY, DateTime.now().toIso8601String());
    } else {
      NotificationHandler().sendNotification(
          title: failNotificationTitle,
          body: kDebugMode
              ? failNotificationBody + "\n\nError Message\n" + googleDriveBackupResult.errorMessage
              : failNotificationBody,
          notificationId: NotificationHandler.NOTIFICATION_ID_DRIVE_BACKUP);
    }
  }

  Future<void> updatePositionsTableOnChangingLocations(String newLocation, String oldLocation) async {
    // check old location was stored
    List<Map<String, dynamic>> queryOld = await DBController().queryCoordinatesByLocation(oldLocation);
    // no entry
    if (queryOld == null || queryOld.isEmpty) return;

    List<Map<String, dynamic>> sessions = await dbController.queryAllRows(DBController.sessionTable);
    int count = sessions.where((element) => element[DBController.sessionLocation] == oldLocation).length;

    // check if location is already stored
    List<Map<String, dynamic>> queryNew = await DBController().queryCoordinatesByLocation(newLocation);
    if (queryNew != null && queryNew.isNotEmpty) {
      // delete old location if no session with old location is stored anymore
      if (count <= 0) {
        dbController.deleteLocation(oldLocation);
      }
      return;
    }

    Map<String, dynamic> locationEntry = Map.from(queryOld.first);
    locationEntry[DBController.sessionLocation] = newLocation;

    if (count >= 1) {
      // copy location with new name
      dbController.insert(DBController.locationsTable, locationEntry);
    } else {
      // change location to new name
      locationEntry[DBController.sessionLocation] = newLocation;
      await dbController.updateLocation(locationEntry, oldLocation);
    }
    loadLocations();
  }

  Future<void> deleteLocationsTableEntryOnDeletingSession(String location) async {
    List<Map<String, dynamic>> result = await DBController().queryCoordinatesByLocation(location);
    // no entry
    if (result == null || result.isEmpty) return;

    List<Map<String, dynamic>> sessions = await dbController.queryAllRows(DBController.sessionTable);
    int count = sessions.where((element) => element[DBController.sessionLocation] == location).length;

    if (count <= 0) {
      // deleting locations table entry if no session is available anymore
      dbController.deleteLocation(location);
    }
  }
}
