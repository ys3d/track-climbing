import 'package:climbing_track/util/extensions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

enum ClimbingType {
  SPORT_CLIMBING,
  BOULDER,
  SPEED_CLIMBING,
  FREE_SOLO,
  DEEP_WATER_SOLO,
  ICE_CLIMBING,
}

extension climbingTypeExtension on ClimbingType {
  /// Translates ClimbingType enum to translated String for UI
  String toName(BuildContext context) {
    return {
      ClimbingType.SPORT_CLIMBING: AppLocalizations.of(context).style_sport_climbing,
      ClimbingType.BOULDER: AppLocalizations.of(context).style_bouldering,
      ClimbingType.SPEED_CLIMBING: AppLocalizations.of(context).style_speed_climbing,
      ClimbingType.FREE_SOLO: AppLocalizations.of(context).style_free_solo_climbing,
      ClimbingType.DEEP_WATER_SOLO: AppLocalizations.of(context).style_dws_climbing,
      ClimbingType.ICE_CLIMBING: AppLocalizations.of(context).style_ice_climbing,
    }[this];
  }

  String toTitle(BuildContext context) {
    switch (this) {
      case ClimbingType.SPORT_CLIMBING:
        return AppLocalizations.of(context).style_sport_climbing_ascent;
      case ClimbingType.SPEED_CLIMBING:
        return AppLocalizations.of(context).style_speed_ascend;
      case ClimbingType.DEEP_WATER_SOLO:
        return AppLocalizations.of(context).style_dws_ascent;
      case ClimbingType.FREE_SOLO:
        return AppLocalizations.of(context).style_free_solo_ascent;
      case ClimbingType.ICE_CLIMBING:
        return AppLocalizations.of(context).style_ice_ascent;
      case ClimbingType.BOULDER:
        return AppLocalizations.of(context).style_boulder;
      default:
        return this.name.toTitleCase();
    }
  }
}