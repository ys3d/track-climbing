import 'package:flutter/cupertino.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

enum Classes { LEAD, TOPROPE, BOULDER, FREE, WATER, ICE, SPEED }

enum StyleType {
  ONSIGHT,
  FLASH,
  REDPOINT,
  TOP,
  PROJECT,
  NONE
}

extension styleTypeExtension on StyleType {
  /// Translates StyleType enum to translated String for UI
  String toTranslatedString(BuildContext context) {
    switch (this) {
      case StyleType.ONSIGHT:
        return AppLocalizations.of(context).style_onsight;
      case StyleType.FLASH:
        return AppLocalizations.of(context).style_flash;
      case StyleType.REDPOINT:
        return AppLocalizations.of(context).style_redpoint;
      case StyleType.TOP:
        return AppLocalizations.of(context).style_top;
      case StyleType.PROJECT:
        return AppLocalizations.of(context).style_project;
      case StyleType.NONE:
      default:
        return "";
    }
  }
}

/// Translates Classes enum to translated String for UI
extension classesExtension on Classes {
  String toTranslatedString(BuildContext context) {
    switch (this) {
      case Classes.LEAD:
        return AppLocalizations.of(context).style_lead;
      case Classes.TOPROPE:
        return AppLocalizations.of(context).style_toprope;
      case Classes.BOULDER:
        return AppLocalizations.of(context).style_bouldering;
      case Classes.FREE:
        return AppLocalizations.of(context).style_free_solo;
      case Classes.WATER:
        return AppLocalizations.of(context).style_dws;
      case Classes.ICE:
        return AppLocalizations.of(context).style_ice_climbing;
      case Classes.SPEED:
        return AppLocalizations.of(context).style_speed_climbing;
    }
    return "";
  }
}
