import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/util/extensions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:latlong2/latlong.dart';

class Session with ChangeNotifier {
  int sessionId;
  DateTime startDate;
  DateTime endDate;
  String _locationName;
  bool _outdoor;
  int routeCount;
  double standardHeight;
  LatLng coordinates;

  Session(
    this.startDate,
    this.endDate,
    this._locationName,
    this._outdoor,
    this.routeCount, {
    this.sessionId,
    this.standardHeight = 0,
    this.coordinates,
  });

  Session.getDefault() {
    this.startDate = null;
    this.endDate = null;
    this._locationName = "";
    this._outdoor = false;
    this.routeCount = 0;
    this.standardHeight = 0;
    this.coordinates = null;
  }

  Session.fromMap(Map<String, dynamic> data, {int routeCount = 0, double standardHeight = 0}) {
    this.startDate =
        data[DBController.sessionTimeStart] == null ? null : DateTime.parse(data[DBController.sessionTimeStart]);
    this.endDate = data[DBController.sessionTimeEnd] == null ? null : DateTime.parse(data[DBController.sessionTimeEnd]);
    this._locationName = data[DBController.sessionLocation] ?? "";
    this._outdoor = data[DBController.sessionOutdoor] == 1;
    this.routeCount = routeCount;
    this.sessionId = data[DBController.sessionId] ?? -1;
    this.standardHeight = standardHeight;
    double long = data[DBController.longitude];
    double lat = data[DBController.latitude];
    if (long != null && lat != null) this.coordinates = LatLng(lat, long);
  }

  bool start() {
    if (_locationName == "") {
      return false;
    } else {
      this.startDate = DateTime.now();
      return true;
    }
  }

  void stop(DateTime end) {
    this.endDate = end;
  }

  String getDuration() {
    return getDurationD(this.endDate);
  }

  String getDurationD(DateTime end) {
    final int difference = end.difference(this.startDate).inMinutes.abs();
    final int hours = difference ~/ 60;
    final int minutes = difference % 60;
    return "${hours.toString().padLeft(2, '0')}:${minutes.toString().padLeft(2, '0')}";
  }

  void addRoute() {
    this.routeCount++;
    notifyListeners();
  }

  int getSecondsSinceStart() {
    return DateTime.now().difference(this.startDate).inSeconds.abs();
  }

  bool get outdoor => _outdoor;

  set outdoor(bool value) {
    _outdoor = value;
    notifyListeners();
  }

  Map<String, dynamic> toMap(String status) {
    var map = {
      DBController.sessionStatus: status,
      DBController.sessionTimeStart: this.startDate == null ? null : this.startDate.toIso8601String(),
      DBController.sessionTimeEnd: this.endDate == null ? null : this.endDate.toIso8601String(),
      DBController.sessionOutdoor: this._outdoor ? 1 : 0,
      DBController.sessionLocation: this._locationName
    };
    if (sessionId != null) map[DBController.sessionId] = sessionId;
    return map;
  }

  Map<String, dynamic> toJson(int sessionId) {
    return {
      DBController.sessionId: sessionId,
      DBController.sessionTimeStart: this.startDate == null ? null : this.startDate.toIso8601String(),
      DBController.sessionTimeEnd: this.endDate == null ? null : this.endDate.toIso8601String(),
      DBController.sessionOutdoor: this._outdoor ? 1 : 0,
      DBController.sessionLocation: this._locationName
    };
  }

  String get locationName => _locationName;

  set locationName(String value) {
    _locationName = value.trim();
    notifyListeners();
  }

  Future<void> setLocationNameAndUpdateStandardHeight(String name) async {
    this._locationName = name;
    await _setStandardHeightFromLocation(name);
  }

  Future<void> _setStandardHeightFromLocation(String name) async {
    List<Map<String, dynamic>> result = await DBController().getStandardHeightFromLocation(locationName);
    if (result.isNotEmpty && result.first[DBController.standardHeight] != 0) {
      this.standardHeight = result.first[DBController.standardHeight];
    }
  }

  Session copy() {
    return Session(
      startDate != null ? startDate.copy() : null,
      endDate != null ? endDate.copy() : null,
      _locationName,
      _outdoor,
      routeCount,
      sessionId: sessionId,
      standardHeight: standardHeight,
      coordinates: coordinates != null ? coordinates.copy() : null,
    );
  }
}
