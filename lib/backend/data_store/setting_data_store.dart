import 'dart:io';

import 'package:climbing_track/backend/model/height_measurement.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsDataStore {
  /// SharedPreferences keys
  static const String _LIMIT_CHART_KEY = "limit_charts";
  static const String _NOTIFICATIONS_KEY = "notifications_activated";
  static const String _SESSION_REMINDER_DURATION_KEY = "session_reminder_duration";
  static const String _AUTOMATIC_BACKUP_KEY = "automatic_backups";
  static const String _OVERWRITE_AUTOMATIC_BACKUP_KEY = "overwrite_automatic_backup";
  static const String _HEIGHT_MEASUREMENT_KEY = "height_measurement";
  static const String _LANGUAGE_KEY = "language_key";
  static const String _TICHY_MODE_KEY = "tichy_mode_key";
  static const String _GOOGLE_DRIVE_KEY = "google_drive_key";

  /// Default values
  static const int STANDARD_REMINDER_DURATION = 180;

  bool _limitCharts;
  bool _notificationsActivated;
  int _sessionReminderDuration;
  bool _automaticBackups;
  bool _overwriteAutomaticBackups;
  HeightMeasurement _heightMeasurement;
  String _language;
  bool _tichyMode;
  bool _googleDriveActivated;

  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _limitCharts = prefs.getBool(_LIMIT_CHART_KEY) ?? true;
    _notificationsActivated = prefs.getBool(_NOTIFICATIONS_KEY) ?? true;
    _automaticBackups = prefs.getBool(_AUTOMATIC_BACKUP_KEY) ?? false;
    _overwriteAutomaticBackups = prefs.getBool(_OVERWRITE_AUTOMATIC_BACKUP_KEY) ?? true;
    _sessionReminderDuration = prefs.getInt(_SESSION_REMINDER_DURATION_KEY) ?? STANDARD_REMINDER_DURATION;
    String defaultUnit = Platform.localeName == "en_US" ? HeightMeasurement.FEET.name : HeightMeasurement.METER.name;
    _heightMeasurement = EnumToString.fromString(
      HeightMeasurement.values,
      prefs.getString(_HEIGHT_MEASUREMENT_KEY) ?? defaultUnit,
    );
    _language = prefs.getString(_LANGUAGE_KEY) ?? Platform.localeName.split("_")[0];
    _tichyMode = prefs.getBool(_TICHY_MODE_KEY) ?? false;
    _googleDriveActivated = prefs.getBool(_GOOGLE_DRIVE_KEY) ?? false;
  }

  bool get limitCharts => _limitCharts;

  int get sessionReminderDuration => _sessionReminderDuration;

  bool get notificationsActivated => _notificationsActivated;

  bool get overwriteAutomaticBackups => _overwriteAutomaticBackups;

  bool get automaticBackups => _automaticBackups;

  HeightMeasurement get heightMeasurement => _heightMeasurement;

  String get language => _language;

  bool get tichyMode => _tichyMode;

  bool get googleDriveActivated => _googleDriveActivated;

  set limitCharts(bool value) {
    _limitCharts = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_LIMIT_CHART_KEY, _limitCharts);
    });
  }

  set sessionReminderDuration(int value) {
    _sessionReminderDuration = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setInt(_SESSION_REMINDER_DURATION_KEY, _sessionReminderDuration);
    });
  }

  set notificationsActivated(bool value) {
    _notificationsActivated = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_NOTIFICATIONS_KEY, _notificationsActivated);
    });
  }

  set overwriteAutomaticBackups(bool value) {
    _overwriteAutomaticBackups = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_OVERWRITE_AUTOMATIC_BACKUP_KEY, _overwriteAutomaticBackups);
    });
  }

  set automaticBackups(bool value) {
    _automaticBackups = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_AUTOMATIC_BACKUP_KEY, _automaticBackups);
    });
  }

  set heightMeasurement(HeightMeasurement value) {
    _heightMeasurement = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setString(_HEIGHT_MEASUREMENT_KEY, _heightMeasurement.name);
    });
  }

  set language(String value) {
    _language = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setString(_LANGUAGE_KEY, _language);
    });
  }

  set tichyMode(bool value) {
    _tichyMode = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_TICHY_MODE_KEY, _tichyMode);
    });
  }

  set googleDriveActivated(bool value) {
    _googleDriveActivated = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_GOOGLE_DRIVE_KEY, _googleDriveActivated);
    });
  }
}
