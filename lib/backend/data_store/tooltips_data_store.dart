import 'package:shared_preferences/shared_preferences.dart';

class ToolTipsDataStore {
  /// SharedPreferences keys
  static const String _TT_CLOSE_SESSION_KEY = "tool_tip_close_session";
  static const String _TT_SESSION_SUMMARY_KEY = "tool_tip_session_summary";
  static const String _TT_CHART_SETTINGS_KEY = "tool_tip_chart_settings";
  static const String _TT_OVERVIEW_PAGE_KEY = "tool_tip_overview_page";
  static const String _TT_ASCEND_PAGE_KEY = "tool_tip_ascend_page";

  bool _tooltipCloseSession;
  bool _tooltipSessionSummary;
  bool _tooltipChartSettings;
  bool _tooltipOverviewPage;
  bool _tooltipAscendPage;

  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _tooltipCloseSession = prefs.getBool(_TT_CLOSE_SESSION_KEY) ?? true;
    _tooltipSessionSummary = prefs.getBool(_TT_SESSION_SUMMARY_KEY) ?? true;
    _tooltipChartSettings = prefs.getBool(_TT_CHART_SETTINGS_KEY) ?? true;
    _tooltipOverviewPage = prefs.getBool(_TT_OVERVIEW_PAGE_KEY) ?? true;
    _tooltipAscendPage = prefs.getBool(_TT_ASCEND_PAGE_KEY) ?? true;
  }

  bool get tooltipOverviewPage => _tooltipOverviewPage;

  bool get tooltipChartSettings => _tooltipChartSettings;

  bool get tooltipSessionSummary => _tooltipSessionSummary;

  bool get tooltipCloseSession => _tooltipCloseSession;

  bool get tooltipAscendPage => _tooltipAscendPage;

  set tooltipOverviewPage(bool value) {
    _tooltipOverviewPage = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_TT_OVERVIEW_PAGE_KEY, _tooltipOverviewPage);
    });
  }

  set tooltipChartSettings(bool value) {
    _tooltipChartSettings = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_TT_CHART_SETTINGS_KEY, _tooltipChartSettings);
    });
  }

  set tooltipSessionSummary(bool value) {
    _tooltipSessionSummary = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_TT_SESSION_SUMMARY_KEY, _tooltipSessionSummary);
    });
  }

  set tooltipCloseSession(bool value) {
    _tooltipCloseSession = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_TT_CLOSE_SESSION_KEY, _tooltipCloseSession);
    });
  }

  set tooltipAscendPage(bool value) {
    _tooltipAscendPage = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_TT_ASCEND_PAGE_KEY, _tooltipAscendPage);
    });
  }
}
