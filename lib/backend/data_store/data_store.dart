import 'package:climbing_track/backend/data_store/setting_data_store.dart';
import 'package:climbing_track/backend/data_store/tooltips_data_store.dart';
import 'package:climbing_track/backend/search_settings.dart';

class DataStore {
  /// Singleton attributes and settings
  static final DataStore _instance = DataStore._privateConstructor();

  DataStore._privateConstructor();

  factory DataStore() {
    return _instance;
  }

  SettingsDataStore settingsDataStore = SettingsDataStore();
  ToolTipsDataStore toolTipsDataStore = ToolTipsDataStore();
  SearchSettingsDataStore searchSettingsDataStore = SearchSettingsDataStore();



  Future<void> init() async {
    await settingsDataStore.init();
    await toolTipsDataStore.init();
    await searchSettingsDataStore.init();
  }
}
