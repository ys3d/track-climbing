import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/model/enums.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OverviewDataLoader {
  static const String TOTAL_ASCENDS_KEY = "total_ascends";
  static const String TOTAL_HEIGHT_KEY = "total_height";
  static const String TOTAL_SPORT_ASCENDS_KEY = "sport_ascends";
  static const String TOTAL_LEAD_KEY = "total_lead";
  static const String TOTAL_ONSIGHT_KEY = "total_onsight";
  static const String TOTAL_SESSIONS_KEY = "total_sessions";
  static const String EXPANDED_VALUES_KEY = "expanded_values";

  static const int INDOOR_KEY = 0;
  static const int OUTDOOR_KEY = 1;

  List<Map<Classes, Map<StyleType, Map<String, dynamic>>>> calculatedData = [];
  List<Map<String, dynamic>> overAllData = [Map(), Map()];
  List<Map<String, dynamic>> speedData = [];

  final Function onCompleted;
  final bool fetchOutdoor;
  final bool fetchIndoor;
  final bool fetchSpeed;
  final bool fetchExpandedValues;

  OverviewDataLoader({
    this.onCompleted,
    this.fetchOutdoor = true,
    this.fetchIndoor = true,
    this.fetchSpeed = true,
    this.fetchExpandedValues = true,
  });

  Future<void> init() async {
    this._resetData();
    await this._calculateData();
  }

  void _resetData() {
    this.calculatedData.clear();
    for (int index in [INDOOR_KEY, OUTDOOR_KEY]) {
      // reset overall data
      this.overAllData[index] = {
        EXPANDED_VALUES_KEY: _getDefaultExpandedValues(),
        TOTAL_SESSIONS_KEY: 0,
        TOTAL_ASCENDS_KEY: 0,
        TOTAL_SPORT_ASCENDS_KEY: 0,
        TOTAL_LEAD_KEY: 0,
        TOTAL_ONSIGHT_KEY: 0,
        TOTAL_HEIGHT_KEY: 0.0
      };

      this.calculatedData.add(_getEmptyData());
    }
  }

  Map<Classes, bool> _getDefaultExpandedValues() {
    Map<Classes, bool> values = Map();
    for (Classes type in Classes.values) {
      values[type] = type == Classes.TOPROPE || type == Classes.LEAD;
    }
    return values;
  }

  Map<Classes, Map<StyleType, Map<String, dynamic>>> _getEmptyData() {
    Map<Classes, Map<StyleType, Map<String, dynamic>>> data = Map();

    // creating data map
    for (Classes x in Classes.values) {
      Map<StyleType, Map<String, dynamic>> styles = Map();
      for (StyleType styleType in StyleType.values) {
        styles[styleType] = null;
      }
      data[x] = styles;
    }
    return data;
  }

  Future<void> _calculateData() async {
    if (fetchOutdoor) {
      List<Map<String, dynamic>> outdoorData = await _fetchDataStatistics(outdoor: true);
      List<Map<String, dynamic>> sessionDataOutdoor = await _fetchSessionData(indoor: false);
      if (sessionDataOutdoor.isEmpty) {
        this.overAllData[OUTDOOR_KEY][TOTAL_SESSIONS_KEY] = 0;
      } else {
        this.overAllData[OUTDOOR_KEY][TOTAL_SESSIONS_KEY] = sessionDataOutdoor.first["count"];
      }
      this.calculatedData[OUTDOOR_KEY] = _calculateOverview(OUTDOOR_KEY, outdoorData);
    }

    if (fetchIndoor) {
      List<Map<String, dynamic>> indoorData = await _fetchDataStatistics(outdoor: false);
      List<Map<String, dynamic>> sessionDataIndoor = await _fetchSessionData(indoor: true);
      if (sessionDataIndoor.isEmpty) {
        this.overAllData[INDOOR_KEY][TOTAL_SESSIONS_KEY] = 0;
      } else {
        this.overAllData[INDOOR_KEY][TOTAL_SESSIONS_KEY] = sessionDataIndoor.first["count"];
      }
      this.calculatedData[INDOOR_KEY] = _calculateOverview(INDOOR_KEY, indoorData);
    }

    if (fetchExpandedValues) {
      Map<Classes, bool> expandedValuesOutdoor = await _fetchExpandedValues(true);
      Map<Classes, bool> expandedValuesIndoor = await _fetchExpandedValues(false);
      this.overAllData[INDOOR_KEY][EXPANDED_VALUES_KEY] = expandedValuesIndoor;
      this.overAllData[OUTDOOR_KEY][EXPANDED_VALUES_KEY] = expandedValuesOutdoor;
    }

    if (fetchSpeed) {
      this.speedData = await _fetchSpeedStatistics();
    }

    if (this.onCompleted != null) this.onCompleted();
  }

  Map<Classes, Map<StyleType, Map<String, dynamic>>> _calculateOverview(
      int indoorKey, List<Map<String, dynamic>> ascends) {
    Map<Classes, Map<StyleType, Map<String, dynamic>>> data = _getEmptyData();

    overAllData[indoorKey][TOTAL_ASCENDS_KEY] = 0;
    overAllData[indoorKey][TOTAL_SPORT_ASCENDS_KEY] = 0;
    overAllData[indoorKey][TOTAL_LEAD_KEY] = 0;
    overAllData[indoorKey][TOTAL_ONSIGHT_KEY] = 0;
    overAllData[indoorKey][TOTAL_HEIGHT_KEY] = 0.0;

    LOOP:
    for (Map<String, dynamic> ascend in ascends) {
      overAllData[indoorKey][TOTAL_ASCENDS_KEY] += 1;
      overAllData[indoorKey][TOTAL_HEIGHT_KEY] += ascend[DBController.ascendHeight];

      Classes type;

      ClimbingType ascendType = EnumToString.fromString(ClimbingType.values, ascend[DBController.ascendType]);

      switch (ascendType) {
        case ClimbingType.SPORT_CLIMBING:
          type = ascend[DBController.routeTopRope] == 0 ? Classes.LEAD : Classes.TOPROPE;
          if (ascend[DBController.routeTopRope] == 0) overAllData[indoorKey][TOTAL_LEAD_KEY] += 1;
          if (ascend[DBController.ascendStyleId] == 1) overAllData[indoorKey][TOTAL_ONSIGHT_KEY] += 1;
          overAllData[indoorKey][TOTAL_SPORT_ASCENDS_KEY] += 1;
          break;
        case ClimbingType.BOULDER:
          type = Classes.BOULDER;
          break;
        case ClimbingType.FREE_SOLO:
          type = Classes.FREE;
          break;
        case ClimbingType.DEEP_WATER_SOLO:
          type = Classes.WATER;
          break;
        case ClimbingType.ICE_CLIMBING:
          type = Classes.ICE;
          break;
        case ClimbingType.SPEED_CLIMBING:
          continue LOOP;
      }

      switch (Util.mapStyleToStyleType(ascend[DBController.ascendStyleId])) {
        case StyleType.ONSIGHT:
          if (_check(data[type][StyleType.ONSIGHT], ascend[DBController.ascendGradeId],
              ascend[DBController.sessionTimeStart])) {
            data[type][StyleType.ONSIGHT] = ascend;
            continue FLASH;
          }
          break;
        FLASH:
        case StyleType.FLASH:
          if (_check(
              data[type][StyleType.FLASH], ascend[DBController.ascendGradeId], ascend[DBController.sessionTimeStart])) {
            data[type][StyleType.FLASH] = ascend;
            continue REDPOINT;
          }
          break;
        REDPOINT:
        case StyleType.REDPOINT:
          if (_check(data[type][StyleType.REDPOINT], ascend[DBController.ascendGradeId],
              ascend[DBController.sessionTimeStart])) {
            data[type][StyleType.REDPOINT] = ascend;
            continue TOP;
          }
          break;
        TOP:
        case StyleType.TOP:
          if (_check(
              data[type][StyleType.TOP], ascend[DBController.ascendGradeId], ascend[DBController.sessionTimeStart])) {
            data[type][StyleType.TOP] = ascend;
            continue PROJECT;
          }
          break;
        PROJECT:
        case StyleType.PROJECT:
          if (_check(data[type][StyleType.PROJECT], ascend[DBController.ascendGradeId],
              ascend[DBController.sessionTimeStart])) {
            data[type][StyleType.PROJECT] = ascend;
          }
          break;
        case StyleType.NONE:
          break;
      }
    }
    return data;
  }

  bool _check(Map<String, dynamic> ascend, int gradeId, String dateString) {
    if (ascend == null) {
      return true;
    }
    if (ascend[DBController.ascendGradeId] < gradeId) {
      return true;
    }

    if (ascend[DBController.ascendGradeId] == gradeId) {
      if (DateTime.parse(ascend[DBController.sessionTimeStart]).isAfter(DateTime.parse(dateString))) {
        return true;
      }
    }

    return false;
  }

  Future<List<Map<String, dynamic>>> _fetchDataStatistics({@required bool outdoor}) async {
    return DBController().rawQuery(
      "SELECT * FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using (${DBController.sessionId}) "
      "WHERE ${DBController.sessionOutdoor} == (?)",
      [outdoor ? 1 : 0],
    );
  }

  Future<List<Map<String, dynamic>>> _fetchSpeedStatistics() async {
    return DBController().rawQuery(
      "SELECT * FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using (${DBController.sessionId}) "
      "WHERE ${DBController.ascendType} == (?) AND "
      "${DBController.speedType} == (?) "
      "ORDER BY ${DBController.speedTime} "
      "LIMIT (?)",
      [ClimbingType.SPEED_CLIMBING.name, 0, 3],
    );
  }

  Future<List<Map<String, dynamic>>> _fetchSessionData({bool indoor = false}) async {
    return DBController().rawQuery(
        "SELECT COUNT(${DBController.sessionId}) as count "
        "FROM ${DBController.sessionTable} "
        "WHERE ${DBController.sessionStatus} != (?) AND "
        "${DBController.sessionOutdoor} == (?) ",
        [Data.SESSION_ACTIVE_STATE, indoor ? 0 : 1]);
  }

  Future<Map<Classes, bool>> _fetchExpandedValues(bool outdoor) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<Classes, bool> values = Map();
    for (Classes type in Classes.values) {
      bool standardVal = type == Classes.TOPROPE || type == Classes.LEAD;
      values[type] =
          prefs.getBool("expandend_value_${outdoor ? "out" : "in"}_${type.name.toLowerCase()}") ?? standardVal;
    }
    return values;
  }
}
