import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchSettingsDataStore {
  static const String ASCEND_LIST_SORTING_KEY = "ascend_list_sorting";
  static const String ALL_LOCATIONS_KEY = "all_locations_key";
  static const String ALL_TYPES_KEY = "all_types_key";

  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    this._currentSorting = Sorting.values[prefs.getInt(ASCEND_LIST_SORTING_KEY) ?? 0];
  }

  bool onlyCurrentLocation = false;
  bool indoor = true;
  bool outdoor = true;
  bool onlyNotFinished = false;
  bool onlyMarked = false;
  bool onlyBestAscend = false;
  Sorting _currentSorting = Sorting.NEWEST;
  String currentLocation = ALL_LOCATIONS_KEY;
  String type = ALL_TYPES_KEY;

  void reset() {
    onlyCurrentLocation = false;
    indoor = true;
    outdoor = true;
    onlyNotFinished = false;
    onlyMarked = false;
    currentSorting = Sorting.NEWEST;
    currentLocation = ALL_LOCATIONS_KEY;
    type = ALL_TYPES_KEY;
  }

  List<dynamic> getSQLConditionString() {
    String sql = "1";
    List<dynamic> arguments = [];
    if (this.onlyCurrentLocation) {
      sql += " AND ${DBController.sessionLocation} == (?)";
      arguments.add(Data().getCurrentSession().locationName);
    } else if (this.currentLocation != ALL_LOCATIONS_KEY) {
      sql += " AND ${DBController.sessionLocation} == (?)";
      arguments.add(this.currentLocation);
    }
    if (!this.indoor || !this.outdoor) {
      if (this.indoor) {
        sql += " AND NOT ${DBController.sessionOutdoor}";
      }
      if (this.outdoor) {
        sql += " AND ${DBController.sessionOutdoor}";
      }
    }
    if (type != ALL_TYPES_KEY) {
      sql += " AND ${DBController.ascendType} == (?)";
      arguments.add(type);
    }
    if (onlyNotFinished) {
      sql += " AND (${DBController.ascendStyleId} BETWEEN (?) AND (?)) OR ${DBController.ascendStyleId} == (?)";
      arguments.add(6);
      arguments.add(50);
      arguments.add(102); //Boulder Project
    }
    if (onlyMarked) {
      sql += " AND ${DBController.ascendMarked}";
    }
    return [sql, arguments];
  }

  Sorting get currentSorting => _currentSorting;

  set currentSorting(Sorting value) {
    _currentSorting = value;
    SharedPreferences.getInstance().then((pref) => pref.setInt(ASCEND_LIST_SORTING_KEY, value.index));
  }

  static Map<Sorting, String> sortingKeys() {
    return {
      Sorting.NEWEST: "${DBController.sessionTimeStart} DESC",
      Sorting.OLDEST: DBController.sessionTimeStart,
      Sorting.ALPHABETIC: "${DBController.ascendName} == '', ${DBController.ascendName}",
      Sorting.EASIEST: DBController.ascendGradeId,
      Sorting.HARDEST: "${DBController.ascendGradeId} DESC",
      Sorting.BEST: "${DBController.ascendRating} DESC"
    };
  }
}

enum Sorting { NEWEST, OLDEST, ALPHABETIC, EASIEST, HARDEST, BEST }
