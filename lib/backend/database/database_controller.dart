import 'dart:io';

import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DBController {
  @protected
  static final _databaseName = 'TrackClimbing.db';

  static final sessionTable = 'session';
  static final ascendsTable = 'routes';
  static final boulderTable = 'boulders';
  static final locationsTable = 'locations';
  static final standardHeightTable = 'standard_height_table';

  /// session table columns
  static final sessionId = 'session_id';
  static final sessionStatus = 'session_status';
  static final sessionTimeStart = 'time_start';
  static final sessionTimeEnd = 'time_end';
  static final sessionOutdoor = 'outdoor';
  static final sessionLocation = 'location';

  /// routes table columns
  static final ascendId = "route_id";
  static final ascendGradeId = "grade_id";
  static final ascendStyleId = "style_id";
  static final routeTopRope = "top_rope";
  static final ascendName = "route_name";
  static final ascendRating = "rating";
  static final ascendMarked = "marked";
  static final ascendComment = "comment";
  static final ascendType = "route_type";
  static final speedTime = "speed_time";
  static final speedType = "speed_type";
  static final ascendHeight = "ascend_height";
  static final ascendTries = "ascend_tries";

  /// location table columns
  static final longitude = 'longitude';
  static final latitude = 'latitude';

  /// standard height table
  static final standardHeightId = 'height_id';
  static final standardHeight = 'height';

  DBController._privateConstructor();

  static final DBController instance = DBController._privateConstructor();

  factory DBController() {
    return instance;
  }

  @protected
  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path, version: 8, onCreate: _onCreate, onUpgrade: _onUpgrade);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $sessionTable (
            $sessionId INTEGER PRIMARY KEY AUTOINCREMENT,
            $sessionStatus TEXT NOT NULL,
            $sessionTimeStart DATE NOT NULL,
            $sessionTimeEnd DATE,
            $sessionOutdoor BOOLEAN NOT NULL,
            $sessionLocation TEXT
          )
          ''');
    await db.execute('''
          CREATE TABLE $ascendsTable (
            $ascendId INTEGER PRIMARY KEY AUTOINCREMENT,
            $sessionId INTEGER NOT NULL,
            $ascendGradeId INTEGER NOT NULL,
            $ascendStyleId INTEGER NOT NULL,
            $routeTopRope BOOLEAN NOT NULL,
            $ascendName TEXT NOT NULL,
            $ascendRating INTEGER NOT NULL,
            $ascendMarked BOOLEAN NOT NULL,
            $ascendComment TEXT,
            $ascendType TEXT,
            $speedTime INTEGER NOT NULL DEFAULT (0),
            $speedType INTEGER NOT NULL DEFAULT (0),
            $ascendHeight DOUBLE NOT NULL DEFAULT (0),
            $ascendTries INTEGER NULL
          )
          ''');
    await db.execute('''
          CREATE TABLE $locationsTable (
            $sessionLocation TEXT PRIMARY KEY,
            $longitude DOUBLE,
            $latitude DOUBLE
          )
          ''');

    await db.execute('''
          CREATE TABLE $standardHeightTable (
            $standardHeightId INTEGER PRIMARY KEY,
            $sessionLocation TEXT,
            $standardHeight DOUBLE NOT NULL
          )
          ''');
  }

  void _onUpgrade(Database db, int oldVersion, int newVersion) async {
    //add location table
    if (oldVersion <= 3) {
      await db.execute('''
          CREATE TABLE $locationsTable (
            $sessionLocation TEXT PRIMARY KEY,
            $longitude DOUBLE,
            $latitude DOUBLE
          )
          ''');
    }
    // add route type column
    if (oldVersion < 6) {
      await db.execute("ALTER TABLE $ascendsTable ADD COLUMN $ascendType TEXT;");
      await db.execute("UPDATE $ascendsTable SET $ascendType = (?)", [ClimbingType.SPORT_CLIMBING.name]);
    }

    // update route type
    if (oldVersion < 7) {
      await db.update(
        ascendsTable,
        {ascendType: ClimbingType.SPORT_CLIMBING.name},
        where: "$ascendType == (?)",
        whereArgs: ["ROUTE"],
      );
      await db.execute("ALTER TABLE $ascendsTable ADD COLUMN $speedTime INTEGER NOT NULL DEFAULT (0);");
      await db.execute("ALTER TABLE $ascendsTable ADD COLUMN $speedType INTEGER NOT NULL DEFAULT (0);");

      // height
      await db.execute("ALTER TABLE $ascendsTable ADD COLUMN $ascendHeight DOUBLE NOT NULL DEFAULT (0);");
      await db.execute('''
          CREATE TABLE $standardHeightTable (
            $standardHeightId INTEGER PRIMARY KEY,
            $sessionLocation TEXT,
            $standardHeight DOUBLE NOT NULL
          )
          ''');
    }

    // added tries
    if (oldVersion < 8) {
      // height
      await db.execute("ALTER TABLE $ascendsTable ADD COLUMN $ascendTries INTEGER NULL;");
    }
  }

  Future<int> insert(String table, Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(table, row);
  }

  Future<int> delete(String table, int id) async {
    Database db = await instance.database;
    return await db.delete(table, where: '$sessionId = ?', whereArgs: [id]);
  }

  Future<int> deleteAscend(int id) async {
    Database db = await instance.database;
    return await db.delete(ascendsTable, where: '$ascendId = ?', whereArgs: [id]);
  }

  Future<int> deleteRoutes(int sessionIDLocal) async {
    Database db = await instance.database;
    return await db.delete(ascendsTable, where: '$sessionId == ?', whereArgs: [sessionIDLocal]);
  }

  Future<int> deleteLocation(String location) async {
    Database db = await instance.database;
    return await db.delete(locationsTable, where: '$sessionLocation == ?', whereArgs: [location]);
  }

  Future<List<Map<String, dynamic>>> queryAllRows(String table) async {
    Database db = await instance.database;
    return await db.query(table);
  }

  Future<List<Map<String, dynamic>>> rawQuery(String sql, [List<dynamic> arguments]) async {
    Database db = await instance.database;
    return await db.rawQuery(sql, arguments);
  }

  Future<List<Map<String, dynamic>>> queryRows(String table,
      {String where, List<dynamic> whereArgs, String groupBy, String orderBy, int limit}) async {
    Database db = await instance.database;
    return await db.query(
      table,
      where: where,
      whereArgs: whereArgs,
      orderBy: orderBy,
      groupBy: groupBy,
      limit: limit,
    );
  }

  Future<List<Map<String, dynamic>>> queryAscendsBySessionId(int id) async {
    Database db = await instance.database;
    return await db.query(ascendsTable, where: '$sessionId = ?', whereArgs: [id]);
  }

  Future<List<Map<String, dynamic>>> queryCoordinatesByLocation(String location) async {
    Database db = await instance.database;
    return await db.query(locationsTable, where: '$sessionLocation = (?)', whereArgs: [location]);
  }

  Future<void> updateSessionTable(Map<String, dynamic> row, int sessionId) async {
    Database db = await instance.database;
    await db.update(sessionTable, row, where: "${DBController.sessionId} = ?", whereArgs: [sessionId]);
  }

  Future<void> updateLocation(Map<String, dynamic> row, String location) async {
    Database db = await instance.database;
    await db.update(locationsTable, row, where: "${DBController.sessionLocation} = ?", whereArgs: [location]);
  }

  Future<void> updateAscend(Map<String, dynamic> row, int routeId) async {
    Database db = await instance.database;
    await db.update(ascendsTable, row, where: "${DBController.ascendId} = ?", whereArgs: [routeId]);
  }

  getActiveSessionId() async {
    Database db = await instance.database;
    var result = await db.query(sessionTable, where: "$sessionStatus = ?", whereArgs: [Data.SESSION_ACTIVE_STATE]);
    return result.first[sessionId];
  }

  Future<List<Map<String, dynamic>>> getStandardHeightFromLocation(String locationName) async {
    Database db = await instance.database;
    return db.query(standardHeightTable, where: "$sessionLocation = ?", whereArgs: [locationName]);
  }

  Future<void> setStandardHeight(Map<String, dynamic> row, int rowID) async {
    Database db = await instance.database;
    await db.update(standardHeightTable, row, where: "${DBController.standardHeightId} = ?", whereArgs: [rowID]);
  }

  void deleteAll() async {
    Database db = await instance.database;
    db.delete(ascendsTable);
    db.delete(sessionTable);
    db.delete(locationsTable);
    db.delete(standardHeightTable);
  }

  Future<void> updateRoutesHeight(double oldHeight, double newHeight, String location) async {
    Database db = await instance.database;
    await db.rawQuery(
      "UPDATE $ascendsTable "
      "SET $ascendHeight = (?) "
      "WHERE ($ascendHeight == (?) OR $ascendHeight == (?)) AND "
      "$ascendsTable.$sessionId IN (SELECT $sessionId FROM $sessionTable WHERE $sessionLocation == (?))",
      [newHeight, oldHeight, 0.0, location],
    );
  }
}
