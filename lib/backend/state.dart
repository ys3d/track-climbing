import 'package:flutter/material.dart';

class SessionState with ChangeNotifier {
  SessionState._privateConstructor();

  static final SessionState _instance = SessionState._privateConstructor();

  SessionStateEnum _currentState = SessionStateEnum.none;

  factory SessionState() {
    return _instance;
  }

  Future<bool> init(bool activeSession) async {
    this._currentState = activeSession ? SessionStateEnum.active : SessionStateEnum.none;
    return true;
  }

  void setState(SessionStateEnum state) {
    _currentState = state;
    notifyListeners();
  }

  void nextState() {
    _currentState = nextSessionStateEnum(_currentState);
    notifyListeners();
  }

  SessionStateEnum get currentState => _currentState;
}

enum SessionStateEnum { none, start, active, end }

SessionStateEnum nextSessionStateEnum(SessionStateEnum state) {
  switch (state) {
    case SessionStateEnum.none:
      return SessionStateEnum.start;
    case SessionStateEnum.start:
      return SessionStateEnum.active;
    case SessionStateEnum.active:
      return SessionStateEnum.end;
    case SessionStateEnum.end:
      return SessionStateEnum.none;
  }
  return SessionStateEnum.none;
}
