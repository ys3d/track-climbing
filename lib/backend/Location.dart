import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Location {
  static const String LOCATION_ENABLED_KEY = "location_enabled";

  Location._privateConstructor();

  static final Location _instance = Location._privateConstructor();

  factory Location() {
    return _instance;
  }

  bool _saveLocation;

  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    this._saveLocation = prefs.getBool(LOCATION_ENABLED_KEY) ?? false;
  }

  bool get saveLocation => _saveLocation;

  Future<void> setLocation(bool value) async {
    _saveLocation = value;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(LOCATION_ENABLED_KEY, value);
  }

  Future<bool> hasPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return false;
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return false;
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse && permission != LocationPermission.always) {
        return false;
      }
    }

    return true;
  }

  Future<Position> determinePosition(BuildContext context) async {
    if (await hasPermission()) {
      return await Geolocator.getCurrentPosition();
    } else {
      return Future.error(AppLocalizations.of(context).location_warning_permission);
    }
  }
}
