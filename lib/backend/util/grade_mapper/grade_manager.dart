import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/util/grade_mapper/abstract_mapper.dart';
import 'package:climbing_track/backend/util/grade_mapper/boulder_grade_mapper.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_mapper.dart';
import 'package:climbing_track/backend/util/grade_mapper/ice_grade_mapper.dart';
import 'package:flutter/cupertino.dart';

class GradeManager {
  GradeManager._privateConstructor();

  static final GradeManager _instance = GradeManager._privateConstructor();

  factory GradeManager() {
    return _instance;
  }

  static const int STANDARD_ROUTE_GRADE = 1100;
  static const int STANDARD_BOULDER_GRADE = 11700;
  static const int STANDARD_ICE_GRADE = 20;

  AbstractGradeMapper climbingMapper;
  AbstractGradeMapper boulderMapper;
  AbstractGradeMapper iceMapper;

  Future<void> init() async {
    this.climbingMapper = ClimbingGradeMapper();
    this.boulderMapper = BoulderGradeMapper();
    this.iceMapper = IceGradeMapper();
    await this.climbingMapper.init();
    await this.boulderMapper.init();
    await this.iceMapper.init();
  }

  AbstractGradeMapper getGradeMapper({@required ClimbingType type}) {
    if (type == null && Data().currentAscend != null) {
      type = Data().currentAscend.type;
    }
    switch (type) {
      case ClimbingType.ICE_CLIMBING:
        return this.iceMapper;
      case ClimbingType.BOULDER:
        return this.boulderMapper;
      case ClimbingType.SPEED_CLIMBING:
      case ClimbingType.FREE_SOLO:
      case ClimbingType.DEEP_WATER_SOLO:
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.SPORT_CLIMBING:
      default:
        return this.climbingMapper;
    }
  }

  int getStandardGrade(ClimbingType type) {
    switch (type) {
      case ClimbingType.ICE_CLIMBING:
        return STANDARD_ICE_GRADE;
      case ClimbingType.BOULDER:
        return STANDARD_BOULDER_GRADE;
      case ClimbingType.SPEED_CLIMBING:
      case ClimbingType.FREE_SOLO:
      case ClimbingType.DEEP_WATER_SOLO:
      case ClimbingType.SPORT_CLIMBING:
      default:
        return STANDARD_ROUTE_GRADE;
    }
  }
}
