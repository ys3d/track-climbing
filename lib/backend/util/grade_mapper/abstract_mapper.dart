import 'package:climbing_track/util/json_parser.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class AbstractGradeMapper with ChangeNotifier {
  static const String EXPANDED_SYSTEM_KEY = "expanded_system";

  static const String SYSTEM_KEY = "systems";
  static const String EXTENDED_KEY = "extended";
  static const String GRADES_KEY = "grades";

  String _currentGradingSystem;
  static bool _useExtendedSystem = true;
  String _gradePickerSystem;

  var _gradeMapping;
  var extendedMapping;
  var normalMapping;
  Map<String, int> _reverseMapper = Map();
  Map<String, int> _reverseMapperNormalized = Map();
  Map<int, int> roundMapping = Map();

  List<String> _pickerGrades;
  List<String> _axisLabels;
  List<String> grades;

  int maxNormalized = 0;

  Future<bool> init() async {

    this._currentGradingSystem = getStandardGradeSystem();
    await _loadGradingSystem();
    await _loadIsExpandedSystem();
    await _loadSystem();
    _loadAxisLabels();
    _loadPickerGrades();
    return true;
  }

  Future<void> _loadSystem() async {
    _gradePickerSystem = _currentGradingSystem;

    JsonParser jsonParser = JsonParser();
    var mapping = await jsonParser.parseAssetJsonFile(getFile());

    extendedMapping = mapping[EXTENDED_KEY];
    normalMapping = mapping[SYSTEM_KEY];

    _gradeMapping = mapping[GRADES_KEY];

    int i = 0;
    _gradeMapping.forEach((key, value) {
      for (String system in getGradeSystems()) {
        _reverseMapper[value[system.toLowerCase()]] = int.parse(key);
        _reverseMapperNormalized[value[system.toLowerCase()]] = i;
        _reverseMapperNormalized["_$key"] = i;
      }
      i++;
    });
    maxNormalized = i - 1;
  }

  void _loadAxisLabels() {
    roundMapping = Map();
    _axisLabels = [];
    grades = [];
    int roundedKey = 100;
    int j = 0;
    String grade = _gradeMapping[lowestValueKey()][_currentGradingSystem.toLowerCase()];
    _gradeMapping.forEach((key, value) {
      grades.add(value[_currentGradingSystem.toLowerCase()]);
      if (int.parse(key) == normalMapping[_currentGradingSystem.toLowerCase()][j]) {
        grade = value[_currentGradingSystem.toLowerCase()];
        j++;
        roundedKey = int.parse(key);
      }
      _axisLabels.add(grade);
      roundMapping[int.parse(key)] = roundedKey;
    });
  }

  void _loadPickerGrades() {
    _pickerGrades = [];
    var tmp = _useExtendedSystem ? extendedMapping : normalMapping;
    int j = 0;
    String grade = _gradeMapping[lowestValueKey()][_gradePickerSystem.toLowerCase()];
    _gradeMapping.forEach((key, value) {
      if (int.parse(key) == tmp[_gradePickerSystem.toLowerCase()][j]) {
        grade = _gradeMapping[key][_gradePickerSystem.toLowerCase()];
        j++;
        _pickerGrades.add(grade);
      }
    });
  }

  String getGradeFromId(int id) {
    return _gradeMapping[id.toString()][_currentGradingSystem.toLowerCase()];
  }

  String getGradeForGradePicker(int id) {
    return _gradeMapping[id.toString()][_gradePickerSystem.toLowerCase()];
  }

  int getIdFromGrade(String grade) {
    return _reverseMapper[grade];
  }

  int normalize(int gradeId) {
    return _reverseMapperNormalized["_$gradeId"];
  }

  List<String> getLabels() {
    return _axisLabels;
  }

  List<String> getGradesForPicker() {
    return _pickerGrades;
  }

  String get currentGradingSystem => _currentGradingSystem;

  set currentGradingSystem(String value) {
    _currentGradingSystem = value;
    _saveToSharedFiles(getGradingSystemKey(), _currentGradingSystem);
    _loadAxisLabels();
  }

  String get gradePickerSystem => _gradePickerSystem;

  set gradePickerSystem(String value) {
    _gradePickerSystem = value;
    _loadPickerGrades();
    notifyListeners();
  }

  bool get useExtendedSystem => _useExtendedSystem;

  set useExtendedSystem(bool value) {
    _useExtendedSystem = value;
    _saveExpandedSystemToSharedFiles(useExtendedSystem);
    _loadPickerGrades();
    notifyListeners();
  }

  _saveToSharedFiles(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  _saveExpandedSystemToSharedFiles(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(EXPANDED_SYSTEM_KEY, value);
  }

  _loadGradingSystem() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _currentGradingSystem = prefs.getString(getGradingSystemKey()) ?? getStandardGradeSystem();
  }

  _loadIsExpandedSystem() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _useExtendedSystem = prefs.getBool(EXPANDED_SYSTEM_KEY) ?? false;
  }

  String getFile();

  String lowestValueKey();

  List<String> getGradeSystems();

  String getStandardGradeSystem();

  String getGradingSystemKey();

  int getRoundedKey(int key) {
    return roundMapping[key];
  }
}
