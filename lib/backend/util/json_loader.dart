import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:climbing_track/util/json_parser.dart';
import 'package:shared_preferences/shared_preferences.dart';

class JsonLoader {
  Data data = Data();
  Map<int, Session> sessionMap = Map();

  Future<bool> loadFile(String path) async {
    try {
      JsonParser jsonParser = JsonParser();
      var _result = await jsonParser.parseJsonFile(path);
      return load(_result);
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> load(dynamic jsonObject) async {
    try {
      // loading Location Position
      final DBController dbController = DBController();

      _loadSessions(jsonObject["sessions"]);
      _loadRoutes(jsonObject["routes"]);
      _loadCoordinates(dbController, jsonObject["positions"]);
      _loadStandardHeights(dbController, jsonObject[DBController.standardHeightTable]);

      sessionMap.forEach((key, value) {
        data.addSession(value);
      });
      await data.loadLocations();
      await GradeManager().init();
      loadFirstDateOfSession();
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  void _loadSessions(var sessions) {
    for (var session in sessions) {
      sessionMap[session[DBController.sessionId]] = Session(
        DateTime.parse(session[DBController.sessionTimeStart]),
        DateTime.parse(session[DBController.sessionTimeEnd]),
        session[DBController.sessionLocation],
        session[DBController.sessionOutdoor] == 1,
        0,
      );
      sessionMap[session[DBController.sessionId]].sessionId = session[DBController.sessionId];
    }
  }

  void _loadCoordinates(DBController dbController, var locations) async {
    if (locations == null) {
      return; //For backwards compatibility
    }
    for (var location in locations) {
      dbController.insert(DBController.locationsTable, location);
    }
  }

  void _loadRoutes(var routes) {
    for (var route in routes) {
      Session session = sessionMap[route[DBController.sessionId]];
      session.routeCount += 1;
      if (route[DBController.ascendType] == null) {
        route[DBController.ascendType] = ClimbingType.SPORT_CLIMBING.name;
      }

      // because in old backup files type of [Ascend.SPORT_CLIMBING] was 'ROUTE'
      if (route[DBController.ascendType] == "ROUTE") {
        route[DBController.ascendType] = ClimbingType.SPORT_CLIMBING.name;
      }

      DBController().insert(DBController.ascendsTable, route);
    }
  }

  void _loadStandardHeights(DBController dbController, var standardHeights) async {
    if (standardHeights == null) {
      return; //For backwards compatibility
    }
    for (var standardHeight in standardHeights) {
      dbController.insert(DBController.standardHeightTable, standardHeight);
    }
  }

  Future<void> loadFirstDateOfSession() async {
    int lastYear;
    List<Map<String, dynamic>> result = await DBController().queryRows(
      DBController.sessionTable,
      orderBy: DBController.sessionTimeStart,
      limit: 1,
    );
    if (result.isEmpty) {
      DateTime now = DateTime.now();
      lastYear = now.year;
    } else {
      DateTime dateTime = DateTime.parse(result.first[DBController.sessionTimeStart]);
      lastYear = dateTime.year;
    }
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt("first_year", lastYear);
  }
}
