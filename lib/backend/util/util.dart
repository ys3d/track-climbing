import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/enums.dart';
import 'package:climbing_track/backend/model/height_measurement.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/util/style_mapper.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Util {
  static const String NULL_REPLACEMENT = "\u{200B}";

  static String getAscendSummary(Ascend route, BuildContext context) {
    return _getSummary(route.gradeID, route.styleID, route.toprope, route.speedTime, route.type, context: context);
  }

  static String getDataSummary(Map<String, dynamic> data, BuildContext context) {
    return _getSummary(
      data[DBController.ascendGradeId],
      data[DBController.ascendStyleId],
      data[DBController.routeTopRope] == 1,
      data[DBController.speedTime],
      EnumToString.fromString(ClimbingType.values, data[DBController.ascendType]),
      context: context,
    );
  }

  static String _getSummary(
    int gradeID,
    int styleID,
    bool toprope,
    int speedTime,
    ClimbingType type, {
    @required BuildContext context,
  }) {
    switch (type) {
      case ClimbingType.BOULDER:
        return "${getGrade(gradeID, type)} | ${getStyle(styleID, context)} | ${AppLocalizations.of(context).style_boulder}";
      case ClimbingType.SPORT_CLIMBING:
        return "${getGrade(gradeID, type)} | ${getStyle(styleID, context)} | ${getTopOrLead(toprope, context)}";
      case ClimbingType.SPEED_CLIMBING:
        return "${(speedTime / 1000).toStringAsFixed(2)} ${AppLocalizations.of(context).util_seconds}";
      case ClimbingType.FREE_SOLO:
        return "${getGrade(gradeID, type)} | ${AppLocalizations.of(context).style_free_solo}";
      case ClimbingType.DEEP_WATER_SOLO:
        return "${getGrade(gradeID, type)} | ${getStyle(styleID, context)} | ${AppLocalizations.of(context).style_dws}";
      case ClimbingType.ICE_CLIMBING:
        return "${getGrade(gradeID, type)} | ${getStyle(styleID, context)} | ${getTopOrLead(toprope, context)}";
      default:
        return "[TODO]";
    }
  }

  static String getStyle(int styleID, BuildContext context, {int tries}) {
    if (tries == null || styleID <= 2 || styleID == 100) {
      return StyleMapper().getStyleFromId(styleID, context);
    } else if (styleID == 3 || styleID == 101 || styleID == 102) {
      return "${StyleMapper().getStyleFromId(styleID, context)} "
          "(${tries.toString()}. ${AppLocalizations.of(context).ascend_attempts})";
    } else if (styleID >= 4) {
      return "${StyleMapper().getStyleFromId(styleID, context)} "
          "(${tries.toString()} ${AppLocalizations.of(context).ascend_hangs(tries)})";
    }
    return StyleMapper().getStyleFromId(styleID, context);
  }

  static String getGrade(int gradeID, ClimbingType type) {
    return GradeManager().getGradeMapper(type: type).getGradeFromId(gradeID);
  }

  static String getTopOrLead(bool toprope, BuildContext context) {
    return toprope ? AppLocalizations.of(context).style_toprope : AppLocalizations.of(context).style_lead;
  }

  static Widget getRating(int rating) {
    List<Widget> stars = [];
    for (int i = 0; i < 5; i++) {
      if (i < rating) {
        stars.add(Constants.FULL_STAR);
      } else {
        stars.add(Constants.EMPTY_STAR);
      }
    }
    return Wrap(children: stars);
  }

  static String getComment(String comment) {
    if (comment == null || comment == '') {
      return '-';
    } else {
      return comment;
    }
  }

  static String getAscendNameWithoutPrefix(
    String name,
    BuildContext context, {
    @required int speedType,
    @required ClimbingType type,
  }) {
    if (type == ClimbingType.SPEED_CLIMBING && speedType == 0) {
      return AppLocalizations.of(context).style_speed_ascend_comp.replaceAll("", NULL_REPLACEMENT);
    }
    return replaceStringWithBlankSpaces(
      name == "" ? AppLocalizations.of(context).util_name_missing : name,
    );
  }

  static String getAscendName(
    BuildContext context,
    String name, {
    @required ClimbingType type,
    @required int speedType,
  }) {
    String prefix = "";
    switch (type) {
      case ClimbingType.BOULDER:
        prefix = "[${AppLocalizations.of(context).style_boulder_prefix}] ";
        break;
      case ClimbingType.SPEED_CLIMBING:
        if (speedType == 0) {
          return AppLocalizations.of(context).style_speed_ascend_comp.replaceAll("", NULL_REPLACEMENT);
        }
        if (name == "") return AppLocalizations.of(context).style_speed_ascend.replaceAll("", NULL_REPLACEMENT);
        prefix = "[${AppLocalizations.of(context).style_speed_prefix.toUpperCase()}] ";
        break;
      case ClimbingType.FREE_SOLO:
        prefix = "[${AppLocalizations.of(context).style_free_solo_prefix}] ";
        break;
      case ClimbingType.DEEP_WATER_SOLO:
        prefix = "[${AppLocalizations.of(context).style_dws}] ";
        break;
      case ClimbingType.ICE_CLIMBING:
        prefix = "[${AppLocalizations.of(context).style_ice_prefix}] ";
        break;
      case ClimbingType.SPORT_CLIMBING:
      default:
        prefix = "";
    }
    return replaceStringWithBlankSpaces('$prefix${name == "" ? AppLocalizations.of(context).util_name_missing : name}');
  }

  static String getRouteNameForFiltering(String name, BuildContext context) {
    return name == "" ? AppLocalizations.of(context).util_name_missing : name;
  }

  static String getLocationDateString(String date, String location) {
    return replaceStringWithBlankSpaces("${DateFormat("dd.MM.yy").format(DateTime.parse(date))} | $location");
  }

  static String getLocation(String location) {
    return replaceStringWithBlankSpaces(location);
  }

  static String getHeightWithShortUnit(double meters, {String zeroFilling = "- - -"}) {
    String unit = DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET ? "ft" : "m";
    return meters <= 0 ? zeroFilling : "${heightFromMeters(meters)} $unit";
  }

  static String getHeightString(double meters) {
    return meters <= 0 ? "- - -" : "${heightFromMeters(meters)}";
  }

  /// Calculates height in meters from in app selected length unit (meters or feet)
  /// Returns height in meters as double
  static double heightToMeters(double height) {
    if (DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET) {
      return height * 0.3048;
    }
    return height;
  }

  /// Calculates meters to in app selected length unit
  /// Possible units so far: meters & feet
  static dynamic heightFromMeters(double meters) {
    if (DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET) {
      return (meters * 3.280839895).round();
    }
    return roundToNearestPointFive(meters);
  }

  /// rounding double to nearest .5 number
  ///
  /// Returns rounded value as double
  static double roundToNearestPointFive(double value) {
    return ((value * 2).round() / 2);
  }

  /// Replaces blank spaces with special character
  /// Text overflow splitting is no longer limited to words
  /// Splits are possible after each letter
  static String replaceStringWithBlankSpaces(String string) {
    return string.replaceAll("", NULL_REPLACEMENT);
  }

  /// Maps style id to [StyleType] instance
  ///
  /// Return [StyleType] instance
  static StyleType mapStyleToStyleType(int style) {
    return {
      1: StyleType.ONSIGHT,
      2: StyleType.FLASH,
      3: StyleType.REDPOINT,
      4: StyleType.TOP,
      5: StyleType.TOP,
      6: StyleType.NONE,
      7: StyleType.NONE,
      8: StyleType.NONE,
      100: StyleType.FLASH,
      101: StyleType.TOP,
      102: StyleType.PROJECT,
    }[style];
  }
}
