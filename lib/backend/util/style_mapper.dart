import 'package:flutter/cupertino.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class StyleMapper {
  StyleMapper._privateConstructor();

  static final StyleMapper _instance = StyleMapper._privateConstructor();

  factory StyleMapper() {
    return _instance;
  }

  Map<int, String> getMapping(BuildContext context) {
    return {
      1: AppLocalizations.of(context).style_onsight,
      2: AppLocalizations.of(context).style_flash,
      3: AppLocalizations.of(context).style_redpoint,
      4: AppLocalizations.of(context).style_af,
      5: AppLocalizations.of(context).style_hangdogging,
      6: '3/4',
      7: '1/2',
      8: '1/4',
      100: AppLocalizations.of(context).style_flash,
      101: AppLocalizations.of(context).style_top,
      102: AppLocalizations.of(context).style_project,
    };
  }

  String getStyleFromId(int id, BuildContext context) {
    return getMapping(context)[id];
  }

  int getDefaultClimbingStyle() {
    return 3;
  }

  int getDefaultBoulderStyle() {
    return 101;
  }

  List<_StyleMapping> getDeepWaterMapping(BuildContext context) {
    return getMapping(context)
        .entries
        .where((element) => [1, 2, 3, 6, 7, 8].contains(element.key))
        .map((e) => _StyleMapping(e.key, e.value))
        .toList();
  }

  List<_StyleMapping> getSportClimbingMapping(BuildContext context) {
    return getMapping(context)
        .entries
        .where((element) => element.key < 50)
        .map((e) => _StyleMapping(e.key, e.value))
        .toList();
  }

  List<_StyleMapping> getBoulderMapping(BuildContext context) {
    return getMapping(context)
        .entries
        .where((element) => element.key >= 50)
        .map((e) => _StyleMapping(e.key, e.value))
        .toList();
  }
}

class _StyleMapping {
  final int id;
  final String style;

  _StyleMapping(this.id, this.style);
}
