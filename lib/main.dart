import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:flutter/material.dart';
import 'package:climbing_track/app/climbing_track_app.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'app/app_theme.dart';
import 'init.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Init.initialize();
  runApp(InitializationApp());
}

class InitializationApp extends StatelessWidget {
  static GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp(
      onGenerateTitle: (BuildContext context) => AppLocalizations.of(context).main_title,
      theme: AppThemeDataFactory.prepareThemeData(),
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      locale: DataStore().settingsDataStore.language == "de_TY"
          ? Locale("de", "TY")
          : Locale(DataStore().settingsDataStore.language),
      localeListResolutionCallback: (locales, supportedLocales) {
        for (Locale locale in locales) {
          if (supportedLocales.contains(locale)) {
            return locale;
          }
        }
        return Locale('en');
      },
      navigatorKey: navigatorKey,
      home: ClimbingTrackApp(),
    );
  }
}
