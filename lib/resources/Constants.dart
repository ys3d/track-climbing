import 'package:flutter/material.dart';

abstract class Constants {
  static const double STANDARD_PADDING = 20.0;
  static const double SMALL_PADDING = 8.0;
  static const int STANDARD_ANIMATION_TIME = 200; // in milliseconds
  static const double STANDARD_BORDER_RADIUS = 24.0;
  static const double STANDARD_ICON_SIZE = 36.0;

  static const Icon EMPTY_STAR = Icon(Icons.star_border);
  static const Icon FULL_STAR = Icon(Icons.star, color: Color(0xFFFFB300));
}
