import 'dart:io';

import 'package:climbing_track/backend/Location.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';

class Init {
  static Future initialize() async {
    await _registerServices();
    await _loadSettings();
  }

  static _registerServices() async {}

  static _loadSettings() async {
    debugPrint("starting loading settings");
    await DataStore().init();
    await GradeManager().init();
    Data data = Data();
    await data.loadData();
    await SessionState().init(data.getCurrentSession() != null);
    await Location().init();

    // Firebase not working with
    // ios so only activate if on android
    if (Platform.isAndroid) {
      try {
        await Firebase.initializeApp();
      } catch (e) {
        print(e);
      }
    }
    debugPrint("finished loading settings");
  }
}
