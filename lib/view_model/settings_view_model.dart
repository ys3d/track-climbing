import 'dart:convert';
import 'dart:io';

import 'package:climbing_track/backend/Location.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/data_store/setting_data_store.dart';
import 'package:climbing_track/backend/data_store/tooltips_data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/util/json_creator.dart';
import 'package:climbing_track/backend/util/json_loader.dart';
import 'package:climbing_track/view_model/google_drive_backup_handler.dart';
import 'package:climbing_track/view_model/google_drive_backup_result.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:share_plus/share_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:store_redirect/store_redirect.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SettingsViewModel with ChangeNotifier {
  String versionNumber = '';
  String buildNumber = '';

  SettingsDataStore settings = DataStore().settingsDataStore;
  ToolTipsDataStore toolTipsDataStore = DataStore().toolTipsDataStore;
  GoogleDriveBackupHandler _googleDriveBackupHandler;
  SharedPreferences prefs;

  bool isUploadingGoogleDrive = false;
  bool isDownloadingGoogleDrive = false;
  bool isCreatingBackup = false;
  bool isLoadingBackup = false;
  bool isLoadingAutomaticBackup = false;

  Future<void> init() async {
    if (Platform.isAndroid) {
      _googleDriveBackupHandler = GoogleDriveBackupHandler();
    }
    await _loadVersionNumber();
    await _loadSharedPrefs();
    notifyListeners();
  }

  DateTime getLastDriveBackup() {
    if (prefs == null) return null;
    String result = prefs.getString(Data.LAST_DRIVE_BACKUP_KEY);
    if (result == null) return null;
    return DateTime.parse(result);
  }

  Future<bool> checkStoragePermission() async {
    PermissionStatus status = await Permission.storage.request();
    if (status.isGranted) {
      return true;
    }

    settings.automaticBackups = false;
    notifyListeners();
    return false;
  }

  Future<void> _loadSharedPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  Future<void> _loadVersionNumber() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    this.versionNumber = packageInfo.version;
    this.buildNumber = packageInfo.buildNumber;
  }

  Future<bool> checkLocationPermission() async {
    bool status = await Location().hasPermission();
    if (status) {
      return true;
    }

    Location().setLocation(false);
    notifyListeners();
    return false;
  }

  void resetTooltips() {
    toolTipsDataStore.tooltipCloseSession = true;
    toolTipsDataStore.tooltipSessionSummary = true;
    toolTipsDataStore.tooltipChartSettings = true;
    toolTipsDataStore.tooltipOverviewPage = true;
    toolTipsDataStore.tooltipAscendPage = true;
  }

  Future<bool> loadFile(String value) async {
    if (value == null) {
      return false;
    }
    if (!value.endsWith('.json')) {
      return false;
    }
    Data().reset();
    DBController().deleteAll();
    return await JsonLoader().loadFile(value);
  }

  void deleteDataBase() {
    DBController().deleteAll();
    Data().reset();
  }

  Future<void> sendFeedBack() async {
    final Email email = Email(
      recipients: ['lukas.freudenmann+climbing-tracker@gmail.com'],
      isHTML: false,
    );
    await FlutterEmailSender.send(email);
  }

  void rateApp() {
    StoreRedirect.redirect();
  }

  Future<void> createBackupFile() async {
    String result = await JsonCreator().createBackupFile();
    isCreatingBackup = false;
    _shareFile(result);
  }

  void _shareFile(String filePath) async {
    Share.shareFiles([filePath]);
  }

  Future<void> uploadGoogleDriveBackup(BuildContext context) async {
    isUploadingGoogleDrive = true;
    final String content = await JsonCreator().createJsonString();
    final GoogleDriveBackupResult uploadResult = await _googleDriveBackupHandler.uploadFileToGoogleDrive(content);
    String message;
    if (uploadResult.result) {
      message = AppLocalizations.of(context).google_drive_backup_uploaded;
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString(Data.LAST_DRIVE_BACKUP_KEY, DateTime.now().toIso8601String());
    } else {
      message = AppLocalizations.of(context).google_drive_backup_not_uploaded;

      // Only for debugging and error finding
      if (kDebugMode) uploadResult.showErrorDialog(context);
    }
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(message),
        ),
      );
    isUploadingGoogleDrive = false;
  }

  Future<void> restoreGoogleDriveBackup(BuildContext context) async {
    isDownloadingGoogleDrive = true;
    final GoogleDriveDownloadResult _driveDownloadResult = await _googleDriveBackupHandler.downloadGoogleDriveFile();

    String message;

    if (_driveDownloadResult.content == null) {
      message = AppLocalizations.of(context).google_drive_backup_not_downloaded;

      // Only for debugging and error finding
      if (kDebugMode) _driveDownloadResult.showErrorDialog(context);
    } else {
      try {
        Data().reset();
        DBController().deleteAll();
        bool result = await JsonLoader().load(jsonDecode(_driveDownloadResult.content));
        if (!result) {
          message = AppLocalizations.of(context).google_drive_backup_not_restored;
        } else {
          message = AppLocalizations.of(context).google_drive_backup_restored;
        }
      } catch (e) {
        message = AppLocalizations.of(context).google_drive_backup_not_restored;
      }
    }
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(message),
        ),
      );
    isDownloadingGoogleDrive = false;
  }

  Future<void> toggleGoogleCloudBackups(BuildContext context, bool value) async {
    if (value) {
      GoogleDriveBackupResult signInResult = await _googleDriveBackupHandler.signIn();
      if (!signInResult.result) {
        settings.googleDriveActivated = false;

        // Only for debugging and error finding
        if (kDebugMode) signInResult.showErrorDialog(context);

        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              content: Text(AppLocalizations.of(context).google_drive_backup_not_activated),
            ),
          );
      } else {
        settings.googleDriveActivated = true;
      }
    } else {
      settings.googleDriveActivated = false;
      _googleDriveBackupHandler.signOut();
    }
  }

  String getVersionString() => '$versionNumber ($buildNumber)';
}
