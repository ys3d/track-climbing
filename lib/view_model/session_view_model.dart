import 'dart:io';
import 'dart:math';

import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:climbing_track/ui/widgets/location_picker_map.dart';
import 'package:climbing_track/util/extensions.dart';
import 'package:climbing_track/util/screenshot.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:latlong2/latlong.dart';
import 'package:share_plus/share_plus.dart';

class SessionViewModel {
  static const String AVERAGE_KEY = "average_key";
  static const String MAX_KEY = "max_key";
  static const String HEIGHT_KEY = "height_key";
  static const String COUNT_KEY = "count_key";
  static const String CLIMBING_SYSTEM_KEY = "climbing_type_key";

  Session session;
  Session editedSession;
  ScreenshotController screenshotController;
  Future<List<Map<String, dynamic>>> ascents;
  Map<dynamic, dynamic> sessionSummaryData = {
    SessionViewModel.HEIGHT_KEY: 0.0,
    SessionViewModel.AVERAGE_KEY: 0,
    SessionViewModel.COUNT_KEY: 0,
  };

  // for edit page
  List<Map> toDelete = [];
  bool changedLocationCoordinates = false;

  SessionViewModel({@required Map<String, dynamic> sessionDat, this.screenshotController}) {
    if (screenshotController == null) screenshotController = ScreenshotController();
    this.session = Session.fromMap(sessionDat);
    this.editedSession = session.copy();
    this.ascents = fetchAscents();
  }

  Future<List<Map<String, dynamic>>> fetchAscents() async {
    return DBController().queryRows(
      DBController.ascendsTable,
      where: "${DBController.sessionId} == ${session.sessionId}",
    );
  }

  Future<List<Map<String, dynamic>>> getAscentsAndCalculateSessionSummary() async {
    this.sessionSummaryData = _calculateSessionSummaryData(await ascents);
    return ascents;
  }

  Map<dynamic, dynamic> _calculateSessionSummaryData(List<Map<String, dynamic>> ascents) {
    Map<dynamic, dynamic> result = {
      AVERAGE_KEY: 0,
      MAX_KEY: null,
      HEIGHT_KEY: 0.0,
      COUNT_KEY: ascents.length,
      CLIMBING_SYSTEM_KEY: ClimbingType.SPORT_CLIMBING,
    };
    int climbing = 0, boulder = 0, ice = 0;

    for (Map ascent in ascents) {
      String type = ascent[DBController.ascendType];
      if (type == ClimbingType.BOULDER.name) {
        boulder++;
      } else if (type == ClimbingType.ICE_CLIMBING.name) {
        ice++;
      } else {
        climbing++;
      }
    }

    if (climbing >= boulder && climbing >= ice) {
      result[CLIMBING_SYSTEM_KEY] = ClimbingType.SPORT_CLIMBING;
    } else if (boulder >= climbing && boulder >= ice) {
      result[CLIMBING_SYSTEM_KEY] = ClimbingType.BOULDER;
    } else {
      result[CLIMBING_SYSTEM_KEY] = ClimbingType.ICE_CLIMBING;
    }

    for (Map ascent in ascents) {
      result[HEIGHT_KEY] += ascent[DBController.ascendHeight];

      final bool _check = result[CLIMBING_SYSTEM_KEY] == ClimbingType.SPORT_CLIMBING
          ? ascent[DBController.ascendType] != EnumToString.convertToString(ClimbingType.BOULDER) &&
              ascent[DBController.ascendType] != EnumToString.convertToString(ClimbingType.ICE_CLIMBING)
          : ascent[DBController.ascendType] == EnumToString.convertToString(result[CLIMBING_SYSTEM_KEY]);

      if (_check) {
        result[AVERAGE_KEY] += GradeManager()
            .getGradeMapper(type: EnumToString.fromString(ClimbingType.values, ascent[DBController.ascendType]))
            .normalize(
              ascent[DBController.ascendGradeId],
            );

        if (result[MAX_KEY] == null ||
            ascent[DBController.ascendGradeId] > result[MAX_KEY][DBController.ascendGradeId]) {
          result[MAX_KEY] = ascent;
        }
      }
    }
    if (ascents.isNotEmpty) {
      result[AVERAGE_KEY] /= max(climbing, max(boulder, ice));
    }
    return result;
  }

  String get maxGrade {
    return sessionSummaryData[MAX_KEY] != null
        ? GradeManager()
            .getGradeMapper(
              type: EnumToString.fromString(
                ClimbingType.values,
                sessionSummaryData[MAX_KEY][DBController.ascendType],
              ),
            )
            .getGradeFromId(sessionSummaryData[MAX_KEY][DBController.ascendGradeId])
        : "---";
  }

  String get avgGrade {
    return sessionSummaryData[AVERAGE_KEY] != 0
        ? GradeManager()
            .getGradeMapper(type: sessionSummaryData[CLIMBING_SYSTEM_KEY])
            .grades[sessionSummaryData[AVERAGE_KEY].round()]
            .toString()
        : "---";
  }

  int get count => sessionSummaryData[COUNT_KEY];

  double getClimbedMeters(List<Map<String, dynamic>> ascendList) {
    return ascendList.fold(0.0, (previousValue, element) => previousValue += element[DBController.ascendHeight]);
  }

  void createScreenShot() {
    String dateString = DateFormat("yyyy-MM-dd").format(session.startDate);

    screenshotController.capture(fileName: "$dateString-session-summary").then((File image) {
      Share.shareFiles([image.path]);
    }).catchError((onError) {
      print(onError);
    });
  }

  void deleteSession(BuildContext context) async {
    // If Session Summary in session page is open, then delete of this session would lead to null pointer.
    // So session state must be updated
    if (SessionState().currentState == SessionStateEnum.end) SessionState().setState(SessionStateEnum.none);

    DBController dbController = DBController();
    await dbController.deleteRoutes(session.sessionId);
    await dbController.delete(DBController.sessionTable, session.sessionId);
    Data().deleteLocationsTableEntryOnDeletingSession(session.locationName);
    Navigator.of(context).pop(false);
    Navigator.of(context).pop(false);
    Navigator.of(context).pop(true);
  }

  void onSave(BuildContext context, Future<List<Map<String, dynamic>>> ascentList) async {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();

    // delete ascends
    for (Map ascend in toDelete) {
      DBController().deleteAscend(ascend[DBController.ascendId]);
    }
    toDelete.clear();

    // update ascend
    for (Map ascend in await ascentList) {
      DBController().updateAscend(ascend, ascend[DBController.ascendId]);
    }

    await DBController().updateSessionTable(
      editedSession.toJson(editedSession.sessionId),
      editedSession.sessionId,
    );
    await Data().updatePositionsTableOnChangingLocations(
      editedSession.locationName,
      session.locationName,
    );

    // update location coordinates
    _saveLocationCoordinates();

    await Data().loadLocations();
    this.session = editedSession;
    this.ascents = fetchAscents();

    Navigator.of(context).pop(true);
  }

  Future<void> _saveLocationCoordinates() async {
    if (changedLocationCoordinates) {
      if (editedSession.coordinates == null) {
        await DBController().deleteLocation(editedSession.locationName);
      } else {
        // check location coordinated do exist
        List<Map<String, dynamic>> query = await DBController().queryCoordinatesByLocation(editedSession.locationName);
        if (query == null || query.isEmpty) {
          // do no exist
          await DBController().insert(DBController.locationsTable, {
            DBController.sessionLocation: editedSession.locationName,
            DBController.latitude: editedSession.coordinates.latitude,
            DBController.longitude: editedSession.coordinates.longitude,
          });
        } else {
          // do exist
          DBController().updateLocation(
            {
              DBController.latitude: editedSession.coordinates.latitude,
              DBController.longitude: editedSession.coordinates.longitude,
            },
            editedSession.locationName,
          );
        }
      }
    }
  }

  void initEditFunctions() {
    this.toDelete = [];
    this.editedSession = session.copy();
  }

  Future<void> chooseLocationCoordinatesFromMap(BuildContext context, Function setState) async {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    LatLng coordinates = LatLng(51.5, -0.09);
    double zoom = 5;

    if (this.editedSession.coordinates != null) {
      coordinates = this.editedSession.coordinates.copy();
      zoom = 13;
    }

    bool result = await Navigator.push(
      context,
      MaterialPageRoute<bool>(
        builder: (BuildContext context) => LocationCoordinatesPickerMap(
          coordinates: coordinates,
          initialZoom: zoom,
        ),
      ),
    );

    if (result != null && result) {
      changedLocationCoordinates = true;
      editedSession.coordinates = coordinates;
      setState(() {});
    }
  }

  bool deleteAscend(Map ascend, List<Map> data) {
    data.remove(ascend);
    toDelete.add(ascend);
    return true;
  }

  void onReAdd(int index, Map ascend, List<Map> data) {
    data.insert(index, ascend);
    toDelete.remove(ascend);
  }
}
