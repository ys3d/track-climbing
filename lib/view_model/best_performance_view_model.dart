import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/enums.dart';
import 'package:climbing_track/backend/overview_data_loader.dart';
import 'package:flutter/material.dart';

class BestPerformanceViewModel {
  static bool informed = false;
  final bool indoor;
  final int lastSessionId;
  OverviewDataLoader dataLoader;

  BestPerformanceViewModel({@required this.indoor, @required this.lastSessionId}) {
    dataLoader = OverviewDataLoader(
      fetchIndoor: indoor,
      fetchOutdoor: !indoor,
      fetchSpeed: true,
      fetchExpandedValues: false,
    );
  }

  Future<List<ResultTriple>> calculateBestPerformances() async {
    if (informed) return [];
    informed = true;

    List<ResultTriple> bestNewPerformances = [];

    await dataLoader.init();
    Map<Classes, Map<StyleType, Map<String, dynamic>>> bestPerformancesData =
        dataLoader.calculatedData[this.indoor ? OverviewDataLoader.INDOOR_KEY : OverviewDataLoader.OUTDOOR_KEY];

    for (Classes _class in bestPerformancesData.keys) {
      for (StyleType styleType in bestPerformancesData[_class].keys) {
        if (bestPerformancesData[_class][styleType] == null) continue;

        // Test if ascend is in newest session
        // If so, add to list
        // Don't add if list already contains ascend (same ascendId)
        if (bestPerformancesData[_class][styleType][DBController.sessionId] == this.lastSessionId) {
          if (!bestNewPerformances.any((element) =>
              element.ascend[DBController.ascendId] ==
              bestPerformancesData[_class][styleType][DBController.ascendId])) {
            bestNewPerformances.add(ResultTriple(bestPerformancesData[_class][styleType], _class, styleType));
          }
        }
      }
    }

    if (dataLoader.speedData.isNotEmpty && dataLoader.speedData.first[DBController.sessionId] == this.lastSessionId) {
      bestNewPerformances.add(ResultTriple(dataLoader.speedData.first, Classes.SPEED, StyleType.TOP));
    }

    return bestNewPerformances;
  }
}

class ResultTriple {
  final Map ascend;
  final Classes classStyle;
  final StyleType styleType;

  ResultTriple(this.ascend, this.classStyle, this.styleType);

  @override
  String toString() {
    return 'ResultTriple{ascend: $ascend, classStyle: $classStyle, styleType: $styleType}';
  }
}
