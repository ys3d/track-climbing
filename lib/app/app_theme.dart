import 'package:flutter/material.dart';
import 'package:climbing_track/app/app_colors.dart';

class AppThemeDataFactory {
  static ThemeData prepareThemeData({useMaterial3: true}) => ThemeData.from(
        colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor),
        useMaterial3: useMaterial3,
        textTheme: TextTheme(
          bodyText1: TextStyle(
            letterSpacing: 0.0,
            wordSpacing: 0.0,
            height: 0.0,
            fontSize: 18,
          ),
          bodyText2: TextStyle(
            fontSize: 14,
            letterSpacing: 0.0,
            wordSpacing: 0.0,
            height: 0.0,
          ),
          subtitle1: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
            color: AppColors.textPrimary,
            letterSpacing: 0.0,
            wordSpacing: 0.0,
            height: 0.0,
          ),
        ),
      );
}
