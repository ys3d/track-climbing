import 'package:climbing_track/ui/pages/home/session_page.dart';
import 'package:climbing_track/ui/pages/overview/overview_page.dart';
import 'package:climbing_track/ui/pages/sessions/session_overview.dart';
import 'package:climbing_track/ui/pages/statistics/chart_page.dart';
import 'package:climbing_track/ui/pages/ascends/ascents_pages.dart';
import 'package:climbing_track/ui/pages/settings/settings_page.dart';
import 'package:climbing_track/util/notifications_handler.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ClimbingTrackApp extends StatefulWidget {
  final int selectedIndex;

  const ClimbingTrackApp({Key key, this.selectedIndex = _ClimbingTrackAppState.MAIN_PAGE_INDEX}) : super(key: key);

  @override
  _ClimbingTrackAppState createState() => _ClimbingTrackAppState();
}

class _ClimbingTrackAppState extends State<ClimbingTrackApp> {
  static const int MAIN_PAGE_INDEX = 0;
  final List<Widget> pages = [
    SessionPage(),
    OverviewPage(),
    const SessionsOverview(),
    AscendsPage(),
    ChartPage(),
    SettingsPage(),
  ];

  int _selectedIndex;
  bool canBack = false;


  @override
  void initState() {
    super.initState();
    _selectedIndex = widget.selectedIndex;
  }

  @override
  Widget build(BuildContext context) {
    NotificationHandler().init();
    return KeyboardVisibilityProvider(
      child: Scaffold(
        body: createBody(),
        bottomNavigationBar: createBottomNavigationBar(),
      ),
    );
  }

  void _onItemTapped(int index) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    setState(() {
      _selectedIndex = index;
    });
  }

  Widget createBody() {
    return Stack(
      children: <Widget>[
        const Positioned.fill(
          child: Image(
            image: AssetImage('assets/images/background.jpg'),
            fit: BoxFit.fill,
          ),
        ),
        pages[_selectedIndex],
      ],
    );
  }

  Widget createBottomNavigationBar() {
    return Container(
      decoration: const BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black12,
            spreadRadius: 1.0,
            blurRadius: 10,
          ),
        ],
      ),
      child: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: const Icon(Icons.home_outlined),
            label: AppLocalizations.of(context).home_title,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.person_search),
            label: AppLocalizations.of(context).overview_title,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.place_outlined),
            label: AppLocalizations.of(context).sessions_title,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.list_alt),
            label: AppLocalizations.of(context).ascends_title,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.bar_chart),
            label: AppLocalizations.of(context).statistics_title,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.settings),
            label: AppLocalizations.of(context).settings_title,
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        unselectedItemColor: Colors.black45,
        onTap: _onItemTapped,
        showUnselectedLabels: false,
        unselectedFontSize: 14.0,
      ),
    );
  }
}
