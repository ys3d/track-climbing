import 'package:latlong2/latlong.dart';

extension StringCasingExtension on String {
  String toCapitalized() => length > 0 ? '${this[0].toUpperCase()}${substring(1).toLowerCase()}' : '';

  String toTitleCase() => replaceAll(RegExp(' +'), ' ').split(' ').map((str) => str.toCapitalized()).join(' ');
}

extension DateTimeExtension on DateTime {
  DateTime copy() => DateTime(year, month, day, hour, minute, second, millisecond, microsecond);
}

extension LatLngExtension on LatLng {
  LatLng copy() => LatLng(latitude, longitude);
}
