import 'dart:io';
import 'dart:ui';
import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:climbing_track/ui/dialog/end_session_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ConfirmDialog {
  static const double SMALL_FONT_SIZE = 14;
  static const double MATERIAL_BUTTON_HEIGHT = 38;
  static final ImageFilter blurFilter = ImageFilter.blur(sigmaX: 6, sigmaY: 6);

  Future<void> showEndSessionDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, setState) {
          return EndSessionDialog();
        });
      },
    );
  }

  Future<bool> showDeleteDatabaseDialog(BuildContext context, Function onAccept) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 24.0),
          titleTextStyle: Theme.of(context).textTheme.headline6,
          contentTextStyle: Theme.of(context).textTheme.bodyText2,
          scrollable: true,
          content: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(top: 12.0),
                child: const Icon(Icons.delete_forever_rounded, color: Colors.white, size: 42),
                width: 50.0,
                height: 50.0,
                decoration: const BoxDecoration(
                  color: Colors.red,
                  shape: BoxShape.circle,
                ),
              ),
              const SizedBox(width: 14.0),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: Constants.SMALL_PADDING),
                    Text(
                      AppLocalizations.of(context).confirm_delete_data,
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    const SizedBox(height: Constants.SMALL_PADDING),
                    Text(
                      AppLocalizations.of(context).confirm_delete_permanently_lose_data,
                      overflow: TextOverflow.clip,
                    ),
                    const SizedBox(height: Constants.SMALL_PADDING),
                    Text(
                      AppLocalizations.of(context).confirm_delete_sessions,
                      overflow: TextOverflow.clip,
                    ),
                    const SizedBox(height: 3.0),
                    Text(
                      AppLocalizations.of(context).confirm_delete_ascends,
                      overflow: TextOverflow.clip,
                    ),
                    const SizedBox(height: 3.0),
                    Text(
                      AppLocalizations.of(context).confirm_delete_statistics,
                      overflow: TextOverflow.clip,
                    )
                  ],
                ),
              ),
            ],
          ),
          actions: <Widget>[
            OutlinedButton(
              onPressed: () => {FocusScope.of(context).unfocus(), Navigator.of(context).pop()},
              child: Text(
                AppLocalizations.of(context).general_cancel,
                style: TextStyle(color: Colors.red.shade700),
              ),
              style: OutlinedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
            ),
            TextButton(
              style: TextButton.styleFrom(
                padding: EdgeInsets.symmetric(horizontal: 18, vertical: 8),
                backgroundColor: Colors.red.shade700,
                foregroundColor: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
              onPressed: () => {onAccept(), FocusScope.of(context).unfocus(), Navigator.of(context).pop()},
              child: Text(AppLocalizations.of(context).general_delete),
            ),
          ],
        );
      },
    );
  }

  Future<bool> showLoadBackupDialog(BuildContext context, Function onAccept) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 24.0),
          titleTextStyle: Theme.of(context).textTheme.subtitle1,
          scrollable: true,
          content: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Padding(
                padding: EdgeInsets.only(top: 8),
                child: Icon(Icons.warning_amber_rounded, color: Colors.orange, size: 42),
              ),
              const SizedBox(width: 14.0),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: Constants.SMALL_PADDING),
                    Text(
                      "${AppLocalizations.of(context).confirm_load_backup}?",
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    const SizedBox(height: Constants.SMALL_PADDING),
                    Text(
                      AppLocalizations.of(context).confirm_load_backup_warning,
                      style: Theme.of(context).textTheme.bodyText2,
                      overflow: TextOverflow.clip,
                    ),
                  ],
                ),
              ),
            ],
          ),
          actions: <Widget>[
            OutlinedButton(
              onPressed: () => {
                FocusScope.of(context).unfocus(),
                Navigator.of(context).pop(),
              },
              child: Text(
                AppLocalizations.of(context).general_cancel,
                style: TextStyle(color: AppColors.accentColor),
              ),
              style: OutlinedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
            ),
            TextButton(
              style: TextButton.styleFrom(
                padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 8),
                backgroundColor: AppColors.accentColor,
                foregroundColor: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
              onPressed: () => {FocusScope.of(context).unfocus(), Navigator.pop(context), onAccept()},
              child: Text(AppLocalizations.of(context).confirm_load_backup),
            ),
          ],
        );
      },
    );
  }

  Future<bool> showChooseBackupDialog(
    BuildContext context,
    Function(String) onAccept,
    List<FileSystemEntity> backupList,
  ) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 24.0),
          titleTextStyle: Theme.of(context).textTheme.subtitle1,
          contentTextStyle: Theme.of(context).textTheme.bodyText2,
          content: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(top: 8),
                      child: const Icon(Icons.warning_amber_rounded, color: Colors.orange, size: 42),
                    ),
                    const SizedBox(width: 14.0),
                    Flexible(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: Constants.SMALL_PADDING),
                          Text(
                            "${AppLocalizations.of(context).confirm_load_backup}?",
                            style: Theme.of(context).textTheme.headline6,
                          ),
                          const SizedBox(height: Constants.SMALL_PADDING),
                          Text(
                            AppLocalizations.of(context).confirm_choose_backup_warning,
                            overflow: TextOverflow.clip,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: Constants.STANDARD_PADDING),
                ...List.generate(
                  backupList.length,
                  (index) => InkWell(
                    child: Container(
                      decoration: BoxDecoration(
                          border: new Border(
                              bottom: const BorderSide(color: Colors.black26),
                              top: index == 0
                                  ? const BorderSide(color: Colors.black26)
                                  : const BorderSide(color: Colors.transparent))),
                      child: ListTile(
                        trailing: const Icon(Icons.north_west, size: 18),
                        visualDensity: VisualDensity.compact,
                        title: Text(
                          backupList[index].path.split("/").last.replaceAll("-climbing-tracker.json", ""),
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        onTap: () {
                          FocusScope.of(context).unfocus();
                          Navigator.pop(context);
                          onAccept(backupList[index].path);
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
              child: Text(
                AppLocalizations.of(context).general_cancel,
                style: TextStyle(color: AppColors.accentColor),
              ),
              style: TextButton.styleFrom(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Future<bool> showHeightUpdateDialog(BuildContext context) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button
      builder: (BuildContext context) {
        return AlertDialog(
          titleTextStyle: Theme.of(context).textTheme.subtitle1,
          contentTextStyle: Theme.of(context).textTheme.bodyText2,
          actionsPadding: const EdgeInsets.only(
            bottom: Constants.STANDARD_PADDING,
            left: Constants.STANDARD_PADDING,
            right: Constants.STANDARD_PADDING,
          ),
          title: Text(AppLocalizations.of(context).confirm_update_ascends),
          content: Text(AppLocalizations.of(context).confirm_update_ascends_info),
          actions: <Widget>[
            OutlinedButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: Text(
                AppLocalizations.of(context).general_no,
                style: TextStyle(color: AppColors.accentColor),
              ),
              style: OutlinedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
            ),
            OutlinedButton(
              onPressed: () => Navigator.of(context).pop(true),
              child: Text(
                AppLocalizations.of(context).general_yes,
                style: TextStyle(color: AppColors.accentColor),
              ),
              style: OutlinedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
