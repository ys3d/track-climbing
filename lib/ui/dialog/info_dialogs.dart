import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:url_launcher/url_launcher_string.dart';

class InfoDialog {
  Future<bool> showExistingRoutesDialog(BuildContext context, List<Ascend> existingAscents) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(AppLocalizations.of(context).existing_ascents_list(existingAscents.length)),
          titleTextStyle: Theme.of(context).textTheme.headline6,
          contentTextStyle: Theme.of(context).textTheme.bodyText1,
          content: SingleChildScrollView(
            child: _getExistingWarningAscendListing(context, existingAscents),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
              child: Text(AppLocalizations.of(context).general_back),
            )
          ],
        );
      },
    );
  }

  Future<bool> showAscendDialog(BuildContext context, Ascend ascend) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("${ascend.type.toTitle(context)}:"),
          titleTextStyle: Theme.of(context).textTheme.headline6,
          contentTextStyle: Theme.of(context).textTheme.bodyText1,
          content: SingleChildScrollView(
            child: Wrap(
              runSpacing: 10.0,
              children: _createRouteInfo(ascend, context),
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
              child: Text(AppLocalizations.of(context).general_back),
            )
          ],
        );
      },
    );
  }

  Future<void> showStyleInfoDialog(BuildContext context, ClimbingType type) async {
    String text = type == ClimbingType.BOULDER
        ? AppLocalizations.of(context).boulder_style(2)
        : AppLocalizations.of(context).climbing_styles(2);

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(text),
          titleTextStyle: Theme.of(context).textTheme.headline6,
          contentTextStyle: Theme.of(context).textTheme.bodyText1,
          content: SingleChildScrollView(
            child: Wrap(
              runSpacing: 10.0,
              children: type == ClimbingType.BOULDER ? _getBoulderStyleInfo(context) : getStyleInfo(context),
            ),
          ),
          contentPadding: const EdgeInsets.only(
            left: 24.0,
            right: 24.0,
            top: 20.0,
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context).general_back),
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> showLeadTopRopeInfoDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: true,
          contentTextStyle: Theme.of(context).textTheme.bodyText1,
          contentPadding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 20.0),
          content: SingleChildScrollView(
            child: Wrap(
              runSpacing: 10.0,
              children: getLeadTopInfo(context),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context).general_back),
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> showSpeedInfoDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          contentTextStyle: Theme.of(context).textTheme.bodyText1,
          contentPadding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 20.0),
          content: SingleChildScrollView(
            child: Wrap(
              runSpacing: 10.0,
              children: _getSpeedInfo(context),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context).general_back),
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> showBoulderRouteInfoDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: false,
          contentTextStyle: Theme.of(context).textTheme.bodyText1.copyWith(fontWeight: FontWeight.w400),
          contentPadding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 20.0),
          content: SingleChildScrollView(
            child: Wrap(
              runSpacing: 10.0,
              children: _getBoulderRouteInfo(context),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context).general_back),
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  List<Widget> getStyleInfo(BuildContext context) {
    return <Widget>[
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).style_onsight,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_onsight_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).style_flash,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_flash_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).style_redpoint,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_redpoint_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).style_af,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_af_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).style_hangdogging,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_hang_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          '3/4 | 1/2 | 1/4',
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_half_info),
    ];
  }

  List<Widget> _getBoulderStyleInfo(BuildContext context) {
    return <Widget>[
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).style_flash,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_flash_boulder_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).style_top,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_top_boulder_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).style_project,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_project_boulder_info),
    ];
  }

  List<Widget> getLeadTopInfo(BuildContext context) {
    return <Widget>[
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).style_lead,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_lead_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).style_toprope,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_toprope_info),
    ];
  }

  List<Widget> _getSpeedInfo(BuildContext context) {
    return <Widget>[
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).general_comp_route,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_speed_comp_route_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).general_normal_route,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_speed_normal_route_info),
    ];
  }

  List<Widget> _getBoulderRouteInfo(BuildContext context) {
    return <Widget>[
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).style_sport_climbing,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_sport_climbing_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).style_bouldering,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_bouldering_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).style_speed_climbing,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_speed_climbing_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).style_dws_climbing,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_dws_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).style_free_solo_climbing,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_free_soloing_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).style_ice_climbing,
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_ice_climbing_info),
    ];
  }

  List<Widget> _createRouteInfo(Ascend ascend, BuildContext context) {
    return <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "${AppLocalizations.of(context).general_location}:",
            style: TextStyle(fontWeight: FontWeight.w500),
          ),
          const SizedBox(width: Constants.STANDARD_PADDING),
          Flexible(
            child: Text(
              ascend.location,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.end,
            ),
          ),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "${AppLocalizations.of(context).general_date}:",
            style: TextStyle(fontWeight: FontWeight.w500),
          ),
          const SizedBox(width: Constants.STANDARD_PADDING),
          Flexible(
            child: Text("${DateFormat("dd.MM.yy").format(ascend.dateTime)}", overflow: TextOverflow.ellipsis),
          ),
        ],
      ),
      _name(ascend, context),
      ..._speed(ascend, context),
      _difficulty(ascend, context),
      ..._style(ascend, context),
      _height(ascend, context),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "${AppLocalizations.of(context).general_rating}:",
            style: TextStyle(fontWeight: FontWeight.w500),
          ),
          const SizedBox(width: 12.0),
          Flexible(child: Util.getRating(ascend.rating))
        ],
      ),
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "${AppLocalizations.of(context).general_comment}:",
            style: TextStyle(fontWeight: FontWeight.w500),
          ),
          const SizedBox(width: Constants.STANDARD_PADDING),
          Flexible(
            child: Text(
              Util.getComment(ascend.comment),
              overflow: TextOverflow.clip,
              textAlign: TextAlign.end,
            ),
          ),
        ],
      ),
    ];
  }

  Widget _name(Ascend ascend, BuildContext context) {
    return Visibility(
      visible: !(ascend.type == ClimbingType.SPEED_CLIMBING && ascend.speedType == 0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "${AppLocalizations.of(context).general_name}:",
            style: TextStyle(fontWeight: FontWeight.w500),
          ),
          const SizedBox(width: Constants.STANDARD_PADDING),
          Flexible(
            child: Text(
              Util.getAscendNameWithoutPrefix(
                ascend.name,
                context,
                speedType: ascend.speedType,
                type: ascend.type,
              ),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.end,
            ),
          )
        ],
      ),
    );
  }

  Widget _difficulty(Ascend ascend, BuildContext context) {
    return Visibility(
      visible: !(ascend.type == ClimbingType.SPEED_CLIMBING && ascend.speedType == 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text("${AppLocalizations.of(context).general_difficulty}:", style: TextStyle(fontWeight: FontWeight.w500)),
          const SizedBox(width: 4.0),
          Spacer(flex: 1),
          Flexible(
            flex: 2,
            child: Text(Util.getGrade(ascend.gradeID, ascend.type), textAlign: TextAlign.end),
          ),
        ],
      ),
    );
  }

  List<Widget> _style(Ascend ascend, BuildContext context) {
    String first = "";
    String second = "";

    switch (ascend.type) {
      case ClimbingType.SPEED_CLIMBING:
        if (ascend.speedType == 0) return [];
        first = Util.getTopOrLead(ascend.toprope, context);
        break;
      case ClimbingType.FREE_SOLO:
        return [];
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.SPORT_CLIMBING:
        first = Util.getStyle(ascend.styleID, context, tries: ascend.tries);
        second = Util.getTopOrLead(ascend.toprope, context);
        break;
      case ClimbingType.DEEP_WATER_SOLO:
      case ClimbingType.BOULDER:
      default:
        first = Util.getStyle(ascend.styleID, context, tries: ascend.tries);
    }

    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text("${AppLocalizations.of(context).style}:", style: TextStyle(fontWeight: FontWeight.w500)),
          Text(first),
        ],
      ),
      if (second != "")
        Align(
          alignment: Alignment.centerRight,
          child: Text(second),
        ),
    ];
  }

  List<Widget> _speed(Ascend ascend, BuildContext context) {
    return [
      Visibility(
        visible: ascend.type == ClimbingType.SPEED_CLIMBING,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("${AppLocalizations.of(context).general_time}:", style: TextStyle(fontWeight: FontWeight.w500)),
            Text("${(ascend.speedTime / 1000).toStringAsFixed(2)} ${AppLocalizations.of(context).util_seconds}"),
          ],
        ),
      ),
      Visibility(
        visible: ascend.type == ClimbingType.SPEED_CLIMBING,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "${AppLocalizations.of(context).general_type}:",
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            Text(
              ascend.speedType == 0
                  ? AppLocalizations.of(context).general_comp_route
                  : AppLocalizations.of(context).general_normal_route,
            ),
          ],
        ),
      ),
    ];
  }

  Widget _height(Ascend ascend, BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          "${AppLocalizations.of(context).general_height}:",
          style: TextStyle(fontWeight: FontWeight.w500),
        ),
        Text(Util.getHeightWithShortUnit(ascend.height))
      ],
    );
  }

  Future<void> showIceGradingInfo(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: false,
          contentTextStyle: Theme.of(context).textTheme.bodyText1,
          contentPadding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 20.0),
          content: SingleChildScrollView(
            child: Wrap(
              runSpacing: 10.0,
              children: _getIceGradingInfo(context),
            ),
          ),
          actions: <Widget>[
            TextButton(
                child: Text(AppLocalizations.of(context).general_back),
                onPressed: () {
                  FocusScope.of(context).unfocus();
                  Navigator.of(context).pop();
                })
          ],
        );
      },
    );
  }

  List<Widget> _getIceGradingInfo(BuildContext context) {
    return <Widget>[
      Text(
        AppLocalizations.of(context).info_dialog_further_information,
        style: Theme.of(context).textTheme.headline6,
      ),
      SelectableLinkify(
        onOpen: (link) async {
          if (await canLaunchUrlString(link.url)) {
            await launchUrlString(link.url);
          } else {
            throw 'Could not launch $link';
          }
        },
        options: LinkifyOptions(humanize: false),
        text: "https://ascentionism.com/ice-climbing-grades-a-complete-guide",
      ),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'WI-1:',
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_ice_climbing_wi1),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'WI-2:',
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_ice_climbing_wi2),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'WI-3:',
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_ice_climbing_wi3),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'WI-4:',
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_ice_climbing_wi4),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'WI-5:',
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_ice_climbing_wi5),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'WI-6:',
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_ice_climbing_wi6),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'WI-7:',
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_ice_climbing_wi7),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'WI-8:',
          style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context).info_dialog_ice_climbing_wi8),
    ];
  }

  Widget _getExistingWarningAscendListing(BuildContext context, List<Ascend> existingAscents) {
    String languageCode = Localizations.localeOf(context).languageCode;
    List<Widget> children = [];

    for (Ascend ascend in existingAscents) {
      children.add(
        Text(
          "• ${DateFormat.yMMMd(languageCode).format(ascend.dateTime)}:",
          style: Theme.of(context).textTheme.bodyText2,
        ),
      );
      children.add(const SizedBox(width: 8.0));
      children.add(
        Padding(
          padding: const EdgeInsets.only(left: 12.0),
          child: Text(
            Util.getAscendNameWithoutPrefix(
              ascend.name,
              context,
              speedType: ascend.speedType,
              type: ascend.type,
            ),
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
      );
      children.add(
        Padding(
          padding: const EdgeInsets.only(left: 12.0),
          child: Text(
            "(${Util.getAscendSummary(ascend, context)})",
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
      );
      children.add(const SizedBox(height: 10.0));
    }
    return Column(children: children, crossAxisAlignment: CrossAxisAlignment.start);
  }
}
