import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:climbing_track/util/notifications_handler.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class EndSessionDialog extends StatefulWidget {
  @override
  _EndSessionDialogState createState() => _EndSessionDialogState(DateTime.now());
}

class _EndSessionDialogState extends State<EndSessionDialog> {
  DateTime endTime;

  _EndSessionDialogState(this.endTime);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      titleTextStyle: Theme.of(context).textTheme.subtitle1,
      content: createContent(context),
      contentTextStyle: Theme.of(context).textTheme.bodyText1,
      actions: <Widget>[
        OutlinedButton(
          onPressed: () {
            try {
              NotificationHandler().scheduleReminderNotification(
                context,
                startTime: Data().getCurrentSession().startDate,
              );
            } on Exception catch (_) {}
            Navigator.of(context).pop(false);
          },
          child: Text(AppLocalizations.of(context).general_cancel),
          style: OutlinedButton.styleFrom(
            foregroundColor: AppColors.accentColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
        ),
        TextButton(
          style: TextButton.styleFrom(
            padding: EdgeInsets.symmetric(horizontal: 28, vertical: 8),
            backgroundColor: AppColors.accentColor,
            foregroundColor: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
          onPressed: () {
            NotificationHandler().cancelNotification();
            if (Data().getCurrentSession().routeCount <= 0) {
              Data().deleteCurrentSession();
              SessionState().setState(SessionStateEnum.none);
              Navigator.of(context).pop(false);
            } else {
              Data().endSession(
                endTime,
                failNotificationTitle: AppLocalizations.of(context).notification_drive_backup_failed_title,
                failNotificationBody: AppLocalizations.of(context).notification_drive_backup_failed_body,
              );
              SessionState().nextState();
              Navigator.of(context).pop(true);
            }
          },
          child: Data().getCurrentSession().routeCount <= 0
              ? Text(AppLocalizations.of(context).general_delete)
              : Text(AppLocalizations.of(context).general_end),
        ),
      ],
    );
  }

  Widget createContent(BuildContext context) {
    Duration diff = endTime.difference(Data().getCurrentSession().startDate);
    if (Data().getCurrentSession().routeCount <= 0) {
      return Text(AppLocalizations.of(context).end_session_dialog_no_route_added);
    } else {
      return Wrap(
        runSpacing: 14.0,
        children: [
          Align(
            alignment: Alignment.center,
            child: Text(
              "${AppLocalizations.of(context).session_time}:",
              style: Theme.of(context).textTheme.headline5.copyWith(color: Colors.black),
            ),
          ),
          InkWell(
            onTap: () {
              showTimePicker(
                builder: (BuildContext context, Widget child) {
                  return MediaQuery(
                    data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
                    child: child,
                  );
                },
                helpText: AppLocalizations.of(context).end_session_dialog_change_duration,
                context: context,
                initialTime: TimeOfDay(
                  hour: diff.inHours,
                  minute: diff.inMinutes % 60,
                ),
              ).then(
                (time) {
                  if (time != null) {
                    endTime = Data().getCurrentSession().startDate.add(
                          Duration(hours: time.hour, minutes: time.minute),
                        );
                    setState(() {});
                  }
                },
              );
            },
            child: Stack(
              children: [
                const Align(alignment: Alignment.centerRight, child: Icon(Icons.more_time, color: Colors.black54)),
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    "${Data().getCurrentSession().getDurationD(endTime)} h",
                    style: Theme.of(context).textTheme.headline5.copyWith(
                          color: AppColors.accentColor,
                          height: 0.0,
                        ),
                  ),
                ),
              ],
            ),
          ),
        ],
      );
    }
  }
}
