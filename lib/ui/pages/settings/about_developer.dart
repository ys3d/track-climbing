/*
 *  Created by Lukas on 30.07.22, 12:03
 *  Copyright (c) 2022 . All rights reserved.
 */

import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:climbing_track/ui/pages/settings/donation_page.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:store_redirect/store_redirect.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AboutPage extends StatelessWidget {
  final String versionString;

  const AboutPage({Key key, @required this.versionString}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            child: _body(context),
          ),
        ),
      ),
    );
  }

  Widget _body(BuildContext context) {
    return Column(
      children: [
        Container(
          alignment: Alignment.topLeft,
          padding: const EdgeInsets.only(left: 10.0, top: 10.0),
          child: IconButton(
            tooltip: AppLocalizations.of(context).general_back,
            onPressed: () => Navigator.of(context).pop(),
            icon: const Icon(Icons.arrow_back, size: 28),
          ),
        ),
        _appName(context),
        const SizedBox(height: 40.0),
        if (Platform.isAndroid) _donationWidget(context),
        _feedBack(context),
        _developer(context),
        _credits(context),
        const SizedBox(height: 60),
      ],
    );
  }

  Widget _appName(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: 120,
          height: 120,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(32.0),
            child: Image.asset('assets/icon/ios_icon.png'),
          ),
        ),
        const SizedBox(height: 20.0),
        Center(
          child: Text(
            AppLocalizations.of(context).main_title,
            style: Theme.of(context).textTheme.headline3?.copyWith(
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                  letterSpacing: -1.0,
                ),
          ),
        ),
        Text(
          "${AppLocalizations.of(context).screen_about_version}: $versionString",
          style: Theme.of(context).textTheme.bodyText2,
        ),
      ],
    );
  }

  Widget _container(Widget containerBody) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 32.0),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black54),
        borderRadius: const BorderRadius.all(Radius.circular(32)),
      ),
      child: containerBody,
    );
  }

  Widget _containerBody({
    @required BuildContext context,
    @required List<Widget> innerText,
    Widget primaryButton,
    Widget secondaryButton,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ...innerText,
        if (primaryButton != null || secondaryButton != null) const SizedBox(height: 20.0),
        if (secondaryButton != null) secondaryButton,
        if (primaryButton != null) primaryButton,
      ],
    );
  }

  List<Widget> _containerBodyInnerText({
    @required BuildContext context,
    @required String title,
    @required String text,
  }) {
    return [
      Text(
        title,
        overflow: TextOverflow.clip,
        style: Theme.of(context).textTheme.headline6?.copyWith(fontWeight: FontWeight.w500),
      ),
      const SizedBox(height: 12.0),
      Text(
        text,
        style: Theme.of(context).textTheme.bodyText1?.copyWith(height: 1.3),
        overflow: TextOverflow.clip,
      ),
    ];
  }

  Widget _primaryButton({
    @required BuildContext context,
    @required String text,
    @required IconData icon,
    @required VoidCallback onPressed,
  }) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.red.shade300,
          padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 16.0),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Text(
                text,
                style: const TextStyle(color: Colors.white),
              ),
            ),
            const SizedBox(width: 12.0),
            Icon(icon, color: Colors.white),
          ],
        ),
      ),
    );
  }

  Widget _secondaryButton({
    @required BuildContext context,
    @required String text,
    @required IconData icon,
    @required VoidCallback onPressed,
  }) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: TextButton(
          onPressed: onPressed,
          style: TextButton.styleFrom(
            foregroundColor: Colors.red.shade300,
            padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 16.0),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(child: Text(text)),
              const SizedBox(width: 12.0),
              Icon(icon),
            ],
          ),
        ),
      ),
    );
  }

  Widget _donationWidget(BuildContext context) {
    return _container(
      _containerBody(
        context: context,
        innerText: _containerBodyInnerText(
          context: context,
          title: AppLocalizations.of(context).screen_about_support,
          text: AppLocalizations.of(context).screen_about_support_info,
        ),
        primaryButton: _primaryButton(
          context: context,
          text: AppLocalizations.of(context).screen_about_support_button,
          icon: Icons.favorite,
          onPressed: () => _donate(context),
        ),
      ),
    );
  }

  Widget _feedBack(BuildContext context) {
    return _container(
      _containerBody(
        context: context,
        innerText: _containerBodyInnerText(
          context: context,
          title: AppLocalizations.of(context).screen_about_feedback,
          text: AppLocalizations.of(context).screen_about_feedback_info,
        ),
        secondaryButton: _secondaryButton(
          context: context,
          text: AppLocalizations.of(context).screen_about_feedback_button_contact,
          icon: Icons.mail_outline,
          onPressed: _sendFeedBack,
        ),
        primaryButton: _primaryButton(
          context: context,
          text: AppLocalizations.of(context).screen_about_feedback_button_rate,
          icon: Icons.star_outline_sharp,
          onPressed: () => StoreRedirect.redirect(),
        ),
      ),
    );
  }

  Widget _developer(BuildContext context) {
    return _container(
      _containerBody(
        context: context,
        innerText: [
          Text(
            AppLocalizations.of(context).screen_about_dev_developed,
            style: Theme.of(context).textTheme.bodyText1?.copyWith(
                  height: 1.3,
                  fontSize: 14,
                  fontStyle: FontStyle.italic,
                ),
            overflow: TextOverflow.clip,
          ),
          const SizedBox(height: 16.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Image.asset(
                  "assets/images/profile.png",
                  width: 70.0,
                  height: 70.0,
                  fit: BoxFit.cover,
                ),
              ),
              const SizedBox(width: 14.0),
              Flexible(
                child: AutoSizeText(
                  AppLocalizations.of(context).screen_about_dev_name,
                  overflow: TextOverflow.clip,
                  maxLines: 2,
                  style: Theme.of(context).textTheme.headline6?.copyWith(
                        fontWeight: FontWeight.w500,
                        fontSize: 32,
                        height: 0.95,
                      ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 16.0),
          Text(
            AppLocalizations.of(context).screen_about_dev_info,
            style: Theme.of(context).textTheme.bodyText1?.copyWith(height: 1.3),
            overflow: TextOverflow.clip,
            maxLines: 10,
          ),
        ],
        primaryButton: _primaryButton(
          context: context,
          text: AppLocalizations.of(context).screen_about_dev_button_other_apps,
          icon: Icons.open_in_new,
          onPressed: _launchDeveloperPage,
        ),
      ),
    );
  }

  Widget _credits(BuildContext context) {
    return _container(
      _containerBody(
        context: context,
        innerText: [
          Text(
            AppLocalizations.of(context).screen_about_credits,
            overflow: TextOverflow.clip,
            style: Theme.of(context).textTheme.headline6?.copyWith(fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 12.0),
          SelectableText.rich(
            TextSpan(
              children: [
                _normalText(context: context, text: "${AppLocalizations.of(context).screen_about_credits_flaticon} ("),
                _hyperLinkText(context: context, text: "www.flaticon.com", url: "https://www.flaticon.com/"),
                _normalText(context: context, text: "):\n\n"),
                _normalText(context: context, text: "- "),
                _hyperLinkText(
                  context: context,
                  text: "pack: vacation-planning-adventure-1\n",
                  url: "https://www.flaticon.com/packs/vacation-planning-adventure-1",
                ),
                _normalText(context: context, text: "- "),
                _hyperLinkText(
                  context: context,
                  text: "pack: free-time-32\n",
                  url: "https://www.flaticon.com/packs/free-time-32",
                ),
                _normalText(context: context, text: "- "),
                _hyperLinkText(
                  context: context,
                  text: "pack: big-data-98\n",
                  url: "https://www.flaticon.com/packs/big-data-98",
                ),
                _normalText(context: context, text: "- "),
                _hyperLinkText(
                  context: context,
                  text: "pack: internet-marketing-strategy-1\n",
                  url: "https://www.flaticon.com/de/packs/internet-marketing-strategy-1",
                ),
                _normalText(context: context, text: "- "),
                _hyperLinkText(
                  context: context,
                  text: "pack: quiz-7\n",
                  url: "https://www.flaticon.com/packs/quiz-7",
                ),
                _normalText(context: context, text: "- "),
                _hyperLinkText(
                  context: context,
                  text: "pack: material-actions\n",
                  url: "https://www.flaticon.com/packs/material-actions",
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  TextSpan _normalText({@required BuildContext context, @required String text}) {
    return TextSpan(
      style: Theme.of(context).textTheme.bodyText1?.copyWith(height: 1.3),
      text: text,
    );
  }

  TextSpan _hyperLinkText({@required BuildContext context, @required String text, @required String url}) {
    return TextSpan(
      style: Theme.of(context).textTheme.bodyText1?.copyWith(
            height: 1.3,
            color: Theme.of(context).colorScheme.primary,
          ),
      text: text,
      recognizer: TapGestureRecognizer()
        ..onTap = () async {
          if (await canLaunchUrlString(url)) {
            await launchUrlString(url);
          }
        },
    );
  }

  Future<void> _sendFeedBack() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    final Email email = Email(
      body: "device: ${androidInfo.model}\n"
          "app_version: ${packageInfo.version} (${packageInfo.buildNumber})\n"
          "sdk_version: ${androidInfo.version.sdkInt}\n\n\n"
          "Hey Lukas,\n",
      recipients: ['lukas.freudenmann+climbing-tracker@gmail.com'],
      isHTML: false,
    );
    await FlutterEmailSender.send(email);
  }

  _launchDeveloperPage() async {
    const url = 'https://play.google.com/store/apps/dev?id=4830015395735899289';
    if (await canLaunchUrlString(url)) {
      await launchUrlString(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void _donate(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => DonationPage()),
    );
  }
}
