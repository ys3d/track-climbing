import 'dart:async';

import 'package:climbing_track/app/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DonationPage extends StatefulWidget {
  @override
  _DonationPageState createState() => _DonationPageState();
}

class _DonationPageState extends State<DonationPage> {
  StreamSubscription _purchaseUpdatedSubscription;
  StreamSubscription _purchaseErrorSubscription;
  StreamSubscription _connectionSubscription;

  final List<String> _productIds = [
    "1_euro_donation",
    "2_euro_donation",
    "5_euro_donation",
    "10_euro_donation",
    "20_euro_donation",
    "50_euro_donation"
  ];
  final List<String> _productNames = ["1€", "2€", "5€", "10€", "20€", "50€"];

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  @override
  void dispose() {
    FlutterInappPurchase.instance.finalize();
    if (_connectionSubscription != null) {
      _connectionSubscription.cancel();
      _connectionSubscription = null;
      _purchaseErrorSubscription.cancel();
      _purchaseErrorSubscription = null;
      _purchaseUpdatedSubscription.cancel();
      _purchaseUpdatedSubscription = null;
    }
    super.dispose();
  }

  Future<void> initPlatformState() async {
    // prepare
    var result = await FlutterInappPurchase.instance.initialize();
    print('result: $result');

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    // refresh items for android
    try {
      String msg = await FlutterInappPurchase.instance.consumeAll();
      print('consumeAllItems: $msg');
    } catch (err) {
      print('consumeAllItems error: $err');
    }

    _connectionSubscription = FlutterInappPurchase.connectionUpdated.listen((connected) {
      print('connected: $connected');
    });

    _purchaseUpdatedSubscription = FlutterInappPurchase.purchaseUpdated.listen((productItem) {
      FlutterInappPurchase.instance.finishTransaction(productItem);
      print('purchase-updated: $productItem');
    });

    _purchaseErrorSubscription = FlutterInappPurchase.purchaseError.listen((purchaseError) {
      print('purchase-error: $purchaseError');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.accentColor,
        leading: const BackButton(color: Colors.white),
        title: Text(
          AppLocalizations.of(context).settings_donation_support,
          style: const TextStyle(color: Colors.white, fontSize: 22),
        ),
      ),
      body: SingleChildScrollView(child: _createPage()),
    );
  }

  Widget _createPage() {
    return Container(
      padding: const EdgeInsets.fromLTRB(20, 20, 20, 60),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              AppLocalizations.of(context).settings_donation_support_why,
              style: Theme.of(context).textTheme.subtitle1.copyWith(color: Colors.blue),
            ),
            const SizedBox(height: 10),
            Text(
              AppLocalizations.of(context).settings_donation_support_info,
              overflow: TextOverflow.clip,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
            ),
            const SizedBox(height: 10),
            const Divider(thickness: 2.0),
            const SizedBox(height: 10),
            Text(
              AppLocalizations.of(context).settings_donation_donate,
              style: Theme.of(context).textTheme.subtitle1.copyWith(color: Colors.blue),
            ),
            const SizedBox(height: 10),
            Text(
              AppLocalizations.of(context).settings_donation_donate_info,
              overflow: TextOverflow.clip,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
            ),
            const SizedBox(height: 24),
            Align(
              alignment: Alignment.topCenter,
              child: _createDonateButtons(),
            )
          ],
        ),
      ),
    );
  }

  Widget _createDonateButtons() {
    return Wrap(
      alignment: WrapAlignment.center,
      runSpacing: 24,
      spacing: 20,
      children: List.generate(
        _productIds.length,
        (index) => TextButton(
          onPressed: () async {
            try {
              List<IAPItem> items = await FlutterInappPurchase.instance.getProducts(_productIds);

              FlutterInappPurchase.instance
                  .requestPurchase(items.firstWhere((element) => element.productId == _productIds[index]).productId);
            } catch (exception) {
              showWarningScreen(context);
            }
          },
          style: TextButton.styleFrom(
            padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 32.0),
            backgroundColor: Colors.grey[300],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(48.0),
            ),
          ),
          child: Text(
            _productNames[index],
            style: TextStyle(fontSize: 18),
          ),
        ),
      ),
    );
  }

  Future<bool> showWarningScreen(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: true,
          contentPadding: const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 24.0),
          titleTextStyle: Theme.of(context).textTheme.subtitle1,
          title: Text(AppLocalizations.of(context).settings_donation_donate_warning),
          content: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Icon(Icons.warning_amber_rounded, color: Colors.orange, size: 48),
              const SizedBox(width: 14.0),
              Flexible(
                child: Text(
                  AppLocalizations.of(context).settings_donation_donate_warning_info,
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
                  overflow: TextOverflow.clip,
                ),
              ),
            ],
          ),
          contentTextStyle: Theme.of(context).textTheme.bodyText2.copyWith(fontWeight: FontWeight.w400),
          actions: <Widget>[
            TextButton(
              onPressed: () => {FocusScope.of(context).unfocus(), Navigator.pop(context)},
              child: Text(AppLocalizations.of(context).general_back),
            ),
          ],
        );
      },
    );
  }
}
