import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/pages/statistics/chart_edit_page.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/average_boulder_grade_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/boulder_grades_distribution_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/timeline_boulder_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/average_climbing_grade_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/best_grades_boulder.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/best_grades_climbing.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/boulder_last_year_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/climbing_grades_distribution_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/climbing_last_year_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/styles_climbing.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/timeline_climbing.dart';
import 'package:climbing_track/ui/pages/statistics/factory/dws/average_dws_grade_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/dws/best_grades_dws.dart';
import 'package:climbing_track/ui/pages/statistics/factory/dws/dws_grades_distribution_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/dws/dws_last_year_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/dws/styles_dws_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/lead_toprope_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/dws/timeline_dws_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/general/heatmap_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/general/height_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/general/location_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/general/route_count.dart';
import 'package:climbing_track/ui/pages/statistics/factory/general/session_time.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/styles_boulder_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/average_ice_grade_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/best_grades_ice_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/ice_grades_distribution_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/ice_last_year_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/lead_toprope_ice_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/styles_ice_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/timeline_ice_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/solo/average_solo_grade_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/solo/best_grades_solo.dart';
import 'package:climbing_track/ui/pages/statistics/factory/solo/solo_grades_distribution_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/solo/solo_last_year_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/solo/timeline_solo_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/speed/best_speed_times.dart';
import 'package:climbing_track/ui/pages/statistics/factory/speed/speed_last_year_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/speed/timeline_speed_chart.dart';
import 'package:climbing_track/ui/widgets/empty_list_placeholder.dart';
import 'package:flutter/material.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ChartPage extends StatefulWidget {
  ChartPage({Key key}) : super(key: key);

  @override
  _ChartPageState createState() => _ChartPageState();

  //shared prefs parameter
  static const String ORDER_KEY = "chart_order";
}

class _ChartPageState extends State<ChartPage> {
  final List<AbstractChart> _chartList = [
    BestGradesClimbingChart(),
    ClimbingStyleChart(),
    RouteCountChart(),
    LeadTopropeChart(),
    SessionTimeChart(),
    LocationsChart(),
    AverageClimbingGradeChart(),
    LastYearClimbingChart(),
    ClimbingGradesDistributionChart(),
    HeightChart(),
    TimelineClimbingChart(),
    HeatMapChart(),

    // Boulder
    BestGradesBoulderChart(),
    BoulderStyleChart(),
    LastYearBoulderChart(),
    BoulderGradesDistributionChart(),
    AverageBoulderGradeChart(),
    TimelineBoulderChart(),

    // SPEED
    BestSpeedTimes(),
    LastYearSpeedChart(),
    TimelineSpeedChart(),

    // DWS
    BestGradesDwsChart(),
    DwsStylesChart(),
    AverageDwsGradeChart(),
    LastYearDwsChart(),
    DwsGradesDistributionChart(),
    TimelineDWSChart(),

    // ICE
    BestGradesIceChart(),
    IceStylesChart(),
    LeadTopropeIceChart(),
    AverageIceGradeChart(),
    LastYearIceChart(),
    IceGradesDistributionChart(),
    TimelineIceChart(),

    //SOLO
    BestGradesFreeSoloChart(),
    AverageFreeSoloGradeChart(),
    LastYearFreeSoloChart(),
    SoloGradesDistributionChart(),
    TimelineSoloChart(),
  ];

  Future futureData;
  ChartType _currentChartType = ChartType.ALL;

  @override
  void initState() {
    super.initState();
    this.futureData = _loadData();
  }

  @override
  Widget build(BuildContext context) {
    final List<int> orderList = [];

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: AppColors.accentColor,
        title: _getTitle(),
        actions: [
          if (_currentChartType == ChartType.ALL)
            IconButton(
              color: Colors.white,
              tooltip: AppLocalizations.of(context).general_edit,
              icon: Icon(Icons.edit),
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute<void>(
                  builder: (BuildContext context) => ChartEditPage(
                    elements: _chartList,
                    order: orderList,
                  ),
                ),
              ).then((value) => setState(() {
                    this.futureData = _loadData(wait: 0);
                  })),
            ),
        ],
      ),
      body: FutureBuilder(
        future: this.futureData,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const LinearProgressIndicator(),
                EmptyListPlaceHolder(
                  text: AppLocalizations.of(context).chart_page_calc_statistics,
                  image: "assets/images/2318736.png",
                ),
                const SizedBox(height: 1.0),
              ],
            );
          }

          SharedPreferences prefs = snapshot.data;

          String order = prefs.getString(ChartPage.ORDER_KEY) ?? buildStandardOrder();

          orderList.clear();
          orderList.addAll(order.split("_").map(int.parse));
          if (orderList.length != _chartList.length) {
            prefs.remove(ChartPage.ORDER_KEY);
            order = buildStandardOrder();
            orderList.clear();
            orderList.addAll(order.split("_").map(int.parse));
          }

          AbstractChart.loadYears(prefs);
          return LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              if (Data().sessionCount < 3) return _createNoDataWarning();
              if (!_chartList.any((element) => element.visible) && _currentChartType == ChartType.ALL) {
                return _createNoChartSelectedWarning();
              }
              return _createBody(context, orderList);
            },
          );
        },
      ),
    );
  }

  Widget _getTitle() {
    return Center(
      child: DropdownButton(
        borderRadius: BorderRadius.circular(20.0),
        isExpanded: true,
        alignment: Alignment.center,
        iconEnabledColor: Colors.white,
        dropdownColor: AppColors.accentColor,
        underline: Container(),
        style: const TextStyle(fontSize: 24, color: Colors.white, fontWeight: FontWeight.w600),
        onChanged: (ChartType chart) => setState(() {
          this._currentChartType = chart;
          this.futureData = _loadData(wait: 0);
        }),
        value: _currentChartType,
        items: ChartType.values.map((ChartType item) {
          return DropdownMenuItem<ChartType>(
            alignment: Alignment.center,
            child: Text(
              Util.replaceStringWithBlankSpaces(getNameMapping(context, item)),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.bodyText2.copyWith(
                    color: Colors.white,
                    fontSize: 22,
                  ),
            ),
            value: item,
          );
        }).toList(),
      ),
    );
  }

  Widget _createNoDataWarning() {
    return EmptyListPlaceHolder(
      text: AppLocalizations.of(context).chart_page_no_data,
      image: "assets/images/2920349.png",
    );
  }

  Widget _createNoChartSelectedWarning() {
    return EmptyListPlaceHolder(
      text: AppLocalizations.of(context).chart_page_no_selected_chart,
      image: "assets/images/2920349.png",
    );
  }

  Widget _createBody(BuildContext context, List<int> indices) {
    List<Widget> list = _chartListWithContainer(indices);
    return ScrollablePositionedList.builder(
      itemCount: list.length,
      itemBuilder: (BuildContext context, int index) => list[index],
    );
  }

  List<Widget> _chartListWithContainer(List<int> indices) {
    List<Widget> list;
    if (_currentChartType == ChartType.ALL) {
      list = List.generate(
        indices.length,
        (index) => _chartList[indices[index]].getWidget(context),
      );
    } else {
      list = _chartList
          .where((element) => element.getType() == _currentChartType)
          .map((e) => e.getWidget(context, force: true))
          .toList();
    }
    list.add(const SizedBox(height: 8.0));
    return list;
  }

  Future<SharedPreferences> _loadData({int wait = 600}) async {
    for (AbstractChart chartFactory in _chartList) {
      chartFactory.init(force: _currentChartType != ChartType.ALL);
    }
    Future<SharedPreferences> prefs = SharedPreferences.getInstance();
    await Future.delayed(Duration(milliseconds: wait)); //to have no white screen effect
    return prefs;
  }

  String buildStandardOrder() {
    return List<String>.generate(_chartList.length, (index) => "$index").join("_");
  }

  String getNameMapping(BuildContext context, ChartType type) {
    return {
      ChartType.ALL: AppLocalizations.of(context).chart_page_overview,
      ChartType.SPORT_CLIMBING: AppLocalizations.of(context).style_sport_climbing,
      ChartType.BOULDER: AppLocalizations.of(context).style_bouldering,
      ChartType.SPEED_CLIMBING: AppLocalizations.of(context).style_speed_climbing,
      ChartType.FREE_SOLO: AppLocalizations.of(context).style_free_solo_climbing,
      ChartType.DEEP_WATER_SOLO: AppLocalizations.of(context).style_dws_climbing,
      ChartType.ICE_CLIMBING: AppLocalizations.of(context).style_ice_climbing,
    }[type];
  }
}
