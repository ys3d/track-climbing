import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ExpandedChart extends StatefulWidget {
  const ExpandedChart({
    Key key,
    this.expanded,
    this.title,
    this.onChangeSelection,
    this.appBarActions = const [],
  }) : super(key: key);

  final AbstractChart expanded;
  final List<String> title;
  final Function(String) onChangeSelection;
  final List<Widget> appBarActions;

  @override
  _ExpandedChartState createState() => _ExpandedChartState();
}

class _ExpandedChartState extends State<ExpandedChart> {
  String currentValue;

  @override
  void initState() {
    super.initState();
    currentValue = widget.title.first;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.accentColor,
        leading: const BackButton(color: Colors.white),
        title: widget.title.length > 1
            ? _titleList()
            : Tooltip(
                message: widget.title.first,
                child: Text(
                  widget.title.first,
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                        color: Colors.white,
                        overflow: TextOverflow.ellipsis,
                      ),
                ),
              ),
        actions: widget.appBarActions,
      ),
      body: Column(
        children: [
          if (widget.expanded.leadCheckBoxActivated() ||
              widget.expanded.topropeCheckBoxActivated() ||
              widget.expanded.indoorCheckBoxActivated() ||
              widget.expanded.outdoorCheckBoxActivated()) ...[
            _checkBoxSettings(context),
            const Divider(thickness: 1.5, height: 1.5),
          ],
          Flexible(
            child: widget.expanded.getExpandedWidget(),
          ),
        ],
      ),
    );
  }

  Widget _titleList() {
    return Padding(
      padding: const EdgeInsets.only(top: 4.0),
      child: DropdownButton(
        borderRadius: BorderRadius.circular(20.0),
        isExpanded: true,
        alignment: Alignment.centerLeft,
        iconEnabledColor: Colors.white,
        dropdownColor: AppColors.accentColor,
        value: this.currentValue,
        underline: Container(),
        style: const TextStyle(fontSize: 24, color: Colors.white, fontWeight: FontWeight.w600),
        onChanged: (value) => setState(() {
          widget.onChangeSelection(value);
          this.currentValue = value;
        }),
        items: widget.title.map((String item) {
          return DropdownMenuItem<String>(
            child: Padding(
              padding: EdgeInsets.only(right: 8.0),
              child: Text(
                item,
                overflow: TextOverflow.clip,
                maxLines: 2,
                style: Theme.of(context).textTheme.bodyText1.copyWith(color: Colors.white),
              ),
            ),
            value: item,
          );
        }).toList(),
      ),
    );
  }

  Widget _checkBoxSettings(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 6.0),
      child: Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        direction: Axis.horizontal,
        spacing: 4.0,
        runSpacing: 0.0,

        children: [
          if (widget.expanded.topropeCheckBoxActivated()) ...[
            _checkBox(
              text: AppLocalizations.of(context).style_toprope,
              value: widget.expanded.toprope,
              onChanged: (val) {
                widget.expanded.toprope = val;
                widget.expanded.onCheckboxSelectionChanged();
                setState(() {});
              },
            ),
            _verticalDivider(),
          ],
          if (widget.expanded.leadCheckBoxActivated()) ...[
            _checkBox(
              text: AppLocalizations.of(context).style_lead,
              value: widget.expanded.lead,
              onChanged: (val) {
                widget.expanded.lead = val;
                widget.expanded.onCheckboxSelectionChanged();
                setState(() {});
              },
            ),
            _verticalDivider(),
          ],
          if (widget.expanded.indoorCheckBoxActivated()) ...[
            _checkBox(
              text: AppLocalizations.of(context).general_indoor,
              value: widget.expanded.indoor,
              onChanged: (val) {
                widget.expanded.indoor = val;
                widget.expanded.onCheckboxSelectionChanged();
                setState(() {});
              },
            ),
            _verticalDivider(),
          ],
          if (widget.expanded.outdoorCheckBoxActivated()) ...[
            _checkBox(
              text: AppLocalizations.of(context).general_outdoor,
              value: widget.expanded.outdoor,
              onChanged: (val) {
                widget.expanded.outdoor = val;
                widget.expanded.onCheckboxSelectionChanged();
                setState(() {});
              },
            ),
          ],
        ],
      ),
    );
  }

  Widget _checkBox({@required String text, @required bool value, @required ValueChanged<bool> onChanged}) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Checkbox(
          activeColor: Colors.blue,
          value: value,
          onChanged: onChanged,
          visualDensity: VisualDensity.compact,
        ),
        Text(text),
      ],
    );
  }

  Widget _verticalDivider() {
    return Container(
      margin: const EdgeInsets.only(left: 4.0),
      height: 20.0,
      width: 0.0,
      decoration: const BoxDecoration(
        border: Border.symmetric(vertical: BorderSide(color: Colors.black87, width: 0.5)),
      ),
    );
  }
}
