import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/style_mapper.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/styles_climbing.dart';
import 'package:flutter/material.dart';

class DwsStylesChart extends ClimbingStyleChart {
  @override
  dynamic calculateData(BuildContext context, input) {
    // Filter data
    List<Map<String, dynamic>> data = filterData(input);

    // return if no data is left
    if (data.isEmpty) return [];

    final StyleMapper _styleMapper = StyleMapper();
    Map<String, double> styles = Map();
    _styleMapper.getDeepWaterMapping(context).forEach((value) {
      styles[value.style] = 0;
    });

    for (Map<String, dynamic> route in data) {
      styles[_styleMapper.getStyleFromId(route[DBController.ascendStyleId], context)] += 1;
    }

    return styles;
  }

  @override
  Future<List<Map<String, dynamic>>> fetchData() {
    return DBController().rawQuery(
      "SELECT ${DBController.ascendStyleId}, ${DBController.routeTopRope}, ${DBController.sessionOutdoor} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
      "WHERE ${DBController.ascendType} == (?)",
      [ClimbingType.DEEP_WATER_SOLO.name],
    );
  }

  @override
  ChartType getType() => ChartType.DEEP_WATER_SOLO;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;
}
