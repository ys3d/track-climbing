import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/enums.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/timeline_climbing.dart';

class TimelineDWSChart extends TimelineClimbingChart {
  @override
  ChartType getType() => ChartType.DEEP_WATER_SOLO;

  ClimbingType getClimbingType() => ClimbingType.DEEP_WATER_SOLO;

  @override
  String getKey() => "timeline_sport_dws";

  @override
  Classes getClassType(Map<String, dynamic> ascend) => Classes.WATER;

  @override
  bool get activateTopropeLeadCheckBox => false;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;
}
