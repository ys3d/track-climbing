import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/boulder_last_year_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';

class LastYearDwsChart extends LastYearBoulderChart {
  @override
  ChartType getType() => ChartType.DEEP_WATER_SOLO;

  ClimbingType getClimbingType() => ClimbingType.DEEP_WATER_SOLO;

  @override
  int getMaxAscendStyle() => 3;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;
}
