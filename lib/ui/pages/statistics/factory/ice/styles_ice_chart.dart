import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/styles_climbing.dart';

class IceStylesChart extends ClimbingStyleChart {
  @override
  Future<List<Map<String, dynamic>>> fetchData() {
    return DBController().rawQuery(
      "SELECT ${DBController.ascendStyleId}, ${DBController.routeTopRope}, ${DBController.sessionOutdoor} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
      "WHERE ${DBController.ascendType} == (?)",
      [ClimbingType.ICE_CLIMBING.name],
    );
  }

  @override
  ChartType getType() => ChartType.ICE_CLIMBING;
}
