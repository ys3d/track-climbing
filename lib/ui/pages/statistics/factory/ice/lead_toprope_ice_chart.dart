import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/pie_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/lead_toprope_chart.dart';
import 'package:flutter/material.dart';

class LeadTopropeIceChart extends LeadTopropeChart {
  @override
  ChartType getType() => ChartType.ICE_CLIMBING;

  @override
  ClimbingType getClimbingType() => ClimbingType.ICE_CLIMBING;

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT "
      "COUNT(CASE WHEN ${DBController.routeTopRope} == 0 THEN 1 END) as 'Lead', "
      "COUNT(CASE WHEN ${DBController.routeTopRope} THEN 1 END) as 'Toprope' "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
      "WHERE ${DBController.ascendType} == (?)",
      [getClimbingType().name],
    );
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended) {
    return CustomPieChart(
      extended,
      preCalculatedData,
      colors: [
        AppColors.chart_colors.elementAt(1),
        AppColors.chart_colors.elementAt(0),
      ],
    );
  }
}
