import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/climbing_last_year_chart.dart';

class LastYearIceChart extends LastYearClimbingChart {
  @override
  ChartType getType() => ChartType.ICE_CLIMBING;

  ClimbingType getClimbingType() => ClimbingType.ICE_CLIMBING;
}
