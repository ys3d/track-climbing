import 'dart:convert';

import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HeatMapChart extends AbstractChart {
  static const String COUNT_KEY = "count";
  static const String DISTANCE_KEY = "distance";
  static const String LOCATION_KEY = "location";

  WebViewController _controller;
  String _locationString = "";
  String bounds = "";
  bool _loading = true;

  @override
  calculateData(BuildContext context, input) {
    _loading = true;
    if (input.length <= 0) {
      _locationString = "[]";
      return [];
    }
    _locationString = "";

    List sortedInput = List.generate(input.length, (index) => Map<String, dynamic>.of(input[index]));
    sortedInput.sort((a, b) => a[COUNT_KEY].compareTo(b[COUNT_KEY]));

    Map<String, dynamic> max = sortedInput.last;
    int lastValue = sortedInput.first[COUNT_KEY];
    int counter = 1;
    for (Map<String, dynamic> location in sortedInput) {
      if (location[COUNT_KEY] != lastValue) {
        lastValue = location[COUNT_KEY];
        counter++;
      }
      location[COUNT_KEY] = counter;

      _locationString +=
          '[${location[DBController.latitude]}, ${location[DBController.longitude]}, ${location[COUNT_KEY]}],';
    }

    // remove last ','
    _locationString = _locationString.substring(0, _locationString.length - 1);

    // find two nearest points
    List<Map<String, dynamic>> distances = List.generate(
        sortedInput.length,
        (index) => {
              DISTANCE_KEY: Geolocator.distanceBetween(
                sortedInput[index][DBController.latitude],
                sortedInput[index][DBController.longitude],
                max[DBController.latitude],
                max[DBController.longitude],
              ),
              LOCATION_KEY: sortedInput[index]
            });

    distances.sort((a, b) => a[DISTANCE_KEY].compareTo(b[DISTANCE_KEY]));
    distances = distances.take(3).toList();

    bounds = "";
    for (Map<String, dynamic> bound in distances.map((e) => e[LOCATION_KEY])) {
      bounds += "[${bound[DBController.latitude]}, ${bound[DBController.longitude]}],";
    }
    // remove last ','
    bounds = bounds.substring(0, bounds.length - 1);

    return sortedInput;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended) {
    return IgnorePointer(
      ignoring: !extended,
      child: StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) => Stack(
          children: [
            WebView(
              initialUrl: 'about:blank',
              onWebViewCreated: (WebViewController webViewController) {
                _controller = webViewController;
                _loadHtmlFromAssets(
                  extended ? 'assets/html/heatmap_expanded.html' : 'assets/html/heatmap.html',
                  setState,
                );
              },
              javascriptMode: JavascriptMode.unrestricted,
              navigationDelegate: (NavigationRequest request) {
                if (!request.url.startsWith("data:text/html")) {
                  print('blocking navigation to ${request.url}');
                  return NavigationDecision.prevent;
                }
                return NavigationDecision.navigate;
              },
            ),
            if (_loading && extended) Center(child: CircularProgressIndicator()),
          ],
        ),
      ),
    );
  }

  @override
  Widget getExpandedWidget() {
    return expanded();
  }

  @override
  Widget expanded() {
    return getChart(extended: true, checkInternet: true);
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context).charts_heatmap;

  @override
  ChartType getType() => ChartType.ALL;

  @override
  void loadData() {
    this.futureData = _fetchData();
  }

  Future<List<Map<String, dynamic>>> _fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.sessionLocation}, COUNT(${DBController.sessionLocation}) as $COUNT_KEY, "
      "${DBController.longitude}, ${DBController.latitude}, ${DBController.sessionOutdoor} "
      "FROM ${DBController.sessionTable} "
      "JOIN ${DBController.locationsTable} using (${DBController.sessionLocation}) "
      "WHERE ${DBController.longitude} IS NOT NULL AND "
      "${DBController.latitude} IS NOT NULL "
      "GROUP BY ${DBController.sessionLocation}",
    );
  }

  @override
  String getKey() => "heatmap";

  @override
  Widget createChartContainer(BuildContext context) {
    return InkWell(
      onTap: () => showChart(context),
      focusColor: Colors.transparent,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        margin: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
        padding: const EdgeInsets.all(8.0),
        width: MediaQuery.of(context).size.width,
        height: 100,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black12, width: 2), // added
          borderRadius: BorderRadius.circular(25.0),
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: getChart(addPaddingIfNoData: true, checkInternet: true),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12.0),
                    topRight: Radius.circular(12.0),
                  ),
                ),
                padding: const EdgeInsets.only(top: 2.0, left: 8.0, right: 8.0),
                child: Text(
                  getTitle(context),
                  maxLines: 1,
                  overflow: TextOverflow.visible,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        color: Colors.white,
        onPressed: () => _showHeatMapInfoDialog(context),
        icon: Icon(Icons.info_outline_rounded),
      ),
    ];
  }

  _loadHtmlFromAssets(String htmlPath, StateSetter setState) async {
    String fileText = await rootBundle.loadString(htmlPath);
    fileText = fileText.replaceAll("&&", _locationString);
    fileText = fileText.replaceAll("&bound", bounds);
    await _controller.loadUrl(
      Uri.dataFromString(
        fileText,
        mimeType: 'text/html',
        encoding: Encoding.getByName('utf-8'),
      ).toString(),
    );
    _loading = false;
    setState(() {});
  }

  Future<void> _showHeatMapInfoDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: true,
          title: Text(AppLocalizations.of(context).charts_heatmap_info_title),
          titleTextStyle: Theme.of(context).textTheme.headline6,
          contentTextStyle: Theme.of(context).textTheme.bodyText1,
          contentPadding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 20.0),
          content: Text(AppLocalizations.of(context).charts_heatmap_info_title_content),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context).general_back),
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;
}
