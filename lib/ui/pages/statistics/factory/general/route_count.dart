import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/date_time_bar_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class RouteCountChart extends AbstractChart {
  @override
  calculateData(BuildContext context, input) {
    if (input.isEmpty) return [];

    List<List<dynamic>> lastSessions = [[], [], [], [], [], [], [], []];
    for (Map<String, dynamic> element in input.reversed) {
      lastSessions[0].add(DateTime.parse(element[DBController.sessionTimeStart]));
      lastSessions[1].add(element["lead"].toDouble());
      lastSessions[2].add(element["top"].toDouble());
      lastSessions[3].add(element["boulders"].toDouble());
      lastSessions[4].add(element["dws"].toDouble());
      lastSessions[5].add(element["ice"].toDouble());
      lastSessions[6].add(element["free"].toDouble());
      lastSessions[7].add(element["speed"].toDouble());
    }
    return lastSessions;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended) {
    List<Color> colors = AppColors.chart_colors.getRange(1, 7).toList();
    colors.insert(1, AppColors.chart_colors.first);
    return DateTimeBarChart(
      preCalculatedData,
      extended,
      getTitles(context),
      colors,
    );
  }

  @override
  Widget expanded() {
    return getChart(extended: true);
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context).chart_route_count;

  @override
  ChartType getType() => ChartType.ALL;

  @override
  void loadData() {
    this.futureData = _fetchData();
  }

  Future<List<Map<String, dynamic>>> _fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.sessionTimeStart}, "
      "COUNT(CASE WHEN ${DBController.routeTopRope} AND ${DBController.ascendType} == (?) THEN 1 END) as top, "
      "COUNT(CASE WHEN ${DBController.routeTopRope} == 0 AND ${DBController.ascendType} == (?) THEN 1 END) as lead, "
      "COUNT(CASE WHEN ${DBController.ascendType} == (?) THEN 1 END) as boulders, "
      "COUNT(CASE WHEN ${DBController.ascendType} == (?) THEN 1 END) as dws, "
      "COUNT(CASE WHEN ${DBController.ascendType} == (?) THEN 1 END) as ice, "
      "COUNT(CASE WHEN ${DBController.ascendType} == (?) THEN 1 END) as free, "
      "COUNT(CASE WHEN ${DBController.ascendType} == (?) THEN 1 END) as speed "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
      "WHERE (${DBController.sessionOutdoor} == (?) OR (?)) AND (?) "
      "GROUP BY ${DBController.sessionId} "
      "ORDER BY ${DBController.sessionTimeStart} DESC "
      "LIMIT (?)",
      [
        ClimbingType.SPORT_CLIMBING.name,
        ClimbingType.SPORT_CLIMBING.name,
        ClimbingType.BOULDER.name,
        ClimbingType.DEEP_WATER_SOLO.name,
        ClimbingType.ICE_CLIMBING.name,
        ClimbingType.FREE_SOLO.name,
        ClimbingType.SPEED_CLIMBING.name,
        outdoor ? 1 : 0,
        (outdoor && indoor) ? 1 : 0,
        (outdoor || indoor) ? 1 : 0,
        DataStore().settingsDataStore.limitCharts ? Data.MAX_SESSION_AMOUNT : -1,
      ],
    );
  }

  @override
  String getKey() => "route_count";

  List<String> getTitles(BuildContext context) {
    return [
      '${AppLocalizations.of(context).style_lead}: ',
      '${AppLocalizations.of(context).style_toprope}: ',
      '${AppLocalizations.of(context).style_bouldering}: ',
      '${AppLocalizations.of(context).style_dws_climbing}: ',
      '${AppLocalizations.of(context).style_ice_climbing}: ',
      '${AppLocalizations.of(context).style_free_solo_climbing}: ',
      '${AppLocalizations.of(context).style_speed_climbing}: ',
    ];
  }

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  void onCheckboxSelectionChanged() {
    loadData();
  }
}
