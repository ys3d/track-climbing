import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/pie_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LocationsChart extends AbstractChart {
  static const int LOCATION_SEPARATE_LIMIT = 8;

  @override
  calculateData(BuildContext context, input) {
    // Filter data
    List<Map<String, dynamic>> filteredData = filterData(input);

    Map<String, double> locations = Map();
    Data().getLocations().forEach((element) {
      locations[element] = 0;
    });

    for (Map<String, dynamic> session in filteredData) {
      locations[session[DBController.sessionLocation]] += 1;
    }
    return _stripData(context, locations, filteredData.length);
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended) {
    return CustomPieChart(extended, preCalculatedData);
  }

  @override
  Widget expanded() {
    return Container(
      padding: const EdgeInsets.only(left: 50, right: 50, bottom: 20, top: 20),
      child: getChart(extended: true),
    );
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context).chart_locations;

  @override
  ChartType getType() => ChartType.ALL;

  @override
  void loadData() {
    this.futureData = _fetchData();
  }

  Future<List<Map<String, dynamic>>> _fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.sessionLocation}, ${DBController.sessionOutdoor} "
      "FROM ${DBController.sessionTable} "
      "WHERE ${DBController.sessionStatus} != (?) ",
      [Data.SESSION_ACTIVE_STATE],
    );
  }

  Map<String, double> _stripData(BuildContext context, Map<String, double> map, int totalEntries) {
    double others = 0;
    List<String> remove = [];
    map.forEach((key, value) {
      if (value / totalEntries < 0.0195) {
        others += value;
        remove.add(key);
      }
    });
    remove.forEach((element) {
      map.remove(element);
    });

    List<MapEntry<String, double>> res = map.entries.toList();
    res.sort((a, b) => b.value.compareTo(a.value));

    if (map.length > LOCATION_SEPARATE_LIMIT + 1) {
      while (map.length > LOCATION_SEPARATE_LIMIT) {
        map.removeWhere((String key, double val) => key == res.last.key);
        others += res.removeLast().value;
      }
    }

    Map<String, double> out = Map.fromEntries(
      res.toSet().map((e) => MapEntry(e.key + " (${e.value.toInt()})", e.value)),
    );
    if (others > 0) {
      out[AppLocalizations.of(context).general_others + " (${others.toInt()})"] = others;
    }

    return out;
  }

  @override
  String getKey() => "locations";

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool topropeCheckBoxActivated() => false;
}
