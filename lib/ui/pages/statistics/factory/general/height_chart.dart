import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/model/height_measurement.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/backend/overview_data_loader.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/data_points.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/line_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HeightChart extends AbstractChart {
  @override
  calculateData(BuildContext context, input) {
    List<DateTimeDataPoint> data = [];
    for (Map<String, dynamic> element in input.reversed) {
      DateTime start = DateTime.parse(element[DBController.sessionTimeStart]);
      data.add(
        DateTimeDataPoint(
          start,
          Util.heightFromMeters(element[OverviewDataLoader.TOTAL_HEIGHT_KEY]).toDouble(),
        ),
      );
    }
    return data;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended) {
    return LineChart(
      preCalculatedData,
      extended,
      "",
      measureFormatting: DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET
          ? AppLocalizations.of(context).general_feet(2)
          : AppLocalizations.of(context).general_meter(2),
    );
  }

  @override
  Widget expanded() {
    return getChart(extended: true);
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context).chart_climbed_meters;

  @override
  ChartType getType() => ChartType.ALL;

  @override
  void loadData() {
    this.futureData = _fetchData();
  }

  Future<List<Map<String, dynamic>>> _fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.sessionTimeStart}, "
      "SUM(${DBController.ascendHeight}) as '${OverviewDataLoader.TOTAL_HEIGHT_KEY}' "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
      "WHERE ${DBController.sessionStatus} != (?) AND "
      "((${DBController.sessionOutdoor} == (?) OR (?)) AND (?)) "
      "GROUP BY ${DBController.sessionId} "
      "ORDER BY ${DBController.sessionTimeStart} DESC "
      "LIMIT (?)",
      [
        Data.SESSION_ACTIVE_STATE,
        outdoor ? 1 : 0,
        (outdoor && indoor) ? 1 : 0,
        (outdoor || indoor) ? 1 : 0,
        DataStore().settingsDataStore.limitCharts ? Data.MAX_SESSION_AMOUNT : -1,
      ],
    );
  }

  @override
  String getKey() => "climbed_meters";

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  void onCheckboxSelectionChanged() {
    loadData();
  }
}
