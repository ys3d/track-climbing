import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/data_points.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/line_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SessionTimeChart extends AbstractChart {
  @override
  calculateData(BuildContext context, input) {
    // Filter data
    List<Map<String, dynamic>> filteredData = filterData(input);

    List<DateTimeDataPoint> dataPints = [];
    for (Map<String, dynamic> element in filteredData) {
      DateTime start = DateTime.parse(element[DBController.sessionTimeStart]);
      DateTime end = DateTime.parse(element[DBController.sessionTimeEnd]);
      dataPints.add(DateTimeDataPoint(start, end.difference(start).inMinutes.abs().toDouble()));
    }
    return limitDate(dataPints);
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended) {
    return LineChart(
      preCalculatedData,
      extended,
      "",
      measureFormatting: AppLocalizations.of(context).general_minutes,
    );
  }

  @override
  Widget expanded() {
    return getChart(extended: true);
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context).chart_session_time;

  @override
  ChartType getType() => ChartType.ALL;

  @override
  void loadData() {
    this.futureData = _fetchData();
  }

  Future<List<Map<String, dynamic>>> _fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.sessionTimeStart}, ${DBController.sessionTimeEnd}, ${DBController.sessionOutdoor} "
      "FROM ${DBController.sessionTable} "
      "WHERE ${DBController.sessionStatus} != (?) "
      "ORDER BY ${DBController.sessionTimeStart}",
      [Data.SESSION_ACTIVE_STATE],
    );
  }

  @override
  String getKey() => "session_time";

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool topropeCheckBoxActivated() => false;
}
