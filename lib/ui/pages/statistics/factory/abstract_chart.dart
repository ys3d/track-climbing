import 'dart:math';

import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:climbing_track/ui/pages/statistics/expanded_chart.dart';
import 'package:climbing_track/ui/widgets/empty_list_placeholder.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

enum ChartType {
  ALL,
  SPORT_CLIMBING,
  BOULDER,
  SPEED_CLIMBING,
  FREE_SOLO,
  DEEP_WATER_SOLO,
  ICE_CLIMBING,
}

abstract class AbstractChart {
  static const String FIRST_YEAR_KEY = "first_year";
  static const String ACTIVE_KEY = "chart_active";
  static const String NO_DATA_IMAGE_PATH = "assets/images/3327053.png";
  static const String NO_INTERNET_IMAGE_PATH = "assets/images/no-wifi.png";

  static List<String> yearList = [];
  Future futureData;
  bool visible;

  /// if true this kind of ascends should be displayed
  bool indoor = true;
  bool outdoor = true;
  bool toprope = true;
  bool lead = true;

  /// Initialize chart. Load data (Future) and visibility value
  ///
  /// if [force] data will be loaded regardless of [visible] value
  void init({bool force = false}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    visible = prefs.getBool(_visibilityKey) ?? getStandardVisibility();
    if (visible || force) {
      if (this.futureData == null) loadData();
    }
  }

  ///Returns unique key
  String getKey();

  /// Returns name of chart as [String]
  ///
  /// Must be overwritten
  String getName(BuildContext context);

  /// Returns type of chart as instance of enum [ChartType]
  ///
  /// Must be overwritten to set chart type
  ChartType getType();

  /// Load data as future and writes future in variable [futureData]
  void loadData();

  /// Returns chart as instance of class [Widget] to be rendered
  /// Called in [getChart()]
  ///
  /// [preCalculatedData] The precalculated information to plot chart
  /// [extended] If true chart is expanded on whole page
  Widget chart(BuildContext context, dynamic preCalculatedData, bool extended);

  /// Returns expanded chart as instance of class [Widget] to be rendered
  /// Called in [getExpandedWidget()]
  Widget expanded();

  /// Pre calculate raw data to be accessed by chart builder
  ///
  /// [input] the raw data from database
  dynamic calculateData(BuildContext context, dynamic input);

  /// Pre filter list of ascends
  ///
  /// [ascends] List of ascends, raw from database
  ///
  /// Returns list of ascends fitting to settings
  List<Map<String, dynamic>> filterData(List<Map<String, dynamic>> ascends) {
    return ascends
        .where((elem) =>
            (elem[DBController.sessionOutdoor] == null
                ? true
                : ((elem[DBController.sessionOutdoor] == 1 && outdoor) ||
                    (elem[DBController.sessionOutdoor] == 0 && indoor))) &&
            (elem[DBController.routeTopRope] == null
                ? true
                : ((elem[DBController.routeTopRope] == 1 && toprope) ||
                    (elem[DBController.routeTopRope] == 0 && lead))))
        .toList();
  }

  /// Returns Widget as instance of class [Widget] containing chart container with chart to be rendered if visible
  /// Otherwise returning empty [Container]
  ///
  /// [context] : Instance current [BuildContext]
  /// [force] : If true returning chart container with chart regardless of [visible] value
  Widget getWidget(BuildContext context, {bool force = false}) {
    return visible || force ? createChartContainer(context) : Container();
  }

  /// Returns title as [String] of chart
  /// Consists of name and type of the chart
  String getTitle(BuildContext context) {
    return Util.replaceStringWithBlankSpaces(getName(context) + typeSuffix(context));
  }

  /// Returns standard visible value, if no value of [visible] has been stored yet
  bool getStandardVisibility() {
    return getType() == ChartType.ALL || getType() == ChartType.SPORT_CLIMBING;
  }

  /// Returns expanded chart as instance of class [Widget] to be rendered
  Widget getExpandedWidget() {
    return Container(
      padding: const EdgeInsets.only(left: 10, top: 15),
      child: expanded(),
      color: Colors.white,
    );
  }

  /// Returns widget which is displayed when no data is available
  /// Returns instance of class [Widget]
  Widget noDataWidget(BuildContext context, {bool expanded = false, bool addPadding = false}) {
    if (expanded) {
      return EmptyListPlaceHolder(
        text: AppLocalizations.of(context).charts_no_data_placeholder_text,
        image: NO_DATA_IMAGE_PATH,
      );
    } else {
      return Padding(
        padding: EdgeInsets.only(top: addPadding ? 6.0 : 0.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Opacity(
              opacity: 0.8,
              child: Image.asset(
                NO_DATA_IMAGE_PATH,
                width: 40,
                height: 40,
                fit: BoxFit.cover,
              ),
            ),
            const SizedBox(width: Constants.STANDARD_PADDING),
            Padding(
              padding: EdgeInsets.only(bottom: 2.0),
              child: Text(
                AppLocalizations.of(context).charts_no_data_placeholder_text,
                style: TextStyle(color: Colors.black38, fontSize: 17),
              ),
            ),
          ],
        ),
      );
    }
  }

  /// Returns widget which is displayed when no internet is available
  /// Returns instance of class [Widget]
  Widget noInternetWidget(BuildContext context, {bool expanded = false, bool addPadding = false}) {
    if (expanded) {
      return EmptyListPlaceHolder(
        text: AppLocalizations.of(context).charts_no_internet_placeholder_text_extended,
        image: NO_INTERNET_IMAGE_PATH,
      );
    } else {
      return Padding(
        padding: EdgeInsets.only(top: addPadding ? 6.0 : 0.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Opacity(
              opacity: 0.8,
              child: Image.asset(
                NO_INTERNET_IMAGE_PATH,
                width: 40,
                height: 40,
                fit: BoxFit.cover,
              ),
            ),
            const SizedBox(width: Constants.STANDARD_PADDING),
            Padding(
              padding: EdgeInsets.only(bottom: 2.0),
              child: Text(
                AppLocalizations.of(context).charts_no_internet_placeholder_text,
                style: TextStyle(color: Colors.black38, fontSize: 17),
                overflow: TextOverflow.clip,
              ),
            ),
          ],
        ),
      );
    }
  }

  /// If there is a choice of different charts, override this method.
  /// Should return titles of all sub charts which can be selected
  List<String> getTitleSelection(BuildContext context) => [];

  /// Future builder which return chart in chart container if data are available
  /// Otherwise returns placeholder
  Widget getChart({bool extended = false, bool addPaddingIfNoData = false, bool checkInternet = false}) {
    return FutureBuilder(
      future: Future.wait([futureData, _checkInternetConnection(checkInternet)]),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Container();
        }
        var data = calculateData(context, snapshot.data[0]);

        // check internet
        if (!snapshot.data[1]) {
          return noInternetWidget(context, expanded: extended, addPadding: addPaddingIfNoData);
        }

        if (data.runtimeType is List) {
          if (data.first.runtimeType is List) {
            if (data.first.isEmpty) return noDataWidget(context, expanded: extended, addPadding: addPaddingIfNoData);
          } else {
            if (data.isEmpty) return noDataWidget(context, expanded: extended, addPadding: addPaddingIfNoData);
          }
        } else {
          if (data.isEmpty) return noDataWidget(context, expanded: extended, addPadding: addPaddingIfNoData);
        }

        return chart(context, data, extended);
      },
    );
  }

  /// Will be called if sub chart selection changes
  ///
  /// [value] The title of the selected sub chart
  ///
  /// Override this method to implement appropriate functionality
  void onChangeTitleSelection(BuildContext context, String value) {}

  /// limit data to [Data.MAX_SESSION_AMOUNT] entries if corresponding attribute [limitCharts] is set
  List limitDate(List<dynamic> data) {
    return DataStore().settingsDataStore.limitCharts
        ? data.getRange(max(0, data.length - Data.MAX_SESSION_AMOUNT), data.length).toList()
        : data;
  }

  /// Set visibility of chart
  /// Stores value locally with [SharedPreferences] library
  void setVisibility(bool visibility) async {
    this.visible = visibility;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(_visibilityKey, visible);
  }

  Widget createChartContainer(BuildContext context) {
    return InkWell(
      onTap: () => showChart(context),
      focusColor: Colors.transparent,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        margin: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
        padding: EdgeInsets.only(
          left: Constants.STANDARD_PADDING,
          right: Constants.STANDARD_PADDING,
          bottom: 8.0,
          top: 14,
        ),
        width: MediaQuery.of(context).size.width,
        height: 100,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black12, width: 2), // added
          borderRadius: BorderRadius.circular(25.0),
        ),
        child: Column(
          children: [
            Flexible(
              child: Padding(
                padding: EdgeInsets.only(left: 30.0, right: 30.0),
                child: getChart(),
              ),
              fit: FlexFit.tight,
            ),
            const SizedBox(height: 10.0),
            Text(
              getTitle(context),
              maxLines: 1,
              overflow: TextOverflow.visible,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ],
        ),
      ),
    );
  }

  void showChart(BuildContext context) {
    Navigator.of(context).push(_createChartPage(context));
  }

  Route _createChartPage(BuildContext context) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) {
        return ExpandedChart(
          expanded: this,
          title: getTitleSelection(context).length > 0 ? getTitleSelection(context) : [getTitle(context)],
          onChangeSelection: (val) => onChangeTitleSelection(context, val),
          appBarActions: getAppBarActions(context),
        );
      },
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(1.0, 0.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  String typeSuffix(BuildContext context) {
    switch (getType()) {
      case ChartType.ALL:
        return "";
      case ChartType.SPORT_CLIMBING:
        return " [${AppLocalizations.of(context).style_sport_climbing}]";
      case ChartType.BOULDER:
        return " [${AppLocalizations.of(context).style_boulder}]";
      case ChartType.SPEED_CLIMBING:
        return " [${AppLocalizations.of(context).style_speed_prefix}]";
      case ChartType.FREE_SOLO:
        return " [${AppLocalizations.of(context).style_free_solo}]";
      case ChartType.DEEP_WATER_SOLO:
        return " [${AppLocalizations.of(context).style_dws}]";
      case ChartType.ICE_CLIMBING:
        return " [${AppLocalizations.of(context).style_ice_climbing}]";
    }
    return "";
  }

  String get _visibilityKey => "${ACTIVE_KEY}_${getKey()}_${getType().name.toLowerCase()}";

  /// Load list of years which contains each year since app is first used
  static void loadYears(SharedPreferences sharedPreferences) async {
    int lastYear = sharedPreferences.getInt(FIRST_YEAR_KEY);

    DateTime now = DateTime.now();

    if (lastYear == null) {
      List<Map<String, dynamic>> result = await DBController().queryRows(
        DBController.sessionTable,
        orderBy: DBController.sessionTimeStart,
        limit: 1,
      );
      if (result.isEmpty) {
        lastYear = now.year;
      } else {
        DateTime dateTime = DateTime.parse(result.first[DBController.sessionTimeStart]);
        lastYear = dateTime.year;
      }

      sharedPreferences.setInt(FIRST_YEAR_KEY, lastYear);
    }
    if (lastYear > now.year) lastYear = now.year;

    List<String> years = List.generate(now.year - lastYear + 1, (index) => "${lastYear + index}");
    AbstractChart.yearList = years.reversed.toList();
  }

  List<Widget> getAppBarActions(BuildContext context) => const [];

  Future<bool> _checkInternetConnection(bool checkInternet) async {
    if (!checkInternet) return true;
    ConnectivityResult connectivityResult = await (Connectivity().checkConnectivity());
    return connectivityResult != ConnectivityResult.none;
  }

  /// Whether the checkbox for lead should be visible or not
  bool leadCheckBoxActivated() => true;

  /// Whether the checkbox for toprope should be visible or not
  bool topropeCheckBoxActivated() => true;

  /// Whether the checkbox for indoor should be visible or not
  bool indoorCheckBoxActivated() => true;

  /// Whether the checkbox for outdoor should be visible or not
  bool outdoorCheckBoxActivated() => true;

  /// Will be called if checkbox selection changes
  void onCheckboxSelectionChanged() {}
}
