import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/grade_mapper/abstract_mapper.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/bar_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/climbing_grades_distribution_chart.dart';
import 'package:flutter/material.dart';

class SoloGradesDistributionChart extends ClimbingGradesDistributionChart {
  @override
  ChartType getType() => ChartType.FREE_SOLO;

  @override
  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT "
      "COUNT(CASE WHEN ${DBController.ascendStyleId} = 3 THEN 1 END) as Redpoint, "
      "${DBController.ascendGradeId} "
      "FROM ${DBController.ascendsTable} "
      "WHERE ${DBController.ascendType} == (?) "
      "GROUP BY ${DBController.ascendGradeId} ",
      [getClimbingType().name],
    );
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended) {
    return BarChart(
      preCalculatedData,
      extended,
      ["Redpoint"],
    );
  }

  @override
  List<List<dynamic>> calculateData(BuildContext context, input) {
    if (input.isEmpty) return [];

    AbstractGradeMapper mapper = GradeManager().getGradeMapper(type: getClimbingType());
    List<List<dynamic>> out = [mapper.getLabels().toSet().toList()];

    List<Map<String, int>> results = [];

    results.add(Map());

    for (String grade in out[0]) {
      for (int i = 0; i < results.length; i++) {
        results[i][grade] = 0;
      }
    }

    for (Map<String, dynamic> element in input) {
      for (int i = 0; i < results.length; i++) {
        results[i][mapper.getGradeFromId(mapper.getRoundedKey(element[DBController.ascendGradeId]))] +=
            element[getMapping()[2]];
      }
    }

    for (Map value in results) {
      out.add(value.values.toList());
    }

    return out;
  }

  @override
  ClimbingType getClimbingType() => ClimbingType.FREE_SOLO;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;
}
