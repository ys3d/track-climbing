import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/style_mapper.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/pie_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ClimbingStyleChart extends AbstractChart {
  @override
  dynamic calculateData(BuildContext context, input) {
    // Filter data
    List<Map<String, dynamic>> data = filterData(input);

    // return if no data is left
    if (data.isEmpty) return [];

    final StyleMapper _styleMapper = StyleMapper();
    Map<String, double> styles = Map();
    _styleMapper.getMapping(context).forEach((key, value) {
      if (key < 100) {
        // only climbing styles, not bouldering styles
        styles[value] = 0;
      }
    });

    for (Map<String, dynamic> route in data) {
      styles[_styleMapper.getStyleFromId(route[DBController.ascendStyleId], context)] += 1;
    }

    return styles;
  }

  @override
  Widget chart(BuildContext context, dynamic preCalculatedData, bool extended) {
    return CustomPieChart(extended, preCalculatedData);
  }

  @override
  Widget expanded() {
    return Container(
      padding: const EdgeInsets.only(left: 50, right: 50, bottom: 20, top: 20),
      child: getChart(extended: true),
    );
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context).chart_styles;

  @override
  ChartType getType() => ChartType.SPORT_CLIMBING;

  @override
  void loadData() {
    this.futureData = fetchData();
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.ascendStyleId}, ${DBController.routeTopRope}, ${DBController.sessionOutdoor} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
      "WHERE ${DBController.ascendType} == (?)",
      [ClimbingType.SPORT_CLIMBING.name],
    );
  }

  @override
  String getKey() => "styles";
}
