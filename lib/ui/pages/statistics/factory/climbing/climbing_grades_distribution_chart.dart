import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/grade_mapper/abstract_mapper.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/util/style_mapper.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/bar_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ClimbingGradesDistributionChart extends AbstractChart {
  @override
  List<List<dynamic>> calculateData(BuildContext context, input) {
    if (input.isEmpty) return [];

    AbstractGradeMapper mapper = GradeManager().getGradeMapper(type: getClimbingType());
    List<List<dynamic>> out = [mapper.getLabels().toSet().toList()];

    List<Map<String, int>> results = [];

    for (dynamic _ in StyleMapper().getMapping(context).values.take(5)) {
      results.add(Map());
    }

    for (String grade in out[0]) {
      for (int i = 0; i < results.length; i++) {
        results[i][grade] = 0;
      }
    }

    for (Map<String, dynamic> element in input) {
      for (int i = 0; i < results.length; i++) {
        results[i][mapper.getGradeFromId(mapper.getRoundedKey(element[DBController.ascendGradeId]))] +=
            element[getMapping()[i]];
      }
    }

    for (Map value in results) {
      out.add(value.values.toList());
    }
    return out;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended) {
    return BarChart(
      preCalculatedData,
      extended,
      StyleMapper().getMapping(context).values.take(5).toList(),
    );
  }

  @override
  Widget expanded() {
    return Container(
      padding: const EdgeInsets.only(right: 10, bottom: 20),
      child: getChart(extended: true),
    );
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context).chart_grade_distribution;

  @override
  ChartType getType() => ChartType.SPORT_CLIMBING;

  @override
  void loadData() {
    this.futureData = fetchData();
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT "
      "COUNT(CASE WHEN ${DBController.ascendStyleId} = 1 THEN 1 END) as Onsight, "
      "COUNT(CASE WHEN ${DBController.ascendStyleId} = 2 THEN 1 END) as Flash, "
      "COUNT(CASE WHEN ${DBController.ascendStyleId} = 3 THEN 1 END) as Redpoint, "
      "COUNT(CASE WHEN ${DBController.ascendStyleId} = 4 THEN 1 END) as 'a.f.', "
      "COUNT(CASE WHEN ${DBController.ascendStyleId} = 5 THEN 1 END) as Hangdogging, "
      "${DBController.ascendGradeId} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
      "WHERE ${DBController.ascendType} == (?) AND "
      "((${DBController.sessionOutdoor} == (?) OR (?)) AND (?)) AND "
      "((${DBController.routeTopRope} == (?) OR (?)) AND (?)) "
      "GROUP BY ${DBController.ascendGradeId} ",
      [
        getClimbingType().name,
        outdoor ? 1 : 0,
        (outdoor && indoor) ? 1 : 0,
        (outdoor || indoor) ? 1 : 0,
        toprope ? 1 : 0,
        (toprope && lead) ? 1 : 0,
        (toprope || lead) ? 1 : 0,
      ],
    );
  }

  ClimbingType getClimbingType() => ClimbingType.SPORT_CLIMBING;

  @override
  String getKey() => "grade_distribution";

  List getMapping() {
    return const ["Onsight", "Flash", "Redpoint", "a.f.", "Hangdogging"];
  }

  @override
  void onCheckboxSelectionChanged() {
    loadData();
  }
}
