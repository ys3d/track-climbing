import 'dart:math';

import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/year_combined_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LastYearClimbingChart extends AbstractChart {
  DateTime from;
  DateTime to;

  @override
  List<String> getTitleSelection(BuildContext context) {
    return [getName(context)]..addAll(AbstractChart.yearList);
  }

  @override
  List<List<double>> calculateData(BuildContext context, input) {
    // Filter data
    List<Map<String, dynamic>> filteredData = filterData(input);

    DateTime now = DateTime.now();
    DateTime fromUpdated = this.from ?? DateTime(now.year - 1, now.month + 1, 1, 0, 0, 0, 0, 0);
    DateTime toUpdated = this.to ?? now;

    List data = filteredData
        .where((element) =>
            DateTime.parse(element[DBController.sessionTimeStart]).isAfter(fromUpdated) &&
            DateTime.parse(element[DBController.sessionTimeStart]).isBefore(toUpdated))
        .toList();

    List<List<double>> lastYear = [
      List.filled(12, 0.0),
      List.filled(12, 0.0),
      List.filled(12, 0.0),
      List.filled(12, 0.0)
    ];
    for (Map<String, dynamic> element in data) {
      if (element[DBController.routeTopRope] == 1) {
        lastYear[3][DateTime.parse(element[DBController.sessionTimeStart]).month - 1] += 1;
        if (element[DBController.ascendStyleId] <= 3) {
          lastYear[1][DateTime.parse(element[DBController.sessionTimeStart]).month - 1] = max(
            lastYear[1][DateTime.parse(element[DBController.sessionTimeStart]).month - 1],
            GradeManager()
                .getGradeMapper(type: getClimbingType())
                .normalize(element[DBController.ascendGradeId])
                .toDouble(),
          );
        }
      } else {
        lastYear[2][DateTime.parse(element[DBController.sessionTimeStart]).month - 1] += 1;
        if (element[DBController.ascendStyleId] <= 3) {
          lastYear[0][DateTime.parse(element[DBController.sessionTimeStart]).month - 1] = max(
            lastYear[0][DateTime.parse(element[DBController.sessionTimeStart]).month - 1],
            GradeManager()
                .getGradeMapper(type: getClimbingType())
                .normalize(element[DBController.ascendGradeId])
                .toDouble(),
          );
        }
      }
    }
    return lastYear;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended) {
    return YearBarChart(
      extended,
      preCalculatedData,
      "Best ${AppLocalizations.of(context).style_lead}:",
      "#${AppLocalizations.of(context).style_lead}:",
      lineLegendTwo: "Best ${AppLocalizations.of(context).style_toprope}:",
      barLegendTwo: "#${AppLocalizations.of(context).style_toprope}:",
      type: getClimbingType(),
      reorderMonths: from == null,
    );
  }

  @override
  Widget expanded() {
    return Container(
      key: UniqueKey(),
      padding: const EdgeInsets.only(right: 10, bottom: 20),
      child: getChart(extended: true),
    );
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context).chart_past_months;

  @override
  ChartType getType() => ChartType.SPORT_CLIMBING;

  @override
  void loadData() {
    this.futureData = fetchData();
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.sessionTimeStart}, ${DBController.routeTopRope}, "
      "${DBController.ascendGradeId}, ${DBController.ascendStyleId}, ${DBController.sessionOutdoor} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
      "WHERE ${DBController.ascendType} == (?)",
      [getClimbingType().name],
    );
  }

  @override
  void onChangeTitleSelection(BuildContext context, String value) {
    from = value == getName(context) ? null : DateTime(int.parse(value), 1, 1, 0);
    to = value == getName(context) ? null : DateTime(int.parse(value), 12, 31, 23, 59);
  }

  ClimbingType getClimbingType() => ClimbingType.SPORT_CLIMBING;

  @override
  String getKey() => "past_months";

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;
}
