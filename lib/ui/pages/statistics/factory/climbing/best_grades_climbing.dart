import 'dart:math';

import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/data_points.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/line_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BestGradesClimbingChart extends AbstractChart {
  @override
  String getName(BuildContext context) => AppLocalizations.of(context).chart_best_grades;

  @override
  ChartType getType() => ChartType.SPORT_CLIMBING;

  @override
  void loadData() {
    this.futureData = Future.wait([fetchData1(), fetchData2()]);
  }

  @override
  Widget chart(BuildContext context, dynamic preCalculatedData, extended) {
    List<List<DateTimeDataPoint>> data = preCalculatedData;
    return LineChart(
      limitDate(data[1]),
      extended,
      "${AppLocalizations.of(context).style_lead}:",
      legendTwo: "${AppLocalizations.of(context).style_toprope}:",
      data2: limitDate(data[0]),
      useGradeLabels: true,
      maximizeViewPoint: true,
      type: getClimbingType(),
    );
  }

  @override
  Widget expanded() => getChart(extended: true);

  @override
  dynamic calculateData(BuildContext context, dynamic input) {
    List<Map<String, dynamic>> sessions = input[0];
    List<Map<String, dynamic>> data = input[1];
    if (data.isEmpty) return [];

    Map<int, int> toprope = Map();
    Map<int, int> lead = Map();
    Map<int, dynamic> dateIDMapper = Map();
    for (Map<String, dynamic> session in sessions) {
      toprope[session[DBController.sessionId]] = 0;
      lead[session[DBController.sessionId]] = 0;
      dateIDMapper[session[DBController.sessionId]] = session;
    }

    for (Map<String, dynamic> element in data) {
      if (element[DBController.routeTopRope] == 1) {
        toprope[element[DBController.sessionId]] = max(
          element[DBController.ascendGradeId],
          toprope[element[DBController.sessionId]],
        );
      } else {
        lead[element[DBController.sessionId]] = max(
          element[DBController.ascendGradeId],
          lead[element[DBController.sessionId]],
        );
      }
    }

    List<DateTimeDataPoint> data1 = [];
    List<DateTimeDataPoint> data2 = [];
    for (int sessionID in lead.keys) {
      if (!((dateIDMapper[sessionID][DBController.sessionOutdoor] == 1 && outdoor) ||
          (dateIDMapper[sessionID][DBController.sessionOutdoor] == 0 && indoor))) {
        continue;
      }

      double bestTop = toprope[sessionID] == 0
          ? null
          : GradeManager().getGradeMapper(type: getClimbingType()).normalize(toprope[sessionID]).toDouble();
      double bestLead = lead[sessionID] == 0
          ? null
          : GradeManager().getGradeMapper(type: getClimbingType()).normalize(lead[sessionID]).toDouble();
      DateTime start = DateTime.parse(dateIDMapper[sessionID][DBController.sessionTimeStart]);
      data1.add(DateTimeDataPoint(start, bestTop));
      data2.add(DateTimeDataPoint(start, bestLead));
    }

    if (data1.isEmpty) return [];
    return [data1, data2];
  }

  Future<List<Map<String, dynamic>>> fetchData1() async {
    return DBController().rawQuery(
      "SELECT ${DBController.sessionTimeStart}, ${DBController.sessionId}, ${DBController.sessionOutdoor} "
      "FROM ${DBController.sessionTable} "
      "WHERE ${DBController.sessionId} IN "
      "(SELECT ${DBController.sessionId} FROM ${DBController.ascendsTable} "
      "WHERE ${DBController.ascendType} == (?)) "
      "ORDER BY ${DBController.sessionTimeStart}",
      [getClimbingType().name],
    );
  }

  Future<List<Map<String, dynamic>>> fetchData2() async {
    return DBController().rawQuery(
      "SELECT ${DBController.ascendGradeId}, ${DBController.routeTopRope}, ${DBController.sessionId} "
      "FROM ${DBController.ascendsTable} "
      "WHERE ${DBController.ascendStyleId} <= (?) AND "
      "${DBController.ascendType} == (?)",
      [3, getClimbingType().name],
    );
  }

  ClimbingType getClimbingType() => ClimbingType.SPORT_CLIMBING;

  @override
  String getKey() => "best_grades";

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;
}
