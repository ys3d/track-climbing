import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/average_grade_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AverageClimbingGradeChart extends AbstractChart {
  @override
  calculateData(BuildContext context, input) {
    if (input[0].length <= 1) return [];

    List<double> average;
    Map<int, double> amountMap = Map();
    Map<int, int> countMap = Map();
    Map<int, DateTime> dateMap = Map();
    for (Map<String, dynamic> session in input[1]) {
      amountMap[session[DBController.sessionId]] = 0;
      countMap[session[DBController.sessionId]] = 0;
      dateMap[session[DBController.sessionId]] = DateTime.parse(session[DBController.sessionTimeStart]);
    }

    for (Map<String, dynamic> element in input[0]) {
      amountMap[element[DBController.sessionId]] += GradeManager().getGradeMapper(type: getClimbingType()).normalize(
            element[DBController.ascendGradeId],
          );
      countMap[element[DBController.sessionId]] += 1;
    }

    average =
        amountMap.map((key, value) => MapEntry(key, countMap[key] == 0 ? 0.0 : value / countMap[key])).values.toList();
    List<DateTime> dates = dateMap.values.toList();

    // chart only visible if contains more than 2 sessions
    if (dates.length <= 2) return [];

    return [average, dates];
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended) {
    return AverageGradeChart(extended, preCalculatedData[0], preCalculatedData[1], getClimbingType());
  }

  @override
  Widget expanded() {
    return Container(
      padding: EdgeInsets.only(left: 0, right: 20, top: 10.0),
      child: getChart(extended: true),
    );
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context).charts_average_grade_chart;

  @override
  ChartType getType() => ChartType.SPORT_CLIMBING;

  @override
  void loadData() {
    this.futureData = Future.wait([fetchData1(), _fetchData2()]);
  }

  Future<List<Map<String, dynamic>>> fetchData1() async {
    //TODO nicer queries
    return DBController().rawQuery(
      "SELECT ${DBController.sessionId}, ${DBController.ascendGradeId}, ${DBController.ascendType} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using (${DBController.sessionId}) "
      "WHERE ${DBController.ascendType} == (?) AND "
      "((${DBController.routeTopRope} == (?) OR (?)) AND (?)) "
      "AND ${DBController.sessionId} IN "
      "(SELECT ${DBController.sessionId} FROM ${DBController.sessionTable} "
      "WHERE ${DBController.sessionStatus} != (?) AND "
      "${DBController.sessionId} IN "
      "(SELECT ${DBController.sessionId} FROM ${DBController.ascendsTable} "
      "WHERE ${DBController.ascendType} == (?)) AND "
      "((${DBController.sessionOutdoor} == (?) OR (?)) AND (?)) "
      "ORDER BY ${DBController.sessionTimeStart} DESC LIMIT (?)) "
      "ORDER BY ${DBController.sessionTimeStart} DESC",
      [
        getClimbingType().name,
        toprope ? 1 : 0,
        (toprope && lead) ? 1 : 0,
        (toprope || lead) ? 1 : 0,
        Data.SESSION_ACTIVE_STATE,
        getClimbingType().name,
        outdoor ? 1 : 0,
        (outdoor && indoor) ? 1 : 0,
        (outdoor || indoor) ? 1 : 0,
        Data.MAX_SESSION_AMOUNT,
      ],
    );
  }

  Future<List<Map<String, dynamic>>> _fetchData2() async {
    return DBController().rawQuery(
      "SELECT ${DBController.sessionId}, ${DBController.sessionTimeStart} FROM ${DBController.sessionTable} "
      "WHERE ${DBController.sessionStatus} != (?) AND "
      "${DBController.sessionId} IN "
      "(SELECT ${DBController.sessionId} FROM ${DBController.ascendsTable} "
      "WHERE ${DBController.ascendType} == (?)) AND "
      "((${DBController.sessionOutdoor} == (?) OR (?)) AND (?)) "
      "ORDER BY ${DBController.sessionTimeStart} DESC LIMIT (?)",
      [
        Data.SESSION_ACTIVE_STATE,
        getClimbingType().name,
        outdoor ? 1 : 0,
        (outdoor && indoor) ? 1 : 0,
        (outdoor || indoor) ? 1 : 0,
        Data.MAX_SESSION_AMOUNT,
      ],
    );
  }

  ClimbingType getClimbingType() => ClimbingType.SPORT_CLIMBING;

  @override
  String getKey() => "average_grade";

  @override
  void onCheckboxSelectionChanged() {
    loadData();
  }
}
