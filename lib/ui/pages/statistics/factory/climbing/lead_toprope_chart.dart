import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/pie_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LeadTopropeChart extends AbstractChart {
  @override
  calculateData(BuildContext context, input) {
    Map<String, double> data = Map<String, double>();
    input[0].forEach((key, value) => data[key] = value.toDouble());
    return data;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended) {
    return CustomPieChart(
      extended,
      preCalculatedData,
      colors: [
        AppColors.chart_colors.elementAt(1),
        AppColors.chart_colors.elementAt(1).withOpacity(0.3),
        AppColors.chart_colors.elementAt(0),
        AppColors.chart_colors.elementAt(0).withOpacity(0.3)
      ],
    );
  }

  @override
  Widget expanded() {
    return Container(
      padding: const EdgeInsets.only(left: 50, right: 50, bottom: 20, top: 20),
      child: getChart(extended: true),
    );
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context).chart_lead_toprope;

  @override
  ChartType getType() => ChartType.SPORT_CLIMBING;

  @override
  void loadData() {
    this.futureData = fetchData();
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT "
      "COUNT(CASE WHEN ${DBController.routeTopRope} == 0 AND ${DBController.sessionOutdoor} == 0 THEN 1 END) as 'Lead (indoor)', "
      "COUNT(CASE WHEN ${DBController.routeTopRope} == 0 AND ${DBController.sessionOutdoor} THEN 1 END) as 'Lead (outdoor)', "
      "COUNT(CASE WHEN ${DBController.routeTopRope} AND ${DBController.sessionOutdoor} == 0 THEN 1 END) as 'Toprope (indoor)', "
      "COUNT(CASE WHEN ${DBController.routeTopRope} AND ${DBController.sessionOutdoor} THEN 1 END) as 'Toprope (outdoor)' "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
      "WHERE ${DBController.ascendType} == (?)",
      [getClimbingType().name],
    );
  }

  ClimbingType getClimbingType() => ClimbingType.SPORT_CLIMBING;

  @override
  String getKey() => "lead_toprope";

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;

  @override
  bool topropeCheckBoxActivated() => false;
}
