import 'dart:math';

import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/data_points.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/line_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/best_grades_climbing.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BestGradesBoulderChart extends BestGradesClimbingChart {
  @override
  ClimbingType getClimbingType() => ClimbingType.BOULDER;

  @override
  Future<List<Map<String, dynamic>>> fetchData2() async {
    return DBController().rawQuery(
      "SELECT ${DBController.ascendGradeId}, ${DBController.sessionId} "
      "FROM ${DBController.ascendsTable} "
      "WHERE ${DBController.ascendStyleId} <= (?) AND "
      "${DBController.ascendType} == (?)",
      [101, getClimbingType().name],
    );
  }

  @override
  ChartType getType() => ChartType.BOULDER;

  @override
  Widget chart(BuildContext context, dynamic preCalculatedData, extended) {
    return LineChart(
      limitDate(preCalculatedData),
      extended,
      AppLocalizations.of(context).charts_grade,
      type: getClimbingType(),
      useGradeLabels: true,
      maximizeViewPoint: true,
    );
  }

  @override
  dynamic calculateData(BuildContext context, dynamic input) {
    List<Map<String, dynamic>> sessions = input[0];
    List<Map<String, dynamic>> data = input[1];
    if (data.isEmpty) return [];

    Map<int, int> grades = Map();
    Map<int, dynamic> dateIDMapper = Map();
    for (Map<String, dynamic> session in sessions) {
      grades[session[DBController.sessionId]] = 0;
      dateIDMapper[session[DBController.sessionId]] = session;
    }

    for (Map<String, dynamic> element in data) {
      grades[element[DBController.sessionId]] = max(
        element[DBController.ascendGradeId],
        grades[element[DBController.sessionId]],
      );
    }

    List<DateTimeDataPoint> out = [];
    for (int sessionID in grades.keys) {
      if (!((dateIDMapper[sessionID][DBController.sessionOutdoor] == 1 && outdoor) ||
          (dateIDMapper[sessionID][DBController.sessionOutdoor] == 0 && indoor))) {
        continue;
      }

      double bestGrade = grades[sessionID] == 0
          ? null
          : GradeManager().getGradeMapper(type: getClimbingType()).normalize(grades[sessionID]).toDouble();
      DateTime start = DateTime.parse(dateIDMapper[sessionID][DBController.sessionTimeStart]);
      out.add(DateTimeDataPoint(start, bestGrade));
    }

    return out;
  }
}
