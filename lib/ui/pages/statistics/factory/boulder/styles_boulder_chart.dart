import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/style_mapper.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/styles_climbing.dart';
import 'package:flutter/cupertino.dart';

class BoulderStyleChart extends ClimbingStyleChart {
  @override
  dynamic calculateData(BuildContext context, input) {
    // Filter data
    List<Map<String, dynamic>> filteredData = filterData(input);

    // return if no data is left
    if (filteredData.isEmpty) return [];

    final StyleMapper _styleMapper = StyleMapper();
    Map<String, double> styles = Map();
    _styleMapper.getMapping(context).forEach((key, value) {
      if (key >= 100) {
        // only climbing styles, not bouldering styles
        styles[value] = 0;
      }
    });

    for (Map<String, dynamic> route in filteredData) {
      styles[_styleMapper.getStyleFromId(route[DBController.ascendStyleId], context)] += 1;
    }

    return styles;
  }

  @override
  Future<List<Map<String, dynamic>>> fetchData() {
    return DBController().rawQuery(
      "SELECT ${DBController.ascendStyleId}, ${DBController.routeTopRope}, ${DBController.sessionOutdoor} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
      "WHERE ${DBController.ascendType} == (?)",
      [ClimbingType.BOULDER.name],
    );
  }

  @override
  ChartType getType() => ChartType.BOULDER;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;
}
