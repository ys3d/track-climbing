import 'dart:math';

import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/year_combined_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/climbing_last_year_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LastYearBoulderChart extends LastYearClimbingChart {
  @override
  ChartType getType() => ChartType.BOULDER;

  @override
  List<List<double>> calculateData(BuildContext context, input) {
    // Filter data
    List<Map<String, dynamic>> filteredData = filterData(input);

    DateTime now = DateTime.now();
    DateTime fromUpdated = this.from ?? DateTime(now.year - 1, now.month + 1, 1, 0, 0, 0, 0, 0);
    DateTime toUpdated = this.to ?? now;
    List data = filteredData
        .where((element) =>
            DateTime.parse(element[DBController.sessionTimeStart]).isAfter(fromUpdated) &&
            DateTime.parse(element[DBController.sessionTimeStart]).isBefore(toUpdated))
        .toList();

    List<List<double>> lastYear = [
      List.filled(12, 0.0),
      List.filled(12, 0.0),
    ];
    for (Map<String, dynamic> element in data) {
      lastYear[1][DateTime.parse(element[DBController.sessionTimeStart]).month - 1] += 1;
      if (element[DBController.ascendStyleId] <= getMaxAscendStyle()) {
        lastYear[0][DateTime.parse(element[DBController.sessionTimeStart]).month - 1] = max(
          lastYear[0][DateTime.parse(element[DBController.sessionTimeStart]).month - 1],
          GradeManager()
              .getGradeMapper(type: getClimbingType())
              .normalize(element[DBController.ascendGradeId])
              .toDouble(),
        );
      }
    }
    return lastYear;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended) {
    return YearBarChart(
      extended,
      preCalculatedData,
      AppLocalizations.of(context).charts_best,
      AppLocalizations.of(context).charts_amount,
      type: getClimbingType(),
      reorderMonths: from == null,
    );
  }

  @override
  ClimbingType getClimbingType() => ClimbingType.BOULDER;

  int getMaxAscendStyle() => 101;
}
