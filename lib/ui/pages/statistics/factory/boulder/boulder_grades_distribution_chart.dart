import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/grade_mapper/abstract_mapper.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/util/style_mapper.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/bar_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/climbing_grades_distribution_chart.dart';
import 'package:flutter/material.dart';

class BoulderGradesDistributionChart extends ClimbingGradesDistributionChart {
  @override
  ChartType getType() => ChartType.BOULDER;

  @override
  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT "
      "COUNT(CASE WHEN ${DBController.ascendStyleId} = 100 THEN 1 END) as Flash, "
      "COUNT(CASE WHEN ${DBController.ascendStyleId} = 101 THEN 1 END) as Top, "
      "COUNT(CASE WHEN ${DBController.ascendStyleId} = 102 THEN 1 END) as Project, "
      "${DBController.ascendGradeId} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
      "WHERE ${DBController.ascendType} == (?) AND "
      "((${DBController.sessionOutdoor} == (?) OR (?)) AND (?)) "
      "GROUP BY ${DBController.ascendGradeId} ",
      [
        getClimbingType().name,
        outdoor ? 1 : 0,
        (outdoor && indoor) ? 1 : 0,
        (outdoor || indoor) ? 1 : 0,
      ],
    );
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended) {
    return BarChart(
      preCalculatedData,
      extended,
      StyleMapper()
          .getMapping(context)
          .values
          .toList()
          .getRange(
            StyleMapper().getMapping(context).values.length - 3,
            StyleMapper().getMapping(context).values.length,
          )
          .toList(),
    );
  }

  @override
  List<List<dynamic>> calculateData(BuildContext context, input) {
    if (input.isEmpty) return [];

    AbstractGradeMapper mapper = GradeManager().getGradeMapper(type: getClimbingType());
    List<List<dynamic>> out = [mapper.getLabels().toSet().toList()];

    List<Map<String, int>> results = [];

    for (dynamic _ in StyleMapper().getMapping(context).values.toList().reversed.take(3)) {
      results.add(Map());
    }

    for (String grade in out[0]) {
      for (int i = 0; i < results.length; i++) {
        results[i][grade] = 0;
      }
    }

    for (Map<String, dynamic> element in input) {
      for (int i = 0; i < results.length; i++) {
        results[i][mapper.getGradeFromId(mapper.getRoundedKey(element[DBController.ascendGradeId]))] +=
            element[getMapping()[i]];
      }
    }

    for (Map value in results) {
      out.add(value.values.toList());
    }

    return out;
  }

  @override
  ClimbingType getClimbingType() => ClimbingType.BOULDER;

  @override
  List getMapping() {
    return const ["Flash", "Top", "Project"];
  }

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;
}
