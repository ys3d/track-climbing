import 'dart:math';

import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/year_combined_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LastYearSpeedChart extends AbstractChart {
  DateTime from;
  DateTime to;

  @override
  List<String> getTitleSelection(BuildContext context) {
    return [AppLocalizations.of(context).chart_past_months]..addAll(AbstractChart.yearList);
  }

  @override
  List<List<double>> calculateData(BuildContext context, input) {
    DateTime now = DateTime.now();
    DateTime fromUpdated = this.from ?? DateTime(now.year - 1, now.month + 1, 1, 0, 0, 0, 0, 0);
    DateTime toUpdated = this.to ?? now;

    List data = input
        .where((element) =>
            DateTime.parse(element[DBController.sessionTimeStart]).isAfter(fromUpdated) &&
            DateTime.parse(element[DBController.sessionTimeStart]).isBefore(toUpdated))
        .toList();

    List<List<double>> lastYear = [
      List.filled(12, 0.0),
      List.filled(12, 0.0),
    ];
    for (Map<String, dynamic> element in data) {
      DateTime dateTime = DateTime.parse(element[DBController.sessionTimeStart]);
      lastYear[1][dateTime.month - 1] += element["tries"].toDouble();

      if (lastYear[0][dateTime.month - 1] == 0.0) {
        lastYear[0][dateTime.month - 1] = element["time"] / 1000;
      } else {
        lastYear[0][dateTime.month - 1] = min(lastYear[0][dateTime.month - 1], element["time"] / 1000);
      }
    }
    return lastYear;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended) {
    return YearBarChart(
      extended,
      preCalculatedData,
      AppLocalizations.of(context).charts_best_time,
      AppLocalizations.of(context).charts_tries,
      type: getClimbingType(),
      reorderMonths: from == null,
      useGradeLabels: false,
      measureFormatting: AppLocalizations.of(context).util_seconds,
    );
  }

  @override
  Widget expanded() {
    return Container(
      key: UniqueKey(),
      padding: const EdgeInsets.only(right: 10, bottom: 20),
      child: getChart(extended: true),
    );
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context).chart_past_months;

  @override
  String getTitle(BuildContext context) {
    return Util.replaceStringWithBlankSpaces(
        getName(context) + " (${AppLocalizations.of(context).chart_page_comp_wall})");
  }

  @override
  ChartType getType() => ChartType.SPEED_CLIMBING;

  @override
  void loadData() {
    this.futureData = _fetchData();
  }

  Future<List<Map<String, dynamic>>> _fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.sessionTimeStart}, "
      "MIN(${DBController.speedTime}) as time, "
      "COUNT(${DBController.ascendId}) as tries "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
      "WHERE ${DBController.ascendType} == (?) AND "
      "${DBController.sessionStatus} != (?) AND "
      "${DBController.speedType} == (?) "
      "GROUP BY ${DBController.sessionId}",
      [getClimbingType().name, Data.SESSION_ACTIVE_STATE, 0],
    );
  }

  @override
  void onChangeTitleSelection(BuildContext context, String value) {
    from = value == getName(context) ? null : DateTime(int.parse(value), 1, 1, 0);
    to = value == getName(context) ? null : DateTime(int.parse(value), 12, 31, 23, 59);
  }

  ClimbingType getClimbingType() => ClimbingType.SPEED_CLIMBING;

  @override
  String getKey() => "past_moths_speed";

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;
}
