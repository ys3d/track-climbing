import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/enums.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/timeline_climbing.dart';
import 'package:flutter/material.dart';

class TimelineSpeedChart extends TimelineClimbingChart {
  @override
  ChartType getType() => ChartType.SPEED_CLIMBING;

  ClimbingType getClimbingType() => ClimbingType.SPEED_CLIMBING;

  @override
  String getKey() => "timeline_sport_speed";

  @override
  Classes getClassType(Map<String, dynamic> ascend) => Classes.SPEED;

  @override
  bool get showCheckBoxes => false;

  @override
  dynamic calculateData(BuildContext context, dynamic input) {
    if (input.isEmpty) return [];
    Map<String, Map<String, dynamic>> bestNormalSpeedRoutes = {};
    List<Map<String, dynamic>> allAscents = [];

    Map<String, dynamic> bestSpeedAscend = input.firstWhere(
      (element) => element[DBController.speedType] == 0,
      orElse: () => null,
    );
    if (bestSpeedAscend != null) allAscents.add(bestSpeedAscend);

    for (Map<String, dynamic> ascend in input) {
      if (ascend[DBController.speedType] == 0) {
        if (bestSpeedAscend != null && ascend[DBController.speedTime] < bestSpeedAscend[DBController.speedTime]) {
          bestSpeedAscend = ascend;
          if (bestSpeedAscend != null) allAscents.add(bestSpeedAscend);
        }
      } else {
        String name = ascend[DBController.ascendName];

        // if no name do skip
        if (name == "") continue;

        String key = name + "_" + ascend[DBController.sessionLocation];

        if (!bestNormalSpeedRoutes.containsKey(key)) {
          bestNormalSpeedRoutes[key] = ascend;
        } else if (bestNormalSpeedRoutes[key][DBController.speedTime] > ascend[DBController.speedTime]) {
          bestNormalSpeedRoutes[key] = ascend;
        }
      }
    }

    allAscents.addAll(bestNormalSpeedRoutes.values);
    allAscents.sort((a, b) => a[DBController.sessionTimeStart].compareTo(b[DBController.sessionTimeStart]));

    // if no data
    if (allAscents.isEmpty) return [];

    // calculate right data format list
    List<List<Map<String, dynamic>>> output = [];
    int sessionID = allAscents.first[DBController.sessionId];
    List<Map<String, dynamic>> _innerList = [];
    for (Map<String, dynamic> ascend in allAscents) {
      if (ascend[DBController.sessionId] != sessionID) {
        sessionID = ascend[DBController.sessionId];
        output.add(_innerList);
        _innerList = [];
      }
      _innerList.add(ascend);
    }

    if (_innerList.isNotEmpty) output.add(_innerList);
    return output;
  }
}
