import 'dart:convert';
import 'dart:math';

import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/data_points.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/line_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BestSpeedTimes extends AbstractChart {
  List<String> _titles = [""];
  List<String> _names = [""];
  List<String> _locations = [""];
  int _currentIndex = 0;
  bool hasComp = true;

  @override
  String getName(BuildContext context) => AppLocalizations.of(context).chart_speed_times;

  @override
  ChartType getType() => ChartType.SPEED_CLIMBING;

  @override
  void loadData() {
    this.futureData = _fetchCompData();
  }

  @override
  Widget chart(BuildContext context, dynamic preCalculatedData, extended) {
    return LineChart(
      preCalculatedData,
      extended,
      "${AppLocalizations.of(context).general_time}:",
      useGradeLabels: false,
      type: getClimbingType(),
      measureFormatting: AppLocalizations.of(context).util_seconds,
      valueToInt: false,
    );
  }

  @override
  Widget expanded() {
    return Container(
      key: UniqueKey(),
      child: getChart(extended: true),
    );
  }

  @override
  dynamic calculateData(BuildContext context, dynamic input) {
    if (input.first.isEmpty) return [];

    List<DateTimeDataPoint> data = [];
    for (var dataPoint in input[0]) {
      DateTime start = DateTime.parse(dataPoint[DBController.sessionTimeStart]);
      data.add(DateTimeDataPoint(start, dataPoint["time"] / 1000));
    }

    this._titles.clear();
    this._names.clear();
    this._locations.clear();

    if (hasComp) {
      this._titles.add(AppLocalizations.of(context).chart_page_comp_wall);
      this._names.add("");
      this._locations.add("");
    }

    Map<String, dynamic> noDuplicates = {};
    for (var element in input[1]) {
      noDuplicates["${element[DBController.ascendName].toString().toLowerCase().trim()} "
          "(${element[DBController.sessionLocation].toString().toLowerCase().trim()})"] = element;
    }

    for (var element in noDuplicates.values) {
      this._titles.add("${element[DBController.ascendName]} (${element[DBController.sessionLocation]})");
      this._names.add(element[DBController.ascendName]);
      this._locations.add(element[DBController.sessionLocation]);
    }

    return data;
  }

  Future<List<List<Map<String, dynamic>>>> _fetchCompData() async {
    List<Map<String, dynamic>> names = await _fetchOtherSpeedClimbingNames();
    List<Map<String, dynamic>> data = await DBController().rawQuery(
      "SELECT ${DBController.sessionTimeStart}, MIN(${DBController.speedTime}) as time, ${DBController.ascendName} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
      "WHERE ${DBController.ascendType} == (?) AND "
      "${DBController.speedType} == (?) AND "
      "${DBController.ascendName} == (?) AND "
      "(${DBController.sessionLocation} == (?) OR (?)) AND "
      "${DBController.sessionStatus} != (?) "
      "GROUP BY ${DBController.sessionId} "
      "ORDER BY ${DBController.sessionTimeStart} "
      "LIMIT (?)",
      [
        getClimbingType().name,
        (_currentIndex == 0 && hasComp) ? 0 : 1,
        _names[_currentIndex],
        _locations[_currentIndex],
        (_currentIndex == 0 && hasComp) ? 1 : 0,
        Data.SESSION_ACTIVE_STATE,
        DataStore().settingsDataStore.limitCharts ? Data.MAX_SESSION_AMOUNT : -1,
      ],
    );

    hasComp = (data.isNotEmpty || names.length == 0) && hasComp;

    if (!hasComp) {
      names = _removeDuplicates(names);
      data = await DBController().rawQuery(
        "SELECT ${DBController.sessionTimeStart}, MIN(${DBController.speedTime}) as time, ${DBController.ascendName} "
        "FROM ${DBController.ascendsTable} "
        "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
        "WHERE ${DBController.ascendType} == (?) AND "
        "${DBController.speedType} == (?) AND "
        "${DBController.ascendName} == (?) AND "
        "(${DBController.sessionLocation} == (?) OR (?)) AND "
        "${DBController.sessionStatus} != (?) "
        "GROUP BY ${DBController.sessionId} "
        "ORDER BY ${DBController.sessionTimeStart} "
        "LIMIT (?)",
        [
          getClimbingType().name,
          1,
          names[_currentIndex][DBController.ascendName],
          names[_currentIndex][DBController.sessionLocation],
          0,
          Data.SESSION_ACTIVE_STATE,
          DataStore().settingsDataStore.limitCharts ? Data.MAX_SESSION_AMOUNT : -1,
        ],
      );
    }

    return [data, names];
  }

  Future<List<Map<String, dynamic>>> _fetchOtherSpeedClimbingNames() async {
    return DBController().rawQuery(
      "SELECT ${DBController.ascendName}, ${DBController.sessionLocation} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
      "WHERE ${DBController.ascendType} == (?) AND "
      "${DBController.speedType} == (?) AND "
      "${DBController.ascendName} != '' AND "
      "${DBController.sessionStatus} != (?) "
      "ORDER BY ${DBController.sessionTimeStart}",
      [getClimbingType().name, 1, Data.SESSION_ACTIVE_STATE],
    );
  }

  ClimbingType getClimbingType() => ClimbingType.SPEED_CLIMBING;

  @override
  List<String> getTitleSelection(BuildContext context) {
    return this._titles;
  }

  @override
  void onChangeTitleSelection(BuildContext context, String value) {
    this._currentIndex = max(0, _titles.indexOf(value));
    loadData();
  }

  @override
  String getKey() => "speed_times";

  /// removes duplicates from list containing maps
  ///
  /// return duplicate free list
  List<Map<String, dynamic>> _removeDuplicates(List<Map<String, dynamic>> list) {
    final jsonList = list.map((item) => jsonEncode(item)).toList();

    // using toSet - toList strategy
    final uniqueJsonList = jsonList.toSet().toList();

    // convert each item back to the original form using JSON decoding
    final result = uniqueJsonList.map<Map<String, dynamic>>((item) => jsonDecode(item)).toList();
    return result;
  }

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;
}
