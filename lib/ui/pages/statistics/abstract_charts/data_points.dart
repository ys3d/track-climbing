class DateTimeDataPoint {
  final DateTime xPoint;
  final double yPoint;

  DateTimeDataPoint(this.xPoint, this.yPoint);
}

class DataPoint {
  final double xPoint;
  final double yPoint;

  DataPoint(this.xPoint, this.yPoint);
}
