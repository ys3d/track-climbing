import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AverageGradeChart extends StatefulWidget {
  final bool expanded;
  final List<double> data;
  final List<DateTime> dates;
  final ClimbingType climbingType;

  AverageGradeChart(this.expanded, this.data, this.dates, this.climbingType);

  @override
  _AverageGradeChartState createState() => _AverageGradeChartState();
}

class _AverageGradeChartState extends State<AverageGradeChart> {
  static const double Y_LABELS_INTERVAL = 4;
  bool _flatPoints = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (mounted) setState(() => this._flatPoints = false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return createLineChart();
  }

  Widget createLineChart() {
    return Stack(
      children: [
        LineChart(
          LineChartData(
            lineTouchData: LineTouchData(
              enabled: widget.expanded,
              touchTooltipData: LineTouchTooltipData(
                fitInsideHorizontally: true,
                fitInsideVertically: true,
                tooltipBgColor: Colors.grey.withOpacity(0.8),
                getTooltipItems: (List<LineBarSpot> touchedBarSpots) {
                  return touchedBarSpots.map((barSpot) {
                    return LineTooltipItem(
                      '${GradeManager().getGradeMapper(type: widget.climbingType).grades[barSpot.y.round()]}',
                      const TextStyle(color: Colors.white),
                    );
                  }).toList();
                },
              ),
              touchCallback: (FlTouchEvent event, LineTouchResponse touchResponse) {},
            ),
            borderData: FlBorderData(show: false),
            gridData: FlGridData(
              show: widget.expanded,
              drawVerticalLine: false,
              horizontalInterval: Y_LABELS_INTERVAL - 2,
            ),
            titlesData: FlTitlesData(
              show: widget.expanded,
              bottomTitles: AxisTitles(
                sideTitles: SideTitles(
                  interval: 1,
                  showTitles: widget.expanded,
                  reservedSize: 70,
                  getTitlesWidget: (double val, TitleMeta titleMeta) => Padding(
                    padding: EdgeInsets.only(top: 8),
                    child: RotatedBox(
                      quarterTurns: 1,
                      child: Text(
                        DateFormat("dd.MM.yy").format(widget.dates.reversed.elementAt(val.toInt())),
                        style: const TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                          fontSize: 12.0,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              rightTitles: AxisTitles(sideTitles: SideTitles(showTitles: false)),
              topTitles: AxisTitles(sideTitles: SideTitles(showTitles: false)),
              leftTitles: AxisTitles(
                sideTitles: SideTitles(
                  showTitles: true,
                  getTitlesWidget: (double val, TitleMeta titleMeta) => Text(
                    GradeManager().getGradeMapper(type: widget.climbingType).getLabels()[val.toInt()],
                    style: const TextStyle(
                      color: const Color(0xff67727d),
                      fontWeight: FontWeight.normal,
                      fontSize: 12,
                    ),
                  ),
                  interval: 5,
                  reservedSize: 24,
                  //margin: 12,
                ),
              ),
            ),
            minY: 0,
            maxY: GradeManager().getGradeMapper(type: widget.climbingType).maxNormalized.toDouble(),
            lineBarsData: [
              LineChartBarData(
                spots: List.generate(
                  widget.data.length,
                  (i) => FlSpot(i.toDouble(), _flatPoints ? 0 : widget.data.reversed.elementAt(i)),
                ),
                isCurved: false,
                gradient: LinearGradient(colors: [AppColors.chart_colors[0], AppColors.chart_colors[2]]),
                barWidth: Y_LABELS_INTERVAL,
                isStrokeCapRound: true,
                dotData: FlDotData(show: widget.expanded),
                belowBarData: BarAreaData(
                  show: widget.expanded,
                  gradient: LinearGradient(
                    colors:
                        [AppColors.chart_colors[0], AppColors.chart_colors[2]].map((e) => e.withOpacity(0.3)).toList(),
                  ),
                ),
              ),
            ],
          ),
          swapAnimationDuration: Duration(milliseconds: 500),
        ),
        //This container makes graph touchable, so it can be expanded
        if (!widget.expanded) Container(color: Colors.transparent)
      ],
    );
  }
}
