import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/enums.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/dialog/info_dialogs.dart';
import 'package:climbing_track/ui/widgets/empty_list_placeholder.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:timelines/timelines.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class TimeLineChart extends StatefulWidget {
  final bool activateTopropeLeadCheckBoxes;
  final bool showCheckBoxes;

  final List<List<Map<String, dynamic>>> data;

  TimeLineChart(this.data, {this.activateTopropeLeadCheckBoxes = false, this.showCheckBoxes = true});

  @override
  _TimeLineChartState createState() => _TimeLineChartState();
}

class _TimeLineChartState extends State<TimeLineChart> {
  bool indoor = true;
  bool outdoor = true;
  bool toprope = true;
  bool lead = true;

  @override
  Widget build(BuildContext context) {
    List<List<Map<String, dynamic>>> filteredData = List.generate(
        widget.data.length,
        (index) => widget.data[index]
            .where((elem) =>
                ((elem[DBController.sessionOutdoor] == 1 && outdoor) ||
                    (elem[DBController.sessionOutdoor] == 0 && indoor)) &&
                ((elem[DBController.routeTopRope] == 1 && toprope) || (elem[DBController.routeTopRope] == 0 && lead)))
            .toList()).where((listElement) => listElement.isNotEmpty).toList();

    return Column(
      children: [
        if (widget.showCheckBoxes) ...[
          _checkBoxSettings(),
          Divider(thickness: 1.5, height: 1.5),
        ],
        filteredData.isEmpty
            ? Padding(

                padding: EdgeInsets.only(top: 40.0),
                child: EmptyListPlaceHolder(
                  text: AppLocalizations.of(context).charts_timeline_no_data,
                  image: "assets/images/3327053.png",
                ),
              )
            : Expanded(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.only(top: widget.showCheckBoxes ? 8.0 : 16.0, left: 12),
                    child: _OuterTimeline(
                      data: filteredData,
                    ),
                  ),
                ),
              ),
      ],
    );
  }

  Widget _checkBoxSettings() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 6.0),
      child: Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        direction: Axis.horizontal,
        spacing: 4.0,
        runSpacing: 0.0,
        children: [
          if (widget.activateTopropeLeadCheckBoxes) ...[
            _checkBox(
              text: AppLocalizations.of(context).style_toprope,
              value: toprope,
              onChanged: (val) => setState(() => toprope = val),
            ),
            _verticalDivider(),
            _checkBox(
              text: AppLocalizations.of(context).style_lead,
              value: lead,
              onChanged: (val) => setState(() => lead = val),
            ),
            _verticalDivider(),
          ],
          _checkBox(
            text: AppLocalizations.of(context).general_indoor,
            value: indoor,
            onChanged: (val) => setState(() => indoor = val),
          ),
          _verticalDivider(),
          _checkBox(
            text: AppLocalizations.of(context).general_outdoor,
            value: outdoor,
            onChanged: (val) => setState(() => outdoor = val),
          ),
        ],
      ),
    );
  }

  Widget _checkBox({@required String text, @required bool value, @required ValueChanged<bool> onChanged}) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Checkbox(
          activeColor: Colors.blue,
          value: value,
          onChanged: onChanged,
          visualDensity: VisualDensity.compact,
        ),
        Text(text),
      ],
    );
  }

  Widget _verticalDivider() {
    return Container(
      margin: const EdgeInsets.only(left: 4.0),
      height: 20.0,
      width: 0.0,
      decoration: const BoxDecoration(
        border: Border.symmetric(vertical: BorderSide(color: Colors.black87, width: 0.5)),
      ),
    );
  }
}

class _OuterTimeline extends StatelessWidget {
  final List<List<Map<String, dynamic>>> data;

  const _OuterTimeline({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FixedTimeline.tileBuilder(
      theme: TimelineThemeData(
        nodePosition: 0,
        color: Color(0xff989898),
        indicatorTheme: IndicatorThemeData(
          position: 0,
          size: 20.0,
        ),
        connectorTheme: ConnectorThemeData(
          thickness: 1.7,
        ),
      ),
      builder: TimelineTileBuilder.connected(
        itemCount: data.length,
        contentsBuilder: (_, index) {
          return Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  _getSessionTitle(data[index].first),
                  style: DefaultTextStyle.of(context).style.copyWith(
                        fontSize: 18.0,
                      ),
                ),
                _InnerTimeline(ascents: data[index]),
              ],
            ),
          );
        },
        indicatorBuilder: (_, index) {
          if (data[index].first[DBController.sessionOutdoor] == 1) {
            return DotIndicator(
              size: 24.0,
              color: Colors.green,
              child: Icon(
                FontAwesomeIcons.mountainSun,
                color: Colors.white,
                size: 12.0,
              ),
            );
          } else {
            return DotIndicator(
              size: 24.0,
              color: Colors.orangeAccent,
              child: Icon(
                Icons.home,
                color: Colors.white,
                size: 18.0,
              ),
            );
          }
        },
        connectorBuilder: (_, index, ___) => SolidLineConnector(),
        lastConnectorBuilder: (_) => SolidLineConnector(),
      ),
    );
  }

  String _getSessionTitle(Map<String, dynamic> ascend) {
    String date = DateFormat("dd.MM.yy").format(DateTime.parse(ascend[DBController.sessionTimeStart]));
    String location = ascend[DBController.sessionLocation];
    return "$date | $location";
  }
}

class _InnerTimeline extends StatelessWidget {
  const _InnerTimeline({
    @required this.ascents,
  });

  final List<Map<String, dynamic>> ascents;

  @override
  Widget build(BuildContext context) {
    bool isEdgeIndex(int index) {
      return index == 0 || index == ascents.length + 1;
    }

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: FixedTimeline.tileBuilder(
        theme: TimelineTheme.of(context).copyWith(
          nodePosition: 0,
          connectorTheme: TimelineTheme.of(context).connectorTheme.copyWith(
                thickness: 1.0,
              ),
          indicatorTheme: TimelineTheme.of(context).indicatorTheme.copyWith(
                size: 10.0,
                position: 0.5,
              ),
        ),
        builder: TimelineTileBuilder(
          indicatorBuilder: (_, index) => !isEdgeIndex(index)
              ? _getIndicator(Util.mapStyleToStyleType(ascents[index - 1][DBController.ascendStyleId]))
              : null,
          startConnectorBuilder: (_, index) => Connector.solidLine(space: 24.0),
          endConnectorBuilder: (_, index) => Connector.solidLine(space: 24.0),
          contentsBuilder: (_, index) {
            if (isEdgeIndex(index)) {
              return null;
            }

            return Padding(
              padding: EdgeInsets.only(left: 8.0),
              child: InkWell(
                onTap: () => InfoDialog().showAscendDialog(context, Ascend.fromMap(ascents[index - 1])),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${_getHeadline(ascents[index - 1], context)}:",
                      style: TextStyle(fontWeight: FontWeight.w500),
                      overflow: TextOverflow.ellipsis,
                    ),
                    Row(
                      children: [
                        Flexible(
                          child: Text(
                            Util.getAscendNameWithoutPrefix(
                              ascents[index - 1][DBController.ascendName],
                              context,
                              speedType: ascents[index - 1][DBController.speedType],
                              type: EnumToString.fromString(
                                ClimbingType.values,
                                ascents[index - 1][DBController.ascendType],
                              ),
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        const SizedBox(width: 6.0),
                        Text(_getSummary(ascents[index - 1], context)),
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
          itemExtentBuilder: (_, index) => isEdgeIndex(index) ? 10.0 : 40.0,
          nodeItemOverlapBuilder: (_, index) => isEdgeIndex(index) ? true : null,
          itemCount: ascents.length + 2,
        ),
      ),
    );
  }

  Widget _getIndicator(StyleType styleType) {
    Widget child;
    switch (styleType) {
      case StyleType.ONSIGHT:
        child = Icon(
          Icons.visibility_outlined,
          size: 20.0,
        );
        break;
      case StyleType.FLASH:
        child = Icon(
          Icons.flash_on_outlined,
          size: 18.0,
        );
        break;
      case StyleType.REDPOINT:
        child = Icon(
          Icons.adjust,
          size: 18.0,
        );
        break;
      case StyleType.TOP:
        child = Icon(
          FontAwesomeIcons.circleUp,
          size: 16,
        );
        break;
      case StyleType.PROJECT:
        child = Icon(
          Icons.construction,
          size: 18.0,
        );
        break;
      case StyleType.NONE:
        break;
    }
    return DotIndicator(
      size: 24.0,
      color: Colors.transparent,
      child: child,
    );
  }

  String _getHeadline(Map<String, dynamic> ascend, BuildContext context) {
    ClimbingType type = EnumToString.fromString(ClimbingType.values, ascend[DBController.ascendType]);

    switch (type) {
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.ICE_CLIMBING:
        bool toprope = ascend[DBController.routeTopRope] == 1;
        return Util.mapStyleToStyleType(ascend[DBController.ascendStyleId]).toTranslatedString(context) +
            " (${Util.getTopOrLead(toprope, context)})";
      case ClimbingType.BOULDER:
      case ClimbingType.FREE_SOLO:
      case ClimbingType.DEEP_WATER_SOLO:
        return Util.mapStyleToStyleType(ascend[DBController.ascendStyleId]).toTranslatedString(context);
      case ClimbingType.SPEED_CLIMBING:
        int speedTime = ascend[DBController.speedTime];
        return AppLocalizations.of(context).general_time +
            ": " +
            (speedTime / 1000).toStringAsFixed(2) +
            " " +
            AppLocalizations.of(context).util_seconds;
    }
    return Util.mapStyleToStyleType(ascend[DBController.ascendStyleId]).toTranslatedString(context);
  }

  String _getSummary(Map<String, dynamic> ascend, BuildContext context) {
    int gradeID = ascend[DBController.ascendGradeId];
    ClimbingType type = EnumToString.fromString(ClimbingType.values, ascend[DBController.ascendType]);

    switch (type) {
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.BOULDER:
      case ClimbingType.FREE_SOLO:
      case ClimbingType.DEEP_WATER_SOLO:
        return "(${Util.getGrade(gradeID, type)})";
      case ClimbingType.SPEED_CLIMBING:
        if (ascend[DBController.speedType] != 0) {
          return "(${Util.getGrade(gradeID, type)})";
        }
        return "";
    }
    return "";
  }
}
