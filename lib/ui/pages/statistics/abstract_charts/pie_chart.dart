import 'package:climbing_track/app/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CustomPieChart extends StatelessWidget {
  final bool expanded;
  final Map<String, double> data;
  final List<Color> colors;

  CustomPieChart(this.expanded, this.data, {this.colors});

  @override
  Widget build(BuildContext context) {
    return createPieChar(context);
  }

  Widget createPieChar(BuildContext context) {
    return PieChart(
      legendOptions: LegendOptions(
        showLegends: expanded,
        legendPosition: LegendPosition.bottom,
        showLegendsInRow: true,
      ),
      chartValuesOptions: ChartValuesOptions(
        showChartValues: expanded,
        showChartValuesOutside: true,
        showChartValuesInPercentage: true,
      ),
      dataMap: data,
      chartType: ChartType.ring,
      ringStrokeWidth: expanded ? 60.0 : 15.0,
      colorList: colors == null
          ? (data.keys.any((val) => val.contains(AppLocalizations.of(context).general_others))
              ? AppColors.chart_colors.take(data.length - 1).toList() + [Colors.grey]
              : AppColors.chart_colors.take(data.length).toList())
          : colors,
    );
  }
}
