import 'dart:math';

import 'package:climbing_track/backend/model/data.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'data_points.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class DateTimeBarChart extends StatefulWidget {
  final List<List<dynamic>> data;
  final bool expanded;
  final List<String> titles;
  final List<Color> colors;

  DateTimeBarChart(this.data, this.expanded, this.titles, this.colors);

  @override
  _DateTimeBarChartState createState() => _DateTimeBarChartState();
}

class _DateTimeBarChartState extends State<DateTimeBarChart> {
  static const int STANDARD_LENGTH = Data.MAX_SESSION_AMOUNT;
  List<List<DateTimeDataPoint>> dataPoints;
  List<bool> hasType = [];

  @override
  void initState() {
    super.initState();
    _calculateDataPoints();
  }

  @override
  Widget build(BuildContext context) {
    return _createBarChart();
  }

  void _calculateDataPoints() {
    dataPoints = [];
    for (int n = 0; n < widget.data.length - 1; n++) {
      dataPoints.add([]);
      hasType.add(false);
    }

    for (int i = 0; i < widget.data[0].length; i++) {
      for (int n = 0; n < widget.data.length - 1; n++) {
        dataPoints[n].add(DateTimeDataPoint(widget.data[0].elementAt(i), widget.data[n + 1].elementAt(i)));
        hasType[n] = hasType[n] || widget.data[n + 1].elementAt(i) > 0;
      }
    }
  }

  List<charts.Series<DateTimeDataPoint, String>> _getSeriesData() {
    return dataPoints
        .where((element) => hasType[dataPoints.indexOf(element)])
        .map(
          (e) => charts.Series<DateTimeDataPoint, String>(
            id: widget.titles[dataPoints.indexOf(e)],
            domainFn: (DateTimeDataPoint value, _) => DateFormat("dd.MM.yy").format(value.xPoint),
            measureFn: (DateTimeDataPoint value, _) => value.yPoint,
            data: e,
            colorFn: (_, __) => charts.ColorUtil.fromDartColor(widget.colors[dataPoints.indexOf(e)]),
          ),
        )
        .toList();
  }

  Widget _createBarChart() {
    return charts.BarChart(
      _getSeriesData(),
      layoutConfig: widget.expanded
          ? null
          : charts.LayoutConfig(
              leftMarginSpec: charts.MarginSpec.fixedPixel(0),
              topMarginSpec: charts.MarginSpec.fixedPixel(0),
              rightMarginSpec: charts.MarginSpec.fixedPixel(0),
              bottomMarginSpec: charts.MarginSpec.fixedPixel(0),
            ),
      animate: true,
      defaultInteractions: widget.expanded,
      behaviors: widget.expanded
          ? [
              charts.SeriesLegend(
                cellPadding: EdgeInsets.only(right: 10.0),
                desiredMaxColumns: 1,
                desiredMaxRows: 8,
                horizontalFirst: true,
                showMeasures: true,
              ),
              charts.InitialSelection(
                selectedDataConfig: List.generate(
                  dataPoints.length,
                  (index) => charts.SeriesDatumConfig<String>(
                    widget.titles[index],
                    DateFormat("dd.MM.yy").format(dataPoints[index].last.xPoint),
                  ),
                ),
              ),
              //charts.SlidingViewport(),
              charts.PanAndZoomBehavior()
            ]
          : [],
      primaryMeasureAxis: new charts.NumericAxisSpec(
          renderSpec: widget.expanded ? charts.GridlineRendererSpec() : charts.NoneRenderSpec()),
      domainAxis: new charts.OrdinalAxisSpec(
        viewport: charts.OrdinalViewport(
          DateFormat("dd.MM.yy").format(widget.data[0][max(0, widget.data[0].length - STANDARD_LENGTH)]),
          min(STANDARD_LENGTH, widget.data[0].length),
        ),
        renderSpec: widget.expanded ? charts.SmallTickRendererSpec(labelRotation: 75) : charts.NoneRenderSpec(),
      ),
      defaultRenderer: new charts.BarRendererConfig(groupingType: charts.BarGroupingType.stacked),
    );
  }
}
