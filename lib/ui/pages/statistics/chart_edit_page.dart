import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/pages/statistics/chart_page.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:highlighter_coachmark/highlighter_coachmark.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ChartEditPage extends StatefulWidget {
  const ChartEditPage({Key key, this.elements, this.order}) : super(key: key);

  @override
  State<ChartEditPage> createState() => _ChartEditPageState(order.map((e) => Item(elements[e], e)).toList());

  final List<AbstractChart> elements;
  final List<int> order;
}

class Item {
  final AbstractChart chart;
  final int number;

  Item(this.chart, this.number);
}

class _ChartEditPageState extends State<ChartEditPage> {
  final List<Item> _items;
  GlobalKey _tipKey = GlobalObjectKey("tipKey");

  _ChartEditPageState(this._items);

  @override
  Widget build(BuildContext context) {
    //showing tooltip
    if (this._tipKey != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) => _showToolTip());
    }

    return WillPopScope(
      onWillPop: () async {
        await _storeNewValues();
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.accentColor,
          leading: const BackButton(color: Colors.white),
          title: Text(
            AppLocalizations.of(context).chart_page_setting_title,
            style: TextStyle(color: Colors.white, fontSize: 22),
          ),
          actions: [
            IconButton(
              color: Colors.white,
              tooltip: AppLocalizations.of(context).general_reset,
              onPressed: () {
                this._items.sort((a, b) => a.number.compareTo(b.number));
                setState(() {});
                ScaffoldMessenger.of(context)
                  ..removeCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(
                      behavior: SnackBarBehavior.floating,
                      content: Text(AppLocalizations.of(context).chart_page_reset_chart_order),
                    ),
                  );
              },
              icon: Icon(Icons.refresh),
            ),
          ],
        ),
        body: _createReorderableList(),
      ),
    );
  }

  Widget _createReorderableList() {
    return ReorderableListView(
      key: _tipKey,
      children: <Widget>[
        for (int index = 0; index < _items.length; index++)
          ListTile(
            key: Key('$index'),
            title: Text(
              _items[index].chart.getTitle(context).replaceAll(Util.NULL_REPLACEMENT, ""),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style:Theme.of(context).textTheme.bodyText1,
            ),
            leading: Checkbox(
              activeColor: Colors.blue,
              value: _items[index].chart.visible,
              onChanged: (bool value) {
                setState(() {
                  _items[index].chart.setVisibility(value);
                });
              },
            ),
            trailing: Icon(Icons.menu),
          ),
      ],
      onReorder: (int oldIndex, int newIndex) {
        setState(() {
          if (oldIndex < newIndex) {
            newIndex -= 1;
          }
          final Item item = _items.removeAt(oldIndex);
          _items.insert(newIndex, item);
        });
      },
    );
  }

  Future<void> _storeNewValues() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String order = _items.map((e) => e.number).join("_");
    prefs.setString(ChartPage.ORDER_KEY, order);
  }

  void _showToolTip() {
    if (!DataStore().toolTipsDataStore.tooltipChartSettings) return;
    DataStore().toolTipsDataStore.tooltipChartSettings = false;

    CoachMark coachMarkFAB = CoachMark();
    RenderBox target = this._tipKey.currentContext.findRenderObject();

    Rect markRect = target.localToGlobal(Offset.zero) & target.size;
    markRect = Rect.fromCenter(center: markRect.center, width: markRect.width * 0, height: markRect.height * 0);

    coachMarkFAB.show(
      targetContext: this._tipKey.currentContext,
      markRect: markRect,
      markShape: BoxShape.rectangle,
      children: [
        Center(
          child: Text(
            AppLocalizations.of(context).chart_page_settings_tooltip,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 24.0,
              fontStyle: FontStyle.italic,
              color: Colors.white,
            ),
          ),
        ),
      ],
      duration: const Duration(seconds: 5),
    );
  }
}
