import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:climbing_track/ui/dialog/info_dialogs.dart';
import 'package:climbing_track/backend/overview_data_loader.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OverViewStats extends StatelessWidget {
  OverViewStats({Key key, this.dataMap, this.overAllData, this.speedData, this.outdoor, this.loading = false})
      : super(key: key);

  final Map<Classes, Map<StyleType, Map<String, dynamic>>> dataMap;
  final Map<String, dynamic> overAllData;
  final List<Map<String, dynamic>> speedData;
  final bool outdoor;
  final bool loading;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.zero,
      child: _createOverview(context),
    );
  }

  Widget _createOverview(BuildContext context) {
    return Column(
      children: [
        if (loading) const LinearProgressIndicator(),
        if (loading) const SizedBox(height: Constants.STANDARD_PADDING - 4),
        if (!loading) const SizedBox(height: Constants.STANDARD_PADDING),
        _paddingWidget(_createSummaryWidget(context)),
        const SizedBox(height: Constants.STANDARD_PADDING),
        _divider(),
        _bestAscendsExpansionTile(
          context: context,
          title: AppLocalizations.of(context).overview_best_lead,
          type: Classes.LEAD,
          styleTypes: [StyleType.ONSIGHT, StyleType.FLASH, StyleType.REDPOINT, StyleType.TOP],
        ),
        _divider(),
        _bestAscendsExpansionTile(
          context: context,
          title: AppLocalizations.of(context).overview_best_toprope,
          type: Classes.TOPROPE,
          styleTypes: [StyleType.ONSIGHT, StyleType.FLASH, StyleType.REDPOINT, StyleType.TOP],
        ),
        _divider(),
        _bestAscendsExpansionTile(
          context: context,
          title: AppLocalizations.of(context).overview_best_boulder,
          type: Classes.BOULDER,
          styleTypes: [StyleType.FLASH, StyleType.TOP, StyleType.PROJECT],
        ),
        _divider(),
        _speedExpansionTile(context: context),
        _divider(),
        _bestAscendsExpansionTile(
          context: context,
          title: AppLocalizations.of(context).overview_best_dws,
          type: Classes.WATER,
          styleTypes: [StyleType.ONSIGHT, StyleType.FLASH, StyleType.REDPOINT],
        ),
        _divider(),
        _bestAscendsExpansionTile(
          context: context,
          title: AppLocalizations.of(context).overview_best_ice,
          type: Classes.ICE,
          styleTypes: [StyleType.ONSIGHT, StyleType.FLASH, StyleType.REDPOINT, StyleType.TOP],
        ),
        _divider(),
        _bestAscendsExpansionTile(
          context: context,
          title: AppLocalizations.of(context).overview_best_free_solo,
          type: Classes.FREE,
          styleTypes: [StyleType.TOP],
        ),
        const SizedBox(height: 60),
      ],
    );
  }

  Widget _divider() {
    return const Divider(
      height: 2,
      thickness: 2.0,
      indent: Constants.STANDARD_PADDING,
      endIndent: Constants.STANDARD_PADDING,
    );
  }

  Widget _paddingWidget(Widget child) {
    return Container(
      padding: const EdgeInsets.fromLTRB(Constants.STANDARD_PADDING, 0, Constants.STANDARD_PADDING, 0),
      alignment: Alignment.center,
      child: child,
    );
  }

  Widget _createSummaryWidget(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppLocalizations.of(context).overview_total_sessions,
                    style: Theme.of(context).textTheme.subtitle1.copyWith(height: 0.0),
                  ),
                  Text(
                    overAllData[OverviewDataLoader.TOTAL_SESSIONS_KEY].toString(),
                    style: Theme.of(context).textTheme.subtitle1.copyWith(
                          color: AppColors.color1,
                          height: 0.0,
                        ),
                  ),
                ],
              ),
            ),
            const SizedBox(width: 30.0),
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppLocalizations.of(context).overview_total_ascents,
                    style: Theme.of(context).textTheme.subtitle1.copyWith(height: 0.0),
                  ),
                  Text(
                    overAllData[OverviewDataLoader.TOTAL_ASCENDS_KEY].toString(),
                    style: Theme.of(context).textTheme.subtitle1.copyWith(
                          color: AppColors.color1,
                          height: 0.0,
                        ),
                  ),
                ],
              ),
            ),
          ],
        ),
        const SizedBox(height: 15),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppLocalizations.of(context).overview_total_onsight_percentage,
                    style: Theme.of(context).textTheme.subtitle1.copyWith(height: 0.0),
                  ),
                  Text(
                    overAllData[OverviewDataLoader.TOTAL_SPORT_ASCENDS_KEY] == 0
                        ? "0%"
                        : "${(overAllData[OverviewDataLoader.TOTAL_ONSIGHT_KEY] / overAllData[OverviewDataLoader.TOTAL_SPORT_ASCENDS_KEY] * 100).toStringAsFixed(2)}%",
                    style: Theme.of(context).textTheme.subtitle1.copyWith(
                          color: AppColors.color1,
                          height: 0.0,
                        ),
                  ),
                ],
              ),
            ),
            const SizedBox(width: 30.0),
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppLocalizations.of(context).overview_total_meters,
                    style: Theme.of(context).textTheme.subtitle1.copyWith(height: 0.0),
                  ),
                  Text(
                    Util.getHeightWithShortUnit(overAllData[OverviewDataLoader.TOTAL_HEIGHT_KEY]),
                    style: Theme.of(context).textTheme.subtitle1.copyWith(
                          color: AppColors.color1,
                          height: 0.0,
                        ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _bestAscendsExpansionTile(
      {@required BuildContext context,
      @required String title,
      @required Classes type,
      @required List<StyleType> styleTypes}) {
    return Theme(
      data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
      child: ExpansionTile(
        textColor: Colors.black,
        iconColor: Colors.black54,
        tilePadding: const EdgeInsets.symmetric(horizontal: Constants.STANDARD_PADDING),
        childrenPadding: const EdgeInsets.only(bottom: 15.0),
        initiallyExpanded: overAllData[OverviewDataLoader.EXPANDED_VALUES_KEY][type],
        title: Center(
          child: Text(
            title,
            style: Theme.of(context).textTheme.subtitle1.copyWith(
                  height: 0.0,
                  fontSize: 18,
                ),
          ),
        ),
        onExpansionChanged: (expanded) => SharedPreferences.getInstance().then(
            (prefs) => prefs.setBool("expandend_value_${outdoor ? "out" : "in"}_${type.name.toLowerCase()}", expanded)),
        children: List.generate(
          styleTypes.length,
          (index) => _createBestAscendContainer(context, index.isEven, type, styleTypes[index]),
        ),
      ),
    );
  }

  Widget _createBestAscendContainer(BuildContext context, bool isDark, Classes type, StyleType style) {
    return Container(
      padding: const EdgeInsets.fromLTRB(Constants.STANDARD_PADDING, 4, Constants.STANDARD_PADDING, 4),
      color: isDark ? Colors.grey.shade200 : Colors.transparent,
      child: _createBestAscendListTile(context, type, style),
    );
  }

  Widget _createBestAscendListTile(BuildContext context, Classes type, StyleType style) {
    Text styleText = Text(
      "${style.toTranslatedString(context)}:",
      style: Theme.of(context).textTheme.bodyText1.copyWith(
            letterSpacing: 0.0,
            wordSpacing: 0.0,
            height: 0.0,
          ),
    );

    if (dataMap[type][style] == null) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          styleText,
          Text("---", style: Theme.of(context).textTheme.bodyText1),
        ],
      );
    }

    TextStyle textStyle = Theme.of(context).textTheme.bodyText1.copyWith(
          fontWeight: FontWeight.w500,
          letterSpacing: 0.0,
          wordSpacing: 0.0,
          height: 0.0,
        );

    Ascend ascend = Ascend.fromMap(dataMap[type][style]);

    String grade = GradeManager().getGradeMapper(type: ascend.type).getGradeFromId(ascend.gradeID);

    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () => InfoDialog().showAscendDialog(context, ascend),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          styleText,
          const SizedBox(width: 14.0),
          Flexible(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Flexible(
                      child: Text(
                        Util.getAscendNameWithoutPrefix(
                          ascend.name,
                          context,
                          speedType: ascend.speedType,
                          type: ascend.type,
                        ),
                        overflow: TextOverflow.ellipsis,
                        style: textStyle,
                      ),
                    ),
                    Text(" ($grade)", style: textStyle),
                  ],
                ),
                Text(
                  Util.getLocationDateString(
                    dataMap[type][style][DBController.sessionTimeStart],
                    ascend.location,
                  ),
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.bodyText2.copyWith(
                        fontSize: 12,
                        letterSpacing: 0.0,
                        wordSpacing: 0.0,
                        height: 0.0,
                      ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _speedExpansionTile({@required BuildContext context}) {
    return Theme(
      data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
      child: ExpansionTile(
        textColor: Colors.black,
        iconColor: Colors.black54,
        tilePadding: const EdgeInsets.symmetric(horizontal: Constants.STANDARD_PADDING),
        childrenPadding: const EdgeInsets.only(bottom: 15.0),
        initiallyExpanded: overAllData[OverviewDataLoader.EXPANDED_VALUES_KEY][Classes.SPEED],
        onExpansionChanged: (expanded) => SharedPreferences.getInstance().then((prefs) =>
            prefs.setBool("expandend_value_${outdoor ? "out" : "in"}_${Classes.SPEED.name.toLowerCase()}", expanded)),
        title: Center(
          child: Text(AppLocalizations.of(context).overview_best_speed_comp, style: TextStyle(fontSize: 18)),
        ),
        children: List.generate(
          3,
          (index) {
            Widget styleText = Text("${index + 1}.", style: const TextStyle(fontWeight: FontWeight.w400));
            if (speedData.length <= index || speedData[index] == null) {
              return Container(
                padding: const EdgeInsets.fromLTRB(Constants.STANDARD_PADDING, 4, Constants.STANDARD_PADDING, 4),
                color: index.isEven ? Colors.grey.shade200 : Colors.transparent,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [styleText, const Text("---")],
                ),
              );
            }
            TextStyle textStyle = Theme.of(context).textTheme.bodyText1.copyWith(
              fontWeight: FontWeight.w500,
              letterSpacing: 0.0,
              wordSpacing: 0.0,
              height: 0.0,
            );
            Ascend ascend = Ascend.fromMap(speedData[index]);

            return Container(
              padding: const EdgeInsets.fromLTRB(Constants.STANDARD_PADDING, 4, Constants.STANDARD_PADDING, 4),
              color: index.isEven ? Colors.grey.shade200 : Colors.transparent,
              child: InkWell(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onTap: () => InfoDialog().showAscendDialog(context, ascend),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    styleText,
                    const SizedBox(width: 14.0),
                    Flexible(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            "${(speedData[index][DBController.speedTime] / 1000).toStringAsFixed(2)} "
                            "${AppLocalizations.of(context).util_seconds}",
                            style: textStyle,
                          ),
                          Text(
                            Util.getLocationDateString(
                              speedData[index][DBController.sessionTimeStart],
                              ascend.location,
                            ),
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
