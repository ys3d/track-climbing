import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:climbing_track/backend/overview_data_loader.dart';
import 'package:climbing_track/ui/pages/overview/overview_stats.dart';
import 'package:flutter/material.dart';
import 'package:highlighter_coachmark/highlighter_coachmark.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OverviewPage extends StatefulWidget {
  OverviewPage({Key key}) : super(key: key);

  @override
  _OverviewPageState createState() => _OverviewPageState();
}

class _OverviewPageState extends State<OverviewPage> {
  static const int INITIALIZE_PAGE = 0;
  final PageController _controller = PageController(initialPage: INITIALIZE_PAGE);
  final GlobalKey _globalKey = GlobalObjectKey("overview_page_key");
  int currentPage = INITIALIZE_PAGE;
  OverviewDataLoader _overviewDataLoader;
  bool loading = true;

  @override
  void initState() {
    super.initState();
    _overviewDataLoader = OverviewDataLoader(
      onCompleted: () {
        if (mounted) {
          this.loading = false;
          setState(() {});
        }
      },
    );
    this._overviewDataLoader.init();
  }

  @override
  Widget build(BuildContext context) {
    if (_globalKey != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) => _showToolTip());
    }
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.accentColor,
        title: getTitle(),
      ),
      body: _createPageView(),
    );
  }

  Widget _createPageView() {
    return PageView(
      onPageChanged: (index) => setState(() {
        this.currentPage = index;
      }),
      controller: _controller,
      children: [
        OverViewStats(
          key: UniqueKey(),
          dataMap: _overviewDataLoader.calculatedData[0],
          overAllData: _overviewDataLoader.overAllData[0],
          speedData: _overviewDataLoader.speedData,
          outdoor: false,
          loading: loading,
        ),
        OverViewStats(
          key: UniqueKey(),
          dataMap: _overviewDataLoader.calculatedData[1],
          overAllData: _overviewDataLoader.overAllData[1],
          speedData: _overviewDataLoader.speedData,
          outdoor: true,
          loading: loading,
        ),
      ],
    );
  }

  Widget getTitle() {
    switch (this.currentPage) {
      case 0:
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(fit: FlexFit.tight, child: const Text("")),
            Flexible(
              fit: FlexFit.tight,
              child: Text(
                AppLocalizations.of(context).general_indoor.toUpperCase(),
                style: TextStyle(color: Colors.white, fontSize: 22),
                textAlign: TextAlign.center,
              ),
            ),
            Flexible(
              fit: FlexFit.tight,
              child: InkWell(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onTap: () => _controller.animateToPage(1, curve: Curves.easeIn, duration: Duration(milliseconds: 200)),
                child: Row(
                  key: this._globalKey,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      AppLocalizations.of(context).general_outdoor.toLowerCase(),
                      style: TextStyle(color: Colors.white, fontSize: 16),
                      textAlign: TextAlign.end,
                    ),
                    const Icon(Icons.arrow_right, color: Colors.white)
                  ],
                ),
              ),
            ),
          ],
        );
      case 1:
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              fit: FlexFit.tight,
              child: InkWell(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onTap: () => _controller.animateToPage(0, curve: Curves.easeIn, duration: Duration(milliseconds: 200)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Icon(Icons.arrow_left, color: Colors.white),
                    Text(
                      AppLocalizations.of(context).general_indoor.toLowerCase(),
                      style: TextStyle(color: Colors.white, fontSize: 16),
                      textAlign: TextAlign.start,
                    )
                  ],
                ),
              ),
            ),
            Flexible(
              fit: FlexFit.tight,
              child: Text(
                AppLocalizations.of(context).general_outdoor.toUpperCase(),
                style: TextStyle(color: Colors.white, fontSize: 22),
                textAlign: TextAlign.center,
              ),
            ),
            Flexible(
              fit: FlexFit.tight,
              child: const Text(""),
            ),
          ],
        );
    }
    return Container();
  }

  void _showToolTip() {
    if (!DataStore().toolTipsDataStore.tooltipOverviewPage) return;
    DataStore().toolTipsDataStore.tooltipOverviewPage = false;

    CoachMark coachMarkFAB = CoachMark();
    RenderBox target = _globalKey.currentContext.findRenderObject();

    Rect markRect = target.localToGlobal(Offset.zero) & target.size;
    markRect = Rect.fromCenter(
      center: markRect.center,
      width: markRect.width * 1.25,
      height: markRect.height * 2.0,
    );

    coachMarkFAB.show(
      targetContext: _globalKey.currentContext,
      markRect: markRect,
      markShape: BoxShape.rectangle,
      children: [
        Padding(
          padding: EdgeInsets.all(Constants.STANDARD_PADDING),
          child: Center(
            child: Text(
              AppLocalizations.of(context).overview_page_tooltip,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 24.0,
                fontStyle: FontStyle.italic,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
      duration: null,
    );
  }
}
