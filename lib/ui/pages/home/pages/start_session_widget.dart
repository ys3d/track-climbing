import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/model/height_measurement.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:climbing_track/ui/pages/home/widgets/filtered_text_field_with_list.dart';
import 'package:climbing_track/util/notifications_handler.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/Picker.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class StartSessionWidget extends StatefulWidget {
  const StartSessionWidget({Key key}) : super(key: key);

  @override
  _StartSessionWidgetState createState() => _StartSessionWidgetState();
}

class _StartSessionWidgetState extends State<StartSessionWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Align(
          child: Text(
            AppLocalizations.of(context).home_page_environment,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          alignment: Alignment.centerLeft,
        ),
        const SizedBox(height: Constants.STANDARD_PADDING),
        _createIndoorOutdoorSwitch(context),
        const SizedBox(height: Constants.STANDARD_PADDING),
        const Divider(thickness: 2.0),
        const SizedBox(height: Constants.STANDARD_PADDING),
        _createHeightPicker(context),
        const SizedBox(height: Constants.STANDARD_PADDING),
        const Divider(thickness: 2.0),
        const SizedBox(height: Constants.STANDARD_PADDING),
        Align(
          child: Text(
            "${AppLocalizations.of(context).general_location}:",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          alignment: Alignment.centerLeft,
        ),
        const SizedBox(height: Constants.STANDARD_PADDING),
        _createLocationInput(),
        const SizedBox(height: Constants.STANDARD_PADDING),
        const Divider(thickness: 2.0),
        const SizedBox(height: Constants.STANDARD_PADDING),
        _createButtons(context),
        const SizedBox(height: 40.0),
      ],
    );
  }

  Widget _createIndoorOutdoorSwitch(BuildContext context) {
    return ToggleSwitch(
      initialLabelIndex: Data().getCurrentSession().outdoor ? 1 : 0,
      minWidth: MediaQuery.of(context).size.width * 0.35,
      minHeight: 45.0,
      cornerRadius: 20.0,
      activeBgColor: [Colors.green],
      activeFgColor: Colors.white,
      inactiveBgColor: AppColors.accentColor2,
      inactiveFgColor: Colors.white,
      labels: [
        AppLocalizations.of(context).general_indoor.toUpperCase(),
        AppLocalizations.of(context).general_outdoor.toUpperCase()
      ],
      onToggle: (index) {
        Data().getCurrentSession().outdoor = index == 1;
      },
      totalSwitches: 2,
    );
  }

  Widget _createHeightPicker(BuildContext context) {
    return Column(
      children: [
        Align(
          child: Text(
            AppLocalizations.of(context).home_page_standard_route_length,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          alignment: Alignment.centerLeft,
        ),
        const SizedBox(height: 14),
        InkWell(
          borderRadius: BorderRadius.circular(24.0),
          onTap: () => _showPickerNumber(context),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 2.0),
                    child: Text(
                      Util.getHeightString(Data().getCurrentSession().standardHeight),
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(color: Colors.black26),
                    ),
                    color: Colors.white,
                  ),
                ),
                const SizedBox(width: 25),
                Padding(
                  padding: const EdgeInsets.only(bottom: 3.0),
                  child: Text(
                    DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET
                        ? AppLocalizations.of(context)
                            .general_feet(Util.heightFromMeters(Data().getCurrentSession().standardHeight).round())
                        : AppLocalizations.of(context)
                            .general_meter(Util.heightFromMeters(Data().getCurrentSession().standardHeight).round()),
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _createLocationInput() {
    return FilteredTextFieldWithList(onChanged: () => setState(() {}));
  }

  Widget _createButtons(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        MaterialButton(
          minWidth: 160,
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 14.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ),
          elevation: 0.0,
          color: AppColors.accentColor,
          textColor: Colors.white,
          onPressed: () => {startSession(context)},
          child: Text(
            AppLocalizations.of(context).general_start.toUpperCase(),
            style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 24, color: AppColors.textSecondary),
          ),
        ),
        const SizedBox(height: 10.0),
        MaterialButton(
          minWidth: 160,
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 14.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
            side: const BorderSide(color: Colors.black26),
          ),
          highlightColor: AppColors.accentColor.withOpacity(0.3),
          elevation: 0.0,
          onPressed: () => SessionState().setState(SessionStateEnum.none),
          child: Text(
            AppLocalizations.of(context).general_cancel.toUpperCase(),
            style: Theme.of(context).textTheme.bodyText1.copyWith(
                  fontSize: 24,
                  color: AppColors.accentColor,
                ),
          ),
        ),
      ],
    );
  }

  void startSession(BuildContext context) async {
    if (await Data().startSession(context)) {
      try {
        await NotificationHandler().scheduleReminderNotification(context);
      } on Exception catch (e) {
        print(e);
      }
      SessionState().nextState();
    } else {
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            behavior: SnackBarBehavior.floating,
            content: Text(AppLocalizations.of(context).home_page_enter_location_hint),
          ),
        );
    }
  }

  void _showPickerNumber(BuildContext context) {
    dynamic height = Util.heightFromMeters(Data().getCurrentSession().standardHeight);
    int meters = height.floor();
    int cm = ((height % 1) * 10).floor();
    if (DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET) {
      new Picker(
        cancelTextStyle: TextStyle(
          color: AppColors.accentColor,
          fontSize: 18,
        ),
        confirmTextStyle: TextStyle(
          color: AppColors.accentColor,
          fontSize: 18,
        ),
        adapter: NumberPickerAdapter(
          data: [
            NumberPickerColumn(begin: 0, end: 999),
          ],
        ),
        delimiter: [
          PickerDelimiter(
            column: 1,
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  AppLocalizations.of(context).general_feet(2),
                  style: TextStyle(fontWeight: FontWeight.normal),
                ),
              ),
            ),
          ),
        ],
        selecteds: [height],
        hideHeader: false,
        onConfirm: (Picker picker, List value) {
          setState(() {
            Data().getCurrentSession().standardHeight = Util.heightToMeters(value[0].toDouble());
          });
        },
      ).showModal(context);
    } else {
      new Picker(
        cancelTextStyle: TextStyle(
          color: AppColors.accentColor,
          fontSize: 18,
        ),
        confirmTextStyle: TextStyle(
          color: AppColors.accentColor,
          fontSize: 18,
        ),
        adapter: NumberPickerAdapter(
          data: [
            NumberPickerColumn(begin: 0, end: 999),
            NumberPickerColumn(items: [0, 5], onFormatValue: (val) => val.toString().padRight(2, "0")),
          ],
        ),
        delimiter: [
          PickerDelimiter(
            column: 1,
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: Align(
                alignment: Alignment.centerLeft,
                child: const Text("m", style: TextStyle(fontWeight: FontWeight.normal)),
              ),
            ),
          ),
          PickerDelimiter(
            column: 3,
            child: Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: Align(
                alignment: Alignment.centerLeft,
                child: const Text("cm", style: TextStyle(fontWeight: FontWeight.normal)),
              ),
            ),
          ),
        ],
        selecteds: [meters, cm],
        hideHeader: false,
        onConfirm: (Picker picker, List value) {
          setState(() {
            double result = value[0] + (value[1] * 0.5);
            Data().getCurrentSession().standardHeight = result;
          });
        },
      ).showModal(context);
    }
  }
}
