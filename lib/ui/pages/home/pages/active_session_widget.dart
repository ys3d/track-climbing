import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:climbing_track/ui/pages/home/widgets/save_button.dart';
import 'package:climbing_track/ui/widgets/edit_ascend_widget.dart';
import 'package:climbing_track/ui/widgets/reorderable_ascents_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ActiveSessionWidget extends StatefulWidget {
  final ScrollController scrollController;

  const ActiveSessionWidget({Key key, this.scrollController}) : super(key: key);

  @override
  _ActiveSessionWidgetState createState() => _ActiveSessionWidgetState();
}

class _ActiveSessionWidgetState extends State<ActiveSessionWidget> {
  Future<List<Map<String, dynamic>>> _ascentsFutureData;
  final Data data = Data();
  Function listener;
  Session sessionWithListener;

  @override
  void initState() {
    super.initState();
    listener = () {
      if (this.mounted) {
        _ascentsFutureData = this._loadRoutes();
        setState(() {});
      }
    };
    _ascentsFutureData = this._loadRoutes();
    this.sessionWithListener = data.getCurrentSession();
    this.sessionWithListener.addListener(listener);
  }

  @override
  void dispose() {
    super.dispose();
    if (sessionWithListener != null) {
      sessionWithListener.removeListener(listener);
    }
  }

  @override
  Widget build(BuildContext context) {
    final bool isKeyboardVisible = KeyboardVisibilityProvider.isKeyboardVisible(context);

    return Column(
      children: [
        EditAscendWidget(ascend: data.currentAscend),
        if (isKeyboardVisible) ...[
          const SizedBox(height: Constants.STANDARD_PADDING),
          SaveButton(scrollController: widget.scrollController),
          const SizedBox(height: Constants.STANDARD_PADDING),
          const Padding(
            padding: const EdgeInsets.all(Constants.STANDARD_PADDING),
            child: Divider(thickness: 2.0),
          ),
        ],
        Visibility(
          visible: Data().getCurrentSession().routeCount > 0,
          child: Theme(
            data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
            child: ExpansionTile(
              initiallyExpanded: true,
              iconColor: Colors.black54,
              title: Text(
                AppLocalizations.of(context).home_page_ascents,
                style: Theme.of(context).textTheme.headline6.copyWith(height: 0.0),
              ),
              children: [ascendList()],
            ),
          ),
        ),
        const SizedBox(height: 120.0),
      ],
    );
  }

  Widget ascendList() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: Constants.STANDARD_PADDING),
      child: FutureBuilder(
        future: this._ascentsFutureData,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Container(
              child: SizedBox(height: 200, child: CircularProgressIndicator()),
            );
          }
          return ReorderableAscentsList(
            data: snapshot.data,
            onDelete: (Map ascend) => _deleteAscend(ascend, snapshot.data),
            onReAdd: (int index, Map ascend) => _onReAdd(index, ascend, snapshot.data),
            onReorder: (int oldIndex, int newIndex) => onReorder(snapshot.data, oldIndex, newIndex),
          );
        },
      ),
    );
  }

  bool _deleteAscend(Map ascend, List<Map> data) {
    data.remove(ascend);
    return Data().removeRouteFromCurrentSession(ascend);
  }

  void _onReAdd(int index, Map ascend, List<Map> data) {
    data.insert(index, ascend);
    Data().insertRouteToCurrentSession(index, ascend);
  }

  void onReorder(List<Map> data, int oldIndex, int newIndex) {
    _updateAscendsReorder();
  }

  void _updateAscendsReorder() async {
    // update ascend
    for (Map ascend in await _ascentsFutureData) {
      DBController().updateAscend(ascend, ascend[DBController.ascendId]);
    }
  }

  Future<List<Map<String, dynamic>>> _loadRoutes() async {
    int sessionId = Data().getCurrentSession().sessionId;
    List<Map<String, dynamic>> res = await DBController().queryAscendsBySessionId(sessionId ?? -999);
    return List.generate(res.length, (index) => Map.of(res[index]));
  }
}
