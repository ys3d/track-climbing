import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/model/enums.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:climbing_track/ui/widgets/session_summary.dart';
import 'package:climbing_track/util/screenshot.dart';
import 'package:climbing_track/view_model/best_performance_view_model.dart';
import 'package:climbing_track/view_model/session_view_model.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class EndSessionWidget extends StatelessWidget {
  final ScreenshotController _screenshotController;

  EndSessionWidget(this._screenshotController);

  final BestPerformanceViewModel bestPerformanceViewModel = BestPerformanceViewModel(
    indoor: !Data().getCurrentSession().outdoor,
    lastSessionId: Data().getCurrentSession().sessionId,
  );

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback(
        (_) => bestPerformanceViewModel.calculateBestPerformances().then((List<ResultTriple> data) {
              if (data.isNotEmpty) this.showBestPerformancesDialog(context, data);
            }));
    return Column(
      children: [
        SessionSummary(
          SessionViewModel(
            sessionDat: Data().getCurrentSession().toMap(Data.SESSION_CLOSED_STATE),
            screenshotController: _screenshotController,
          ),
        ),
        const Divider(thickness: 2.0, indent: Constants.STANDARD_PADDING, endIndent: Constants.STANDARD_PADDING),
        const SizedBox(height: Constants.STANDARD_PADDING),
        _createConfirmButton(context),
        const SizedBox(height: 40.0),
      ],
    );
  }

  /// Create "confirm" button to close session summary
  /// Goes back to session create page
  /// Clear Session on Button press
  /// Changes state on button press
  Widget _createConfirmButton(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
        padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 14.0),
        backgroundColor: AppColors.accentColor,
        foregroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
      ),
      onPressed: () => {Data().clearSession(), SessionState().nextState()},
      child: Text(
        AppLocalizations.of(context).general_confirm.toUpperCase(),
        style: Theme.of(context).textTheme.headline6.copyWith(color: AppColors.textSecondary),
      ),
    );
  }

  /// shows dialog containing info about all new best performances ascends
  /// For example if you crushed new best grade in onsight, lead. This ascend will be displayed
  void showBestPerformancesDialog(BuildContext context, List<ResultTriple> data) {
    // set up the button
    Widget okButton = TextButton(
      child: Text(AppLocalizations.of(context).okay),
      onPressed: () => Navigator.of(context).pop(),
    );

    // set up dialog title
    String titleString;
    if (data.length > 1) {
      titleString = AppLocalizations.of(context).new_best_performances;
    } else {
      titleString = AppLocalizations.of(context).new_best_performance;
    }
    Widget title = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Icon(Icons.info_outline, color: Colors.green, size: 56),
        const SizedBox(width: 16.0),
        Flexible(child: Text(titleString)),
      ],
    );

    // set up content content
    List<Widget> children = [];
    for (ResultTriple dataPoint in data) {
      // style + class
      String contentString = dataPoint.classStyle.toTranslatedString(context);
      if (dataPoint.classStyle != Classes.SPEED) {
        contentString += " - ";
        contentString += dataPoint.styleType.toTranslatedString(context);
      }
      contentString += ":";
      children.add(
        Row(
          children: [
            Icon(Icons.star, size: 24, color: Colors.amber.shade600),
            const SizedBox(width: 12.0),
            Text(contentString, style: TextStyle(fontWeight: FontWeight.w500)),
          ],
        ),
      );

      // ascends
      // ascend name
      String ascendName = Util.getAscendName(
        context,
        dataPoint.ascend[DBController.ascendName],
        type: EnumToString.fromString(ClimbingType.values, dataPoint.ascend[DBController.ascendType]),
        speedType: dataPoint.ascend[DBController.speedType],
      );
      children.add(
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(width: 24.0),
            const Icon(Icons.arrow_right, size: 24),
            const SizedBox(width: 2.0),
            Flexible(child: Text(ascendName)),
          ],
        ),
      );

      // ascend info
      children.add(
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(width: 24.0),
            const Icon(Icons.arrow_right, size: 24),
            const SizedBox(width: 2.0),
            Flexible(child: Text(Util.getDataSummary(dataPoint.ascend, context))),
          ],
        ),
      );

      // padding
      children.add(const SizedBox(height: 20.0));
    }

    Widget content = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: children,
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: title,
      content: SingleChildScrollView(
        child: content,
      ),
      contentPadding: EdgeInsets.all(Constants.STANDARD_PADDING),
      scrollable: false,
      titleTextStyle: Theme.of(context).textTheme.subtitle1.copyWith(fontSize: 20),
      contentTextStyle: Theme.of(context).textTheme.bodyText2.copyWith(fontWeight: FontWeight.w400),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
