import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class FilteredTextFieldWithList extends StatefulWidget {
  final Function onChanged;

  FilteredTextFieldWithList({Key key, this.onChanged}) : super(key: key);

  @override
  _FilteredTextFieldWithListState createState() => new _FilteredTextFieldWithListState();
}

class _FilteredTextFieldWithListState extends State<FilteredTextFieldWithList> {
  static const int MAX_ITEM_AMOUNT = 4;
  final TextEditingController editingController = TextEditingController();
  final Session session = Data().getCurrentSession();
  final Data data = Data();
  List<String> locations;
  List<String> items = [];

  @override
  void initState() {
    super.initState();
    Data().getCurrentSession().addListener(_updateLocations);
    _updateLocations();
  }

  @override
  void dispose() {
    super.dispose();
    Data().getCurrentSession().removeListener(_updateLocations);
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> entries = [];
    for (String location in items.take(MAX_ITEM_AMOUNT)) {
      entries.add(getRowEntry(location));
    }
    return Container(
      child: Column(
        children: <Widget>[
          createTextField(),
          const SizedBox(height: 8.0),
          Column(children: entries),
        ],
      ),
    );
  }

  void _updateLocations() {
    Data().getCurrentSession().outdoor
        ? locations = Data().getOutdoorLocations()
        : locations = Data().getIndoorLocations();
    items = [];
    filterSearchResults(editingController.text.trim());
  }

  void filterSearchResults(String query) {
    List<String> dummySearchList = [];
    dummySearchList.addAll(locations);
    if (query.isNotEmpty) {
      List<String> dummyListData = [];
      dummySearchList.forEach((item) {
        if (item.toLowerCase().contains(query.toLowerCase())) {
          dummyListData.add(item);
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(locations);
      });
    }
  }

  Widget getRowEntry(String text) {
    return Padding(
      padding: const EdgeInsets.only(top: 12.0),
      child: InkWell(
        onTap: () => {
          editingController.text = text,
          filterSearchResults(text),
          session.setLocationNameAndUpdateStandardHeight(text).then((value) => widget.onChanged())
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Icon(Icons.query_builder, color: AppColors.accentColor2),
            const SizedBox(width: 8.0),
            Flexible(
                child: Text(
                  text,
                  style: Theme.of(context).textTheme.bodyText2,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.clip,
                ),
                fit: FlexFit.loose),
            const SizedBox(width: 8.0),
            const Icon(Icons.north_west, color: AppColors.accentColor2),
          ],
        ),
      ),
    );
  }

  Widget createTextField() {
    return Theme(
      data: ThemeData.from(
        colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor),
        useMaterial3: false,
      ),
      child: TextFormField(
        textAlign: TextAlign.start,
        textAlignVertical: TextAlignVertical.center,
        controller: editingController,
        style: TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
        onChanged: (value) {
          filterSearchResults(value.trim());
          session.setLocationNameAndUpdateStandardHeight(value.trim()).then((value) => widget.onChanged());
        },
        textCapitalization: TextCapitalization.words,
        decoration: new InputDecoration(
          isDense: true,
          labelText: AppLocalizations.of(context).home_page_enter_location,
          labelStyle: TextStyle(fontSize: 18, color: AppColors.accentColor2),
          fillColor: AppColors.accentColor,
          border: OutlineInputBorder(
            borderRadius: new BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
            borderSide: new BorderSide(),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
            borderRadius: BorderRadius.circular(
              Constants.STANDARD_BORDER_RADIUS,
            ),
          ),
        ),
      ),
    );
  }

  Future<void> checkLocationForStandardHeight(String locationName) async {}
}
