import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/dialog/confirm_ascend_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SaveButton extends StatelessWidget {
  final ConfirmAscendDialog confirmDialog = ConfirmAscendDialog();
  final ScrollController scrollController;

  SaveButton({Key key, this.scrollController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: ElevatedButton.icon(
        style: ElevatedButton.styleFrom(
          elevation: 6.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50.0),
          ),
          padding: const EdgeInsets.only(left: 24.0, top: 12.0, right: 24.0, bottom: 12.0),
          backgroundColor: Colors.black,
        ),
        onPressed: () async {
          String routeName = Util.getAscendName(
            context,
            Data().currentAscend.name,
            type: Data().currentAscend.type,
            speedType: Data().currentAscend.speedType,
          );
          bool result = await confirmDialog.showConfirmAscendDialog(context, Data().currentAscend.copy());
          if (result != null && result) {
            this.scrollController.animateTo(0.0, duration: Duration(milliseconds: 500), curve: Curves.ease);
            ScaffoldMessenger.of(context)
              ..removeCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                  behavior: SnackBarBehavior.floating,
                  content: Data().currentAscend.isBoulder()
                      ? Text(AppLocalizations.of(context).home_page_added_boulder(routeName))
                      : Text(AppLocalizations.of(context).home_page_added_route(routeName)),
                  duration: Duration(seconds: 2),
                ),
              );
          }
        },
        icon: const Icon(Icons.save, color: Colors.white, size: 26),
        label: Text(
          AppLocalizations.of(context).general_save.toUpperCase(),
          style: Theme.of(context).textTheme.headline6.copyWith(
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
        ),
      ),
    );
  }
}
