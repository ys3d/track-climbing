import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:climbing_track/ui/dialog/info_dialogs.dart';
import 'package:climbing_track/ui/pages/home/widgets/scroll_picker/scroll_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class TypePickerDialog {
  final List<ClimbingType> items;
  final Function(ClimbingType) onChange;
  ClimbingType value;

  TypePickerDialog({
    @required this.items,
    @required this.value,
    @required this.onChange,
  });

  Future<void> showMyDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppLocalizations.of(context).home_page_climbing_type,
                style: Theme.of(context).textTheme.headline6,
              ),
              Padding(
                padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
                child: InkWell(
                  onTap: () => InfoDialog().showBoulderRouteInfoDialog(context),
                  child: IconTheme(
                    data: IconThemeData(color: AppColors.accentColor2),
                    child: Icon(Icons.info_outline),
                  ),
                ),
              ),
            ],
          ),
          content: Container(
            height: MediaQuery.of(context).size.height * 0.5,
            width: MediaQuery.of(context).size.width * 0.6,
            child: ScrollPicker(
              initialValue: value,
              onChanged: (val) => this.value = val,
              items: items,
              itemFormatter: (BuildContext context, ClimbingType type) => type.toName(context),
            ),
          ),
          actions: <Widget>[
            OutlinedButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(AppLocalizations.of(context).general_cancel),
              style: OutlinedButton.styleFrom(
                foregroundColor: AppColors.accentColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
            ),
            TextButton(
              style: TextButton.styleFrom(
                padding: EdgeInsets.symmetric(horizontal: 18, vertical: 8),
                backgroundColor: AppColors.accentColor,
                foregroundColor: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
              child: Text(AppLocalizations.of(context).general_save),
              onPressed: () {
                onChange.call(value);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
