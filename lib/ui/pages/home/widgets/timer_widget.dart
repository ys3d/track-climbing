import 'package:climbing_track/backend/util/custom_timer.dart';
import 'package:flutter/material.dart';

class TimerWidget extends StatefulWidget {
  @override
  _TimerWidgetState createState() => _TimerWidgetState();
}

class _TimerWidgetState extends State<TimerWidget> with WidgetsBindingObserver{
  CustomTimer customTimer = CustomTimer();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    customTimer.startTimer(callBack);
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
    customTimer.stopTimer();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      customTimer.stopTimer();
      setState(() {
        customTimer = CustomTimer();
        customTimer.startTimer(callBack);
      });

    }
  }


  @override
  Widget build(BuildContext context) {
    return Text(
      "${customTimer.hoursStr}:${customTimer.minutesStr}:${customTimer.secondsStr}",
      style: Theme.of(context).textTheme.bodyText1.copyWith(color: Colors.white),
    );
  }

  void callBack() {
    setState(() {});
  }
}
