import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

abstract class LocalConstants {
  static const double CIRCLE_WIDTH_VALUE = 220;
  static const double CIRCLE_HEIGHT_VALUE = 220;
  static const double RECTANGLE_HEIGHT_VALUE = 80;
  static const double CIRCLE_OPACITY = 0.8;
  static const double RECTANGLE_OPACITY = 0.8;
  static const double CIRCLE_BORDER_RADIUS = 1000.0;
}

class SessionButton extends StatefulWidget {
  @override
  _SessionButtonState createState() => _SessionButtonState();
}

class _SessionButtonState extends State<SessionButton> with SingleTickerProviderStateMixin {
  final Duration animatedDuration = Duration(milliseconds: Constants.STANDARD_ANIMATION_TIME);
  double _width = LocalConstants.CIRCLE_WIDTH_VALUE;
  double _height = LocalConstants.CIRCLE_HEIGHT_VALUE;
  BoxShape animatedShape = BoxShape.rectangle;
  Alignment animatedAlign = Alignment.center;
  double opacity = LocalConstants.CIRCLE_OPACITY;
  BorderRadius _borderRadius = BorderRadius.all(Radius.circular(LocalConstants.CIRCLE_BORDER_RADIUS));
  List<BoxShadow> _boxShadow = [
    BoxShadow(color: Colors.black26, blurRadius: 5.0, offset: Offset(5.0, 5.0), spreadRadius: 3.0)
  ];

  @override
  Widget build(BuildContext context) => AnimatedAlign(
        duration: animatedDuration,
        alignment: animatedAlign,
        child: AnimatedContainer(
          width: _width == 0 ? MediaQuery.of(context).size.width : _width,
          height: _height,
          child: InkWell(
              onTap: () {
                setState(() {
                  _width = MediaQuery.of(context).size.width;
                  _height = LocalConstants.RECTANGLE_HEIGHT_VALUE;
                  animatedShape = BoxShape.rectangle;
                  animatedAlign = Alignment.topCenter;
                  opacity = LocalConstants.RECTANGLE_OPACITY;
                  _borderRadius = BorderRadius.zero;
                  _boxShadow = [];
                });
              },
              child: Center(
                child: getStartSessionText(),
              )),
          decoration: BoxDecoration(
              shape: animatedShape,
              color: AppColors.accentColor.withOpacity(LocalConstants.CIRCLE_OPACITY),
              boxShadow: _boxShadow,
              borderRadius: _borderRadius),
          duration: animatedDuration,
          curve: Curves.fastOutSlowIn,
          onEnd: () => {
            Data().createSession(),
            SessionState().nextState(),
          },
        ),
      );

  Widget getStartSessionText() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          AppLocalizations.of(context).home_page_session_start.toUpperCase(),
          style: Theme.of(context).textTheme.headline3.copyWith(
                color: Colors.white,
                letterSpacing: 0.0,
                wordSpacing: 0.0,
                height: 0.0,
              ),
        ),
        Text(
          AppLocalizations.of(context).home_page_session.toUpperCase(),
          style: Theme.of(context).textTheme.headline6.copyWith(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: AppColors.textSecondary,
                letterSpacing: 0.0,
                wordSpacing: 0.0,
                height: 0.0,
              ),
        )
      ],
    );
  }
}
