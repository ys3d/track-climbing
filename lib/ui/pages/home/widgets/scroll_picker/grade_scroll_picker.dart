import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:flutter/material.dart';

class GradeScrollPicker extends StatefulWidget {
  const GradeScrollPicker({
    Key key,
    @required this.initialValue,
    @required this.onChanged,
    this.showDivider: true,
    this.ascend,
  }) : super(key: key);

  // Events
  final ValueChanged<String> onChanged;
  final String initialValue;
  final bool showDivider;
  final Ascend ascend;

  @override
  State<GradeScrollPicker> createState() => _GradeScrollPickerState(
        selectedValue: initialValue,
        ascend: ascend,
      );
}

class _GradeScrollPickerState extends State<GradeScrollPicker> {
  // Constants
  static const double itemWidth = 70.0;

  // Variables

  double widgetHeight;
  String selectedValue;
  ScrollController scrollController;
  List<String> items;

  _GradeScrollPickerState({@required this.selectedValue, @required Ascend ascend}) {
    this.items = GradeManager().getGradeMapper(type: ascend.type).getGradesForPicker();
    int initialItem = items.indexOf(selectedValue);
    scrollController = FixedExtentScrollController(initialItem: initialItem);
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    TextStyle defaultStyle = themeData.textTheme.bodyText1;
    TextStyle selectedStyle = themeData.textTheme.bodyText1.copyWith(fontWeight: FontWeight.bold);

    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        widgetHeight = constraints.maxHeight;

        return GestureDetector(
          onTapUp: _itemTapped,
          child: ListWheelScrollView.useDelegate(
            childDelegate: ListWheelChildBuilderDelegate(builder: (BuildContext context, int index) {
              if (index < 0 || index > items.length - 1) {
                return null;
              }

              var value = items[index];

              final TextStyle itemStyle = (value == selectedValue) ? selectedStyle : defaultStyle;

              return Center(
                child: RotatedBox(
                  quarterTurns: 1,
                  child: Text(
                    value,
                    style: itemStyle,
                    textAlign: TextAlign.center,
                  ),
                ),
              );
            }),
            controller: scrollController,
            itemExtent: itemWidth,
            onSelectedItemChanged: _onSelectedItemChanged,
          ),
        );
      },
    );
  }

  void _itemTapped(TapUpDetails details) {
    Offset position = details.localPosition;
    double center = widgetHeight / 2;
    double changeBy = position.dy - center;
    double newPosition = scrollController.offset + changeBy;

    // animate to and center on the selected item
    scrollController.animateTo(newPosition, duration: Duration(milliseconds: 500), curve: Curves.easeInOut);
  }

  void _onSelectedItemChanged(int index) {
    String newValue = items[index];
    if (newValue != selectedValue) {
      widget.onChanged(newValue);
      setState(() {
        selectedValue = newValue;
      });
    }
  }
}
