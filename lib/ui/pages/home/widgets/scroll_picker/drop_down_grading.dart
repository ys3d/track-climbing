import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:flutter/material.dart';

class DropDownGrading extends StatefulWidget {
  final ClimbingType type;
  final Function callBack;

  const DropDownGrading({Key key, @required this.type, @required this.callBack}) : super(key: key);

  @override
  _DropDownGradingState createState() => _DropDownGradingState();
}

class _DropDownGradingState extends State<DropDownGrading> {
  List<String> systems;
  String _value;

  @override
  void initState() {
    super.initState();
    systems = GradeManager().getGradeMapper(type: widget.type).getGradeSystems();
    _value = GradeManager().getGradeMapper(type: widget.type).gradePickerSystem;
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      borderRadius: BorderRadius.circular(20.0),
      underline: Container(),
      onChanged: (String value) {
        setState(() {
          _value = value;
        });
        GradeManager().getGradeMapper(type: widget.type).gradePickerSystem = value;
        widget.callBack();
      },
      value: _value,
      isDense: true,
      style: Theme.of(context).textTheme.bodyText2,
      items: List<DropdownMenuItem<String>>.generate(
        systems.length,
        (index) => DropdownMenuItem(
          value: systems[index],
          child: Text(systems[index]),
        ),
      ),
    );
  }
}
