import 'dart:math';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/util/style_mapper.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:climbing_track/ui/dialog/info_dialogs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class StylePicker extends StatefulWidget {
  final ValueChanged<int> onChangedStyle;
  final ValueChanged<int> onChangedTries;
  final Ascend ascend;

  const StylePicker({
    Key key,
    @required this.onChangedStyle,
    @required this.onChangedTries,
    @required this.ascend,
  }) : super(key: key);

  @override
  _StylePickerState createState() {
    if (ascend.tries == null) onChangedTries(Ascend.getMinTriesValue(ascend.styleID));
    return _StylePickerState(
      selectedStyle: ascend.styleID,
      tries: ascend.tries ?? Ascend.getMinTriesValue(ascend.styleID),
    );
  }
}

class _StylePickerState extends State<StylePicker> {
  int selectedStyle;
  int tries;
  ClimbingType type;
  List<Ascend> existingAscents = [];
  Function listener;

  @override
  void initState() {
    super.initState();
    this.type = widget.ascend.type;

    listener = () async {
      existingAscents = await Data().findExistingAscents(this.widget.ascend);
      if (mounted) setState(() {});
    };
    this.widget.ascend.addListener(listener);

    WidgetsBinding.instance.addPostFrameCallback((_) => listener());
  }

  @override
  void dispose() {
    this.widget.ascend.removeListener(listener);
    super.dispose();
  }

  _StylePickerState({@required this.selectedStyle, @required this.tries});

  @override
  Widget build(BuildContext context) {
    // added this, because selectedCategory wasn't updated from outer
    if (this.type != widget.ascend.type) {
      this.type = widget.ascend.type;
      this.selectedStyle = widget.ascend.styleID;
    }
    return _createStylePicker(context);
  }

  Widget _createStylePicker(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 10.0, left: 0.0, right: 0.0, bottom: 0.0),
      child: Column(
        children: <Widget>[
          Container(
            child: Wrap(
              runSpacing: Constants.STANDARD_PADDING,
              spacing: Constants.STANDARD_PADDING,
              children: _createButtons(context),
            ),
          ),
          // 100 for boulder flash
          if (selectedStyle > 2 && selectedStyle != 100) _createTriesPicker(context),
          if (widget.ascend.name.isNotEmpty && (selectedStyle == 3 || selectedStyle == 101)) ...[
            const SizedBox(height: 8.0),
            _createExistingAscentsInfo(),
          ],
        ],
      ),
    );
  }

  List<Widget> _createButtons(BuildContext context) {
    var styles;
    switch (type) {
      case ClimbingType.BOULDER:
        styles = StyleMapper().getBoulderMapping(context);
        break;
      case ClimbingType.DEEP_WATER_SOLO:
        styles = StyleMapper().getDeepWaterMapping(context);
        break;
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.SPORT_CLIMBING:
      // Don't have Styles
      case ClimbingType.FREE_SOLO:
      case ClimbingType.SPEED_CLIMBING:
      default:
        styles = StyleMapper().getSportClimbingMapping(context);
    }

    List<Widget> widgets = [];
    for (var style in styles) {
      widgets.add(
        InkWell(
          borderRadius: BorderRadius.circular(24.0),
          splashColor: Colors.transparent,
          onTap: () {
            selectedStyle = style.id;
            tries = Ascend.getMinTriesValue(selectedStyle);
            setState(() {});
            widget.onChangedStyle(style.id);
            widget.onChangedTries(tries);
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 12.0),
            decoration: BoxDecoration(
              color: selectedStyle == style.id ? AppColors.accentColor : Colors.grey[300],
              borderRadius: BorderRadius.all(Radius.circular(48.0)),
            ),
            child: Text(
              style.style,
              style: selectedStyle == style.id
                  ? Theme.of(context).textTheme.bodyText2.copyWith(color: Colors.white, fontSize: 16)
                  : Theme.of(context).textTheme.bodyText2.copyWith(fontSize: 16),
            ),
          ),
        ),
      );
    }
    return widgets;
  }

  Widget _createTriesPicker(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            borderRadius: BorderRadius.circular(20.0),
            onTap: () {
              tries = max(Ascend.getMinTriesValue(selectedStyle), tries - 1);
              setState(() {});
              widget.onChangedTries(tries);
            },
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 6.0),
              child: Icon(Icons.remove),
            ),
          ),
          Flexible(
            child: Text(
              "$tries  ${_getTriesString(context, selectedStyle, tries)}",
              style: Theme.of(context).textTheme.bodyText1,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              textAlign: TextAlign.center,
            ),
          ),
          InkWell(
            borderRadius: BorderRadius.circular(20.0),
            onTap: () {
              tries = min(999, tries + 1);
              setState(() {});
              widget.onChangedTries(tries);
            },
            child: const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 6.0),
              child: Icon(Icons.add),
            ),
          ),
        ],
      ),
    );
  }

  String _getTriesString(BuildContext context, int styleId, int count) {
    return styleId > 3 && styleId < 100
        ? AppLocalizations.of(context).ascend_hangs(count)
        : AppLocalizations.of(context).ascend_tries;
  }

  Widget _createExistingAscentsInfo() {
    return Tooltip(
      message: AppLocalizations.of(context).existing_ascents_tooltip,
      child: InkWell(
        onTap: () {
          if (this.existingAscents.isEmpty) return;
          InfoDialog().showExistingRoutesDialog(context, existingAscents);
        },
        onDoubleTap: () {
          tries = max(Ascend.getMinTriesValue(selectedStyle), this.existingAscents.length + 1);
          setState(() {});
          widget.onChangedTries(tries);
        },
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(Icons.info_outline, size: 35, color: Colors.orange),
            const SizedBox(width: 2.0),
            Flexible(
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
                decoration: BoxDecoration(
                  color: Colors.orange.withOpacity(0.2),
                  borderRadius: const BorderRadius.all(Radius.circular(20)),
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      this.existingAscents.length.toString(),
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text(
                      AppLocalizations.of(context).previous_ascends(existingAscents.length),
                      overflow: TextOverflow.clip,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
