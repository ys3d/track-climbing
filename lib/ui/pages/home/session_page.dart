import 'dart:io';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:climbing_track/ui/dialog/confirm_dialog.dart';
import 'package:climbing_track/ui/pages/home/pages/active_session_widget.dart';
import 'package:climbing_track/ui/pages/home/pages/end_session_widget.dart';
import 'package:climbing_track/ui/pages/home/pages/start_session_widget.dart';
import 'package:climbing_track/ui/pages/home/widgets/save_button.dart';
import 'package:climbing_track/ui/pages/home/widgets/session_button.dart';
import 'package:climbing_track/ui/pages/home/widgets/timer_widget.dart';
import 'package:climbing_track/util/screenshot.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:highlighter_coachmark/highlighter_coachmark.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:share_plus/share_plus.dart';

abstract class LocalConstants {
  static const double RECTANGLE_HEIGHT_VALUE = 70;
}

class SessionPage extends StatefulWidget {
  SessionPage({Key key}) : super(key: key);

  @override
  _SessionPageState createState() => _SessionPageState();
}

class _SessionPageState extends State<SessionPage> {
  final SessionState state = SessionState();
  final ConfirmDialog confirmDialog = ConfirmDialog();
  final ScrollController controller = ScrollController();
  final ScreenshotController _screenshotController = ScreenshotController();
  GlobalKey _fabKey = GlobalObjectKey("fab");
  var listener;

  @override
  void initState() {
    super.initState();
    listener = () {
      if (this.mounted) {
        setState(() {});
      }
    };
    state.addListener(listener);

    // unfocus TextFields when keyboard isn't visible anymore
    var keyboardVisibilityController = KeyboardVisibilityController();
    keyboardVisibilityController.onChange.listen((bool visible) {
      if (!visible && mounted && context != null) {
        FocusScope.of(context).unfocus();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bool isKeyboardVisible = KeyboardVisibilityProvider.isKeyboardVisible(context);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: state.currentState == SessionStateEnum.none ? SystemUiOverlayStyle.dark : SystemUiOverlayStyle.light,
      child: getWidget(isKeyboardVisible),
    );
  }

  Widget getWidget(bool isKeyboardVisible) {
    Widget pageWidget;
    EdgeInsetsGeometry padding = const EdgeInsets.all(Constants.STANDARD_PADDING);
    Widget secondWidget = Container();
    switch (state.currentState) {
      case SessionStateEnum.none:
        return SafeArea(
          child: Stack(
            children: <Widget>[
              Align(alignment: Alignment.topCenter, child: _buildTitle()),
              SessionButton(),
            ],
          ),
        );
      case SessionStateEnum.start:
        pageWidget = StartSessionWidget();
        break;
      case SessionStateEnum.active:
        padding = EdgeInsets.zero;
        pageWidget = ActiveSessionWidget(scrollController: this.controller);
        if (!isKeyboardVisible) {
          secondWidget = Padding(
            padding: EdgeInsets.only(
              bottom: Constants.STANDARD_PADDING,
            ),
            child: SaveButton(scrollController: this.controller),
          );
        }
        break;
      case SessionStateEnum.end:
        pageWidget = EndSessionWidget(_screenshotController);
        padding = EdgeInsets.zero;
        break;
    }
    return Container(
      color: AppColors.accentColor,
      child: SafeArea(
        child: Column(
          children: <Widget>[
            createTopBar(),
            Expanded(
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: GestureDetector(
                  onVerticalDragUpdate: (dragDetails) {
                    FocusScope.of(context).unfocus();
                  },
                  onTapDown: (tapDownDetails) {
                    FocusScope.of(context).unfocus();
                  },
                  child: Stack(
                    children: [
                      SingleChildScrollView(
                        controller: controller,
                        padding: padding,
                        child: pageWidget,
                      ),
                      secondWidget
                    ],
                  ),
                ),
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTitle() => Container(
        padding: EdgeInsets.fromLTRB(40, 20, 40, 20),
        child: Text(
          AppLocalizations.of(context).home_page_title,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline6,
        ),
      );

  Widget createTopBar() => Container(
        width: MediaQuery.of(context).size.width,
        height: LocalConstants.RECTANGLE_HEIGHT_VALUE,
        child: InkWell(
          onTap: () => onTapButton(),
          child: Center(
            child: getButtonFilling(),
          ),
        ),
        decoration: BoxDecoration(shape: BoxShape.rectangle, color: AppColors.accentColor),
      );

  Widget getButtonFilling() {
    switch (state.currentState) {
      case SessionStateEnum.none:
        return Container();
      case SessionStateEnum.start:
        return Align(
          alignment: Alignment.center,
          child: Text(
            AppLocalizations.of(context).home_page_create_session.toUpperCase(),
            style: Theme.of(context).textTheme.headline4.copyWith(color: AppColors.textSecondary),
          ),
        );
      case SessionStateEnum.active:
        WidgetsBinding.instance.addPostFrameCallback((_) => _showToolTip());
        return Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    AppLocalizations.of(context).session_time,
                    style: Theme.of(context).textTheme.headline5.copyWith(color: AppColors.textSecondary),
                  ),
                  TimerWidget(),
                ],
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Tooltip(
                message: AppLocalizations.of(context).home_page_close_session,
                child: Padding(
                  padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
                  child: Icon(
                    Icons.wrong_location_outlined,
                    key: _fabKey,
                    color: Colors.white,
                    size: 42,
                  ),
                ),
              ),
            ),
          ],
        );
      case SessionStateEnum.end:
        return Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 10.0),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  AppLocalizations.of(context).session_summary.toUpperCase(),
                  style: Theme.of(context).textTheme.headline5.copyWith(color: AppColors.textSecondary),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
              ),
              IconButton(
                padding: const EdgeInsets.all(12.0),
                color: Colors.white,
                tooltip: AppLocalizations.of(context).general_share,
                onPressed: () {
                  _screenshotController
                      .capture(
                    fileName:
                        "${DateFormat("yyyy-MM-dd").format(Data().getCurrentSession().startDate)}-session-summary",
                  )
                      .then((File image) {
                    Share.shareFiles([image.path]);
                  }).catchError((onError) {
                    print(onError);
                  });
                },
                icon: Icon(Icons.share),
              ),
            ],
          ),
        );
    }
    return Container();
  }

  void onTapButton() async {
    switch (state.currentState) {
      case SessionStateEnum.none:
        this.state.nextState();
        return;
      case SessionStateEnum.start:
        if (await Data().startSession(context)) {
          SessionState().nextState();
        } else {
          ScaffoldMessenger.of(context)
            ..removeCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                behavior: SnackBarBehavior.floating,
                content: Text(AppLocalizations.of(context).home_page_enter_location_hint),
              ),
            );
          return;
        }
        break;
      case SessionStateEnum.active:
        ScaffoldMessenger.of(context).removeCurrentSnackBar();
        confirmDialog.showEndSessionDialog(context);
        break;
      case SessionStateEnum.end:
        this.state.nextState();
        break;
    }
    controller.jumpTo(0.0);
  }

  void _showToolTip() {
    if (!DataStore().toolTipsDataStore.tooltipCloseSession) return;

    DataStore().toolTipsDataStore.tooltipCloseSession = false;

    CoachMark coachMarkFAB = CoachMark();
    RenderBox target = _fabKey.currentContext.findRenderObject();

    Rect markRect = target.localToGlobal(Offset.zero) & target.size;
    markRect = Rect.fromCircle(center: markRect.center, radius: markRect.longestSide * 0.65);

    coachMarkFAB.show(
      targetContext: _fabKey.currentContext,
      markRect: markRect,
      children: [
        Center(
          child: Text(
            AppLocalizations.of(context).home_page_close_session_hint,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 24.0,
              fontStyle: FontStyle.italic,
              color: Colors.white,
            ),
          ),
        ),
      ],
      duration: null,
    );
  }
}
