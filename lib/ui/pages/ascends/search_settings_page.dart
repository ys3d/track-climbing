import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/search_settings.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SearchSettingsPage extends StatefulWidget {
  @override
  _SearchSettingsPageState createState() => _SearchSettingsPageState();
}

class _SearchSettingsPageState extends State<SearchSettingsPage> {
  SearchSettingsDataStore settings = DataStore().searchSettingsDataStore;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.accentColor,
        leading: const BackButton(color: Colors.white),
        title: Text(
          AppLocalizations.of(context).general_search,
          style: const TextStyle(color: Colors.white, fontSize: 22),
        ),
        actions: [
          IconButton(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            tooltip: AppLocalizations.of(context).general_reset,
            icon: const Icon(Icons.refresh),
            color: Colors.white,
            onPressed: () {
              settings.reset();
              setState(() {});
            },
          )
        ],
      ),
      body: Center(child: createSettings()),
    );
  }

  Widget createSettings() {
    return ListView(
        padding: const EdgeInsets.fromLTRB(Constants.STANDARD_PADDING, 8.0, Constants.STANDARD_PADDING, 8.0),
        children: [
          Visibility(
            visible: SessionState().currentState == SessionStateEnum.active,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  AppLocalizations.of(context).search_settings_current_location_only,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                Switch(
                  activeColor: Colors.blue,
                  value: settings.onlyCurrentLocation,
                  onChanged: (value) {
                    settings.onlyCurrentLocation = value;
                    setState(() {});
                  },
                ),
              ],
            ),
          ),
          Visibility(
            visible: SessionState().currentState == SessionStateEnum.active,
            child: const Divider(thickness: 2.0),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppLocalizations.of(context).general_indoor,
                style: Theme.of(context).textTheme.bodyText1,
              ),
              Checkbox(
                activeColor: Colors.blue,
                value: settings.indoor,
                onChanged: (value) {
                  settings.indoor = value;
                  setState(() {});
                },
              ),
            ],
          ),
          const Divider(thickness: 2.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppLocalizations.of(context).general_outdoor,
                style: Theme.of(context).textTheme.bodyText1,
              ),
              Checkbox(
                activeColor: Colors.blue,
                value: settings.outdoor,
                onChanged: (value) {
                  settings.outdoor = value;
                  setState(() {});
                },
              ),
            ],
          ),
          const Divider(thickness: 2.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppLocalizations.of(context).search_settings_marked_only,
                style: Theme.of(context).textTheme.bodyText1,
              ),
              Switch(
                activeColor: Colors.blue,
                value: settings.onlyMarked,
                onChanged: (value) {
                  settings.onlyMarked = value;
                  setState(() {});
                },
              ),
            ],
          ),
          const Divider(thickness: 2.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppLocalizations.of(context).search_settings_not_finished_only,
                style: Theme.of(context).textTheme.bodyText1,
              ),
              Switch(
                activeColor: Colors.blue,
                value: settings.onlyNotFinished,
                onChanged: (value) {
                  settings.onlyNotFinished = value;
                  setState(() {});
                },
              ),
            ],
          ),
          const Divider(thickness: 2.0),
          ..._types(),
          ..._locations(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppLocalizations.of(context).search_settings_sorting,
                style: Theme.of(context).textTheme.bodyText1,
              ),
              DropdownButton<Sorting>(
                borderRadius: BorderRadius.circular(20.0),
                alignment: Alignment.centerRight,
                value: settings.currentSorting,
                underline: Container(),
                style: Theme.of(context).textTheme.bodyText1,
                onChanged: (Sorting value) {
                  setState(() {
                    settings.currentSorting = value;
                  });
                },
                items: Sorting.values.map((Sorting item) {
                  return DropdownMenuItem<Sorting>(
                    child: Text(_getNameFromSorting(item)),
                    value: item,
                  );
                }).toList(),
              ),
            ],
          ),
          const Divider(thickness: 2.0),
        ]);
  }

  List<Widget> _types() {
    List<String> typeList = [
      SearchSettingsDataStore.ALL_TYPES_KEY,
      ...ClimbingType.values.map((e) => e.name),
    ];

    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            AppLocalizations.of(context).search_settings_types,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          DropdownButton(
            borderRadius: BorderRadius.circular(20.0),
            alignment: Alignment.centerRight,
            value: settings.type,
            underline: Container(),
            style: Theme.of(context).textTheme.bodyText1,
            onChanged: (value) {
              setState(() {
                settings.type = value;
              });
            },
            items: List<DropdownMenuItem>.generate(
              typeList.length,
              (i) => DropdownMenuItem(
                value: typeList[i],
                child: Text(
                  _getNameFromType(typeList[i]),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
          ),
        ],
      ),
      const Divider(thickness: 2.0),
    ];
  }

  List<Widget> _locations() {
    List<String> locations = List.of(Data().getLocations());
    locations.sort();

    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            AppLocalizations.of(context).general_location,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          const SizedBox(width: 24.0),
          Flexible(
            child: DropdownButton(
              borderRadius: BorderRadius.circular(20.0),
              alignment: Alignment.centerRight,
              value: settings.currentLocation,
              underline: Container(),
              style: Theme.of(context).textTheme.bodyText1,
              onChanged: (value) {
                setState(() {
                  settings.currentLocation = value;
                });
              },
              items: List<DropdownMenuItem>.generate(
                locations.length,
                (i) => DropdownMenuItem(
                  value: locations[i],
                  child: Text(
                    Util.replaceStringWithBlankSpaces(locations[i]),
                    overflow: TextOverflow.ellipsis,
                    //textAlign: settings.currentLocation == locations[i] ? TextAlign.end : TextAlign.start,
                  ),
                ),
              )..insert(
                  0,
                  DropdownMenuItem(
                    value: SearchSettingsDataStore.ALL_LOCATIONS_KEY,
                    child: Text(
                      AppLocalizations.of(context).search_settings_all_locations,
                    ),
                  ),
                ),
            ),
          ),
        ],
      ),
      const Divider(thickness: 2.0),
    ];
  }

  String _getNameFromSorting(Sorting key) {
    return {
      Sorting.NEWEST: AppLocalizations.of(context).search_setting_newest,
      Sorting.OLDEST: AppLocalizations.of(context).search_setting_oldest,
      Sorting.ALPHABETIC: AppLocalizations.of(context).search_setting_alphabetic,
      Sorting.EASIEST: AppLocalizations.of(context).search_setting_easiest,
      Sorting.HARDEST: AppLocalizations.of(context).search_setting_hardest,
      Sorting.BEST: AppLocalizations.of(context).search_setting_best,
    }[key];
  }

  String _getNameFromType(String name) {
    return {
          SearchSettingsDataStore.ALL_TYPES_KEY: AppLocalizations.of(context).search_settings_all,
          ClimbingType.SPORT_CLIMBING.name: AppLocalizations.of(context).style_sport_climbing,
          ClimbingType.BOULDER.name: AppLocalizations.of(context).style_bouldering,
          ClimbingType.SPEED_CLIMBING.name: AppLocalizations.of(context).style_speed_climbing,
          ClimbingType.FREE_SOLO.name: AppLocalizations.of(context).style_free_solo_climbing,
          ClimbingType.DEEP_WATER_SOLO.name: AppLocalizations.of(context).style_dws_climbing,
          ClimbingType.ICE_CLIMBING.name: AppLocalizations.of(context).style_ice_climbing,
        }[name] ??
        name;
  }
}
