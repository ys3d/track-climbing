import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/pages/ascends/ascend_edit_page.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ExpandableAscend extends StatefulWidget {
  final data;
  final Function(int, int) refresh;
  final int index;
  final int expandedIndex;

  const ExpandableAscend({Key key, this.data, this.refresh, this.index, this.expandedIndex}) : super(key: key);

  @override
  _ExpandableAscendState createState() => _ExpandableAscendState(data);
}

class _ExpandableAscendState extends State<ExpandableAscend> {
  bool expanded = false;
  bool marked;
  var data;

  _ExpandableAscendState(this.data);

  @override
  void initState() {
    super.initState();
    this.expanded = widget.expandedIndex == widget.index;
    marked = data[DBController.ascendMarked].isOdd;
  }

  @override
  Widget build(BuildContext context) {
    return getDataItemWidget();
  }

  Widget getDataItemWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2.0),
      child: InkWell(
        onTap: () => setState(() => expanded = !expanded),
        borderRadius: BorderRadius.circular(18.0),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 10.0),
          child: AnimatedSize(
            duration: Duration(milliseconds: 200),
            child: expanded ? _expandedWidget() : _collapsedWidget(),
          ),
        ),
      ),
    );
  }

  Widget _expandedWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 4.0),
        _name(
          maxLines: 2,
          style: Theme.of(context).textTheme.subtitle1.copyWith(fontSize: 22.0),
        ),
        const SizedBox(height: 20),
        ..._dateAndLocation(),
        if (data[DBController.ascendType] == ClimbingType.SPEED_CLIMBING.name) ..._speed(data[DBController.speedTime]),
        _info(),
        const SizedBox(height: 10),
        ..._height(),
        Row(
          children: [
            const Icon(Icons.stars),
            const SizedBox(width: 20),
            Util.getRating(data[DBController.ascendRating]),
          ],
        ),
        const SizedBox(height: 10),
        _comment(),
        const SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Tooltip(
              message: AppLocalizations.of(context).general_edit,
              child: TextButton(
                style: TextButton.styleFrom(
                  foregroundColor: Colors.black,
                  backgroundColor: Colors.transparent,
                ),
                onPressed: () => Navigator.of(context)
                    .push(_ascendEditRoute(
                      context,
                      Ascend.fromMap(data),
                      (value) => setState(() {
                        this.data = Map()..addAll(this.data);
                        this.data[DBController.ascendGradeId] = value[DBController.ascendGradeId];
                        this.data[DBController.ascendStyleId] = value[DBController.ascendStyleId];
                        this.data[DBController.routeTopRope] = value[DBController.routeTopRope];
                        this.data[DBController.ascendRating] = value[DBController.ascendRating];
                        this.data[DBController.ascendName] = value[DBController.ascendName];
                        this.data[DBController.ascendMarked] = value[DBController.ascendMarked];
                        this.data[DBController.ascendComment] = value[DBController.ascendComment];
                        this.data[DBController.ascendType] = value[DBController.ascendType];
                        this.data[DBController.speedType] = value[DBController.speedType];
                        this.data[DBController.speedTime] = value[DBController.speedTime];
                      }),
                      data[DBController.sessionId],
                    ))
                    .then((value) => widget.refresh(widget.index, widget.index)),
                child: Column(
                  children: <Widget>[
                    const Icon(Icons.edit),
                    const Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
                    Text(AppLocalizations.of(context).general_edit, style: TextStyle(color: Colors.black87)),
                  ],
                ),
              ),
            ),
            Tooltip(
              message: AppLocalizations.of(context).general_mark,
              child: TextButton(
                style: TextButton.styleFrom(
                  foregroundColor: Colors.black,
                  backgroundColor: Colors.transparent,
                ),
                onPressed: () => toggleMarked(),
                child: Column(
                  children: <Widget>[
                    Icon(
                      marked ? Icons.favorite : Icons.favorite_border,
                      color: marked ? Colors.red : Colors.black87,
                    ),
                    const Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
                    Text(AppLocalizations.of(context).general_mark, style: TextStyle(color: Colors.black87)),
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _collapsedWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          fit: FlexFit.tight,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _name(
                maxLines: 1,
                style: Theme.of(context).textTheme.subtitle1,
              ),
              const SizedBox(height: 4.0),
              Text(
                Util.getLocationDateString(
                  data[DBController.sessionTimeStart],
                  data[DBController.sessionLocation],
                ),
                style: Theme.of(context).textTheme.bodyText2,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ],
          ),
        ),
        const SizedBox(width: 14.0),
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              Util.getDataSummary(data, context),
              style: Theme.of(context).textTheme.bodyText2,
            ),
            const SizedBox(height: 4.0),
            Util.getRating(data[DBController.ascendRating]),
          ],
        )
      ],
    );
  }

  void toggleMarked() {
    setState(() {
      this.marked = !this.marked;
    });
    final DBController dbController = DBController();
    dbController.updateAscend({DBController.ascendMarked: this.marked ? 1 : 0}, data[DBController.ascendId]);
  }

  Widget _name({@required int maxLines, TextStyle style}) {
    return Row(
      children: [
        Visibility(visible: marked, child: const Icon(Icons.favorite, color: Colors.red)),
        Visibility(visible: marked, child: const SizedBox(width: 4)),
        Flexible(
          child: Text(
            Util.getAscendName(
              context,
              data[DBController.ascendName],
              type: EnumToString.fromString(ClimbingType.values, data[DBController.ascendType]),
              speedType: data[DBController.speedType],
            ),
            style: style,
            overflow: TextOverflow.ellipsis,
            maxLines: maxLines,
          ),
        ),
      ],
    );
  }

  List<Widget> _dateAndLocation() {
    return [
      Row(
        children: [
          const Icon(Icons.calendar_month),
          const SizedBox(width: 20),
          Text(
            DateFormat("dd.MM.yy").format(DateTime.parse(data[DBController.sessionTimeStart])),
            style: Theme.of(context).textTheme.bodyText2,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
        ],
      ),
      const SizedBox(height: 10.0),
      Row(
        children: [
          Icon(Icons.location_on_outlined),
          const SizedBox(width: 20),
          Text(
            Util.getLocation(data[DBController.sessionLocation]),
            style: Theme.of(context).textTheme.bodyText2,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
        ],
      ),
      const SizedBox(height: 10),
    ];
  }

  Widget _info() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Icon(Icons.info_outline),
        const SizedBox(width: 20),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              EnumToString.fromString(ClimbingType.values, data[DBController.ascendType]).toTitle(context),
              style: Theme.of(context).textTheme.bodyText2,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
            ),
            const SizedBox(height: 4.0),
            ..._grade(data[DBController.ascendType], data[DBController.speedType]),
            ..._style(data[DBController.ascendType]),
            _leadOrTop(
              EnumToString.fromString(ClimbingType.values, data[DBController.ascendType]),
              data[DBController.speedType],
            ),
          ],
        ),
      ],
    );
  }

  List<Widget> _grade(String type, int speedType) {
    if (type == ClimbingType.SPEED_CLIMBING.name && speedType == 0) return [];
    return [
      Text(
        Util.getGrade(
          data[DBController.ascendGradeId],
          EnumToString.fromString(ClimbingType.values, data[DBController.ascendType]),
        ),
        style: Theme.of(context).textTheme.bodyText2,
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
      ),
      const SizedBox(height: 4.0),
    ];
  }

  List<Widget> _style(String type) {
    if (type == ClimbingType.FREE_SOLO.name || type == ClimbingType.SPEED_CLIMBING.name) return [];
    return [
      Text(
        Util.getStyle(
          data[DBController.ascendStyleId],
          context,
          tries: data[DBController.ascendTries],
        ),
        style: Theme.of(context).textTheme.bodyText2,
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
      ),
      const SizedBox(height: 4.0),
    ];
  }

  Widget _leadOrTop(ClimbingType type, int speedType) {
    return Visibility(
      visible: (type == ClimbingType.SPEED_CLIMBING && speedType != 0) ||
          type == ClimbingType.SPORT_CLIMBING ||
          type == ClimbingType.ICE_CLIMBING,
      child: Text(
        Util.getTopOrLead(data[DBController.routeTopRope].isOdd, context),
        style: Theme.of(context).textTheme.bodyText2,
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
      ),
    );
  }

  Widget _comment() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Icon(Icons.comment_outlined),
        const SizedBox(width: 20),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(width: 20),
              Text(
                Util.getComment(data[DBController.ascendComment]),
                style: Theme.of(context).textTheme.bodyText2,
                overflow: TextOverflow.clip,
              ),
            ],
          ),
        ),
      ],
    );
  }

  List<Widget> _speed(int speedTime) {
    return [
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Icon(Icons.timer_outlined),
          const SizedBox(width: 20),
          Text(
            "${(speedTime / 1000).toStringAsFixed(2)} ${AppLocalizations.of(context).util_seconds}",
            style: Theme.of(context).textTheme.bodyText2,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
        ],
      ),
      const SizedBox(height: 10),
    ];
  }

  List<Widget> _height() {
    return [
      Row(
        children: [
          const Icon(Icons.height),
          const SizedBox(width: 20),
          Text(
            Util.getHeightWithShortUnit(data[DBController.ascendHeight]),
            style: Theme.of(context).textTheme.bodyText2,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
        ],
      ),
      const SizedBox(height: 10),
    ];
  }

  Route _ascendEditRoute(
    BuildContext context,
    Ascend ascend,
    Function(Map<String, dynamic>) onChanged,
    int sessionID,
  ) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) {
        return AscendEditPage(ascend: ascend, onChanged: onChanged, sessionID: sessionID);
      },
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(1.0, 0.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }
}
