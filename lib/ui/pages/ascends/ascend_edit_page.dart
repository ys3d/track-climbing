import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/ui/widgets/edit_ascend_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AscendEditPage extends StatelessWidget {
  final Ascend ascend;
  final int sessionID;
  final Function(Map<String, dynamic>) onChanged;

  AscendEditPage({Key key, @required this.ascend, @required this.onChanged, @required this.sessionID})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.accentColor,
        title: Text(
          "${AppLocalizations.of(context).general_edit} ${ascend.type.toTitle(context)}",
          style: TextStyle(color: Colors.white, fontSize: 22),
        ),
        leading: IconButton(
          color: Colors.white,
          onPressed: () => Navigator.of(context).pop(),
          icon: Icon(Icons.close),
          tooltip: AppLocalizations.of(context).general_cancel,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: AppLocalizations.of(context).general_save,
        backgroundColor: Colors.black,
        child: const Icon(
          Icons.save,
          size: 32.0,
          color: Colors.white,
        ),
        onPressed: () async {
          await updateAscendInDatabase(ascend);
          onChanged(ascend.toMap(sessionID));
          Navigator.of(context).pop();
        },
      ),
      body: GestureDetector(
        onVerticalDragUpdate: (dragDetails) {
          FocusScope.of(context).unfocus();
        },
        onTapDown: (tapDownDetails) {
          FocusScope.of(context).unfocus();
        },
        child: SingleChildScrollView(
          child: EditAscendWidget(ascend: ascend),
        ),
      ),
    );
  }

  Future<void> updateAscendInDatabase(Ascend data) async {
    DBController dbController = DBController();
    await dbController.updateAscend(data.toMap(sessionID), data.routeID);
  }
}
