import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/search_settings.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:climbing_track/ui/pages/ascends/expandable_ascend.dart';
import 'package:climbing_track/ui/pages/ascends/search_settings_page.dart';
import 'package:climbing_track/ui/widgets/empty_list_placeholder.dart';
import 'package:flutter/material.dart';
import 'package:highlighter_coachmark/highlighter_coachmark.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AscendsPage extends StatefulWidget {
  AscendsPage({Key key}) : super(key: key);

  @override
  _AscendsPageState createState() => _AscendsPageState();
}

class _AscendsPageState extends State<AscendsPage> {
  static const String PLACEHOLDER_IMAGE_NAME = 'assets/images/3407006.png';
  static const String PLACEHOLDER_IMAGE_ROUTES = 'assets/images/2640193.png';

  final DBController dbController = DBController();
  final ItemScrollController _scrollController = ItemScrollController();
  final TextEditingController textController = TextEditingController();
  final GlobalKey _globalKey = GlobalObjectKey("ascend_page_key");

  Future<List<Map<String, dynamic>>> data;

  int _expandedIndex = -1;
  String searchString = "";
  bool searchBarVisibility = false;

  @override
  void initState() {
    super.initState();
    this.data = fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.accentColor,
        title: _createSearchBar(),
      ),
      body: GestureDetector(
        onVerticalDragUpdate: (dragDetails) {
          FocusScope.of(context).unfocus();
        },
        onTapDown: (tapDownDetails) {
          FocusScope.of(context).unfocus();
        },
        child: Center(
          child: createListView(),
        ),
      ),
    );
  }

  Widget createListView() {
    return FutureBuilder(
      future: this.data,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Align(
            alignment: Alignment.topCenter,
            child: PreferredSize(
              preferredSize: Size.fromHeight(6.0),
              child: LinearProgressIndicator(),
            ),
          );
        }
        var filteredData = [];
        if (snapshot.data != null) {
          filteredData = snapshot.data
              .where((item) => item[DBController.ascendName].toString().toLowerCase().contains(searchString))
              .toList();
        }

        if (_globalKey != null && filteredData.length > 2) {
          WidgetsBinding.instance.addPostFrameCallback((_) => _showToolTip());
        }

        return filteredData.length == 0
            ? snapshot.data.length > 0
                ? EmptyListPlaceHolder(
                    text: AppLocalizations.of(context).ascent_page_placeholder_name,
                    image: PLACEHOLDER_IMAGE_NAME,
                  )
                : EmptyListPlaceHolder(
                    text: AppLocalizations.of(context).ascent_page_placeholder_routes,
                    image: PLACEHOLDER_IMAGE_ROUTES,
                  )
            : Scrollbar(
                child: ScrollablePositionedList.separated(
                  itemScrollController: _scrollController,
                  itemCount: filteredData.length,
                  itemBuilder: (context, index) {
                    return Stack(
                      alignment: Alignment.center,
                      children: [
                        if (index == 1)
                          Align(
                            alignment: Alignment.topCenter,
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width * 0.8,
                              height: 20.0,
                              key: this._globalKey,
                            ),
                          ),
                        ExpandableAscend(
                          key: Key(filteredData[index][DBController.ascendId].toString()),
                          data: filteredData[index],
                          refresh: refresh,
                          index: index,
                          expandedIndex: _expandedIndex,
                        ),
                      ],
                    );
                  },
                  padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 10.0),
                  separatorBuilder: (BuildContext context, int index) => const Divider(
                    thickness: 2.0,
                    height: 2.0,
                  ),
                ),
              );
      },
    );
  }

  void refresh(int index, int expandedIndex) async {
    setState(() {
      this.data = fetchData();
      this._expandedIndex = expandedIndex;
    });

    // hack because we have to wait for setState
    await Future.delayed(Duration(milliseconds: 100));
    this._scrollController.jumpTo(index: index);
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    Sorting sorting = DataStore().searchSettingsDataStore.currentSorting;
    List<dynamic> conditions = DataStore().searchSettingsDataStore.getSQLConditionString();
    return await dbController.rawQuery(
      "SELECT * FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using (${DBController.sessionId}) "
      "WHERE ${conditions[0]} "
      "ORDER BY ${SearchSettingsDataStore.sortingKeys()[sorting]}",
      conditions[1],
    );
  }

  Widget _createSearchBar() {
    return Row(
      children: [
        Expanded(
          child: this.searchBarVisibility
              ? _buildFloatingSearchBar()
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Text(
                        Util.replaceStringWithBlankSpaces(
                            AppLocalizations.of(context).ascent_page_ascent_list.toUpperCase()),
                        style: TextStyle(color: Colors.white, fontSize: 22),
                        textAlign: TextAlign.start,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    IconButton(
                      color: Colors.white,
                      tooltip: AppLocalizations.of(context).general_search,
                      icon: const Icon(
                        Icons.search,
                        size: 30,
                      ),
                      onPressed: () {
                        setState(() => this.searchBarVisibility = true);
                      },
                    ),
                  ],
                ),
        ),
        Padding(
          padding: EdgeInsets.zero,
          child: IconButton(
            color: Colors.white,
            tooltip: AppLocalizations.of(context).ascent_page_ascent_settings,
            icon: const Icon(
              Icons.tune,
              size: 30,
            ),
            onPressed: () {
              Navigator.of(context).push(createSettingsRoute()).then((value) => setState(() {
                    this.data = fetchData();
                  }));
            },
          ),
        ),
      ],
    );
  }

  Widget _buildFloatingSearchBar() {
    return Container(
      height: AppBar().preferredSize.height * 0.70,
      padding: const EdgeInsets.only(right: 16.0),
      child: Theme(
        data: ThemeData.from(
          colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor),
          useMaterial3: false,
        ),
        child: TextFormField(
          autofocus: true,
          controller: textController,
          onChanged: (value) {
            setState(() => this.searchString = value.trim().toLowerCase());
          },
          textAlign: TextAlign.left,
          textAlignVertical: TextAlignVertical.center,
          textCapitalization: TextCapitalization.sentences,
          maxLines: 1,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.zero,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(12.0),
              borderSide: BorderSide.none,
            ),
            filled: true,
            fillColor: Colors.white,
            prefixIcon: InkWell(
              borderRadius: BorderRadius.circular(12.0),
              onTap: () => {
                setState(() {
                  this.searchBarVisibility = false;
                  textController.clear();
                  this.searchString = "";
                })
              },
              child: Icon(Icons.arrow_back, color: Colors.black),
            ),
            suffixIcon: InkWell(
              borderRadius: BorderRadius.circular(12.0),
              onTap: () => setState(
                () {
                  textController.clear();
                  this.searchString = "";
                },
              ),
              child: Icon(
                Icons.clear,
                color: this.textController.value.text == "" ? Colors.white : Colors.black,
              ),
            ),
            hintText: AppLocalizations.of(context).ascent_page_search_info,
            hintStyle: Theme.of(context).textTheme.bodyText1.copyWith(
                  color: Colors.black38,
                ),
          ),
        ),
      ),
    );
  }

  Route createSettingsRoute() {
    return MaterialPageRoute(builder: (BuildContext context) => SearchSettingsPage());
  }

  void _showToolTip() async {
    if (!DataStore().toolTipsDataStore.tooltipAscendPage) return;
    DataStore().toolTipsDataStore.tooltipAscendPage = false;
    await Future.delayed(const Duration(milliseconds: 250));

    CoachMark coachMarkFAB = CoachMark();
    RenderBox target = _globalKey.currentContext.findRenderObject();

    Rect markRect = target.localToGlobal(Offset.zero) & target.size;
    markRect = Rect.fromCenter(
      center: markRect.center,
      width: markRect.width * 1.25,
      height: markRect.height * 4.0,
    );

    coachMarkFAB.show(
      targetContext: _globalKey.currentContext,
      markRect: markRect,
      markShape: BoxShape.rectangle,
      children: [
        Padding(
          padding: EdgeInsets.all(Constants.STANDARD_PADDING),
          child: Center(
            child: Text(
              AppLocalizations.of(context).ascend_page_tooltip,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 24.0,
                fontStyle: FontStyle.italic,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
      duration: null,
    );
  }
}
