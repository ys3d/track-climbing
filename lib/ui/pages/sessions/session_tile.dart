import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/height_measurement.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:climbing_track/ui/pages/sessions/session_summary_container.dart';
import 'package:climbing_track/view_model/session_view_model.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:highlighter_coachmark/highlighter_coachmark.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SessionTile extends StatefulWidget {
  const SessionTile({
    Key key,
    @required this.sessionViewModel,
    @required this.toolTipKey,
    @required this.callBack,
  }) : super(key: key);

  final toolTipKey;
  final SessionViewModel sessionViewModel;
  final Function callBack;

  @override
  _SessionTileState createState() => _SessionTileState();
}

class _SessionTileState extends State<SessionTile> {
  static const int animationTimeInMilliseconds = 200;
  bool extended = false;

  @override
  Widget build(BuildContext context) {
    if (widget.toolTipKey != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) => _showToolTip());
    }
    return InkWell(
      key: widget.toolTipKey,
      borderRadius: const BorderRadius.only(
        topRight: Radius.circular(18.0),
        bottomRight: Radius.circular(18.0),
      ),
      onLongPress: _pushSessionSummaryPage,
      onTap: () => setState(() => this.extended = !this.extended),
      child: _createSessionContainer(context),
    );
  }

  Widget _createSessionContainer(BuildContext context) {
    return FutureBuilder(
      future: widget.sessionViewModel.getAscentsAndCalculateSessionSummary(),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return _createExpansionSessionTile(context, []);
        }
        List ascents = snapshot.data;

        if (snapshot.data == null) {
          ascents = [];
        }
        return _createExpansionSessionTile(context, ascents);
      },
    );
  }

  Widget _createExpansionSessionTile(BuildContext context, List ascents) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(
            width: 4.0,
            color: widget.sessionViewModel.session.outdoor ? Colors.green : Colors.orangeAccent,
          ),
        ),
        color: Colors.transparent,
      ),
      child: _getContent(context, ascents),
    );
  }

  Widget _getContent(BuildContext context, List ascents) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, bottom: 8.0, top: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _sessionInfo(),
          const SizedBox(height: 14.0),
      IntrinsicHeight(
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              getSummary(context),
              IconButton(
                onPressed: () => setState(() => this.extended = !this.extended),
                icon: this.extended ? const Icon(Icons.keyboard_arrow_up) : const Icon(Icons.keyboard_arrow_down),
                iconSize: 22,
                color: Colors.black54,
                padding: EdgeInsets.zero,
                splashRadius: 24.0,
                constraints: const BoxConstraints(
                  minWidth: 32,
                  minHeight: 32,
                ),
              ),
            ],
          ),
          ),
          SizedBox(height: extended ? 16.0 : 0.0),
          AnimatedSize(
            alignment: Alignment.topCenter,
            duration: const Duration(milliseconds: animationTimeInMilliseconds),
            child: _createRouteChildren(context, extended ? ascents : []),
          ),
        ],
      ),
    );
  }

  Widget _sessionInfo() {
    String unit = DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET ? "ft" : "m";
    String height = Util.getHeightWithShortUnit(
      widget.sessionViewModel.sessionSummaryData[SessionViewModel.HEIGHT_KEY],
      zeroFilling: "0 $unit",
    );

    String start = DateFormat.jm(DataStore().settingsDataStore.language).format(
      widget.sessionViewModel.session.startDate,
    );
    String end = DateFormat.jm(DataStore().settingsDataStore.language).format(
      widget.sessionViewModel.session.endDate,
    );
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _getDateTitle(widget.sessionViewModel.session.startDate),
              _getLocationWidget(widget.sessionViewModel.session.locationName),
              _getLocationWidget(
                Util.replaceStringWithBlankSpaces(
                  AppLocalizations.of(context).sessions_page_height_from_to(height, start, end),
                ),
              ),
            ],
          ),
        ),
        IconButton(
          onPressed: _pushSessionSummaryPage,
          icon: Icon(Icons.info_outline),
          color: Colors.black54,
          splashRadius: 24.0,
          constraints: const BoxConstraints(
            minWidth: 32,
            minHeight: 32,
          ),
        ),
      ],
    );
  }

  Widget getSummary(BuildContext context) {
    return Expanded(
      child: Row(
        children: [
          _getSummaryPart(widget.sessionViewModel.count.toString(), AppLocalizations.of(context).ascends_title),
          const VerticalDivider(indent: 8.0, endIndent: 8.0, thickness: 2, width: 28.0, color: Colors.black45),
          _getSummaryPart(widget.sessionViewModel.avgGrade, AppLocalizations.of(context).sessions_page_average),
          const VerticalDivider(indent: 8.0, endIndent: 8.0, thickness: 2, width: 28.0, color: Colors.black45),
          _getSummaryPart(widget.sessionViewModel.maxGrade, AppLocalizations.of(context).sessions_page_max),
        ],
      ),
    );
  }

  Widget _getSummaryPart(String text, String info) {
    return Flexible(
      child: Container(
        constraints: BoxConstraints(minWidth: 60.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              text,
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    fontSize: 22.0,
                    fontWeight: FontWeight.w500,
                    letterSpacing: 0.0,
                    wordSpacing: 0.0,
                    height: 0.0,
                  ),
              overflow: TextOverflow.visible,
              maxLines: 2,
            ),
            const SizedBox(height: 4.0),
            Text(
              info,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    fontSize: 13,
                    color: Colors.black.withOpacity(0.7),
                    fontWeight: FontWeight.w400,
                    letterSpacing: 0.0,
                    wordSpacing: 0.0,
                    height: 0.0,
                  ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getDateTitle(DateTime date) {
    return Text(
      DateFormat("dd.MM.yy").format(date),
      style: Theme.of(context).textTheme.headline5.copyWith(
            fontSize: 26.0,
            fontWeight: FontWeight.w500,
          ),
    );
  }

  Widget _getLocationWidget(String location) {
    return Text(
      Util.getLocation(location),
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style: Theme.of(context).textTheme.bodyText1.copyWith(
            fontSize: 15,
            color: Colors.black54,
            fontWeight: FontWeight.w400,
            letterSpacing: 0.0,
            wordSpacing: 0.0,
            height: 0.0,
          ),
    );
  }

  Widget _createRouteChildren(BuildContext context, List routes) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: List<Widget>.generate(
          routes.length,
          (index) => Padding(
            padding: const EdgeInsets.only(bottom: 1.5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Text(
                    Util.getAscendName(
                      context,
                      routes[index][DBController.ascendName],
                      type: EnumToString.fromString(ClimbingType.values, routes[index][DBController.ascendType]),
                      speedType: routes[index][DBController.speedType],
                    ),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: Theme.of(context).textTheme.bodyText1.copyWith(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          letterSpacing: 0.0,
                          wordSpacing: 0.0,
                          height: 0,
                        ),
                  ),
                ),
                const SizedBox(width: 14.0),
                Text(
                  Util.getDataSummary(routes[index], context),
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        letterSpacing: 0.0,
                        wordSpacing: 0.0,
                        height: 0,
                      ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _pushSessionSummaryPage() {
    Navigator.of(context).push(_sessionSummaryRoute(context)).then((value) {
      if (value != null && value) widget.callBack();
      this.extended = true;
      setState(() {});
    });
  }

  Route _sessionSummaryRoute(BuildContext context) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) {
        return SessionSummaryContainer(sessionViewModel: widget.sessionViewModel);
      },
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(1.0, 0.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  void _showToolTip() {
    if (!DataStore().toolTipsDataStore.tooltipSessionSummary) return;
    DataStore().toolTipsDataStore.tooltipSessionSummary = false;

    CoachMark coachMarkFAB = CoachMark();
    RenderBox target = widget.toolTipKey.currentContext.findRenderObject();

    Rect markRect = target.localToGlobal(Offset.zero) & target.size;
    markRect = Rect.fromCenter(center: markRect.center, width: markRect.width * 1.1, height: markRect.height * 1.1);

    coachMarkFAB.show(
      targetContext: widget.toolTipKey.currentContext,
      markRect: markRect,
      markShape: BoxShape.rectangle,
      children: [
        Padding(
          padding: EdgeInsets.all(Constants.STANDARD_PADDING),
          child: Center(
            child: Text(
              AppLocalizations.of(context).sessions_tooltip_long_press,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 24.0,
                fontStyle: FontStyle.italic,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
      duration: null,
    );
  }
}
