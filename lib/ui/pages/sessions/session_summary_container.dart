import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/pages/sessions/session_edit_page.dart';
import 'package:climbing_track/ui/widgets/session_summary.dart';
import 'package:climbing_track/view_model/session_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SessionSummaryContainer extends StatefulWidget {
  final SessionViewModel sessionViewModel;

  const SessionSummaryContainer({Key key, @required this.sessionViewModel}) : super(key: key);

  @override
  State<SessionSummaryContainer> createState() => _SessionSummaryContainerState();
}

class _SessionSummaryContainerState extends State<SessionSummaryContainer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.accentColor,
        leading: const BackButton(color: Colors.white),
        title: Text(
          Util.replaceStringWithBlankSpaces(AppLocalizations.of(context).session_summary.toUpperCase()),
          style: TextStyle(color: Colors.white, fontSize: 22),
        ),
        actions: [
          IconButton(
            color: Colors.white,
            tooltip: AppLocalizations.of(context).general_share,
            onPressed: widget.sessionViewModel.createScreenShot,
            icon: Icon(Icons.share),
          ),
          IconButton(
            color: Colors.white,
            tooltip: AppLocalizations.of(context).general_edit,
            onPressed: () => pushSessionEditPage(context),
            icon: Icon(Icons.edit),
          ),
        ],
      ),
      body: SessionSummary(widget.sessionViewModel),
    );
  }

  void pushSessionEditPage(BuildContext context) {
    Navigator.of(context).push(_sessionEditRoute(context)).then((value) {
      if (mounted) setState(() {});
    });
  }

  Route _sessionEditRoute(BuildContext context) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) {
        return SessionEditPage(sessionViewModel: widget.sessionViewModel);
      },
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(1.0, 0.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }
}
