import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:climbing_track/ui/pages/sessions/session_tile.dart';
import 'package:climbing_track/ui/widgets/empty_list_placeholder.dart';
import 'package:climbing_track/view_model/session_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SessionsOverview extends StatefulWidget {
  const SessionsOverview({Key key}) : super(key: key);

  @override
  State<SessionsOverview> createState() => _SessionsOverviewState();
}

class _SessionsOverviewState extends State<SessionsOverview> {
  static const GlobalKey _toolTipKey = GlobalObjectKey("toolTipKey");
  static const String IMAGE_PATH = "assets/images/6195619.png";

  Future _futureData;

  @override
  void initState() {
    super.initState();
    this._futureData = _fetchSessionData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.accentColor,
        centerTitle: true,
        title: Text(
          AppLocalizations.of(context).sessions_title.toUpperCase(),
          style: TextStyle(color: Colors.white, fontSize: 22),
        ),
      ),
      body: FutureBuilder(
        future: this._futureData,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const LinearProgressIndicator();
          }
          return _createSessions(snapshot.data);
        },
      ),
    );
  }

  Widget _createSessions(List<Map<String, dynamic>> data) {
    return data == null || data.isEmpty
        ? EmptyListPlaceHolder(text: AppLocalizations.of(context).sessions_no_sessions_info, image: IMAGE_PATH)
        : _getSessionScroll(data);
  }

  Widget _getSessionScroll(var data) {
    return Scrollbar(
      child: ListView.separated(
        padding: EdgeInsets.all(Constants.STANDARD_PADDING),
        itemCount: data.length,
        itemBuilder: (BuildContext context, int index) {
          return SessionTile(
            sessionViewModel: SessionViewModel(sessionDat: data[index]),
            toolTipKey: index == 0 ? _toolTipKey : null,
            callBack: () => setState(() {
              this._futureData = _fetchSessionData();
            }),
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(thickness: 2.0),
      ),
    );
  }

  Future<List<Map<String, dynamic>>> _fetchSessionData() async {
    return DBController().rawQuery(
      "SELECT * FROM ${DBController.sessionTable} "
      "LEFT JOIN ${DBController.locationsTable} using(${DBController.sessionLocation})"
      "WHERE ${DBController.sessionStatus} != (?) "
      "ORDER BY ${DBController.sessionTimeStart} DESC",
      [Data.SESSION_ACTIVE_STATE],
    );
  }
}
