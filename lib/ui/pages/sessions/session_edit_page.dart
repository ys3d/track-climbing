import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:climbing_track/ui/widgets/reorderable_ascents_list.dart';
import 'package:climbing_track/view_model/session_view_model.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:latlong2/latlong.dart';

class SessionEditPage extends StatefulWidget {
  final SessionViewModel sessionViewModel;

  const SessionEditPage({Key key, @required this.sessionViewModel}) : super(key: key);

  @override
  _SessionEditPageState createState() => _SessionEditPageState();
}

class _SessionEditPageState extends State<SessionEditPage> {
  Future<List<Map<String, dynamic>>> _ascentsFutureData;

  @override
  void initState() {
    super.initState();
    _ascentsFutureData = _loadAscends();
    widget.sessionViewModel.initEditFunctions();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      floatingActionButton: FloatingActionButton(
        tooltip: AppLocalizations.of(context).general_save,
        backgroundColor: Colors.black,
        child: const Icon(
          Icons.save,
          size: 32,
          color: Colors.white,
        ),
        onPressed: () => widget.sessionViewModel.onSave(context, this._ascentsFutureData),
      ),
      body: GestureDetector(
        onTapDown: (tapDownDetails) => FocusScope.of(context).unfocus(),
        child: _content(),
      ),
    );
  }

  Widget _appBar() {
    return AppBar(
      backgroundColor: AppColors.accentColor,
      title: Text(
        AppLocalizations.of(context).sessions_page_edit_session,
        style: const TextStyle(color: Colors.white, fontSize: 22),
      ),
      actions: [
        IconButton(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          tooltip: AppLocalizations.of(context).general_delete,
          icon: const Icon(Icons.delete_outline),
          color: Colors.white,
          onPressed: () => _showDeleteConfirmation(context),
        ),
      ],
      leading: IconButton(
        color: Colors.white,
        icon: const Icon(Icons.close),
        tooltip: AppLocalizations.of(context).general_cancel,
        onPressed: () => {
          ScaffoldMessenger.of(context).removeCurrentSnackBar(),
          Navigator.of(context).pop(false),
        },
      ),
    );
  }

  Future<void> _showDeleteConfirmation(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          titleTextStyle: Theme.of(context).textTheme.headline6,
          scrollable: true,
          contentTextStyle: Theme.of(context).textTheme.bodyText2,
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(Icons.warning_amber_rounded, color: Colors.orange, size: 42),
              const SizedBox(width: 8.0),
              Flexible(child: Text(AppLocalizations.of(context).sessions_page_confirm_deletion)),
            ],
          ),
          content: SingleChildScrollView(
            child: Text(AppLocalizations.of(context).sessions_page_delete_info),
          ),
          actions: <Widget>[
            OutlinedButton(
              onPressed: () => {
                Navigator.of(context).pop(false),
              },
              child: Text(
                AppLocalizations.of(context).general_cancel,
                style: TextStyle(color: AppColors.accentColor),
              ),
              style: OutlinedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
            ),
            TextButton(
              style: TextButton.styleFrom(
                padding: EdgeInsets.symmetric(horizontal: 18, vertical: 8),
                backgroundColor: AppColors.accentColor,
                foregroundColor: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
              onPressed: () => widget.sessionViewModel.deleteSession(context),
              child: Text(AppLocalizations.of(context).general_confirm),
            ),
          ],
        );
      },
    );
  }

  Future<bool> _showAscendDeleteConfirmation(BuildContext context) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          titleTextStyle: Theme.of(context).textTheme.headline6,
          scrollable: true,
          contentTextStyle: Theme.of(context).textTheme.bodyText2,
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Icon(Icons.warning_amber_rounded, color: Colors.orange, size: 42),
              const SizedBox(width: 8.0),
              Text(AppLocalizations.of(context).sessions_page_delete_session),
            ],
          ),
          content: SingleChildScrollView(
            child: Text(
              AppLocalizations.of(context).sessions_page_delete_session_warning,
              overflow: TextOverflow.clip,
            ),
          ),
          actions: <Widget>[
            OutlinedButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: Text(
                AppLocalizations.of(context).general_cancel,
                style: TextStyle(color: AppColors.accentColor),
              ),
              style: OutlinedButton.styleFrom(
                foregroundColor: AppColors.accentColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
            ),
            TextButton(
              style: TextButton.styleFrom(
                padding: EdgeInsets.symmetric(horizontal: 18, vertical: 8),
                backgroundColor: AppColors.accentColor,
                foregroundColor: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
              onPressed: () => widget.sessionViewModel.deleteSession(context),
              child: Text(AppLocalizations.of(context).general_confirm),
            ),
          ],
        );
      },
    );
  }

  Widget _content() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(Constants.STANDARD_PADDING),
        child: Column(
          children: [
            const SizedBox(height: Constants.STANDARD_PADDING),
            ..._indoorSwitch(),
            const SizedBox(height: Constants.STANDARD_PADDING),
            _date(),
            const SizedBox(height: Constants.STANDARD_PADDING),
            ..._duration(),
            const SizedBox(height: Constants.STANDARD_PADDING),
            _locationName(),
            const SizedBox(height: Constants.STANDARD_PADDING),
            _position(),
            const SizedBox(height: Constants.STANDARD_PADDING),
            const Divider(thickness: 2.0),
            const SizedBox(height: Constants.STANDARD_PADDING),
            Align(
              child: Text(
                AppLocalizations.of(context).home_page_ascents,
                style: Theme.of(context).textTheme.headline6,
              ),
              alignment: Alignment.centerLeft,
            ),
            const SizedBox(height: 15.0),
            _ascendList(),
            const SizedBox(height: 120.0),
          ],
        ),
      ),
    );
  }

  List<Widget> _indoorSwitch() {
    return [
      ToggleSwitch(
        initialLabelIndex: widget.sessionViewModel.editedSession.outdoor ? 1 : 0,
        minWidth: MediaQuery.of(context).size.width * 0.35,
        minHeight: 45.0,
        cornerRadius: 20.0,
        activeBgColor: [Colors.green],
        activeFgColor: Colors.white,
        inactiveBgColor: AppColors.accentColor2,
        inactiveFgColor: Colors.white,
        labels: [
          AppLocalizations.of(context).general_indoor.toUpperCase(),
          AppLocalizations.of(context).general_outdoor.toUpperCase(),
        ],
        onToggle: (index) {
          widget.sessionViewModel.editedSession.outdoor = index == 1;
        },
        totalSwitches: 2,
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      const Divider(thickness: 2.0),
    ];
  }

  Widget _date() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Icon(Icons.calendar_today_outlined),
        SizedBox(width: 10.0),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                child: Text(
                  "${AppLocalizations.of(context).general_date}:",
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                alignment: Alignment.centerLeft,
              ),
              const SizedBox(height: 6.0),
              InkWell(
                onTap: () => _selectDate(
                  context,
                  widget.sessionViewModel.editedSession.startDate,
                ),
                child: Text(
                  _dateFormatter(widget.sessionViewModel.editedSession.startDate),
                  style: Theme.of(context).textTheme.bodyText1,
                  overflow: TextOverflow.clip,
                ),
              ),
              const SizedBox(height: 4),
              InkWell(
                onTap: () => _selectTime(
                  context,
                  TimeOfDay.fromDateTime(widget.sessionViewModel.editedSession.startDate),
                ),
                child: Text(
                  DateFormat.jm().format(widget.sessionViewModel.editedSession.startDate),
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ],
          ),
        ),
        Column(
          children: [
            ElevatedButton.icon(
              onPressed: () => _selectDate(
                context,
                widget.sessionViewModel.editedSession.startDate,
              ),
              icon: const Icon(Icons.edit, size: 22.0),
              label: Text(AppLocalizations.of(context).general_date),
              style: ElevatedButton.styleFrom(
                foregroundColor: Colors.black,
                minimumSize: const Size(48, 32),
                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
              ),
            ),
            const SizedBox(height: 8.0),
            ElevatedButton.icon(
              onPressed: () => _selectTime(
                context,
                TimeOfDay.fromDateTime(widget.sessionViewModel.editedSession.startDate),
              ),
              icon: const Icon(Icons.edit, size: 22.0),
              label: Text(AppLocalizations.of(context).sessions_page_edit_session_time),
              style: ElevatedButton.styleFrom(
                foregroundColor: Colors.black,
                minimumSize: const Size(48, 32),
                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
              ),
            ),
          ],
        ),
        const SizedBox(width: 4.0),
      ],
    );
  }

  List<Widget> _duration() {
    return [
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Icon(Icons.timer_outlined),
          const SizedBox(width: 10.0),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Align(
                  child: Text(
                    "${AppLocalizations.of(context).sessions_page_duration}:",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  alignment: Alignment.centerLeft,
                ),
                const SizedBox(height: 6.0),
                InkWell(
                  onTap: () => _selectDuration(context),
                  child: Text(
                    "${widget.sessionViewModel.editedSession.getDuration()} h",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
              ],
            ),
          ),
          Column(
            children: [
              const SizedBox(height: 8.0), // added so that button is bottom aligned
              ElevatedButton.icon(
                onPressed: () => _selectDuration(context).then((value) => {if (value) setState(() {})}),
                icon: const Icon(Icons.edit, size: 22.0),
                label: Text(AppLocalizations.of(context).sessions_page_duration),
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.black,
                  minimumSize: const Size(48, 40),
                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                ),
              ),
            ],
          ),
          const SizedBox(width: 4.0),
        ],
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      const Divider(thickness: 2.0),
    ];
  }

  Widget _locationName() {
    return Theme(
      data: ThemeData.from(
        colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor),
        useMaterial3: false,
      ),
      child: TextFormField(
        initialValue: widget.sessionViewModel.editedSession.locationName,
        textAlign: TextAlign.start,
        textAlignVertical: TextAlignVertical.center,
        style: const TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
        onChanged: (value) {
          widget.sessionViewModel.editedSession.locationName = value;
        },
        textCapitalization: TextCapitalization.words,
        decoration: new InputDecoration(
          isDense: true,
          labelText: AppLocalizations.of(context).home_page_enter_location,
          labelStyle: const TextStyle(fontSize: 18, color: AppColors.accentColor2),
          fillColor: AppColors.accentColor,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
            borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
            borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
          ),
        ),
      ),
    );
  }

  Widget _position() {
    LatLng coordinates = widget.sessionViewModel.editedSession.coordinates;
    if (coordinates == null) return _addLocationWidget();

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "${AppLocalizations.of(context).sessions_page_edit_session_coordinates}:",
                style: Theme.of(context).textTheme.subtitle1,
                overflow: TextOverflow.ellipsis,
              ),
              Text(
                "* ${AppLocalizations.of(context).sessions_page_edit_session_longitude}: "
                "${coordinates.longitude.toStringAsFixed(8)}",
                style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 16.0),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              Text(
                "* ${AppLocalizations.of(context).sessions_page_edit_session_latitude}:"
                " ${coordinates.latitude.toStringAsFixed(8)}",
                style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 16.0),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ],
          ),
        ),
        const SizedBox(width: 12.0),
        ElevatedButton(
          onPressed: () => widget.sessionViewModel.chooseLocationCoordinatesFromMap(context, setState),
          child: const Icon(Icons.edit, size: 22.0),
          style: ElevatedButton.styleFrom(
            foregroundColor: Colors.black,
            minimumSize: const Size(48, 38),
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          ),
        ),
        const SizedBox(width: 12.0),
        ElevatedButton(
          onPressed: _deleteLocationCoordinates,
          child: const Icon(Icons.delete, size: 22.0),
          style: ElevatedButton.styleFrom(
            foregroundColor: Colors.black,
            minimumSize: const Size(48, 38),
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          ),
        ),
        const SizedBox(width: 4.0),
      ],
    );
  }

  Widget _addLocationWidget() {
    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(foregroundColor: Colors.black),
      onPressed: () => widget.sessionViewModel.chooseLocationCoordinatesFromMap(context, setState),
      icon: Icon(Icons.location_on_outlined),
      label: Text(
        AppLocalizations.of(context).sessions_page_edit_session_add_coordinates,
        style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 16),
      ),
    );
  }

  String _dateFormatter(DateTime date) {
    String languageCode = Localizations.localeOf(context).languageCode;
    return DateFormat("EEEE, d. MMM. y", languageCode).format(date);
  }

  /// updating session start time
  /// returning true if date changes, false otherwise
  Future<bool> _selectDate(BuildContext context, DateTime selectedDate) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(1995),
      lastDate: DateTime.now(),
    );
    if (picked != null && picked != selectedDate) {
      DateTime endTime = widget.sessionViewModel.editedSession.endDate;
      DateTime startTime = widget.sessionViewModel.editedSession.startDate;
      Duration delta = endTime.difference(startTime);
      widget.sessionViewModel.editedSession.startDate = picked;
      widget.sessionViewModel.editedSession.endDate = picked.add(delta);
      setState(() {});
      return true;
    }
    return false;
  }

  /// updating session start time
  /// returning true if date changes, false otherwise
  Future<bool> _selectTime(BuildContext context, TimeOfDay selectedTime) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );
    if (picked != null && picked != selectedTime) {
      DateTime endTime = widget.sessionViewModel.editedSession.endDate;
      DateTime startTime = widget.sessionViewModel.editedSession.startDate;
      Duration delta = endTime.difference(startTime);
      DateTime updatedTime = DateTime(startTime.year, startTime.month, startTime.day, picked.hour, picked.minute);
      widget.sessionViewModel.editedSession.startDate = updatedTime;
      widget.sessionViewModel.editedSession.endDate = updatedTime.add(delta);
      setState(() {});
      return true;
    }
    return false;
  }

  /// updating session duration
  /// returning true if date changes, false otherwise
  Future<bool> _selectDuration(BuildContext context) async {
    DateTime endTime = widget.sessionViewModel.editedSession.endDate;
    DateTime startTime = widget.sessionViewModel.editedSession.startDate;
    Duration diff = endTime.difference(startTime);
    TimeOfDay selectedTime = TimeOfDay(hour: diff.inHours, minute: diff.inMinutes % 60);

    final TimeOfDay picked = await showTimePicker(
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child,
        );
      },
      context: context,
      helpText: AppLocalizations.of(context).sessions_page_select_duration,
      initialTime: selectedTime,
    );
    if (picked != null && picked != selectedTime) {
      DateTime updatedTime = startTime.add(Duration(hours: picked.hour, minutes: picked.minute));
      widget.sessionViewModel.editedSession.endDate = updatedTime;
      setState(() {});
      return true;
    }
    return false;
  }

  Widget _ascendList() {
    return FutureBuilder(
      future: this._ascentsFutureData,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const LinearProgressIndicator();
        }
        return ReorderableAscentsList(
          data: snapshot.data,
          showEditIcon: false,
          onDelete: (Map ascend) => widget.sessionViewModel.deleteAscend(ascend, snapshot.data),
          onReAdd: (int index, Map ascend) => widget.sessionViewModel.onReAdd(index, ascend, snapshot.data),
          onDeleteLast: (Map ascend) => _showAscendDeleteConfirmation(context),
          onReorder: (int oldIndex, int newIndex) => onReorder(snapshot.data, oldIndex, newIndex),
          // this is not working, because index does not fit after reordering list
          //onEditedAscends: (int index, Map ascend) {
          //  snapshot.data[index] = ascend;
          //  widget.sessionViewModel.ascents = widget.sessionViewModel.fetchAscents();
          //  setState(() {});
          //},
        );
      },
    );
  }

  void onReorder(List<Map> data, int oldIndex, int newIndex) {}

  Future<List<Map<String, dynamic>>> _loadAscends() async {
    List<Map<String, dynamic>> res = await widget.sessionViewModel.ascents;
    return List.generate(res.length, (index) => Map.of(res[index]));
  }

  void _deleteLocationCoordinates() {
    widget.sessionViewModel.changedLocationCoordinates = true;
    LatLng coordinates = widget.sessionViewModel.editedSession.coordinates;
    widget.sessionViewModel.editedSession.coordinates = null;
    setState(() {});
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          duration:  Duration(milliseconds: 2000),
          content: Text(AppLocalizations.of(context).sessions_page_edit_session_coordinates_deleted),
          action: SnackBarAction(
            textColor: Colors.lightBlue,
            label: AppLocalizations.of(context).dismissible_route_list_undo,
            onPressed: () => setState(() {
              widget.sessionViewModel.editedSession.coordinates = coordinates;
            }),
          ),
        ),
      );
  }
}
