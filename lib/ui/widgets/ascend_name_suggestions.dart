import 'dart:math';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:flutter/material.dart';
import 'package:string_similarity/string_similarity.dart';

class AscendNameSuggestion extends StatefulWidget {
  final String location;
  final Ascend ascend;
  final ValueChanged<String> onSelected;

  const AscendNameSuggestion({
    Key key,
    @required this.location,
    @required this.ascend,
    @required this.onSelected,
  }) : super(key: key);

  @override
  State<AscendNameSuggestion> createState() => _AscendNameSuggestionState();
}

class _AscendNameSuggestionState extends State<AscendNameSuggestion> {
  static const int MAX_ITEM_AMOUNT = 4;
  static const double SIMILARITY_THRESHOLD = 0.5;

  List<String> ascendNames = [];
  List<String> queriedNames = [];
  Function listener;

  @override
  void initState() {
    super.initState();

    listener = () async {
      _queryAscendNames();
      setState(() {});
    };
    this.widget.ascend.addListener(listener);

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _loadAscendsNames();
      _queryAscendNames();
    });
  }

  @override
  void dispose() {
    this.widget.ascend.removeListener(listener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Column(
        children: List.generate(
            min(MAX_ITEM_AMOUNT, this.queriedNames.length), (index) => _nameRowEntry(queriedNames[index])),
      ),
    );
  }

  /// Returns list of all previous ascend names from this location
  Future<List<String>> _getAscendNamesByLocation(String location) async {
    if (location == "") return [];

    var result = await DBController().rawQuery(
      "SELECT ${DBController.ascendName} FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
      "WHERE LOWER(${DBController.sessionLocation}) == (?) "
      "GROUP BY ${DBController.ascendName} "
      "ORDER BY ${DBController.sessionTimeStart} DESC",
      [location.toLowerCase()],
    );

    if (result == null || result.isEmpty || result.first == null) return [];

    return List<String>.generate(result.length, (index) => result[index][DBController.ascendName]);
  }

  void _loadAscendsNames() async {
    ascendNames = await _getAscendNamesByLocation(widget.location);
    if (mounted) setState(() {});
  }

  void _queryAscendNames() {
    final String query = widget.ascend.name.toLowerCase();
    if (query.isEmpty) {
      this.queriedNames = [];
    }

    this.queriedNames = this
        .ascendNames
        .where((element) =>
            element.toLowerCase().contains(query) || element.toLowerCase().similarityTo(query) > SIMILARITY_THRESHOLD)
        .toList();
  }

  Widget _nameRowEntry(String ascendName) {
    return Padding(
      padding: const EdgeInsets.only(top: 12.0),
      child: InkWell(
        onTap: () => widget.onSelected(ascendName),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Icon(Icons.query_builder, color: AppColors.accentColor2),
            const SizedBox(width: 12.0),
            Flexible(
              child: Text(
                ascendName,
                style: Theme.of(context).textTheme.bodyText2,
                textAlign: TextAlign.start,
                overflow: TextOverflow.ellipsis,
              ),
              fit: FlexFit.loose,
            ),
            const SizedBox(width: 12.0),
            const Icon(Icons.north_west, color: AppColors.accentColor2),
          ],
        ),
      ),
    );
  }
}
