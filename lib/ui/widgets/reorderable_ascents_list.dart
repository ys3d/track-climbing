import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/pages/ascends/ascend_edit_page.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ReorderableAscentsList extends StatefulWidget {
  final Function(Map ascend) onDeleteLast;
  final bool Function(Map ascend) onDelete;
  final Function(int index, Map ascend) onReAdd;
  final Function(int oldIndex, int newIndex) onReorder;
  final Function(int index, Map ascend) onEditedAscends;
  final List<Map> data;
  final bool showEditIcon;

  const ReorderableAscentsList({
    Key key,
    @required this.data,
    @required this.onDelete,
    @required this.onReAdd,
    @required this.onReorder,
    this.onEditedAscends,
    this.onDeleteLast,
    this.showEditIcon = false,
  }) : super(key: key);

  @override
  State<ReorderableAscentsList> createState() => _ReorderableAscentsListState();
}

class _ReorderableAscentsListState extends State<ReorderableAscentsList> {
  @override
  Widget build(BuildContext context) {
    return _buildList(widget.data);
  }

  Widget _buildList(List<Map<String, dynamic>> ascents) {
    List<Widget> items = List.generate(
      ascents.length,
      (index) => _buildItem(
        ascents,
        index,
        ascents[index],
        ascents.length,
      ),
    );

    return ReorderableListView(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      buildDefaultDragHandles: false,
      children: items,
      onReorder: (int oldIndex, int newIndex) {
        ScaffoldMessenger.of(context).removeCurrentSnackBar();
        Map tmp = ascents[oldIndex];

        if (oldIndex == newIndex) return;
        if (newIndex < oldIndex) {
          int tmpId = ascents[newIndex][DBController.ascendId];
          int currentId = ascents[oldIndex][DBController.ascendId];
          for (int i = oldIndex; i > newIndex; i--) {
            int ascendId = currentId;
            currentId = ascents[i - 1][DBController.ascendId];
            ascents[i] = ascents[i - 1];
            ascents[i][DBController.ascendId] = ascendId;
          }
          ascents[newIndex] = tmp;
          ascents[newIndex][DBController.ascendId] = tmpId;
        } else {
          int tmpId = ascents[newIndex - 1][DBController.ascendId];
          int currentId = ascents[oldIndex][DBController.ascendId];
          for (int i = oldIndex; i < newIndex - 1; i++) {
            int ascendId = currentId;
            currentId = ascents[i + 1][DBController.ascendId];
            ascents[i] = ascents[i + 1];
            ascents[i][DBController.ascendId] = ascendId;
          }
          ascents[newIndex - 1] = tmp;
          ascents[newIndex - 1][DBController.ascendId] = tmpId;
        }
        widget.onReorder(oldIndex, newIndex);
        setState(() {});
      },
    );
  }

  Widget _buildItem(List ascents, int index, Map<String, dynamic> ascend, int ascendCount) {
    return Padding(
      key: ValueKey(index),
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          ReorderableDragStartListener(
            index: index,
            child: Container(
              color: Colors.transparent,
              width: 40,
              height: 36,
              alignment: Alignment.centerLeft,
              child: Icon(Icons.drag_indicator),
            ),
          ),
          _buildAscendInfo(ascend),
          const SizedBox(width: 6.0),
          if (widget.showEditIcon) _buildEditIcon(ascents, ascend, index),
          _buildDeleteIcon(ascents, ascend, index),
          _buildMarkedIcon(ascend),
        ],
      ),
    );
  }

  Widget _buildAscendInfo(Map ascend) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            Util.getAscendName(
              context,
              ascend[DBController.ascendName],
              type: EnumToString.fromString(ClimbingType.values, ascend[DBController.ascendType]),
              speedType: ascend[DBController.speedType],
            ),
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.bodyText1.copyWith(
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0.0,
                  wordSpacing: 0.0,
                  height: 0,
                ),
          ),
          Text(
            Util.getDataSummary(ascend, context),
            style: Theme.of(context).textTheme.bodyText1.copyWith(
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  letterSpacing: 0.0,
                  wordSpacing: 0.0,
                  height: 0,
                ),
          ),
        ],
      ),
    );
  }

  Widget _buildEditIcon(List ascents, Map ascend, int index) {
    return IconButton(
      padding: EdgeInsets.zero,
      splashRadius: 24,
      visualDensity: VisualDensity.compact,
      onPressed: () => _editRoute(ascend, index),
      icon: Icon(Icons.edit, color: AppColors.accentColor2),
    );
  }

  Widget _buildDeleteIcon(List ascents, Map ascend, int index) {
    return IconButton(
      padding: EdgeInsets.zero,
      splashRadius: 24,
      visualDensity: VisualDensity.compact,
      onPressed: () => _deleteAscend(ascents, ascend, index),
      icon: Icon(Icons.delete, color: AppColors.accentColor2),
    );
  }

  Widget _buildMarkedIcon(Map ascend) {
    return IconButton(
      splashRadius: 24,
      padding: EdgeInsets.zero,
      visualDensity: VisualDensity.compact,
      onPressed: () => _toggleMarked(ascend),
      icon: Icon(
        ascend[DBController.ascendMarked] == 1 ? Icons.favorite : Icons.favorite_border,
        color: ascend[DBController.ascendMarked] == 1 ? Colors.red : AppColors.accentColor2,
      ),
    );
  }

  void _toggleMarked(Map ascend) {
    final DBController dbController = DBController();
    bool marked = ascend[DBController.ascendMarked] == 0 ? true : false;
    ascend[DBController.ascendMarked] = marked ? 1 : 0;
    dbController.updateAscend(
      {DBController.ascendMarked: marked ? 1 : 0},
      ascend[DBController.ascendId],
    ).then(
      (value) {
        setState(() {});
        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              content: Text(
                marked
                    ? AppLocalizations.of(context).dismissible_route_mark
                    : AppLocalizations.of(context).dismissible_route_un_mark,
              ),
            ),
          );
      },
    );
  }

  void _deleteAscend(List ascents, Map ascend, int index) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    if (ascents.length <= 1 && widget.onDeleteLast != null) {
      widget.onDeleteLast(ascend);
      return;
    }
    if (widget.onDelete(ascend)) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(
            AppLocalizations.of(context).dismissible_route_list_deleted(
              Util.getAscendName(
                context,
                ascend[DBController.ascendName],
                type: EnumToString.fromString(ClimbingType.values, ascend[DBController.ascendType]),
                speedType: ascend[DBController.speedType],
              ),
            ),
          ),
          action: SnackBarAction(
            textColor: Colors.lightBlue,
            label: AppLocalizations.of(context).dismissible_route_list_undo,
            onPressed: () => setState(() {
              widget.onReAdd(index, ascend);
            }),
          ),
        ),
      );
      setState(() {});
    } else {
      ScaffoldMessenger.of(context)
        ..showSnackBar(
          SnackBar(
            behavior: SnackBarBehavior.floating,
            content: Text(AppLocalizations.of(context).dismissible_route_could_not_delete),
          ),
        );
    }
  }

  void _editRoute(Map<String, dynamic> ascend, int index) {
    Navigator.push(
      context,
      PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) {
          return AscendEditPage(
            ascend: Ascend.fromMap(ascend),
            onChanged: (Map newAscend) => widget.onEditedAscends(index, newAscend),
            sessionID: ascend[DBController.sessionId],
          );
        },
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          var begin = Offset(1.0, 0.0);
          var end = Offset.zero;
          var curve = Curves.ease;

          var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

          return SlideTransition(
            position: animation.drive(tween),
            child: child,
          );
        },
      ),
    );
  }
}
