import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:climbing_track/ui/dialog/info_dialogs.dart';
import 'package:climbing_track/util/screenshot.dart';
import 'package:climbing_track/view_model/session_view_model.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

abstract class LocalConstants {
  static const double RING_STROKE_WIDTH = 32;
}

class SessionSummary extends StatelessWidget {
  final SessionViewModel _sessionViewModel;

  final List<bool> boulders = [false];

  SessionSummary(this._sessionViewModel);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _sessionViewModel.ascents,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return LinearProgressIndicator();
        }
        return SingleChildScrollView(
          child: Screenshot(
            controller: _sessionViewModel.screenshotController,
            child: Container(
              color: Colors.white,
              child: _createInfoContainer(context, snapshot.data),
            ),
          ),
        );
      },
    );
  }

  Widget _createInfoContainer(BuildContext context, List<Map<String, dynamic>> ascents) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(Constants.STANDARD_PADDING),
          child: Wrap(
            runSpacing: Constants.STANDARD_PADDING,
            children: [
              _createSessionSummary(context, ascents),
              const Divider(thickness: 2.0),
              Text(
                AppLocalizations.of(context).home_page_ascents,
                style: Theme.of(context).textTheme.headline6.copyWith(height: 0.0),
              ),
            ],
          ),
        ),
        _createRouteList(context, ascents),
        const SizedBox(height: Constants.STANDARD_PADDING),
      ],
    );
  }

  Widget _createSessionSummary(BuildContext context, List<Map<String, dynamic>> ascents) {
    Map<String, dynamic> bestRoute = _getBestRoute(ascents);
    return Align(
      alignment: Alignment.center,
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _location(context),
              const SizedBox(width: 30.0),
              _totalTime(context),
            ],
          ),
          const SizedBox(height: 30),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _bestRoute(context, bestRoute),
              const SizedBox(width: 24.0),
              _totalAscends(context, ascents),
            ],
          ),
          const SizedBox(height: Constants.STANDARD_PADDING),
          const Divider(thickness: 2.0),
          _createDonutChart(context, ascents),
          const SizedBox(height: 10),
          _createLegend(context),
        ],
      ),
    );
  }

  Widget _location(BuildContext context) {
    return Flexible(
      flex: 3,
      fit: FlexFit.tight,
      child: InkWell(
        onTap: () => _showLocationOnMap(context, _sessionViewModel.session.locationName),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "${AppLocalizations.of(context).general_location}:",
              style: Theme.of(context).textTheme.subtitle1.copyWith(height: 0.0),
            ),
            Text(
              _sessionViewModel.session.locationName,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.subtitle1.copyWith(
                    color: AppColors.color1,
                    height: 0.0,
                  ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _totalTime(BuildContext context) {
    return Flexible(
      flex: 2,
      fit: FlexFit.tight,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppLocalizations.of(context).session_summary_total_time,
            style: Theme.of(context).textTheme.subtitle1.copyWith(height: 0.0),
          ),
          Text(
            _sessionViewModel.session.endDate == null ? "---" : _sessionViewModel.session.getDuration(),
            style: Theme.of(context).textTheme.subtitle1.copyWith(
                  color: AppColors.color1,
                  height: 0.0,
                ),
          ),
        ],
      ),
    );
  }

  Widget _totalAscends(BuildContext context, List<Map<String, dynamic>> ascents) {
    double height = _sessionViewModel.getClimbedMeters(ascents);
    return Flexible(
      flex: 2,
      fit: FlexFit.tight,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppLocalizations.of(context).overview_total_ascents,
            style: Theme.of(context).textTheme.subtitle1.copyWith(height: 0.0),
          ),
          Wrap(
            spacing: 8.0,
            children: [
              Text(
                ascents.length.toString().padLeft(2, '0'),
                style: Theme.of(context).textTheme.subtitle1.copyWith(
                      color: AppColors.color1,
                      height: 0.0,
                      letterSpacing: 0.0,
                      wordSpacing: 0.0,
                    ),
              ),
              if (height > 0.0) ...[
                Text(
                  " - ",
                  style: Theme.of(context).textTheme.subtitle1.copyWith(
                        color: AppColors.color1,
                        height: 0.0,
                        letterSpacing: 0.0,
                        wordSpacing: 0.0,
                      ),
                ),
                Text(
                  Util.getHeightWithShortUnit(height).replaceAll(" ", ""),
                  maxLines: 1,
                  style: Theme.of(context).textTheme.subtitle1.copyWith(
                        color: AppColors.color1,
                        height: 0.0,
                        letterSpacing: 0.0,
                        wordSpacing: 0.0,
                      ),
                ),
                const SizedBox(width: 10.0),
              ],
            ],
          ),
        ],
      ),
    );
  }

  Widget _bestRoute(BuildContext context, Map<String, dynamic> bestAscend) {
    Ascend ascend = Ascend.fromMap(bestAscend);
    ascend.location = _sessionViewModel.session.locationName;
    ascend.dateTime = _sessionViewModel.session.startDate;

    return Flexible(
      flex: 3,
      fit: FlexFit.tight,
      child: InkWell(
        onTap: () => InfoDialog().showAscendDialog(context, ascend),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              bestAscend[DBController.ascendType] == ClimbingType.SPEED_CLIMBING.name
                  ? AppLocalizations.of(context).session_summary_speed_attempt
                  : AppLocalizations.of(context).session_summary_most_difficult,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: Theme.of(context).textTheme.subtitle1.copyWith(height: 0.0),
            ),
            Text(
              Util.getAscendNameWithoutPrefix(
                bestAscend[DBController.ascendName],
                context,
                speedType: bestAscend[DBController.speedType],
                type: EnumToString.fromString(ClimbingType.values, bestAscend[DBController.ascendType]),
              ),
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.subtitle1.copyWith(
                    color: AppColors.color1,
                    height: 0.0,
                  ),
            ),
            bestAscend == null
                ? Container()
                : Text(
                    Util.getDataSummary(bestAscend, context),
                    style: Theme.of(context).textTheme.subtitle1.copyWith(
                          color: AppColors.color1,
                          height: 0.0,
                          fontSize: 14.0,
                        ),
                  ),
          ],
        ),
      ),
    );
  }

  Widget _createDonutChart(BuildContext context, List<Map<String, dynamic>> ascents) {
    int onsight = 0;
    int redpoint = 0;
    int completed = 0;
    int flash = 0;
    for (var route in ascents) {
      if (route[DBController.ascendType] == ClimbingType.BOULDER.name) {
        boulders.first = true;
      }
      if (route[DBController.ascendStyleId] <= 1) {
        onsight++;
      }
      if (route[DBController.ascendStyleId] <= 2 || route[DBController.ascendStyleId] == 100) {
        flash++;
      }
      if (route[DBController.ascendStyleId] <= 3) {
        redpoint++;
      }
      if (route[DBController.ascendStyleId] <= 5 ||
          route[DBController.ascendStyleId] == 101 ||
          route[DBController.ascendStyleId] == 100) {
        completed++;
      }
    }

    if (boulders.first) {
      final Map<String, double> dataMap1 = {"Flash": flash.toDouble(), "1": (ascents.length - flash).toDouble()};
      final Map<String, double> dataMap2 = {
        "completed": completed.toDouble(),
        "2": (ascents.length - completed).toDouble()
      };

      return Stack(alignment: Alignment.center, children: [
        _createPieChart(context, dataMap1, 600, [AppColors.color1, Colors.black12], 270, false, 0),
        _createPieChart(
          context,
          dataMap2,
          800,
          [AppColors.color2, Colors.black12],
          270,
          false,
          LocalConstants.RING_STROKE_WIDTH + 10.0,
        ),
        if (dataMap1["Flash"] > 0)
          _createPieChart(
            context,
            {"1": dataMap1["Flash"]},
            600,
            [Colors.transparent],
            135,
            true,
            -LocalConstants.RING_STROKE_WIDTH * 2.0,
          ),
        if (dataMap2["completed"] > 0)
          _createPieChart(context, {"1": dataMap2["completed"]}, 800, [Colors.transparent], 180, true, 0),
      ]);
    } else {
      final Map<String, double> dataMap1 = {"OnSight": onsight.toDouble(), "1": (ascents.length - onsight).toDouble()};
      final Map<String, double> dataMap2 = {
        "Redpoint": redpoint.toDouble(),
        "2": (ascents.length - redpoint).toDouble()
      };
      final Map<String, double> dataMap3 = {
        "completed": completed.toDouble(),
        "3": (ascents.length - completed).toDouble()
      };

      return Stack(alignment: Alignment.center, children: [
        _createPieChart(context, dataMap1, 600, [AppColors.color1, Colors.black12], 270, false, 0),
        _createPieChart(context, dataMap2, 800, [AppColors.color2, Colors.black12], 270, false,
            LocalConstants.RING_STROKE_WIDTH + 10.0),
        _createPieChart(context, dataMap3, 1000, [AppColors.color3, Colors.black12], 270, false,
            (LocalConstants.RING_STROKE_WIDTH + 10.0) * 2.0),
        if (dataMap1["OnSight"] > 0)
          _createPieChart(context, {"1": dataMap1["OnSight"]}, 600, [Colors.transparent], 135, true,
              -LocalConstants.RING_STROKE_WIDTH * 2.0),
        if (dataMap2["Redpoint"] > 0)
          _createPieChart(context, {"1": dataMap2["Redpoint"]}, 800, [Colors.transparent], 180, true, 0),
        if (dataMap3["completed"] > 0)
          _createPieChart(context, {"1": dataMap3["completed"]}, 1000, [Colors.transparent], 270, true,
              LocalConstants.RING_STROKE_WIDTH * 2),
      ]);
    }
  }

  Widget _createPieChart(BuildContext context, Map<String, double> dataMap, int duration, List<Color> colorList,
      double initialAngleInDegree, bool showChartValues, double offset) {
    return PieChart(
      dataMap: dataMap,
      animationDuration: Duration(milliseconds: duration),
      chartRadius: MediaQuery.of(context).size.width / 3.2 - offset,
      colorList: colorList,
      initialAngleInDegree: initialAngleInDegree,
      chartType: ChartType.ring,
      ringStrokeWidth: 16,
      legendOptions: LegendOptions(
        showLegends: false,
      ),
      chartValuesOptions: ChartValuesOptions(
        decimalPlaces: 0,
        showChartValueBackground: true,
        showChartValues: showChartValues,
        showChartValuesInPercentage: false,
        showChartValuesOutside: false,
      ),
    );
  }

  Widget _createLegend(BuildContext context) {
    final List<Color> colors = [AppColors.color1, AppColors.color2, AppColors.color3];
    final List<String> styles = boulders.first
        ? [
            AppLocalizations.of(context).style_flash.toUpperCase(),
            AppLocalizations.of(context).session_summary_completed.toUpperCase(),
          ]
        : [
            AppLocalizations.of(context).style_onsight.toUpperCase(),
            AppLocalizations.of(context).style_redpoint.toUpperCase(),
            AppLocalizations.of(context).session_summary_completed.toUpperCase(),
          ];
    return Wrap(
      direction: Axis.horizontal,
      spacing: 16,
      children: styles
          .map(
            (item) => Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 2.0),
                  height: 12.0,
                  width: 12.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: colors.elementAt(styles.indexOf(item)),
                  ),
                ),
                const SizedBox(width: 6.0),
                Flexible(
                  fit: FlexFit.loose,
                  child: Opacity(
                    opacity: 0.75,
                    child: Text(item, style: Theme.of(context).textTheme.bodyText1, softWrap: true),
                  ),
                ),
              ],
            ),
          )
          .toList(),
    );
  }

  _createRouteList(BuildContext context, List<Map<String, dynamic>> ascents) {
    List<Widget> items = [];
    for (Map<String, dynamic> ascend in ascents) {
      Ascend ascendPlus = Ascend.fromMap(ascend);
      ascendPlus.location = _sessionViewModel.session.locationName;
      ascendPlus.dateTime = _sessionViewModel.session.startDate;
      items.add(
        Padding(
          padding: const EdgeInsets.only(
            left: Constants.STANDARD_PADDING,
            right: Constants.STANDARD_PADDING,
            bottom: 8.0,
          ),
          child: InkWell(
            onTap: () => InfoDialog().showAscendDialog(context, ascendPlus),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Text(
                        Util.getAscendName(
                          context,
                          ascend[DBController.ascendName],
                          type: EnumToString.fromString(ClimbingType.values, ascend[DBController.ascendType]),
                          speedType: ascend[DBController.speedType],
                        ),
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.subtitle1.copyWith(
                              fontSize: 18.0,
                              height: 0.0,
                              wordSpacing: 0.0,
                            ),
                      ),
                    ),
                    Util.getRating(ascend[DBController.ascendRating]),
                  ],
                ),
                Text(
                  Util.getDataSummary(ascend, context),
                  style: Theme.of(context).textTheme.bodyText2.copyWith(
                        height: 0.0,
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        wordSpacing: 0.0,
                      ),
                ),
              ],
            ),
          ),
        ),
      );
    }
    return Column(children: items);
  }

  void _showLocationOnMap(BuildContext context, String location) async {
    List<Map<String, dynamic>> result = await DBController().queryCoordinatesByLocation(location);
    if (result == null || result.isEmpty) {
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            behavior: SnackBarBehavior.floating,
            content: Text(AppLocalizations.of(context).session_summary_no_coordinates),
            duration: Duration(seconds: 2),
          ),
        );
    } else {
      MapsLauncher.launchCoordinates(
        result.first[DBController.latitude],
        result.first[DBController.longitude],
        location,
      );
    }
  }

  Map<String, dynamic> _getBestRoute(List<Map<String, dynamic>> ascents) {
    bool onlySpeed = !ascents.any(
      (element) => element[DBController.ascendType] != ClimbingType.SPEED_CLIMBING.name,
    );
    if (onlySpeed) return _getBestSpeedRoute(ascents);

    Map<String, dynamic> best = ascents.first;

    for (Map<String, dynamic> route in ascents) {
      if (route[DBController.ascendGradeId] > best[DBController.ascendGradeId]) {
        best = route;
      } else if (route[DBController.ascendGradeId] == best[DBController.ascendGradeId] &&
          route[DBController.ascendStyleId] < best[DBController.ascendStyleId]) {
        best = route;
      } else if (route[DBController.ascendGradeId] == best[DBController.ascendGradeId] &&
          route[DBController.ascendStyleId] == best[DBController.ascendStyleId] &&
          route[DBController.speedTime] <= best[DBController.speedTime]) {
        best = route;
      }
    }
    return best;
  }

  Map<String, dynamic> _getBestSpeedRoute(List<Map<String, dynamic>> ascents) {
    Map<String, dynamic> best = ascents.first;
    for (Map<String, dynamic> route in ascents) {
      if (route[DBController.speedTime] <= best[DBController.speedTime]) {
        best = route;
      }
    }
    return best;
  }
}
