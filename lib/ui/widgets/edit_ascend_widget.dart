import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/climbing_types.dart';
import 'package:climbing_track/backend/model/data.dart';
import 'package:climbing_track/backend/model/height_measurement.dart';
import 'package:climbing_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/util/style_mapper.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/Constants.dart';
import 'package:climbing_track/ui/dialog/info_dialogs.dart';
import 'package:climbing_track/ui/pages/home/widgets/scroll_picker/drop_down_grading.dart';
import 'package:climbing_track/ui/pages/home/widgets/scroll_picker/grade_scroll_picker.dart';
import 'package:climbing_track/ui/pages/home/widgets/style_picker.dart';
import 'package:climbing_track/ui/pages/home/widgets/type_picker.dart';
import 'package:climbing_track/ui/widgets/ascend_name_suggestions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class EditAscendWidget extends StatefulWidget {
  final Ascend ascend;

  const EditAscendWidget({Key key, @required this.ascend}) : super(key: key);

  @override
  _EditAscendWidgetState createState() => _EditAscendWidgetState(ascend);
}

class _EditAscendWidgetState extends State<EditAscendWidget> {
  final InfoDialog _infoDialog = InfoDialog();
  final TextEditingController _routeNameController = TextEditingController();
  final TextEditingController _commentController = TextEditingController();
  final Ascend _ascend;

  _EditAscendWidgetState(this._ascend);

  @override
  Widget build(BuildContext context) {
    _routeNameController.text = _ascend.name;
    _commentController.text = _ascend.comment;
    this._routeNameController.selection = TextSelection.fromPosition(
      TextPosition(offset: _routeNameController.text.length),
    );
    this._commentController.selection = TextSelection.fromPosition(
      TextPosition(offset: _commentController.text.length),
    );

    return GestureDetector(
      onTapDown: (tapDownDetails) {
        FocusScope.of(context).unfocus();
      },
      child: Padding(
        padding: const EdgeInsets.all(Constants.STANDARD_PADDING),
        child: Wrap(
          alignment: WrapAlignment.center,
          direction: Axis.horizontal,
          runSpacing: Constants.STANDARD_PADDING,
          children: [
            ..._createTypeChooser(context),
            ..._createSpeedSpecificItems(),
            ..._createTopRopeToggle(context),
            ..._createDifficultyPicker(context),
            ..._createNameTextField(context),
            ..._createClimbingStyle(),
            ..._height(),
            ..._createRatingBar(),
            ..._createCommentField(context, (widget.ascend.comment ?? "").toString()),
            const SizedBox(height: 220.0),
          ],
        ),
      ),
    );
  }

  List<Widget> _createTopRopeToggle(BuildContext context) {
    switch (_ascend.type) {
      case ClimbingType.SPEED_CLIMBING:
        if (_ascend.speedType == 0) return [];
        continue SPORT_CLIMBING;

      case ClimbingType.ICE_CLIMBING:
      SPORT_CLIMBING:
      case ClimbingType.SPORT_CLIMBING:
        return [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "${AppLocalizations.of(context).style}:",
                style: Theme.of(context).textTheme.bodyText1,
              ),
              Padding(
                padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
                child: InkWell(
                  onTap: () => _infoDialog.showLeadTopRopeInfoDialog(context),
                  child: const IconTheme(
                    data: IconThemeData(color: AppColors.accentColor2),
                    child: Icon(Icons.info_outline),
                  ),
                ),
              ),
            ],
          ),
          ToggleSwitch(
            initialLabelIndex: _ascend.toprope ? 1 : 0,
            minWidth: MediaQuery.of(context).size.width * 0.37,
            minHeight: 45.0,
            cornerRadius: 20.0,
            activeBgColor: [Colors.green],
            activeFgColor: Colors.white,
            inactiveBgColor: AppColors.accentColor2,
            inactiveFgColor: Colors.white,
            fontSize: 16,
            labels: [
              AppLocalizations.of(context).style_lead.toUpperCase(),
              AppLocalizations.of(context).style_toprope.toUpperCase(),
            ],
            onToggle: (index) {
              _ascend.toprope = index == 1;
            },
            totalSwitches: 2,
          ),
          const Divider(thickness: 2.0),
        ];
      case ClimbingType.BOULDER:
      case ClimbingType.FREE_SOLO:
      case ClimbingType.DEEP_WATER_SOLO:
      default:
        return [];
    }
    return [];
  }

  List<Widget> _createSpeedSpecificItems() {
    if (_ascend.type != ClimbingType.SPEED_CLIMBING) return [];
    double time = _ascend.speedTime / 1000;

    String timeString = "${time.toStringAsFixed(2)} ${AppLocalizations.of(context).util_seconds}";

    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Text(
              AppLocalizations.of(context).ascent_edit_comp_wall_or_normal,
              overflow: TextOverflow.clip,
              maxLines: 4,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: Constants.STANDARD_PADDING),
            child: InkWell(
              onTap: () => _infoDialog.showSpeedInfoDialog(context),
              child: const IconTheme(
                data: IconThemeData(color: AppColors.accentColor2),
                child: Icon(Icons.info_outline),
              ),
            ),
          ),
        ],
      ),
      ToggleSwitch(
        initialLabelIndex: _ascend.speedType,
        minWidth: MediaQuery.of(context).size.width * 0.42,
        minHeight: 45.0,
        cornerRadius: 20.0,
        activeBgColor: [Colors.green],
        activeFgColor: Colors.white,
        inactiveBgColor: AppColors.accentColor2,
        inactiveFgColor: Colors.white,
        fontSize: 16,
        labels: [AppLocalizations.of(context).ascent_edit_competition, AppLocalizations.of(context).ascent_edit_normal],
        onToggle: (index) => setState(() {
          _ascend.speedType = index;
          if (_ascend.speedType == 0) {
            _ascend.height = 15.0;
          } else {
            _ascend.height = Data().getCurrentSession() == null ? 0.0 : Data().getCurrentSession().standardHeight;
          }
        }),
        totalSwitches: 2,
      ),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).ascend_edit_your_time,
          overflow: TextOverflow.clip,
          maxLines: 1,
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ),
      InkWell(
        borderRadius: const BorderRadius.all(
          Radius.circular(24.0),
        ),
        onTap: () => _showPickerNumber(context),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 10.0),
          child: Align(
            alignment: Alignment.center,
            child: Text(
              timeString,
              overflow: TextOverflow.clip,
              maxLines: 1,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
        ),
      ),
      const Divider(thickness: 2.0),
    ];
  }

  List<Widget> _createTypeChooser(BuildContext context) {
    final List<ClimbingType> labels = ClimbingType.values;

    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            AppLocalizations.of(context).ascend_edit_choose_type,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          Padding(
            padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
            child: InkWell(
              onTap: () => _infoDialog.showBoulderRouteInfoDialog(context),
              child: const IconTheme(
                data: IconThemeData(color: AppColors.accentColor2),
                child: const Icon(Icons.info_outline),
              ),
            ),
          ),
        ],
      ),
      InkWell(
        borderRadius: const BorderRadius.all(Radius.circular(24.0)),
        onTap: () => TypePickerDialog(
          items: labels,
          value: _ascend.type,
          onChange: (ClimbingType label) {
            setState(() {
              ClimbingType before = _ascend.type;
              _ascend.type = label;
              if (before == _ascend.type) return;

              // style
              if (_ascend.type == ClimbingType.BOULDER) {
                _ascend.styleID = StyleMapper().getDefaultBoulderStyle();
              } else {
                _ascend.styleID = StyleMapper().getDefaultClimbingStyle();
              }

              // height
              if (_ascend.type == ClimbingType.SPEED_CLIMBING) {
                _ascend.height = 15.0;
              } else {
                _ascend.height = Data().getCurrentSession() == null ? 0.0 : Data().getCurrentSession().standardHeight;
              }

              _ascend.gradeID = GradeManager().getStandardGrade(_ascend.type);
              _ascend.speedTime = 0;
              _ascend.speedType = 0;
              _ascend.tries = Ascend.getMinTriesValue(_ascend.styleID);
            });
          },
        ).showMyDialog(context),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20.0),
          child: Container(
            decoration: BoxDecoration(
              color: AppColors.accentColor2,
            ),
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 13.0),
              constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 0.6),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(20.0)),
                color: Colors.amber[700].withOpacity(0.8),
              ),
              child: Text(
                _ascend.type.toName(context),
                style: Theme.of(context).textTheme.bodyText1.copyWith(color: Colors.white),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        ),
      ),
      const Divider(thickness: 2.0),
    ];
  }

  List<Widget> _createNameTextField(BuildContext context) {
    if (_ascend.type == ClimbingType.SPEED_CLIMBING && _ascend.speedType == 0) return [];

    return [
      Align(
        child: Text(
          _ascend.isBoulder()
              ? AppLocalizations.of(context).ascend_edit_boulder_name
              : AppLocalizations.of(context).ascend_edit_route_name,
          style: Theme.of(context).textTheme.bodyText1,
        ),
        alignment: Alignment.centerLeft,
      ),
      Column(
        children: [
          Theme(
            data: ThemeData.from(
              colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor),
              useMaterial3: false,
            ),
            child: TextFormField(
              controller: _routeNameController,
              textAlign: TextAlign.start,
              textAlignVertical: TextAlignVertical.center,
              onChanged: (String value) => _ascend.name = value.trim(),
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.normal,
              ),
              textCapitalization: TextCapitalization.sentences,
              decoration: InputDecoration(
                isDense: true,
                labelText: _ascend.isBoulder()
                    ? AppLocalizations.of(context).ascend_edit_enter_boulder_name
                    : AppLocalizations.of(context).ascend_edit_enter_route_name,
                labelStyle: TextStyle(fontSize: 18, color: AppColors.accentColor2),
                fillColor: AppColors.accentColor,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
                  borderSide: _routeNameController.text == ""
                      ? const BorderSide()
                      : const BorderSide(color: AppColors.accentColor, width: 2.0),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
                  borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
                ),
                enabledBorder: _routeNameController.text == ""
                    ? null
                    : OutlineInputBorder(
                        borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
                        borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
                      ),
              ),
            ),
          ),
          AscendNameSuggestion(
            location: _ascend.location,
            ascend: _ascend,
            onSelected: (value) {
              _routeNameController.text = value.trim();
              _routeNameController.selection = TextSelection.fromPosition(
                TextPosition(offset: _routeNameController.text.length),
              );
              _ascend.name = value.trim();
            },
          ),
        ],
      ),
      const Divider(thickness: 2.0),
    ];
  }

  List<Widget> _createCommentField(BuildContext context, String initValue) {
    return [
      Align(
        child: Text(
          "${AppLocalizations.of(context).general_comment}:",
          style: Theme.of(context).textTheme.bodyText1,
        ),
        alignment: Alignment.centerLeft,
      ),
      Theme(
        data: ThemeData.from(
          colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor),
          useMaterial3: false,
        ),
        child: TextFormField(
          maxLines: 5,
          controller: _commentController,
          textAlign: TextAlign.start,
          textAlignVertical: TextAlignVertical.center,
          onChanged: (String value) => _ascend.comment = value.trim(),
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
          textCapitalization: TextCapitalization.sentences,
          decoration: new InputDecoration(
            alignLabelWithHint: true,
            isDense: false,
            labelText: AppLocalizations.of(context).ascend_edit_enter_comment,
            labelStyle: TextStyle(fontSize: 18, color: AppColors.accentColor2),
            fillColor: AppColors.accentColor,
            border: OutlineInputBorder(
              borderRadius: new BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
              borderSide: new BorderSide(),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
              borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
            ),
          ),
        ),
      ),
    ];
  }

  List<Widget> _createDifficultyPicker(BuildContext context) {
    switch (_ascend.type) {
      case ClimbingType.BOULDER:
        return _difficultyPicker(
          Text(
            AppLocalizations.of(context).ascend_edit_boulder_grade,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          DropDownGrading(key: Key(_ascend.type.name), type: _ascend.type, callBack: () => setState(() {})),
        );
      case ClimbingType.ICE_CLIMBING:
        return _difficultyPicker(
          Text(
            AppLocalizations.of(context).ascend_edit_ice_grade,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          _iceGradingInfo(),
        );
      case ClimbingType.SPEED_CLIMBING:
        if (_ascend.speedType != 0) continue DEFAULT;
        return [];
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.DEEP_WATER_SOLO:
      case ClimbingType.FREE_SOLO:
      DEFAULT:
      default:
        return _difficultyPicker(
          Text(
            AppLocalizations.of(context).ascend_edit_climbing_grade,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          DropDownGrading(
            key: Key(_ascend.type.name),
            type: _ascend.type,
            callBack: () => setState(() {}),
          ),
        );
    }
  }

  Widget _iceGradingInfo() {
    return InkWell(
      onTap: () => _infoDialog.showIceGradingInfo(context),
      child: const IconTheme(
        data: IconThemeData(color: AppColors.accentColor2),
        child: const Icon(Icons.info_outline),
      ),
    );
  }

  List<Widget> _difficultyPicker(Widget title, Widget interactionWidget) {
    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          title,
          Padding(
            padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
            child: interactionWidget,
          )
        ],
      ),
      Align(
        alignment: Alignment.centerLeft,
        child: Container(
          height: 40,
          width: MediaQuery.of(context).size.width,
          child: ShaderMask(
            shaderCallback: (Rect rect) {
              return LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: <Color>[Colors.black, Colors.transparent, Colors.transparent, Colors.black],
                  stops: [0.001, 0.45, 0.55, 9.9]).createShader(rect);
            },
            blendMode: BlendMode.dstOut,
            child: RotatedBox(
              quarterTurns: 3,
              child: GradeScrollPicker(
                key: UniqueKey(),
                initialValue: GradeManager().getGradeMapper(type: _ascend.type).getGradeForGradePicker(_ascend.gradeID),
                onChanged: (String value) {
                  _ascend.gradeID = GradeManager().getGradeMapper(type: _ascend.type).getIdFromGrade(value) ??
                      GradeManager().getStandardGrade(_ascend.type);
                },
                ascend: _ascend,
              ),
            ),
          ),
        ),
      ),
      const Divider(thickness: 2.0),
    ];
  }

  List<Widget> _createRatingBar() {
    return [
      Align(
        child: Text(
          "${AppLocalizations.of(context).general_rating}:",
          style: Theme.of(context).textTheme.bodyText1,
        ),
        alignment: Alignment.centerLeft,
      ),
      RatingBar(
        initialRating: _ascend.rating.toDouble(),
        minRating: 1,
        glow: false,
        direction: Axis.horizontal,
        allowHalfRating: false,
        itemCount: 5,
        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
        onRatingUpdate: (rating) {
          _ascend.rating = rating.toInt();
        },
        ratingWidget: RatingWidget(
          empty: Constants.EMPTY_STAR,
          full: Constants.FULL_STAR,
          half: null,
        ),
      ),
      const Divider(thickness: 2.0),
    ];
  }

  List _createClimbingStyle() {
    if (_ascend.type == ClimbingType.SPEED_CLIMBING || _ascend.type == ClimbingType.FREE_SOLO) return [];

    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            AppLocalizations.of(context).ascend_edit_climbing_style,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          Padding(
            padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
            child: InkWell(
              onTap: () => _infoDialog.showStyleInfoDialog(context, _ascend.type),
              child: IconTheme(
                data: IconThemeData(color: AppColors.accentColor2),
                child: Icon(Icons.info_outline),
              ),
            ),
          ),
        ],
      ),
      StylePicker(
        //key: UniqueKey(), // is this important?
        onChangedStyle: (newValue) => _ascend.styleID = newValue,
        onChangedTries: (int value) => _ascend.tries = value,
        ascend: _ascend,
      ),
      const Divider(thickness: 2.0),
    ];
  }

  List<Widget> _height() {
    if (_ascend.type == ClimbingType.SPEED_CLIMBING && _ascend.speedType == 0) return [];

    return [
      Align(
          child: Text(
            _ascend.isBoulder()
                ? AppLocalizations.of(context).ascend_edit_boulder_length
                : AppLocalizations.of(context).ascend_edit_route_length,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          alignment: Alignment.centerLeft),
      InkWell(
        borderRadius: BorderRadius.circular(24.0),
        onTap: () => _showHeightPicker(context),
        child: Padding(
          padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 2.0),
                    child: Text(
                      Util.getHeightString(_ascend.height),
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(color: Colors.black26),
                    ),
                    color: Colors.transparent,
                  ),
                ),
                const SizedBox(width: 25),
                Padding(
                  padding: const EdgeInsets.only(bottom: 3.0),
                  child: Text(
                    DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET
                        ? AppLocalizations.of(context).general_feet(Util.heightFromMeters(_ascend.height).round())
                        : AppLocalizations.of(context).general_meter(Util.heightFromMeters(_ascend.height).round()),
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      const Divider(thickness: 2.0),
    ];
  }

  void _showPickerNumber(BuildContext context) {
    int milliseconds = (_ascend.speedTime % 1000) ~/ 10;
    int seconds = _ascend.speedTime ~/ 1000 % 60;
    int minutes = _ascend.speedTime ~/ 1000 ~/ 60;

    new Picker(
        cancelTextStyle: TextStyle(
          color: AppColors.accentColor,
          fontSize: 18,
        ),
        confirmTextStyle: TextStyle(
          color: AppColors.accentColor,
          fontSize: 18,
        ),
        adapter: NumberPickerAdapter(
          data: [
            NumberPickerColumn(begin: 0, end: 999),
            NumberPickerColumn(begin: 0, end: 59, onFormatValue: (val) => val.toString().padLeft(2, "0")),
            NumberPickerColumn(
              begin: 0,
              end: 99,
              onFormatValue: (val) => val.toString().padLeft(2, "0").padRight(3, "0"),
            ),
          ],
        ),
        delimiter: [
          PickerDelimiter(
            column: 0,
            child: Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Align(
                alignment: Alignment.center,
                child: Text("min:", style: TextStyle(fontWeight: FontWeight.normal)),
              ),
            ),
          ),
          PickerDelimiter(
            column: 2,
            child: Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Align(
                alignment: Alignment.center,
                child: Text("s:", style: TextStyle(fontWeight: FontWeight.normal)),
              ),
            ),
          ),
          PickerDelimiter(
            column: 4,
            child: Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Align(
                alignment: Alignment.center,
                child: Text("ms:", style: TextStyle(fontWeight: FontWeight.normal)),
              ),
            ),
          ),
        ],
        selecteds: [minutes, seconds, milliseconds],
        hideHeader: false,
        onConfirm: (Picker picker, List value) {
          setState(() {
            int result = (value[0] * 1000 * 60) + (value[1] * 1000) + value[2] * 10;
            _ascend.speedTime = result;
          });
        }).showModal(context);
  }

  void _showHeightPicker(BuildContext context) {
    dynamic height = Util.heightFromMeters(_ascend.height);
    int meters = height.floor();
    int cm = ((height % 1) * 10).floor();

    if (DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET) {
      new Picker(
          cancelTextStyle: TextStyle(
            color: AppColors.accentColor,
            fontSize: 18,
          ),
          confirmTextStyle: TextStyle(
            color: AppColors.accentColor,
            fontSize: 18,
          ),
          adapter: NumberPickerAdapter(
            data: [
              NumberPickerColumn(begin: 0, end: 999),
            ],
          ),
          delimiter: [
            PickerDelimiter(
              column: 1,
              child: Padding(
                padding: EdgeInsets.only(right: 10.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    AppLocalizations.of(context).general_feet(Util.heightFromMeters(_ascend.height)),
                    style: TextStyle(fontWeight: FontWeight.normal),
                  ),
                ),
              ),
            ),
          ],
          selecteds: [height],
          hideHeader: false,
          onConfirm: (Picker picker, List value) {
            setState(() {
              _ascend.height = Util.heightToMeters(value[0].toDouble());
            });
          }).showModal(context);
    } else {
      new Picker(
          cancelTextStyle: TextStyle(
            color: AppColors.accentColor,
            fontSize: 18,
          ),
          confirmTextStyle: TextStyle(
            color: AppColors.accentColor,
            fontSize: 18,
          ),
          adapter: NumberPickerAdapter(
            data: [
              NumberPickerColumn(begin: 0, end: 999),
              NumberPickerColumn(items: [0, 5], onFormatValue: (val) => val.toString().padRight(2, "0")),
            ],
          ),
          delimiter: [
            PickerDelimiter(
              column: 1,
              child: Padding(
                padding: EdgeInsets.only(right: 10.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text("m", style: TextStyle(fontWeight: FontWeight.normal)),
                ),
              ),
            ),
            PickerDelimiter(
              column: 3,
              child: Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text("cm", style: TextStyle(fontWeight: FontWeight.normal)),
                ),
              ),
            ),
          ],
          selecteds: [meters, cm],
          hideHeader: false,
          onConfirm: (Picker picker, List value) {
            setState(() {
              double result = value[0] + (value[1] * 0.5);
              _ascend.height = Util.heightToMeters(result);
            });
          }).showModal(context);
    }
  }
}
