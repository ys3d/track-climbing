import 'dart:async';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/ui/widgets/zoombuttons_plugin_option.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LocationCoordinatesPickerMap extends StatefulWidget {
  final LatLng coordinates;
  final double initialZoom;

  const LocationCoordinatesPickerMap({Key key, this.coordinates, this.initialZoom = 5}) : super(key: key);

  @override
  _LocationCoordinatesPickerMap createState() {
    return _LocationCoordinatesPickerMap();
  }
}

class _LocationCoordinatesPickerMap extends State<LocationCoordinatesPickerMap> {
  static const double minZoom = 3.0;
  static const double maxZoom = 17.0;

  MapController mapController;
  StreamSubscription mapEventSubscription;
  final pointSize = 54.0;
  double pointY = 0.0;

  LatLng latLng;

  @override
  void initState() {
    super.initState();

    latLng = widget.coordinates;
    assert(latLng != null);

    mapController = MapController();
    mapEventSubscription = mapController.mapEventStream.listen((mapEvent) => _onMapEvent(mapEvent, context));

    Future.delayed(Duration.zero, () {
      pointY = MediaQuery.of(context).size.height / 2 - 80;
    });
  }

  @override
  void dispose() {
    mapEventSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    bool needLoadingError = true;

    return Scaffold(
      appBar: _appBar(),
      body: Stack(
        children: [
          FlutterMap(
            mapController: mapController,
            options: MapOptions(
              onMapReady: () => _updatePointLatLng(context),
              center: latLng,
              zoom: widget.initialZoom,
              minZoom: minZoom,
              maxZoom: maxZoom,
              onLongPress: (_, __) => null,
              interactiveFlags: InteractiveFlag.pinchZoom |
                  InteractiveFlag.drag |
                  InteractiveFlag.flingAnimation |
                  InteractiveFlag.doubleTapZoom,
              onPositionChanged: (MapPosition mapPosition, bool _) {
                needLoadingError = true;
              },
            ),
            nonRotatedChildren: [
              FlutterMapZoomButtons(
                minZoom: minZoom,
                maxZoom: maxZoom,
                mini: false,
                padding: 14,
                alignment: Alignment.bottomRight,
                tooltipZoomIn: AppLocalizations.of(context).location_picker_map_tooltip_zoom_in,
                tooltipZoomOut: AppLocalizations.of(context).location_picker_map_tooltip_zoom_out,
                zoomInColor: Colors.black,
                zoomInColorIcon: Colors.white,
                zoomOutColor: Colors.black,
                zoomOutColorIcon: Colors.white,
              ),
            ],
            children: [
              TileLayer(
                urlTemplate: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                subdomains: ['a', 'b', 'c'],
                userAgentPackageName: 'dev.fleaflet.flutter_map.example',
                errorTileCallback: (Tile tile, error) {
                  if (needLoadingError) {
                    WidgetsBinding.instance.addPostFrameCallback((_) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          behavior: SnackBarBehavior.floating,
                          duration: const Duration(seconds: 2),
                          content: Text(
                            AppLocalizations.of(context).location_picker_map_warning_no_internet,
                          ),
                        ),
                      );
                    });
                    needLoadingError = false;
                  }
                },
              ),
              MarkerLayer(
                markers: [
                  Marker(
                    width: pointSize,
                    height: pointSize,
                    point: latLng,
                    builder: (_) => const Icon(
                      Icons.place,
                      size: 54,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _appBar() {
    return AppBar(
      backgroundColor: AppColors.accentColor,
      title: Text(
        AppLocalizations.of(context).location_picker_map_choose_coordinates,
        style: const TextStyle(color: Colors.white, fontSize: 22),
      ),
      leading: IconButton(
        color: Colors.white,
        icon: const Icon(Icons.close),
        tooltip: AppLocalizations.of(context).general_cancel,
        onPressed: () => {
          ScaffoldMessenger.of(context).removeCurrentSnackBar(),
          Navigator.of(context).pop(false),
        },
      ),
      actions: [
        IconButton(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          tooltip: AppLocalizations.of(context).general_save,
          icon: const Icon(Icons.done),
          color: Colors.white,
          onPressed: () => {
            ScaffoldMessenger.of(context).removeCurrentSnackBar(),
            Navigator.of(context).pop(true),
          },
        )
      ],
    );
  }

  void _onMapEvent(MapEvent mapEvent, BuildContext context) {
    _updatePointLatLng(context);
  }

  void _updatePointLatLng(context) {
    final latLng = mapController.center;

    setState(() {
      this.latLng = latLng;
      widget.coordinates.longitude = this.latLng.longitude;
      widget.coordinates.latitude = this.latLng.latitude;
    });
  }
}
